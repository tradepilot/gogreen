/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.fileprocessor.sender;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;



@Component
@Profile("!test")
public final class KafkaSender implements SenderI<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(KafkaSender.class);

    @Autowired
    private KafkaTemplate<String, ProcessingRequest> kafkaTemplate;

    @Value("${kafka.topic.output}")
    private String topic;

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised with topic " + topic);
    }

    @Override
    public void send(ProcessingRequest message) {

        try {

            logger.info("Sending message to topic " + topic + " " + message);
            kafkaTemplate.send(topic, message.getRequestId(), message);

        } catch (Exception e) {
            logger.warn("Error sending message " + message, e);
        }
    }
}
