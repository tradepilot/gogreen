/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fileprocessor.receiver;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.INVOICE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.NEW;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.IdBuilder;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.fileprocessor.parser.InputFile;
import com.deltacapita.trade.pilot.fileprocessor.sender.SenderI;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Component
@ComponentScan(basePackages = { "com.deltacapita.trade.pilot.core.data", "com.deltacapita.trade.pilot.core.data.repo", "com.deltacapita.trade.pilot.core.data.types" })
public class Receiver {

    private static final Logger logger = LoggerFactory.getLogger(Receiver.class);

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private SenderI<ProcessingRequest> sender;
    
    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;

    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileOutAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> instructionCreatedAgent;

    private ObjectMapper mapper = new ObjectMapper();

    private ExecutorService executor;

    @PostConstruct
    public void initialise() throws Exception {

        mapper.registerModule(new JavaTimeModule());
        logger.info("Initialised JSON mapper " + mapper);

        executor = Executors.newSingleThreadExecutor(new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {

                return new Thread(r, "KafkaSender");
            }
        });
        
        fileInAgent = agentFactory.getAgent(MonitoringAgentConstants.FILE_PROCESSOR_IN);
        fileOutAgent = agentFactory.getAgent(MonitoringAgentConstants.FILE_PROCESSOR_OUT);
        instructionCreatedAgent = agentFactory.getAgent(MonitoringAgentConstants.INSTRUCTION_CREATED);
        logger.info("Initialised with agentFactory={}, fileInAgent={}", agentFactory, fileInAgent);
    }

    @KafkaListener(topics = "${kafka.topic.input}")
    public void receive(byte [] bytes, Acknowledgment ack) {

        String message = new String(bytes);
        logger.debug("received message='{}'", message);

        try {

            InputFile inputFile = mapper.readValue(message, InputFile.class);            
            String fileId = inputFile.getId();
            logger.info("Received new file {}", inputFile.getId());

            File file = new File();
            file.setId(fileId);
            file.setNumberOfInstructions(inputFile.getNumberOfInstructions());
            file.setBuyerId(inputFile.getBuyerId());
            file.setRequestId(inputFile.getRequestId());
            file.setTotalValue(inputFile.getTotalValue());
            file.setReceivedDateTime(inputFile.getReceivedDateTime());
            file.setState(NEW);

            ProcessingRequest outputMessage = new ProcessingRequest();
            outputMessage.setFileId(fileId);
            outputMessage.setRoutingId(inputFile.getBuyerId());
            outputMessage.setSource("file-processor");
            outputMessage.setDestination("file-validator");

            long invoices = 0;
            LocalDate maturityDate = null;
            List<Instruction> instructions = inputFile.getInstructions();
            for (Instruction instruction : instructions) {

                // Set the file ID first so its available for the instruction ID
                //
                instruction.setFileId(file.getId());                
                String instructionId = IdBuilder.buildId(instruction);
                
                instruction.setId(instructionId);
                instruction.setState(NEW);
                
                // Maturity date for the loan will be the date of the last
                // invoice due
                //
                LocalDate dueDate = instruction.getDueDate();
                if( maturityDate == null || dueDate.isAfter(maturityDate) ) {
                    maturityDate = dueDate;
                }
                
                if( instruction.getType().equals(INVOICE) ) {
                    invoices++;
                }
            }
            
            file.setOutstandingPaymentRequests(invoices);
            file.setMaturityDate(maturityDate);
            
            logger.info("Injecting {} to {}", outputMessage, fileInAgent);        
            fileInAgent.inject(outputMessage, file);
            
            logger.info("Saving file with ID {}", file.getId());
            fileRepository.save(file);

            instructionRepository.saveAll(instructions);              
            logger.info("Injecting {} to {}", outputMessage, instructionCreatedAgent);        
            
            instructionCreatedAgent.inject(outputMessage, instructions);
            logger.info("Saved {} instructions for file with ID {}", instructions.size(), file.getId());

            executor.submit(() -> {

                try {  
                    
                    sender.send(outputMessage);                          
                    logger.info("Injecting {} to {}", outputMessage, fileOutAgent);        
                    fileOutAgent.inject(outputMessage, file);

                } catch (Exception e) {
                    logger.warn("Error dispatching message ", e);
                }
            });
            
            ack.acknowledge();
            logger.debug("Sent ack='{}'", ack.toString());


        } catch (Throwable e) {
            logger.error("Error reading message", e);

        }
    }
}
