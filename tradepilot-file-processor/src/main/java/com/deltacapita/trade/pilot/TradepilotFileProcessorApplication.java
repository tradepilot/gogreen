/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.deltacapita.trade.pilot.core.metrics.MetricsSource;

@SpringBootApplication
@EnableDiscoveryClient
public class TradepilotFileProcessorApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotFileProcessorApplication.class);

    @Autowired
    private MetricsSource metricsSource;

    public static void main(String[] args) {
        
        try {
            
            SpringApplication.run(TradepilotFileProcessorApplication.class, args);

        } catch (Throwable t) {
            logger.warn("Error initialising TradepilotFileProcessorApplication", t);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Initialised with metrics source {}", metricsSource);
    }

}
