/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fileprocessor.parser;

import java.time.LocalDateTime;
import java.util.List;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;

public class InputFile extends File {

    private List<Instruction> instructions;

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {

        return "InputFile [" 
                + ", id=" + getId() 
                + ", requestId=" + getRequestId() 
                + ", buyerId=" + getBuyerId()
                + ", receivedDateTime=" + getReceivedDateTime() 
                + ", numberOfInstructions=" + getNumberOfInstructions()
                + ", totalValue=" + getTotalValue() 
                + ", instructions=" + instructions 
                + "]";
    }

    @Override
    public String getId() {
        
        String id = super.getId();
        if( id == null ) {
        
            LocalDateTime receivedDateTime = getReceivedDateTime();
            long epochMs = ServiceUtils.toEpochMillis(receivedDateTime);

            id = getRequestId() + "-" + getBuyerId() + "-" + epochMs + "-" + getNumberOfInstructions() + "-" + getTotalValue();
            setId(id);
        }
        
        return id;
    }


}
