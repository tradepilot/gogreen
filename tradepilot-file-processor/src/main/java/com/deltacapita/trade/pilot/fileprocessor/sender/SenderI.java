/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fileprocessor.sender;

public interface SenderI<T> {

    void send(T message);
}
