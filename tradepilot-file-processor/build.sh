#!/bin/sh

#docker stop tp-file-processor
docker rm tp-file-processor
mvn clean package -Dmaven.test.skip=true -U

docker build -t tp-file-processor --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-file-processor dcdockerregistry.azurecr.io/tp-file-processor:v1
#docker push dcdockerregistry.azurecr.io/tp-file-processor:v1

docker run --name tp-file-processor --hostname tp-file-processor -p 1200:1200 -p 9200:9200 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-file-processor


