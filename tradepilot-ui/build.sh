#!/bin/sh

#docker stop tp-ui
docker rm tp-ui
mvn clean package -Dmaven.test.skip=true -U
docker build -t tp-ui --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-ui dcdockerregistry.azurecr.io/tp-ui:v1
#docker push dcdockerregistry.azurecr.io/tp-ui:v1

docker run --name tp-ui --hostname tp-ui -p 1140:1140 -p 9140:9140 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-ui
#docker run --name tp-ui --hostname tp-ui -p 1140:1140 -p 9140:9140 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm dcdockerregistry.azurecr.io/tp-ui:v1


