!function() {
	
	var statusBySystem = {
		version : "1.0.0"
	};
	

    var chartGroup = "statusBySystemChartGroup";
    var statusBySystem_queryApi = remoteHost + "/paymentsbysystem/"		
		
	if( isTest == "true" ) {
		statusBySystem_queryApi = "paymentsbysystemtest/"
	}

	var width = 990;
	var height = 570;

	var fileProcessorCountBox = dc.numberDisplay("#file-processor-sla", chartGroup);
	var fileProcessorCountText = dc.numberDisplay("#file-processor-count", chartGroup);
	var fileProcessorValueText = dc.numberDisplay("#file-processor-value", chartGroup);
	var fileProcessorRiskValueText = dc.numberDisplay("#file-processor-risk-value", chartGroup);
	var fileProcessorRecoveredValueText = dc.numberDisplay("#file-processor-rec-value", chartGroup);
	var fileProcessorCountBoxElement = document.getElementById("file-processor-sla");
	var fileProcessorHeadingElement = document.getElementById("file-processor-heading");
	
	var fileValidatorCountBox = dc.numberDisplay("#file-validator-sla", chartGroup);
	var fileValidatorCountText = dc.numberDisplay("#file-validator-count", chartGroup);
	var fileValidatorValueText = dc.numberDisplay("#file-validator-value", chartGroup);
	var fileValidatorRiskValueText = dc.numberDisplay("#file-validator-risk-value", chartGroup);
	var fileValidatorRecoveredValueText = dc.numberDisplay("#file-validator-rec-value", chartGroup);
	var fileValidatorCountBoxElement = document.getElementById("file-validator-sla");	
	var fileValidatorHeadingElement = document.getElementById("file-validator-heading");	
	
	var fundingRequestCountBox = dc.numberDisplay("#funding-request-sla", chartGroup);
	var fundingRequestCountText = dc.numberDisplay("#funding-request-count", chartGroup);
	var fundingRequestValueText = dc.numberDisplay("#funding-request-value", chartGroup);
	var fundingRequestRiskValueText = dc.numberDisplay("#funding-request-risk-value", chartGroup);
	var fundingRequestRecoveredValueText = dc.numberDisplay("#funding-request-rec-value", chartGroup);
	var fundingRequestCountBoxElement = document.getElementById("funding-request-sla");
	var fundingRequestHeadingElement = document.getElementById("funding-request-heading");

	var loanRequestCountBox = dc.numberDisplay("#loan-request-sla", chartGroup);
	var loanRequestCountText = dc.numberDisplay("#loan-request-count", chartGroup);
	var loanRequestValueText = dc.numberDisplay("#loan-request-value", chartGroup);
	var loanRequestRiskValueText = dc.numberDisplay("#loan-request-risk-value", chartGroup);
	var loanRequestRecoveredValueText = dc.numberDisplay("#loan-request-rec-value", chartGroup);
	var loanRequestCountBoxElement = document.getElementById("loan-request-sla");
	var loanRequestHeadingElement = document.getElementById("loan-request-heading");

	var paymentProcessorCountBox = dc.numberDisplay("#payment-processor-sla", chartGroup);
	var paymentProcessorCountText = dc.numberDisplay("#payment-processor-count", chartGroup);
	var paymentProcessorValueText = dc.numberDisplay("#payment-processor-value", chartGroup);
	var paymentProcessorRiskValueText = dc.numberDisplay("#payment-processor-risk-value", chartGroup);
	var paymentProcessorRecoveredValueText = dc.numberDisplay("#payment-processor-rec-value", chartGroup);
	var paymentProcessorCountBoxElement = document.getElementById("payment-processor-sla");
	var paymentProcessorHeadingElement = document.getElementById("payment-processor-heading");

	var settlementCountBox = dc.numberDisplay("#settlement-sla", chartGroup);
	var settlementCountText = dc.numberDisplay("#settlement-count", chartGroup);
	var settlementValueText = dc.numberDisplay("#settlement-value", chartGroup);
	var settlementRiskValueText = dc.numberDisplay("#settlement-risk-value", chartGroup);
	var settlementRecoveredValueText = dc.numberDisplay("#settlement-rec-value", chartGroup);
	var settlementCountBoxElement = document.getElementById("settlement-sla");
	var settlementHeadingElement = document.getElementById("settlement-heading");
	
	var HEADING_ELEMENT_DEFAULT_COLOUR = settlementHeadingElement.style.color;
	var HEADING_ELEMENT_DEFAULT_BACKGROUND = settlementHeadingElement.style.background;
	var HEADING_ELEMENT_SELECTED_COLOUR = "white";
	var HEADING_ELEMENT_SELECTED_BACKGROUND = "lightgray";
	
	var systemPaymentDetailsTable = dc.dataTable("#system-payment-details-grid",chartGroup);
	var currencyPieChart = dc.pieChart("#systems-ccy-chart",chartGroup);
	var clientsPieChart = dc.pieChart("#systems-clients-chart",chartGroup);	
	var pauseResumeButton = document.getElementById("system-status-pause-btn");
	
	
	var data = 
	[
         {system:"File Processor", 		currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0},
         {system:"File Validator", 		currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0},
         {system:"Funding Request", 		currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0},
         {system:"Loan Request", 		currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0},
         {system:"Payment Processor", 	currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0},
         {system:"Settlement Service", 	currency: "CHF", client: "Client", inFlightCount: 0, atRiskCount: 0, highRiskCount: 0,recoveredCount: 0, inFlightValue:0, atRiskValue: 0.0, highRiskValue: 0.0, recoveredValue : 0.0}
    ];	
	
	var filteredData;
	var all;				
	var allDim;
	var clientDimension;
	var clientDimensionVolumeGroup;
	var ccyDimension;
	var ccyDimensionVolumeGroup;
	var systemDimension;
	
	var fileProcessorCountGroup;
	var fileProcessorValueGroup;
	var fileProcessorRiskCountGroup;
	var fileProcessorRiskValueGroup;
	var fileProcessorRecoveredValueGroup;
	
	var fileValidatorCountGroup;
	var fileValidatorValueGroup;
	var fileValidatorRiskCountGroup;
	var fileValidatorRiskValueGroup;
	var fileValidatorRecoveredValueGroup;
	
	var fundingRequestCountGroup;
	var fundingRequestValueGroup;
	var fundingRequestRiskCountGroup;
	var fundingRequestRiskValueGroup;
	var fundingRequestRecoveredValueGroup;

	var loanRequestCountGroup;
	var loanRequestValueGroup;
	var loanRequestRiskCountGroup;
	var loanRequestRiskValueGroup;
	var loanRequestRecoveredValueGroup;
	
	var paymentProcessorCountGroup;
	var paymentProcessorValueGroup;
	var paymentProcessorRiskCountGroup;
	var paymentProcessorRiskValueGroup;
	var paymentProcessorRecoveredValueGroup;
	
	var settlementCountGroup;
	var settlementValueGroup;
	var settlementRiskCountGroup;
	var settlementRiskValueGroup;
	var settlementRecoveredValueGroup;
		
	var initialised = false;	
	var paused = false; 
	var filteredBySystem = false;
	var currentSystemFilter;
	var currentSystemElement;


	statusBySystem.pauseOrResume = statusBySystem_pauseOrResume;
	function statusBySystem_pauseOrResume() {
		if( !paused ) {
			paused = true;
			pauseResumeButton.innerHTML = "Resume";
		} else {
			paused = false;
			pauseResumeButton.innerHTML = "Pause";
		}
	}
	
	statusBySystem.resetFilter = statusBySystem_resetFilter;
	function statusBySystem_resetFilter() {
		
		systemDimension.filter(null);
		if( currentSystemElement != null ) {
			currentSystemElement.style.background = HEADING_ELEMENT_DEFAULT_BACKGROUND;
			currentSystemElement.style.color = HEADING_ELEMENT_DEFAULT_COLOUR;
		}

		dc.filterAll('statusBySystemChartGroup'); 
		dc.renderAll('statusBySystemChartGroup');
	}

	
	function reduceBySystem(group, system, accessor) {		
		
		return group.reduce(
				
	              function (p, v) {
	                  
	                  if( v.system == system ) {
		                  ++p.n;
		                  p.tot += accessor(v);		                	  
	                  }
	                  return p;
	              },
	              
	              function (p, v) {
	            	  
	                  if( v.system == system ) {
		                  --p.n;
		                  p.tot -= accessor(v);
	                  }	                  
	                  return p;
	              },
	              
	              function () { 
	            	  	return {n:0,tot:0}; 
	              }
	        );				
		
	}
	
	
	function configureSystemBox( box, element, group ) { 
		
		box.formatNumber(d3.format("d"))
		
		 	.valueAccessor(function(d) { 
		 		
		 		if( d.tot <= 0 ) {
		 			element.style.background="darkgreen";			      				 			
		 			element.style.color="white";			      				 			
		 		} else if( d.tot > 0 ) {
		 			element.style.background="yellow";			      				 			
		 			element.style.color="black";			      				 			
		 		} if( d.tot > 10 ) {
		 			element.style.background="red";			      				 			
		 			element.style.color="white";			      				 			
		 		} 
		 		
		 		return d.tot; 
		 	})
		 	.group(group);		
	}
		
	function addSystemBoxClickHandler(elementName, filterValue, headingElement) {
		
		$(elementName).on('click', function() {
			
			if( filteredBySystem ) {
				
				if( currentSystemFilter == filterValue ) {
					systemDimension.filter(null);						
					filteredBySystem = false;	
					
					if( currentSystemElement != null ) {
						currentSystemElement.style.background = HEADING_ELEMENT_DEFAULT_BACKGROUND;
						currentSystemElement.style.color = HEADING_ELEMENT_DEFAULT_COLOUR;
					}
	      				 			
				} else {
					systemDimension.filter(filterValue);	
					
					if( currentSystemElement != null ) {
						currentSystemElement.style.background = HEADING_ELEMENT_DEFAULT_BACKGROUND;
						currentSystemElement.style.color = HEADING_ELEMENT_DEFAULT_COLOUR;
					}
					
					headingElement.style.background = HEADING_ELEMENT_SELECTED_BACKGROUND;
					headingElement.style.color = HEADING_ELEMENT_SELECTED_COLOUR;			      				 								
				}
				
				currentSystemFilter = filterValue;
				currentSystemElement = headingElement;
			} else {
				
				if( currentSystemElement != null ) {
					currentSystemElement.style.background = HEADING_ELEMENT_DEFAULT_BACKGROUND;
					currentSystemElement.style.color = HEADING_ELEMENT_DEFAULT_COLOUR;
				}
				
				headingElement.style.background = HEADING_ELEMENT_SELECTED_BACKGROUND;
				headingElement.style.color = HEADING_ELEMENT_SELECTED_COLOUR;			      				 			
				
				systemDimension.filter(filterValue);						
				filteredBySystem = true;
				currentSystemFilter = filterValue;
				currentSystemElement = headingElement;
			}

			dc.redrawAll(chartGroup);				
		});		
	}
	
	function limitToZero(d) {
		if( d.tot < 0 ) {
 			return 0;
 		}
 		
 		return d.tot; 
	}
	
	function configureSystemGroup( system, box, boxElement, headingElement, boxElementName, boxRiskCountGroup, openCountText, openCountGroup, openValueText, openValueGroup, riskValueText, riskValueGroup, recoveredValueText, recoveredValueGroup) {
		
		configureSystemBox(box, boxElement, boxRiskCountGroup);
		addSystemBoxClickHandler(boxElementName, system, headingElement);
			
		var totalAccessor = limitToZero;
		openCountText.formatNumber(d3.format("d"))
						 	.valueAccessor(totalAccessor)
						 	.group(openCountGroup);

		openValueText.formatNumber(d3.format(",.2f"))
						 	.valueAccessor(totalAccessor)
						 	.group(openValueGroup);		

		riskValueText.formatNumber(d3.format(",.0f"))
						 	.valueAccessor(totalAccessor)
						 	.group(riskValueGroup);		
		
		recoveredValueText.formatNumber(d3.format(",.0f"))
						 	.valueAccessor(totalAccessor)
						 	.group(recoveredValueGroup);
	}
	
	function resetData() {
	    var currencyPieChartFilters = clientsPieChart.filters();
	    var currencyChartFilters = currencyPieChart.filters();
	    
	    clientsPieChart.filter(null);
	    currencyPieChart.filter(null);
		filteredData.remove();

	    clientsPieChart.filter([currencyPieChartFilters]);
	    currencyPieChart.filter([currencyChartFilters]);
	}


	statusBySystem.initialiseWidgets = statusBySystem_initialise_Widgets;
	function statusBySystem_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});
		filteredBySystem = false;
		
		systemDimension = filteredData.dimension(function (d) {
			return d.system;
		});
	    
		clientDimension = filteredData.dimension(function (d) {
			return d.client;
		});

		clientDimensionVolumeGroup = clientDimension.group().reduceSum(function (d) {
			 return d.inFlightValue;
		});

		ccyDimension = filteredData.dimension(function (d) {
			return d.currency;
		});

		ccyDimensionVolumeGroup = ccyDimension.group().reduceSum(function (d) {
			 return d.inFlightValue;
		});

		
		fileProcessorCountGroup 				= reduceBySystem(filteredData.groupAll(), "File Processor", 	function (d) { return d.fileProcessorCount; });
		fileProcessorValueGroup 				= reduceBySystem(filteredData.groupAll(), "File Processor", 	function (d) { return d.fileProcessorValue / 1000000; });
		fileProcessorRiskCountGroup 			= reduceBySystem(filteredData.groupAll(), "File Processor", 	function (d) { return d.fileProcessorRiskCount; });
		fileProcessorRiskValueGroup  		= reduceBySystem(filteredData.groupAll(), "File Processor", 	function (d) { return d.fileProcessorRiskValue; });
		fileProcessorRecoveredValueGroup 	= reduceBySystem(filteredData.groupAll(), "File Processor", 	function (d) { return d.fileProcessorRecoveredValue; });
		
		configureSystemGroup(   "File Processor", fileProcessorCountBox, fileProcessorCountBoxElement, fileProcessorHeadingElement, '#file-processor-sla',  fileProcessorRiskCountGroup, 
				 				fileProcessorCountText, fileProcessorCountGroup, fileProcessorValueText, fileProcessorValueGroup, 
				 				fileProcessorRiskValueText, fileProcessorRiskValueGroup, fileProcessorRecoveredValueText, fileProcessorRecoveredValueGroup );


		fileValidatorCountGroup 				= reduceBySystem(filteredData.groupAll(), "File Validator", function (d) { return d.fileValidatorCount; });
		fileValidatorValueGroup 				= reduceBySystem(filteredData.groupAll(), "File Validator", function (d) { return d.fileValidatorValue / 1000000;});
		fileValidatorRiskCountGroup 			= reduceBySystem(filteredData.groupAll(), "File Validator", function (d) { return d.fileValidatorRiskCount; });
		fileValidatorRiskValueGroup  		= reduceBySystem(filteredData.groupAll(), "File Validator", function (d) { return d.fileValidatorRiskValue; });
		fileValidatorRecoveredValueGroup		= reduceBySystem(filteredData.groupAll(), "File Validator", function (d) { return d.fileValidatorRecoveredValue; });
		
		configureSystemGroup(   "File Validator", fileValidatorCountBox, fileValidatorCountBoxElement, fileValidatorHeadingElement, '#file-validator-sla',  fileValidatorRiskCountGroup, 
				 				fileValidatorCountText, fileValidatorCountGroup, fileValidatorValueText, fileValidatorValueGroup, 
				 				fileValidatorRiskValueText, fileValidatorRiskValueGroup, fileValidatorRecoveredValueText, fileValidatorRecoveredValueGroup );
		
		
		fundingRequestCountGroup 			= reduceBySystem(filteredData.groupAll(), "Funding Request", 	function (d) { return d.fundingRequestCount; });
		fundingRequestValueGroup 			= reduceBySystem(filteredData.groupAll(), "Funding Request", 	function (d) { return d.fundingRequestValue / 1000000;});
		fundingRequestRiskCountGroup 		= reduceBySystem(filteredData.groupAll(), "Funding Request", 	function (d) { return d.fundingRequestRiskCount; });
		fundingRequestRiskValueGroup  		= reduceBySystem(filteredData.groupAll(), "Funding Request", 	function (d) { return d.fundingRequestRiskValue; });
		fundingRequestRecoveredValueGroup 	= reduceBySystem(filteredData.groupAll(), "Funding Request", 	function (d) { return d.fundingRequestRecoveredValue; });
		
		configureSystemGroup(   "Funding Request", fundingRequestCountBox, fundingRequestCountBoxElement, fundingRequestHeadingElement, '#funding-request-sla',  fundingRequestRiskCountGroup, 
				 				fundingRequestCountText, fundingRequestCountGroup, fundingRequestValueText, fundingRequestValueGroup, 
				 				fundingRequestRiskValueText, fundingRequestRiskValueGroup, fundingRequestRecoveredValueText, fundingRequestRecoveredValueGroup );


		loanRequestCountGroup 				= reduceBySystem(filteredData.groupAll(), "Loan Request", 	function (d) { return d.loanRequestCount; });
		loanRequestValueGroup 				= reduceBySystem(filteredData.groupAll(), "Loan Request", 	function (d) { return d.loanRequestValue / 1000000;});
		loanRequestRiskCountGroup 			= reduceBySystem(filteredData.groupAll(), "Loan Request", 	function (d) { return d.loanRequestRiskCount; });
		loanRequestRiskValueGroup  			= reduceBySystem(filteredData.groupAll(), "Loan Request", 	function (d) { return d.loanRequestRiskValue; });
		loanRequestRecoveredValueGroup		= reduceBySystem(filteredData.groupAll(), "Loan Request", 	function (d) { return d.loanRequestRecoveredValue; });
		
		configureSystemGroup(   "Loan Request", loanRequestCountBox, loanRequestCountBoxElement, loanRequestHeadingElement, '#loan-request-sla',  loanRequestRiskCountGroup, 
				 				loanRequestCountText, loanRequestCountGroup, loanRequestValueText, loanRequestValueGroup, 
				 				loanRequestRiskValueText, loanRequestRiskValueGroup, loanRequestRecoveredValueText, loanRequestRecoveredValueGroup );
		
		
		paymentProcessorCountGroup 			= reduceBySystem(filteredData.groupAll(), "Payment Processor", 	function (d) { return d.paymentProcessorCount; });
		paymentProcessorValueGroup 			= reduceBySystem(filteredData.groupAll(), "Payment Processor", 	function (d) { return d.paymentProcessorValue / 1000000;});
		paymentProcessorRiskCountGroup 		= reduceBySystem(filteredData.groupAll(), "Payment Processor", 	function (d) { return d.paymentProcessorRiskCount; });
		paymentProcessorRiskValueGroup  		= reduceBySystem(filteredData.groupAll(), "Payment Processor", 	function (d) { return d.paymentProcessorRiskValue; });
		paymentProcessorRecoveredValueGroup 	= reduceBySystem(filteredData.groupAll(), "Payment Processor", 	function (d) { return d.paymentProcessorRecoveredValue; });
		
		configureSystemGroup(   "Payment Processor", paymentProcessorCountBox, paymentProcessorCountBoxElement, paymentProcessorHeadingElement, '#payment-processor-sla',  paymentProcessorRiskCountGroup, 
				 				paymentProcessorCountText, paymentProcessorCountGroup, paymentProcessorValueText, paymentProcessorValueGroup, 
				 				paymentProcessorRiskValueText, paymentProcessorRiskValueGroup, paymentProcessorRecoveredValueText, paymentProcessorRecoveredValueGroup );
		
		
		settlementCountGroup 				= reduceBySystem(filteredData.groupAll(), "Settlement", 	function (d) { return d.settlementCount; });
		settlementValueGroup 				= reduceBySystem(filteredData.groupAll(), "Settlement", 	function (d) { return d.settlementValue / 100000;});
		settlementRiskCountGroup 			= reduceBySystem(filteredData.groupAll(), "Settlement", 	function (d) { return d.settlementRiskCount; });
		settlementRiskValueGroup  			= reduceBySystem(filteredData.groupAll(), "Settlement", 	function (d) { return d.settlementRiskValue; });
		settlementRecoveredValueGroup 		= reduceBySystem(filteredData.groupAll(), "Settlement", 	function (d) { return d.settlementRecoveredValue; });
		
		configureSystemGroup(   "Settlement", settlementCountBox, settlementCountBoxElement, settlementHeadingElement, '#settlement-sla',  settlementRiskCountGroup, 
				 				settlementCountText, settlementCountGroup, settlementValueText, settlementValueGroup, 
				 				settlementRiskValueText, settlementRiskValueGroup, settlementRecoveredValueText, settlementRecoveredValueGroup );

		
		systemPaymentDetailsTable.width(600).height(420)
	    	.dimension(allDim)
	    	.group(function(d) { return "Selected Payments" })
	    	.size(10000)
	    	.columns([
    	        function(d) { return d.system; },
			    function(d) { return d.currency; },
    	        		function(d) { return d.client; },
    				function(d) { return formatValueAndScale(d.inFlightCount, 1); },
			    function(d) { return formatValueAndScale(d.inFlightValue, 1); },
			    function(d) { return formatValueAndScale(d.atRiskValue, 1); },
			    function(d) { return formatValueAndScale(d.highRiskValue, 1); },
			    function(d) { return formatValueAndScale(d.recoveredValue, 1); },
		    ])
		    .sortBy(function(d) { return d.inFlightValue; })
		    .order(d3.descending)   
		    .on('renderlet', function (table) {    		
		    	
		    	table.select('tr.dc-table-group').remove();		    	
		    	table.selectAll('tr.dc-table-row').on('dblclick', function (row) { 
		    		
		    		var system = row.system;
		    		var client = row.client;
		    		var currency = row.currency;
		    		
		    		console.log("selected system=" + system + ", client=" + client + ", ccy=" + currency);
		    		console.log("data=" + JSON.stringify(row));
		    		itemTraceTabActiveListener();
		    		itemTrace.resetData();
		    		$('.nav-tabs a[href="#itemTrace"]').tab('show')
		    		itemTrace.updateOverlayData(system, client, currency, row);
		    	});
    		});		
	  
		clientsPieChart
			.width(500)
			.height(320)
		 	.radius(150)
		 	.innerRadius(0)
		 	.transitionDuration(500)
		 	.dimension(clientDimension)
		 	.title(function(d) { return d.client; } )
		 	.label(function (d) {
	            if (clientsPieChart.hasFilter() && !clientsPieChart.hasFilter(d.key)) {
	                return "0mm";
	            }
	            
	            var label = d.key;
	            label = d.value  + 'mm';		            
	            return formatAmountAndScale2(d.value,0.000001);
	        })
	        .renderLabel(true)
		 	.group(clientDimensionVolumeGroup)
		 	.legend(dc.legend().x(420).y(10));
		
	    
		currencyPieChart
			.width(500)
			.height(325)
		 	.radius(150)
		 	.innerRadius(0)
		 	.transitionDuration(500)
		 	.dimension(ccyDimension)
		 	.title(function(d) { return d.client; } )
		 	.label(function (d) {
	            if (currencyPieChart.hasFilter() && !currencyPieChart.hasFilter(d.key)) {
	                return "0mm";
	            }
	            
	            var label = d.key;
	            label = d.value  + 'mm';		            
	            return formatAmountAndScale2(d.value,0.000001);
	        })
	        .renderLabel(true)
		 	.group(ccyDimensionVolumeGroup)
		 	.legend(dc.legend().x(420).y(60));	    
	}
	
	statusBySystem.updateOverlayData = statusBySystem_updateOverlayData;
	function statusBySystem_updateOverlayData() {
		
		if( statusBySystemTabActive && !paused ) {

			var statusBySystem_restUrl = statusBySystem_queryApi + "all/all/all"
			d3.json(statusBySystem_restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				resetData();
				
				data.forEach(function(d) { 					
				    
				    switch( d.system ) {
				    
					    case "File Processor":
						    	d.fileProcessorCount = d.inFlightCount;
						    	d.fileProcessorValue = d.inFlightValue;
						    	d.fileProcessorRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.fileProcessorRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.fileProcessorRecoveredValue = d.recoveredValue / 1000000;
						    	console.log(JSON.stringify(d));	 
						    	break;
					    	
					    case "File Validator":
						    	d.fileValidatorCount = d.inFlightCount;
						    	d.fileValidatorValue = d.inFlightValue;
						    	d.fileValidatorRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.fileValidatorRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.fileValidatorRecoveredValue = d.recoveredValue / 1000000;
						    	break;
					    	
					    case "Funding Request":
						    	d.fundingRequestCount = d.inFlightCount;
						    	d.fundingRequestValue = d.inFlightValue;
						    	d.fundingRequestRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.fundingRequestRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.fundingRequestRecoveredValue = d.recoveredValue / 1000000;
						    	break;
					    	
					    case "Loan Request":
						    	d.loanRequestCount = d.inFlightCount;
						    	d.loanRequestValue = d.inFlightValue;
						    	d.loanRequestRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.loanRequestRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.loanRequestRecoveredValue = d.recoveredValue / 1000000;
						    	break;
					    	
					    case "Payment Processor":
						    	d.paymentProcessorCount = d.inFlightCount;
						    	d.paymentProcessorValue = d.inFlightValue;
						    	d.paymentProcessorRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.paymentProcessorRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.paymentProcessorRecoveredValue = d.recoveredValue  / 1000000;
						    	break;

					    case "Settlement":
						    	d.settlementCount = d.inFlightCount;
						    	d.settlementValue = d.inFlightValue;
						    	d.settlementRiskCount = (d.atRiskCount + d.highRiskCount);
						    	d.settlementRiskValue = (d.atRiskValue + d.highRiskValue);
						    	d.settlementRecoveredValue = d.recoveredValue;
						    //console.log(JSON.stringify(d));	   
						    	break;
					    	
					    default:
					    		break;
				    }				    
				});	
				
				filteredData.add(data);

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}		    
			});		
		
		}
	}
	
	
	if (typeof define === "function" && define.amd)
		define(statusBySystem);
	else if (typeof module === "object" && module.exports)
		module.exports = statusBySystem;
	
	this.statusBySystem = statusBySystem;
}();



function statusBySystemUpdateData() {
	statusBySystem.updateOverlayData();	
	setTimeout(statusBySystemUpdateData, REFRESH_PERIOD); 
};

statusBySystemTabActiveListener = function() {
	statusBySystemTabActive = true;
	statusBySystem.updateOverlayData();
}

statusBySystem.initialiseWidgets();
statusBySystemUpdateData();



