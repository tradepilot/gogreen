!function() {
	
	var systemView = {
		version : "1.0.0"
	};
	
	var systemViewApi = remoteHost +  "/connectiongraph/"
	var containerDetailsApi = remoteHost +  "/containerdetails/"
    var chartGroup = "systemViewChartGroup";
	
	if( isTest == "true" ) {
		systemViewApi = "connectiongraphtest/"
	    containerDetailsApi = "/containerdetailstest/"
	}

	
	var filteredData;
	var all;				
	var allDim;
	var initialised = false;	
	var paused = false; 
	var cy;
	var i = 0;
	var pauseResumeButton = document.getElementById("system-view-pause-btn");
	var containerDetailsTable = dc.dataTable("#container-details-grid", chartGroup);

	var data = [
		 	{id:"559a74c528e1-10.0.0.46", container: "559a74c528e1", host : "10.0.0.46", serviceName:"Funding Reuqest Generator", state:"Running" },
		 	{id:"559a74c528e4-10.0.0.47", container: "559a74c528e4", host : "10.0.0.47", serviceName:"Loan Generator", state:"Running" }
    ]


	systemView.resetFilter = systemView_resetFilter;
	function systemView_resetFilter() {	

	}

	systemView.resetData = systemView_resetData;
	function systemView_resetData() {

		filteredData.remove();
		dc.filterAll(chartGroup); 
		dc.renderAll(chartGroup);
	}	
	
	systemView.pauseOrResume = systemView_pauseOrResume;
	function systemView_pauseOrResume() {
		if( !paused ) {
			paused = true;
			pauseResumeButton.innerHTML = "Resume";
		} else {
			paused = false;
			pauseResumeButton.innerHTML = "Pause";
		}
	}

	
	systemView.updateOverlayData = systemView_updateOverlayData;
	function systemView_updateOverlayData() {	

		if( systemViewTabActive && !paused ) {
		
			console.log("Running systemView.updateOverlayData()");	
			$.getJSON(systemViewApi, function (data) {
				   
			    console.log(JSON.stringify(data));
				cy.json({ elements: data.elements });
				systemView.doLayout();
			});

			
			d3.json(containerDetailsApi, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				systemView_resetData();
				filteredData.add(data);
				//console.log(JSON.stringify(data));


			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}					
			});										
		}		
	}
	

	systemView.initialiseWidgets = systemView_initialise_Widgets;
	function systemView_initialise_Widgets() {

		if( !initialised ) {
			
			filteredData = crossfilter(data);
			all = filteredData.groupAll();				
			allDim = filteredData.dimension(function(d) { return d; });
			
			containerDetailsTable.width(600).height(420)
			    	.dimension(allDim)
			    	.group(function(d) { return "Details" })
			    	.size(10000)
			    	.columns([
	    	        		function(d) { return d.serviceName; },
				    function(d) { return d.host; },
				    function(d) { return d.container },
				    function(d) { return d.state; },
			    ])
			    .sortBy(function(d) { return d.host; })
			    .order(d3.descending)
			    .on('renderlet', function (table) {    		
			    		table.select('tr.dc-table-group').remove();		    	
			    	});			

			
			console.log("Creating canvas");	
			cy = cytoscape({
				
				  container: document.getElementById("system-view-chart"),
				  boxSelectionEnabled: false,
				  autounselectify: true,

				  style: cytoscape.stylesheet()
				    .selector('node')
				      .css({
				        'content': 'data(name)',
				        'width': '20',
				        'height': '20',
				        'font-size': '4px',
				        'text-margin-y': '-5',
				        'background-color': 'darkblue',
				        'background-opacity': '0.5',
				        'border-color': 'black',
				        'border-width': '0.25'
				      })
				    .selector('edge')
				      .css({
				        'curve-style': 'bezier',
				        'target-arrow-shape': 'triangle',
				        'arrow-scale': '0.5',
				        'width': .5,
				        'line-color': 'black',
				        'target-arrow-color': 'black'
				      })
				    .selector('.highlighted')
				      .css({
				        'background-color': '#61bffc',
				        'line-color': '#61bffc',
				        'target-arrow-color': '#61bffc',
				        'transition-property': 'background-color, line-color, target-arrow-color',
				        'transition-duration': '0.5s'
				      }),

			});
			
			cy.on('mouseover', 'node', function(event) {

			    var node = event.target;	
			    var nodeData = node._private.data.properties;
			    var nodeText = "<p>";
			    
			    $.each(nodeData, function(key, value) {
			        console.log(key, value);
			        nodeText = nodeText + key + ': ' + value + '<br/>';
			    });
			    
			    nodeText = nodeText + "</p>"
			    
			    node.qtip({
				    	content: {		                    
		                    text: nodeText, 
		                    title: {
		                        text: node._private.data.name,
		                        button: false
		                    }
		                },
			         show: {
			            event: event.type,
			            ready: true,
			            effect: function() {
			                $(this).fadeTo(500, 1);
			            }
			         },
			         hide: {
			            event: 'mouseout unfocus',
			            effect: function() {
			                $(this).slideUp();
			            }
			         },
			         position: {
			             my: 'top left',  // Position my top left...
			             at: 'bottom right', // at the bottom right of...
			             target: node // my target
			         },
		            style: {
		                classes: 'qtip-bootstrap',
		                	width: 500
		            }
			    }, event);
			});

			initialised = true;
		}
	}

	
	var hasInitialised = false;

	systemView.doLayout = systemView_doLayout;
	function systemView_doLayout() {
		
		hasInitialised = false;
		if( !hasInitialised ) {
			
			console.log("Running initial layout");		
			hasInitialised = true;

			var layout = cy.elements().layout({
			    name: 'preset',
			    fit: true,
			    spacingFactor: 1.5,
			    padding: 2,
			    directed: true,
				positions: function( n ) { 
			    	    
					console.log(JSON.stringify(n._private.data) + " " + JSON.stringify(n._private.position));
			    		var pos = n._private.position;
			    		return pos;
				}
			});
					
			layout.pon('layoutstop').then(function( event ){
				  console.log('layoutstop promise fulfilled ' + event);
				});
			
			layout.run();	
		}
	}
	
	if (typeof define === "function" && define.amd)
		define(systemView);
	else if (typeof module === "object" && module.exports)
		module.exports = systemView;
	
	this.systemView = systemView;
}();


var i = 0;
function systemViewUpdateData() {	
	
 	systemView.updateOverlayData();	
	setTimeout(systemViewUpdateData, REFRESH_PERIOD); 
};

systemViewTabActiveListener = function() {
	
	console.log("Sys tab active" );	
	systemView.initialiseWidgets();
	systemViewTabActive = true;
	systemViewUpdateData();
}

if( systemViewTabActive ) {
	systemView.initialiseWidgets();
	systemViewUpdateData();
}





