
// Global constants
//
var REFRESH_PERIOD = 5000;
var BILLION = 1000000000.0;

var SYSTEM_STATUS_TAB_NAME="System Status";
var COUNTRY_SOURCE_TAB_NAME="Country Source";
var ITEM_TRACE_TAB_NAME="Payment Details";
var SYSTEM_VIEW_TAB_NAME="System View";

//var remoteHost="http://localhost:1130"
//var remoteHost="http://tradepilot.cloudapp.net:1130"
var remoteHost=""

// Get any variables from the URL
//
$.extend({
	getUrlVars : function() {
		
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	},

	getUrlVar : function(name) {
		return $.getUrlVars()[name];
	}
});

var allVars = $.getUrlVars();
var myBic = $.getUrlVar('bic');
var isTest = $.getUrlVar('test');
var currentTab;
var lastTab;

var commaFormat = d3.format(",");
var timeFormat = d3.time.format("%H:%M:%S");


var countrySourceTabActive = true;
var statusBySystemTabActive = false;
var itemTraceTabActive = false;
var systemViewTabActive = false;

var itemTraceTabActiveListener;
var statusBySystemTabActiveListener;
var countrySourceTabActiveListener;
var systemViewTabActiveListener;


// Tab listener to allow us to start & stop scripts
// for each tab when the become active or inactive
//
$(document).ready(function() {
	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		currentTab = $(e.target).text(); 
		lastTab = $(e.relatedTarget).text(); 
		
		switch( currentTab ) {
		
			case COUNTRY_SOURCE_TAB_NAME:
				itemTraceTabActive = false;
				statusBySystemTabActive = false;
				countrySourceTabActive = true;
				systemViewTabActive = false;
				console.log("Country Source Tab Active");	
				countrySourceTabActiveListener();
				break;


			case SYSTEM_STATUS_TAB_NAME:
				itemTraceTabActive = false;
				statusBySystemTabActive = true;
				countrySourceTabActive = false;
				systemViewTabActive = false;
				console.log("System Status Tab Active");	
				statusBySystemTabActiveListener();
				break;

			case ITEM_TRACE_TAB_NAME:
				itemTraceTabActive = true;
				statusBySystemTabActive = false;
				countrySourceTabActive = false;
				systemViewTabActive = false;
				console.log("Item Trace Tab Active");	
				itemTraceTabActiveListener();
				break;		

			case SYSTEM_VIEW_TAB_NAME:
				systemViewTabActive = true;
				itemTraceTabActive = false;
				statusBySystemTabActive = false;
				countrySourceTabActive = false;
				console.log("System View Tab Active");	
				systemViewTabActiveListener();
				break;		

		}
	});
});


function pad2(number) {
	return (number < 10 ? '0' : '') + number
}

function formatAmount(amount) {

	return formatAmountAndScale(amount,1000000.0);
}

function formatAmountAndScale(amount, multiplier) {
	
	var notional = d3.round(amount * multiplier, 2)
	var formatted = commaFormat(notional);
	
	if( notional > 1000 ) {
		// Extra scaling for bn
		//
		notional = d3.round((amount / 1000) * multiplier, 2)
		formatted = commaFormat(notional);
		
		return formatted + "bn";
	} else if( notional > 100 ) {
		return formatted + "mm";
	} else if( notional > 10 ) {
		return formatted + "mm";
	} else if( notional > 1 ) {
		return formatted + "mm";
	} else {
		return formatted + "mm";
	}
}

function formatAmountAndScale2(amount, multiplier) {
	
	var notional = 0;
	var scaled = amount * multiplier;
	
	if( scaled < 0 ) {		
		notional = d3.round(scaled, 4)
	} else if( scaled < 1000 ) {
		notional = d3.round(scaled, 3)
	} else if( scaled < 100000 ) {
		notional = d3.round(scaled, 2)
	} else {
		notional = d3.round(scaled, 0)		
	}

	var formatted = commaFormat(notional);
	
	if( scaled > 10000 ) {
		return formatted + "bn";
	} else if( notional > 100 ) {
		return formatted + "mm";
	} else if( notional > 10 ) {
		return formatted + "mm";
	} else if( notional > 1 ) {
		return formatted + "mm";
	} else {
		return formatted + "mm";
	}
}
		

function formatValueAndScale(amount, multiplier) {
	
	var notional = d3.round(amount * multiplier, 2)
	var formatted = commaFormat(notional);
	
	if( notional > 1000 ) {
		// Extra scaling for bn
		//
		notional = d3.round((amount / 1000) * multiplier, 2)
		formatted = commaFormat(notional);		
	} 
	
	return formatted;
}
	

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);


var messageDialog = messageDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-footer">' +
				'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);


