!function() {
	
	var countrySource = {
		version : "1.0.0"
	};
	
	var countrySource_myBic = myBic;
	var countrySource_isTest = isTest;	
	var queryApi = remoteHost + "/paymentsbycountry/"
		
	if( isTest == "true" ) {
		queryApi = "paymentsbycountrytest/"
	}
	
		
    var chartGroup = "countrySourceChartGroup";

	var width = 990;
	var height = 570;
	var countryHeatmapChartHeight = 380;
	var countryMap = new Map();

	var svg;
	var tooltip = d3.select("#funds-location-chart").append("div").attr("class", "tooltip hidden");
	var paymentsByStatusBarChart = dc.rowChart("#payments-by-status-chart",chartGroup);
	var paymentsByClientPieChart = dc.pieChart("#payments-by-client-pie-chart",chartGroup);
	var paymentDetailsTable = dc.dataTable("#payment-details-grid",chartGroup);
    var countryHeatmapChart = dc.heatMap("#payments-by-ccy-country-chart", chartGroup);


    var map;
    var cityMarkers;
	var initialised = false;
	
	var pauseResumeButton = document.getElementById("country-source-pause-btn");
	var paused = false; 

	

	countrySource.pauseOrResume = countrySource_pauseOrResume;
	function countrySource_pauseOrResume() {
		if( !paused ) {
			paused = true;
			pauseResumeButton.innerHTML = "Resume";
		} else {
			paused = false;
			pauseResumeButton.innerHTML = "Pause";
		}
	}
	
	countrySource.initialiseWidgets = countrySource_initialise_Widgets;
	function countrySource_initialise_Widgets() {

		if( !initialised ) {
			countrySource_setup(width,height);
			loadMapData();
			
			// Initialised is set on first render below
			//initialised = true;
		}
	}

	function countrySource_setup(width, height) {

		console.log("Running countrySource_setup()");	
		svg = d3.select("#funds-location-chart")
				.append("svg").attr("width", width)
				.attr("height", height)
				.append("g");

		
		map = L.map('funds-location-chart').setView([31.505, 10.09], 2);

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
		    maxZoom: 12,
		    id: 'simonwicks.p61n9jla',
		    accessToken: 'pk.eyJ1Ijoic2ltb253aWNrcyIsImEiOiJjaWtwaW5xZWwwMGI2d2FtNndnNXNsdmc3In0.fw1PY9cB4qz5tVpAyBMeYw'
		}).addTo(map);

		cityMarkers = L.markerClusterGroup({
			maxClusterRadius : 120,
			singleMarkerMode : true,
			spiderfyDistanceMultiplier : 2,
			
		    iconCreateFunction: function (cluster) {
		    	
		    	var clusterTotal = 0.0;
		    	_.each(cluster.getAllChildMarkers(), function (d) {
		    		var label = d._popup._content;
		    		var res = label.split(" ");
		    		var amt = res[res.length - 1];
		    		var len = amt.length;
		    		var value = amt.substr(0, len - 6);
		    		var nodeValue = parseFloat(value)
		    		
		    		if( isNaN(nodeValue) ) {
		    			console.log("Unable to parse cluster value " + d._popup._content + ", value=" + value);
		    		}
		    		clusterTotal += nodeValue;
		    	});
		    	
				var c = ' marker-cluster-';
				if (clusterTotal < 250) {
					c += 'small';
				} else if (clusterTotal < 1000) {
					c += 'medium';
				} else {
					c += 'large';
				}

				var formattedTotal = formatAmountAndScale2(clusterTotal, 1)		
				return new L.DivIcon({ html: '<div><span><strong>' + formattedTotal + '</strong></span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
			}
		});
	}

	
	countrySource.cacheMapCoordinates = countrySource_cacheMapCoordinates;
	function countrySource_cacheMapCoordinates() {
		
		d3.csv("data/country-capitals.csv", function(err, capitals) {

			capitals.forEach(function(i) {
				//console.log("Caching point " + JSON.stringify(i));
				countryMap.set(i.CapitalName, i);
			});

		});			
	}
	
	countrySource.updateOverlayData = countrySource_updateOverlayData;
	function countrySource_updateOverlayData() {
		
		if( countrySourceTabActive && !paused ) {

			var restUrl = queryApi + "all/all/all"
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				data.forEach(function(d) { 
					
				    d.notional = commaFormat(d3.round(d.amount, 2));			    
				    if( d.complete ) {
				    		d.status = "Complete";
				    } else {
				    		d.status = "Open";
				    }				    				    
				    
				    switch( d.country ) {
					    case "Australia":
						    	d.countryCode = "AUS";
						    	break;
					    	
					    case "Belgium":
						    	d.countryCode = "BEL";
						    	break;
					    	
					    case "Germany":
						    	d.countryCode = "GER";
						    	break;
					    	
					    case "France":
						    	d.countryCode = "FRA";
						    	break;
					    	
					    case "India":
						    	d.countryCode = "IND";
						    	break;
					    	
					    case "Japan":
						    	d.countryCode = "JPN";
						    	break;

					    case "Singapore":
						    	d.countryCode = "SGP";
						    	break;

					    case "Switzerland":
						    	d.countryCode = "SWZ";
						    	break;

					    case "United Kingdom":
						    	d.countryCode = "GBR";
						    	break;
					    	
					    case "United States":
						    	d.countryCode = "USA";
						    	break;
					    	
					    	default:
							d.countryCode = "OTH";
						    	break;
				    }
				    
					//console.log(JSON.stringify(d));	   
				});	
				
				var filteredData = crossfilter(data);
				var all = filteredData.groupAll();				
				var allDim = filteredData.dimension(function(d) {return d;});
				
			    
				var clientDimension = filteredData.dimension(function (d) {
					return d.client;
				});
	
				var clientDimensionCountGroup = clientDimension.group().reduceCount();
	
				var clientDimensionVolumeGroup = clientDimension.group().reduceSum(function (d) {
					 return d.amount;
				});
				
				var statusDimension = filteredData.dimension(function (d) {
					return d.status;
				});
							
				var statusDimensionGroup = statusDimension.group();
				
				var statusDimensionVolumeGroup = statusDimension.group().reduceSum(function (d) {
					 return d.amount;
				});
				
				paymentDetailsTable.width(600).height(420)
				    	.dimension(allDim)
				    	.group(function(d) { return "Selected Payments" })
				    	.size(10000)
				    	.columns([
						    function(d) { return d.client; },
			    	        		function(d) { return d.currency; },
			    	        		function(d) { return d.country; },
						    function(d) { return d.city; },
						    function(d) { return d.notional; },
						    function(d) { return d.complete; },
					    ])
					    .sortBy(function(d) { return d.city; })
					    .order(d3.ascending)
					    .on('renderlet', function (table) {
			    		
					    	table.select('tr.dc-table-group').remove();
						    cityMarkers.clearLayers();
			    		  
				    		var count = 0;
				    		console.log("Table renderlet");
				    		
				    		_.each(allDim.top(Infinity), function (d) {
				    	    	  
				    			//console.log(JSON.stringify(d));	    	    		  
			    				var city = d.city;
			    			    var cityCoordinates = countryMap.get(city);			    
			    			    if( cityCoordinates != null ) {
			    			    	
					    		    	count++;
				    			    	var lat = cityCoordinates.CapitalLongitude;
				    			    	var lon = cityCoordinates.CapitalLatitude;
				    			    	var title = "<p>" + city + " " + formatAmountAndScale2(d.amount, 0.000001) + "</p>";
			    			    	
					    		    var marker = L.marker([lon, lat]);
					    		    marker.bindPopup(title);
					    		    cityMarkers.addLayer(marker);
			    			    	
			    			    } else {
			    				    console.log("No city data for  " + city);		    	
			    			    }	
			    	          
			    	        });
		
				    		
			    		    if( count > 0 ) {
			    		    		map.addLayer(cityMarkers);
					    		map.fitBounds(cityMarkers.getBounds());	
			    		    } 
			    		});		
				    
	
			    paymentsByStatusBarChart.width(700).height(100)
			    		.dimension(statusDimension)
			    		.group(statusDimensionVolumeGroup)
			    	//	.transitionDuration(1500)
			    		.margins({top: 0, right: 20, bottom: 20, left: 10})
			    		.colors(d3.scale.category10())
			    		.label(function (d) {
			    			
				            if (paymentsByStatusBarChart.hasFilter() && !paymentsByStatusBarChart.hasFilter(d.key)) {
				                return d.key + " 0mm";
				            }
			    					    			
			    			return d.key + " " + formatAmountAndScale2(d.value, 0.000001);		    				
			    		})
			    		.elasticX(true)
			    		.xAxis();
			    
			    
			    var countryCurrencyDimension = filteredData.dimension(function(d) { return [ d.countryCode, d.currency]; });
			    
			    var countryCurrencyDimensionGroup = countryCurrencyDimension.group().reduceSum(function (d) {
					 return d.amount;
				});
			    
			    countryHeatmapChart
	                	.width(700)
	                	.height(countryHeatmapChartHeight)
	                	.dimension(countryCurrencyDimension)
	                	.group(countryCurrencyDimensionGroup)
	                	.keyAccessor(function(d) { return d.key[0]; })
	                	.valueAccessor(function(d) { return d.key[1]; })
	                	.colorAccessor(function(d) { return d.value; })
	                	.title(function(d) {
	                		return  d.key[0] + " -> " +  d.key[1] + " " + formatAmountAndScale(d.value,1);
	                	})
	                	//.colors(heatColorMapping)
	                	.linearColors(["#C6DBEF", "#3182BD"])
	                	.calculateColorDomain();
				    
			    countryHeatmapChart.xBorderRadius(0);
			    countryHeatmapChart.yBorderRadius(0);
				
			    
				paymentsByClientPieChart
					.cx(200)
					.width(750)
					.height(320)
				 	.radius(150)
				 	.innerRadius(0)
				 	.transitionDuration(500)
				 	.dimension(clientDimension)
				 	.title(function(d) { return d.client;} )
				 	.label(function (d) {
			            if (paymentsByClientPieChart.hasFilter() && !paymentsByClientPieChart.hasFilter(d.key)) {
			                return "0mm";
			            }
			            
			            var label = d.key;
			            label = d.value  + 'mm';		            
			            return formatAmountAndScale2(d.value,0.000001);
			        })
			        .renderLabel(true)
				 	.group(clientDimensionVolumeGroup)
				 	.legend(dc.legend().x(400).y(10).legendWidth(400));
			    
			    
			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}		    
			});		
		
		}
	}
	
	
	if (typeof define === "function" && define.amd)
		define(countrySource);
	else if (typeof module === "object" && module.exports)
		module.exports = countrySource;
	
	this.countrySource = countrySource;
}();



function loadMapData() {
	countrySource.cacheMapCoordinates();
};


function updateData() {
	countrySource.updateOverlayData();	
	setTimeout(updateData, REFRESH_PERIOD); 
};

countrySourceTabActiveListener = function() {
	countrySource.initialiseWidgets();
	countrySource.updateOverlayData();
}

if( countrySourceTabActive ) {
	countrySource.initialiseWidgets();
	updateData();	
}



