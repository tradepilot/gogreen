!function() {
	
	var itemTrace = {
		version : "1.0.0"
	};
	
	var itemTraceApi = remoteHost +  "/filedetails/"
    var chartGroup = "itemTraceChartGroup";
	
	if( isTest == "true" ) {
		itemTraceApi = "filedetailstest/"
	}

	var width = 990;
	var height = 570;
	
	var svgWidth = 4000;
	var svgHeight = 200;

	
	var filteredData;
	var all;				
	var allDim;
	var initialised = false;	
	var paused = false; 
	var state = "green";

	// Select SVG element		
	var graphSvg = d3.select(".item-trace-graph")
					.attr("width", svgWidth)
					.attr("height", svgHeight);

	var systemPaymentDetailsTable = dc.dataTable("#item-trace-details-grid", chartGroup);

	var data = 
	[
	 	{correlationId:"5accdb75-8e52-44e4-a192-927c901649c1", currency : "NZD", creditorName:"Ahold", debtorName:"Philips", creditorAccount:"IE69INGSMMG0EUE0N", debtorAccount:"IE69INGMJ3MLQZSHG", creditorCountry:"Germany", debtorCountry:"Netherlands", lastAgentPoint:"ClearPaymentRequest", amount:4000000.0, complete:false, 
 			chain:
 			[
			 	{agentId:"RetailOrderReceived", 			timestamp:"Mar 22, 2016 4:58:48 PM"},
			 	{agentId:"ManageOrderRequest",			timestamp:"Mar 22, 2016 4:58:48 PM"},
			 	{agentId:"ManageOrderRequestReceived",	timestamp:"Mar 22, 2016 4:58:48 PM"},
			 	{agentId:"ManageOrderResponse",			timestamp:"Mar 22, 2016 4:58:49 PM"},
			 	{agentId:"ManageOrderResponseReceived",	timestamp:"Mar 22, 2016 4:58:49 PM"},
			 	{agentId:"ExecuteOrder",					timestamp:"Mar 22, 2016 4:58:49 PM"},
			 	{agentId:"ExecuteOrderReceived",			timestamp:"Mar 22, 2016 4:58:50 PM"},
			 	{agentId:"FundsRequest",					timestamp:"Mar 22, 2016 4:58:50 PM"},
			 	{agentId:"FundsRequestReceived",			timestamp:"Mar 22, 2016 4:58:51 PM"},
			 	{agentId:"FundsResponse",				timestamp:"Mar 22, 2016 4:58:51 PM"},
			 	{agentId:"FundsResponseReceived",		timestamp:"Mar 22, 2016 4:58:51 PM"},
			 	{agentId:"ForexRequest",					timestamp:"Mar 22, 2016 4:58:51 PM"},
			 	{agentId:"PostAccountRequest",			timestamp:"Mar 22, 2016 4:58:53 PM"},
			 	{agentId:"ClearPaymentRequest",			timestamp:"Mar 22, 2016 4:58:54 PM"}
 			]
	 	}
	 ]
		

	itemTrace.resetFilter = itemTrace_resetFilter;
	function itemTrace_resetFilter() {
		
		dc.filterAll('itemTraceChartGroup'); 
		dc.renderAll('itemTraceChartGroup');
	}

	itemTrace.resetData = itemTrace_resetData;
	function itemTrace_resetData() {
		
		graphSvg.selectAll("*").remove();
		filteredData.remove();
		dc.filterAll('itemTraceChartGroup'); 
		dc.renderAll('itemTraceChartGroup');
	}


	itemTrace.initialiseWidgets = itemTrace_initialise_Widgets;
	function itemTrace_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});

		
		systemPaymentDetailsTable.width(600).height(420)
	    	.dimension(allDim)
	    	.group(function(d) { return "Selected Files" })
	    	.size(10000)
	    	.columns([
    	        function(d) { return d.buyerName; },
			    function(d) { return d.country; },
			    function(d) { return formatValueAndScale(d.totalValue, 1); },
			    function(d) { return d.lastAgentPoint },
			    function(d) { return d.numberOfInstructions; },
    	        		function(d) { return d.outstandingPaymentRequests; },
       	        function(d) { return d.receivedDateTime; },
			    function(d) { return d.state; },
		    ])
		    .sortBy(function(d) { return d.lastAgentTimestampMillis; })
		    .order(d3.descending)   
		    .on('renderlet', function (table) {    		
		    	
		    	table.select('tr.dc-table-group').remove();		    	
		    	table.selectAll('tr.dc-table-row').on('click', function (row) { 
		    		
		    		var system = row.system;
		    		var client = row.client;
		    		var currency = row.currency;
		    		
				graphSvg.selectAll("*").remove();
		    		console.log("selected =" + JSON.stringify(row));
		    		
					var chain = row.chain;					
					if( chain != null || chain.length > 0) {						
						updateItemTraceGraph(chain);
					}		    		
		    	});
    		});		
	  
	}
	
	
	itemTrace.updateOverlayData = itemTrace_updateOverlayData;
	function itemTrace_updateOverlayData(system, client, ccy, row) {
		
		if( itemTraceTabActive && !paused ) {

			debugger;
			if( row == null ) {
				// TODO Maybe remove?
    				state = "green";				
			} else if( row.highRiskValue != 0 ) {
	    			state = "red";
	    		} else if ( row.atRiskValue != 0 ) {
	    			state = "amber";		    			
	    		}
    		
			waitingDialog.show();
			var restUrl = itemTraceApi + "PAYMENT_PROCESSOR/ALL/ALL"
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				waitingDialog.hide();
				itemTrace_resetData();
				filteredData.add(data);
				console.log(JSON.stringify(data));

				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {

					var item = data[0];
					var chain = item.chain;
					
					if( chain != null && chain.length > 0) {
						updateItemTraceGraph(chain);
					}
				}					
				

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}	
				
			});				
		}
	}
	
	
	function updateItemTraceGraph( chain ) {
		
		var i = 0;
		var radius = 40;
		var leftBorder = 40;
		var spacing = 180;
		
		chain.forEach(function(d) { 		
			d.xpos = (i++ * spacing) + radius;
			console.log("Event = " + JSON.stringify(d));					    
		});	
		
		graphSvg.selectAll("circle")
		    .data(chain)
		    .enter().append("circle")
		    .style("stroke", "gray")
		    .style("fill", state)
		    .attr("r", radius)
		    .attr("cx", function(d, i) { return leftBorder + d.xpos } )
		    .attr("cy", 90);
		
		
		var text = graphSvg.append("svg:g").selectAll("g")
	      	.data(chain)
	      	.enter().append("svg:g");

		text.append("svg:text")
	      	.attr("x", function(d, i) { return d.xpos + leftBorder })
	      	.attr("y", 30)
	      	.attr("text-anchor", "middle")
		    .text(function(d) { return d.agentName; });		
		
		text.append("svg:text")
	      	.attr("x", function(d, i) { return d.xpos + leftBorder; })
	      	.attr("y", 160)
	      	.attr("text-anchor", "middle")
	      	.text(function(d) { return d.timestamp; });		
		
	}
	
	if (typeof define === "function" && define.amd)
		define(itemTrace);
	else if (typeof module === "object" && module.exports)
		module.exports = itemTrace;
	
	this.itemTrace = itemTrace;
}();




function itemTraceUpdateData() {
	itemTrace.updateOverlayData();	
	//setTimeout(itemTraceUpdateData, REFRESH_PERIOD); 
};

itemTraceTabActiveListener = function() {
	itemTraceTabActive = true;
	itemTrace.updateOverlayData();
}

itemTrace.initialiseWidgets();
itemTraceUpdateData();



