/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configure the welcome page 
 * 
 */
@Configuration
public class DefaultPageConfig implements WebMvcConfigurer {

    /**
     * Redirect to the dashboard page when the user visits the app without a
     * destination url.
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/Dashboard.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);      
    }
}
