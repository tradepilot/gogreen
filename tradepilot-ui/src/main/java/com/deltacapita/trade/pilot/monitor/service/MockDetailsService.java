/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.service;

import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.ID;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.NAME;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.SOURCE;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.TARGET;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.WEIGHT;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.X;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.Y;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deltacapita.trade.pilot.MonitorConfig;
import com.deltacapita.trade.pilot.Organisation;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphEdge;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphNode;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraphDetails;
import com.deltacapita.trade.pilot.monitor.types.ContainerDetails;
import com.deltacapita.trade.pilot.monitor.types.FileHistory;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndCountryDetails;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndSystemDetails;


@Component
@RestController 
public class MockDetailsService  {   

    private static final Logger logger = LogManager.getLogger(MockDetailsService.class);
    
    private static final String [] SYSTEMS = new String [] { "File Processor", "File Validator", "Funding Request", "Loan Request", "Payment Processor", "Settlement" };

    private static final String [] CURRENCIES = new String [] { "GBP", "USD", "EUR", "JPY", "SGD" };

    private static final String [] NODES = new String [] { "a", "b", "c", "d", "e", "f", "g", "h" };
    
    private static final String [] NODE_NAMES = new String [] { "File Processor", "File Validator", "Funding Request", "Loan Request", "Payment Request", "Settlement", "Funding Request", "Funding Request" };

    private static final int [] X_POS = new int [] { 50, 100, 150, 200, 250, 300, 150, 150 };
    
    private static final int [] Y_POS = new int [] { 100, 100, 100, 100, 100, 100, 50, 150 };
    
    private static final String [] SOURCES = new String [] { "a", "b", "c", "d", "e", "b", "g", "b", "h" };
    
    private static final String [] TARGETS = new String [] { "b", "c", "d", "e", "f", "g", "d", "h", "d" };
    
    private int ip = 46;

    @Autowired
    private MonitorConfig serviceConfig;
    
    private List<Organisation> organisations;
    
    private List<PaymentAndCountryDetails> countryDetails = new ArrayList<>();
    
    private List<PaymentAndSystemDetails> systemDetails = new ArrayList<>();

    
    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising ");      
        organisations = new ArrayList<>(serviceConfig.getOrganisations());        
    }
    
    


    @CrossOrigin
    @RequestMapping(value = "/filedetailstest/{system}/{client}/{currency}", method = RequestMethod.GET, produces = "application/json")
    public List<FileHistory> getFileDetails( String system, String client, String currency) {
        
        logger.info("Getting file details for system={}, client={}, currency={}", system, client, currency);
          
        List<FileHistory> histories = new ArrayList<>();
        
        Random r = new Random();
        int size = organisations.size();        
        
        for (int i = 0; i < 20; i++) {

            int index = r.nextInt(size);
            Organisation buyer = organisations.get(index);

            FileHistory history = new FileHistory();
            history.setFileId("file-" + i);
            history.setBuyerId("B" + buyer.getOrgId());
            history.setBuyerName(buyer.getOrgName());
            history.setCountry(buyer.getCountry());
            
            Long numInstructions = (long) r.nextInt(100) + 1;
            Long outstandingRequests = (long) r.nextInt(numInstructions.intValue());
            history.setNumberOfInstructions(numInstructions);
            history.setOutstandingPaymentRequests(outstandingRequests);
            
            String sys = SYSTEMS[r.nextInt(SYSTEMS.length)];
            history.setLastAgentPoint(sys);          
            history.setTotalValue(1000000.0 * i);
            
            
            histories.add(history);
        }
        
        return histories;
    }
    

    @CrossOrigin
    @RequestMapping(value = "/paymentsbycountrytest/{client}/{currency}/{country}", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentAndCountryDetails> getPaymentsByCountryAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("country") String country) {
                
        List<PaymentAndCountryDetails> results = new ArrayList<>();
        
        Random r = new Random();
        int size = organisations.size();        
        
        for (int i = 0; i < 20; i++) {
            
            int index = r.nextInt(size);
            Organisation organisation = organisations.get(index);
                        
            PaymentAndCountryDetails details = new PaymentAndCountryDetails();
            details.setAmount(1000.0 * i);
            details.setClient(organisation.getOrgName());
            details.setCountry(organisation.getCountry());
            details.setCity(organisation.getCity());
                      
            if( i % 3 == 0 ) {
                details.setComplete(true);
            } else {
                details.setComplete(false);                
            }
            
            index = r.nextInt(CURRENCIES.length);
            details.setCurrency(CURRENCIES[index]);
            results.add(details);            
        }
        
        countryDetails.addAll(results);
        return countryDetails;
    }
    
    @CrossOrigin
    @RequestMapping(value = "/paymentsbysystemtest/{client}/{currency}/{system}", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentAndSystemDetails> getPaymentsBySystemAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("system") String system) {
                
        List<PaymentAndSystemDetails> results = new ArrayList<>();
        
        Random r = new Random();
        int size = organisations.size();   
        
        for (int i = 0; i < 20; i++) {
            
            int index = r.nextInt(size);
            Organisation organisation = organisations.get(index);
                        
            PaymentAndSystemDetails details = new PaymentAndSystemDetails();
            details.setInFlightCount(r.nextInt(100));            
            details.setInFlightValue(1000.0 * i);
            details.setClient(organisation.getOrgName());
                      
            if( i % 3 == 0 ) {
                details.setAtRiskCount(r.nextInt(20));
                details.setAtRiskValue(1200.0 * i);
                details.setHighRiskCount(0);
                details.setHighRiskValue(0);
                details.setRecoveredCount(0);
                details.setRecoveredValue(0.0);
            } else {
                details.setAtRiskCount(0);
                details.setAtRiskValue(0.0);
                details.setHighRiskCount(0);
                details.setHighRiskValue(0.0);
                details.setRecoveredCount(0);
                details.setRecoveredValue(0.0);
            }
            
            index = r.nextInt(CURRENCIES.length);
            details.setCurrency(CURRENCIES[index]);
                        
            String sys = SYSTEMS[r.nextInt(SYSTEMS.length)];
            details.setSystem(sys);
            
            results.add(details);            
        }

        
        systemDetails.addAll(results);
        return systemDetails;
    }

    
    @CrossOrigin
    @RequestMapping(value = "/connectiongraphtest", method = RequestMethod.GET, produces = "application/json")
    public ConnectionGraphDetails getConnectionGraph() {

        logger.info("Getting connection graph");
        List<GraphNode> nodes = new ArrayList<>();
        
        Random r = new Random();
        double rate = 1.0 + r.nextDouble();
        for (int i = 0; i < NODES.length; i++) {
            
            String nodeName = NODE_NAMES[i];
            GraphNode node = new GraphNode();
            node.getData().put(ID, NODES[i]);
            node.getData().put(NAME, nodeName);      
            
            nodeName = nodeName.toLowerCase();
            nodeName = nodeName.replace(" ", "-") + "-" + i;
            node.getProperties().put("Container", nodeName);
            node.getProperties().put("Rate", rate);
            
            node.getPosition().put(X, X_POS[i]);
            node.getPosition().put(Y, Y_POS[i]);
            
            nodes.add(node);
        }
        
        List<GraphEdge> edges = new ArrayList<>();
        for (int i = 0; i < SOURCES.length; i++) {
            
            String source = SOURCES[i];
            String target = TARGETS[i];
            String id = source + target;
            
            GraphEdge edge = new GraphEdge();
            edge.getData().put(ID, id);
            edge.getData().put(SOURCE, source);
            edge.getData().put(TARGET, target);
            edge.getData().put(WEIGHT, 1);
            
            edges.add(edge);
        }
              
        ConnectionGraph elements = new ConnectionGraph();
        elements.setNodes(nodes);
        elements.setEdges(edges);      
        
        ConnectionGraphDetails details = new ConnectionGraphDetails();
        details.setUpdate(true);
        details.setElements(elements);
        
        return details;
    }
        
    @CrossOrigin
    @RequestMapping(value = "/containerdetailstest", method = RequestMethod.GET, produces = "application/json")
    public List<ContainerDetails> getContainerDetails() {
        
        logger.info("Getting container details");
        List<ContainerDetails> details = new ArrayList<>();
        
        String container = "559a74c528e1";
        String host = "10.0.0." + ip++;
        String id = container + host;
        
        ContainerDetails d1 = new ContainerDetails();
        d1.setId(id);
        d1.setContainer(container);
        d1.setHost(host);
        d1.setServiceName("Funding Request Generator");
        d1.setState("Running");
        
        container = "459a74c528e1";
        host = "10.0.0." + ip++;
        id = container + host;
       
        ContainerDetails d2 = new ContainerDetails();
        d2.setId(id);
        d2.setContainer(container);
        d2.setHost(host);
        d2.setServiceName("Loan Generator");
        d2.setState("Failed");
        
        details.add(d1);
        details.add(d2);
        
        return details;
    }
}


