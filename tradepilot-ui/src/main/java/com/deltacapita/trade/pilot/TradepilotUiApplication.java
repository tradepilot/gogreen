/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

import com.deltacapita.trade.pilot.monitor.service.MockDetailsService;

@EnableZuulProxy
@SpringBootApplication
public class TradepilotUiApplication {
    
    @Autowired
    @SuppressWarnings("unused")
    private MockDetailsService service;
    
	public static void main(String[] args) {
		SpringApplication.run(TradepilotUiApplication.class, args);
	}
}
