/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "monitor-service")
public class MonitorConfig {

    private Collection<Organisation> organisations;
    
    private Map<String, Organisation> organisationsMap = new HashMap<String, Organisation>();

    public Map<String, Organisation> getOrganisationsMap() {
        return organisationsMap;
    }

    public Collection<Organisation> getOrganisations() {
        return organisations;
    }

    public void setOrganisations(Collection<Organisation> organisations) {
        
        for (Organisation org : organisations) {
            this.organisationsMap.put(org.getOrgId(), org);
        }

        this.organisations = organisations;
    }

}
