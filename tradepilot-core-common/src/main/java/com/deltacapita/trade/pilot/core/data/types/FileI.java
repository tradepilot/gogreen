/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Represents an intercace to a file object
 * 
 * @author simonw
 *
 */

public interface FileI {

    public String getId();

    public void setId(String id);

    public String getRequestId();

    public void setRequestId(String requestId);

    public String getBuyerId();

    public void setBuyerId(String buyerId);

    public LocalDateTime getReceivedDateTime();

    public void setReceivedDateTime(LocalDateTime receivedTime);

    public Long getNumberOfInstructions();

    public void setNumberOfInstructions(Long numberOfInstructions);

    public Double getTotalValue();

    public void setTotalValue(Double totalValue);
    
    public String getState();

    public void setState(String state);

    public LocalDate getMaturityDate();

    public void setMaturityDate(LocalDate maturityDate);    

    public Long getOutstandingPaymentRequests();

    public void setOutstandingPaymentRequests(Long outstandingPaymentRequests);
    
}
