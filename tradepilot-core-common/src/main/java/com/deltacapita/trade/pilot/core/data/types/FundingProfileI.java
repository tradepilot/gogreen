/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

/**
 * Represents an interface to a profile that defines the terms by which invoices
 * from a supplier to a buyer are funded before their due date
 * 
 * @author simonw
 *
 */
public interface FundingProfileI {

    public String getId();

    public void setId(String id);

    public String getBuyerId();

    public void setBuyerId(String buyerId);

    public String getSupplierId();

    public void setSupplierId(String supplierId);

    public String getCurrency();

    public void setCurrency(String currency);

    public Long getMaxTenor();

    public void setMaxTenor(Long maxTenor);

    public Long getMaxValue();

    public void setMaxValue(Long maxValue);

    public Long getMaxInvoiceValue();

    public void setMaxInvoiceValue(Long maxInvoiceValue);

    public Double getRate();

    public void setRate(Double rate);
}
