/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.messages;

public interface ProcessingRequestI {

    String getRequestId();

    String getSource();

    String getDestination();

    String getDestinationTopic();

    String getFileId();
    
    String getRoutingId();

    Long getTimestamp();
}
