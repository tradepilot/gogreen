/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;


/**
 * Represents an interface to a profile that defines the terms for the loan to
 * a buyer from the bank to fund early paid invoices
 * 
 * @author simonw
 *
 */
public interface LoanProfileI {

    public String getId();

    public void setId(String id);

    public String getBuyerId();

    public void setBuyerId(String buyerId);

    public String getCurrency();

    public void setCurrency(String currency);

    public Long getMaxTenor();

    public void setMaxTenor(Long maxTenor);

    public Long getMaxValue();

    public void setMaxValue(Long maxValue);

    public Long getMaxInvoiceValue();

    public void setMaxInvoiceValue(Long maxInvoiceValue);

    public Double getRate();

    public void setRate(Double rate);
    
    public String getPaymentFrequency();

    public void setPaymentFrequency(String paymentFrequency);

    public String getDaycountConvention();

    public void setDaycountConvention(String daycountConvention);
}
