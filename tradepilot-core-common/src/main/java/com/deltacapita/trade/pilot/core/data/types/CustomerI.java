/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

/**
 * Represents an interface to a buyer or supplier
 * 
 * @author simonw
 *
 */
public interface CustomerI {

    public String getId();

    public void setId(String id);

    public String getType();
    
    public void setType(String type) ;

    public String getName() ;

    public void setName(String name);

    public String getAddressLine1();

    public void setAddressLine1(String addressLine1);

    public String getAddressLine2();

    public void setAddressLine2(String addressLine2);

    public String getAddressLine3();

    public void setAddressLine3(String addressLine3);

    public String getCity();

    public void setCity(String city);

    public String getState();

    public void setState(String state);

    public String getPostCode();

    public void setPostCode(String postCode);

    public String getCountry();

    public void setCountry(String country);

    public String getEmail();

    public void setEmail(String email);

    public String getTelephoneNo();

    public void setTelephoneNo(String telephoneNo);

    public String getAccountNumber();

    public void setAccountNumber(String accountNumber);
 }
