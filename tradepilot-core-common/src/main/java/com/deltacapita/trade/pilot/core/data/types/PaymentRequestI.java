/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;


public interface PaymentRequestI {

    public String getId();

    public void setId(String id);

    public String getBuyerId();

    public void setBuyerId(String buyerId);

    public String getSupplierId();

    public void setSupplierId(String supplierId);

    public String getCurrency();

    public void setCurrency(String currency);

    public LocalDate getDueDate();

    public void setDueDate(LocalDate dueDate);

    public Double getAmount();

    public void setAmount(Double amount);

    public String getState();

    public void setState(String state);
    
    public String getInstructionId();

    public void setInstructionId(String instructionId);

    public String getPaymentReference();

    public void setPaymentReference(String paymentReference);

    public String getFileId();

    public void setFileId(String fileId);
    
}
