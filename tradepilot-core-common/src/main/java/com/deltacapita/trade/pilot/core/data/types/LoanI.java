/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;


public interface LoanI {

    public String getId();

    public void setId(String id);

    public String getBuyerId();

    public void setBuyerId(String buyerId);

    public String getCurrency();

    public void setCurrency(String currency);

    public LocalDate getMaturityDate();

    public void setMaturityDate(LocalDate maturityDate);

    public Double getAmount();

    public void setAmount(Double amount);

    public String getState();

    public void setState(String state);
    
    public String getFileId();

    public void setFileId(String fileId);
    
    public Double getRate();

    public void setRate(Double rate);

    public String getPaymentFrequency();

    public void setPaymentFrequency(String paymentFrequency);

    public String getDaycountConvention();

    public void setDaycountConvention(String daycountConvention);
}
