/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;
import java.util.Map;


/**
 * Represents the interface to an instruction object
 * 
 * @author simonw
 *
 */
public interface InstructionI {

	public String getId(); 

	public void setId(String id);

	public String getFileId();
	
	public void setFileId(String fileId);

	public String getType();

	public void setType(String type);

	public String getBprn();
	
	public void setBprn(String bprn);

	public String getBuyerId() ;

	public void setBuyerId(String buyerId);

	public String getSupplierId();

	public void setSupplierId(String supplierId);

	public String getSupplierName();

	public void setSupplierName(String supplierName);

	public String getCurrency();

	public void setCurrency(String currency);

	public String getInvoiceNumber();

	public void setInvoiceNumber(String invoiceNumber);

	public LocalDate getDueDate();

	public void setDueDate(LocalDate dueDate);

	public Double getAmount();

	public void setAmount(Double amount);

    public String getState();

    public void setState(String state);

    public Map<String, Object> getProperties();

	public void setProperties(Map<String, Object> properties);
}
