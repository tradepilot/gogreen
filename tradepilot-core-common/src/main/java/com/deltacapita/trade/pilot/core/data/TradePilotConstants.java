/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data;

public class TradePilotConstants {

    public static final String INVOICE = "INVOICE";
    
    public static final String CREDIT_NOTE = "CREDIT-NOTE";
 
    public static final String DEFAULT = "DEFAULT";
    
    public static final String SUPPLIER = "SUPPLIER";

    public static final String BUYER = "BUYER";
    
    public static final String NEW = "NEW";

    public static final String REJECTED = "REJECTED";

    public static final String PROCESSING = "PROCESSING";

    public static final String COMPLETE = "COMPLETE";   

    public static final String FUNDING_REQUESTED = "FUNDING_REQUESTED";   

    public static final String LOAN_CREATED = "LOAN_CREATED";   

    public static final String PAYMENTS_PROCESSING = "PAYMENTS_PROCESSING";   

    public static final String PAYMENTS_EXECUTING = "PAYMENTS_EXECUTING";   

    public static final String PAYMENT_EXECUTED = "PAYMENT_EXECUTED";   

    public static final String TOPIC_END_POINT = "END_POINT";   

    public static final String DISABLED = "DISABLED";   

    public static final String UTC = "UTC";

    public static final String ACCOUNT_BASE_CURRENCY = "GBP";
    
    public static final String DC_30_360 = "30_360";

    public static final String DC_30_365 = "30_365";

    public static final String DC_ACT_360 = "ACT_360";

    public static final String DC_ACT_365 = "ACT_365";

    public static final String DC_ACT_ACT = "ACT_ACT";
    
    public static final String FREQ_MONTHLY = "1M";
    
    public static final String FREQ_QUARTERLY = "3M";

    public static final String FREQ_BIANNUAL = "6M";
    
    public static final String FREQ_YEARLY = "12M";
}
