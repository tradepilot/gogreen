/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deltacapita.trade.pilot.core.data.types.LoanProfileI;

public interface LoanServiceI {

    @RequestMapping(value = "/loan/{buyerId}/{currency}", method = RequestMethod.GET, produces = "application/json")
    LoanProfileI getLoanProfile(@PathVariable("buyerId") String buyerId, @PathVariable("currency") String currency);
}
