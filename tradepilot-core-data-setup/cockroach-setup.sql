DROP DATABASE IF EXISTS TRADEPILOT;
CREATE DATABASE TRADEPILOT;
SET DATABASE = TRADEPILOT;
GRANT ALL ON DATABASE TRADEPILOT to TRADEPILOT;

DROP TABLE IF EXISTS tradepilot.customer;
CREATE TABLE tradepilot.customer( 
	id STRING(120) PRIMARY KEY, 
	type STRING(20), 
	name STRING(120), 
	addressline1 STRING(120), 
	addressline2 STRING(120), 
	addressline3 STRING(120), 
	city STRING(50), 
	state STRING(20), 
	postcode STRING(20), 
	country STRING(50), 
	email STRING(50), 
	telephone STRING(20), 
	accountnumber STRING(30) 
);
CREATE INDEX ON tradepilot.customer (type);
CREATE INDEX ON tradepilot.customer (name);

DROP TABLE IF EXISTS tradepilot.file;
CREATE TABLE tradepilot.file( 
	id STRING(120) PRIMARY KEY, 
	requestid STRING(120), 
	buyerid STRING(120), 
	state STRING(30), 
	receiveddatetime TIMESTAMP, 
	maturitydate DATE, 
	numberofinstructions INT, 
	outstandingpaymentrequests INT, 
	totalvalue INT 
);
CREATE INDEX ON tradepilot.file (buyerid);
CREATE INDEX ON tradepilot.file (requestid);


DROP TABLE IF EXISTS tradepilot.fundingprofile;
CREATE TABLE tradepilot.fundingprofile( 
	id STRING(120) PRIMARY KEY, 
	buyerId STRING(120), 
	supplierId STRING(120), 
	currency STRING(10), 
	maxTenor INT, 
	maxInvoiceValue INT, 
	maxTotalValue INT, 
	rate DECIMAL(4,4) 
);
CREATE INDEX ON tradepilot.fundingprofile (buyerid, supplierId, currency);


DROP TABLE IF EXISTS tradepilot.instruction;
CREATE TABLE tradepilot.instruction( 
	id STRING(120) PRIMARY KEY, 
	fileid STRING(120), 
	type STRING(30), 
	bprn STRING(30), 
	buyerid STRING(120),
	supplierid STRING(120),
	suppliername STRING(120),
	currency STRING(10), 
	invoicenumber STRING(120),  
	duedate DATE, 
	amount DECIMAL(12,4),
	state STRING(30)
);
CREATE INDEX ON tradepilot.instruction (buyerid, type);
CREATE INDEX ON tradepilot.instruction (buyerid, fileid);

DROP TABLE IF EXISTS tradepilot.loanprofile;
CREATE TABLE tradepilot.loanprofile( 
	id STRING(120) PRIMARY KEY, 
	buyerId STRING(120), 
	currency STRING(10), 
	maxTenor INT, 
	maxInvoiceValue INT, 
	maxTotalValue INT, 
	rate DECIMAL(12,4), 
	paymentfrequency STRING(10), 
	daycountconvention STRING(10) 
);
CREATE INDEX ON tradepilot.loanprofile (buyerid, currency);

DROP TABLE IF EXISTS tradepilot.loan;
CREATE TABLE tradepilot.loan( 
	id STRING(120) PRIMARY KEY, 
	fileid STRING(120), 
	buyerid STRING(120),
	currency STRING(10), 
	maturitydate DATE, 
	amount DECIMAL(12,4),
	rate DECIMAL(12,4),
	paymentfrequency STRING(10), 
	daycountconvention STRING(10), 
	state STRING(30)
);
CREATE INDEX ON tradepilot.loan (state);
CREATE INDEX ON tradepilot.loan (maturitydate, state);
CREATE INDEX ON tradepilot.loan (fileid, maturitydate, state);



DROP TABLE IF EXISTS tradepilot.paymentrequest;
CREATE TABLE tradepilot.paymentrequest( 
	id STRING(120) PRIMARY KEY, 
	fileid STRING(120), 
	buyerid STRING(120),
	instructionid STRING(120),
	paymentreference STRING(120),
	supplierid STRING(120),
	currency STRING(10), 
	duedate DATE, 
	amount DECIMAL(12,4),
	state STRING(30)
);
CREATE INDEX ON tradepilot.paymentrequest (state);
CREATE INDEX ON tradepilot.paymentrequest (duedate, state);
CREATE INDEX ON tradepilot.paymentrequest (fileid, duedate, state);



