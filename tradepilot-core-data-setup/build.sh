#!/bin/sh

#docker stop tp-core-data-setup-cockroach
docker rm tp-core-data-setup-cockroach
mvn clean package -Dmaven.test.skip=true -Ddatabase.type-cockroach -U

docker build -t tp-core-data-setup-cockroach --build-arg SPRING_PROFILE=azure-k8s -f Dockerfile .

docker tag tp-core-data-setup-cockroach dcdockerregistry.azurecr.io/tp-core-data-setup-cockroach:v1
docker push dcdockerregistry.azurecr.io/tp-core-data-setup-cockroach:v1

#docker run --name tp-core-data-setup-cockroach --hostname tp-core-data-setup-cockroach -p 1400:1400 -p 9400:9400 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-core-data-setup-cockroach


