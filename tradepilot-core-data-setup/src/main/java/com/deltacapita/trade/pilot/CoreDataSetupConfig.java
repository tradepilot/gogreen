/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "core-data")
public class CoreDataSetupConfig {
    
    private List<String> currencies;
    
    private List<CustomerDataConfig> customerConfigs;

    public List<CustomerDataConfig> getCustomerConfigs() {
        return customerConfigs;
    }

    public void setCustomerConfigs(List<CustomerDataConfig> customerConfigs) {
        this.customerConfigs = customerConfigs;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

}
