
DROP TABLE IF EXISTS tradepilot.loan;
CREATE TABLE tradepilot.loan( 
	id STRING(100) PRIMARY KEY, 
	fileid STRING(100), 
	buyerid STRING(100),
	currency STRING(10), 
	maturitydate DATE, 
	amount DECIMAL(12,4),
	rate DECIMAL(12,4),
	paymentfrequency STRING(10), 
	daycountconvention STRING(10), 
	state STRING(30)
);
CREATE INDEX ON tradepilot.loan (state);
CREATE INDEX ON tradepilot.loan (maturitydate, state);
CREATE INDEX ON tradepilot.loan (fileid, maturitydate, state);
