#!/bin/sh

#docker stop tp-service-registry
docker rm tp-service-registry
mvn clean package -Dmaven.test.skip=true 

docker build -t tp-service-registry --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-service-registry dcdockerregistry.azurecr.io/tp-service-registry:v2
#docker push dcdockerregistry.azurecr.io/tp-service-registry:v2

docker run --name tp-service-registry --hostname tp-service-registry -p 1110:1110 -p 9110:9110 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-service-registry

