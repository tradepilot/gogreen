package com.deltacapita.trade.pilot.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class TradepilotServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradepilotServiceRegistryApplication.class, args);
	}
}
