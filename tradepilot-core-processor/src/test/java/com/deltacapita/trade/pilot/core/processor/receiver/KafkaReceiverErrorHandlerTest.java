package com.deltacapita.trade.pilot.core.processor.receiver;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ActiveProfiles(profiles = "test")
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@TestPropertySource(properties = { "spring.config.location = classpath:application-test-new.yml" })
@ComponentScan(basePackages = { "com.deltacapita.trade.pilot.core.processor", "com.deltacapita.trade.pilot.core.processor.receiver", "com.deltacapita.trade.pilot.core.processor.sender" })
public class KafkaReceiverErrorHandlerTest {
    
    private static final Logger logger = LoggerFactory.getLogger(KafkaReceiverErrorHandlerTest.class);

    private static final String INPUT_TOPIC = "file-processor-input";

    private static final String OUTPUT_TOPIC = "file-processor-output";

    @ClassRule
    public static EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true, INPUT_TOPIC, OUTPUT_TOPIC);

    @Autowired
    private KafkaTemplate<String, ProcessingRequest> kafkaTemplate;

    @Autowired
    private KafkaReceiver receiver;
    
    @Autowired
    private TestSender sender;
    
    @Autowired
    private TestTaskFactory taskFactory;

    @Before
    public void setUp() throws Exception {        
    }

    @After
    public void tearDown() throws Exception {
    }

    //@Test
    public void testInitialise() {
        assertNotNull(receiver);        
    }

    @Test
    @Ignore
    public void testReceive() throws Exception {
        
        taskFactory.setFailureEvery(2);
        taskFactory.setSuccess(false);

        assertNotNull(receiver);
        
        ProcessingRequest message = new ProcessingRequest();
        message.setRequestId("request-1");
        message.setFileId("file-1");
        message.setSource("test-source");
        message.setDestination("test-destination");

        
        logger.info("***** Sending message 1");
        CountDownLatch latch = new CountDownLatch(1);
        receiver.receive(message , new Acknowledgment() {
            
            @Override
            public void acknowledge() {
                logger.info("----- Acknowledged message 1");
                latch.countDown();
            }
            
            @Override
            public String toString() {
                return "Test Acknowledgment for " + message;
            }
            
        });
        
        boolean signalled = latch.await(10, TimeUnit.SECONDS);
        assertTrue("ACK Sent", signalled);
        logger.info("***** ACK signalled for message 1");
        
        CountDownLatch senderLatch = sender.getLatch();
        signalled = senderLatch.await(10, TimeUnit.SECONDS);
        assertTrue("Message Sent", signalled);
        logger.info("***** Onward message sent for message 1");
        

        
        logger.info("***** Sending message 2");
        ProcessingRequest message2 = new ProcessingRequest();
        message2.setRequestId("request-2");
        message2.setFileId("file-2");
        message2.setSource("test-source");
        message2.setDestination("test-destination");

        CountDownLatch senderLatch2 = new CountDownLatch(1);
        sender.setLatch(senderLatch2);		

        CountDownLatch latch2 = new CountDownLatch(1);
        receiver.receive(message2 , new Acknowledgment() {
            
            @Override
            public void acknowledge() {
                logger.info("----- Acknowledging message 2");
                latch2.countDown();
                logger.info("----- Acknowledged message 2");
            }
            
            @Override
            public String toString() {
                return "Test Acknowledgment for " + message;
            }
            
        });
        
        boolean signalled2 = latch2.await(10, TimeUnit.SECONDS);
        assertTrue("ACK Sent", signalled2);
        logger.info("***** ACK signalled for message 2");

        
        signalled2 = senderLatch2.await(10, TimeUnit.SECONDS);
        assertTrue("Message Sent", signalled2);
        logger.info("***** Onward message sent for message 2");
               
    }
    
    @Test
    public void testSendViaQueue() {
    	
    	
        ProcessingRequest message = new ProcessingRequest();
        message.setRequestId("request-1");
        message.setFileId("file-1");
        message.setSource("test-source");
        message.setDestination("test-destination");

        
        waitForKafka();        
        logger.info("***** Sending message 1");
        kafkaTemplate.send(INPUT_TOPIC, message);
        
        try {
        	            
            CountDownLatch senderLatch = sender.getLatch();
            boolean signalled = senderLatch.await(10, TimeUnit.SECONDS);
            assertTrue("Message Sent", signalled);
            logger.info("***** Onward message sent for message 1");

		} catch (Exception e) {
			logger.error("Caught ", e);
		}
        
    }


    @Test
    public void testSendViaQueueWithException() {
    	
        taskFactory.setFailureEvery(2);
        taskFactory.setSuccess(false);
        waitForKafka();        

    	
        ProcessingRequest message = new ProcessingRequest();
        message.setRequestId("request-1");
        message.setFileId("file-1");
        message.setSource("test-source");
        message.setDestination("test-destination");
        
        logger.info("***** Sending message 1");
        kafkaTemplate.send(INPUT_TOPIC, message);
        
        try {
        	            
            CountDownLatch senderLatch = sender.getLatch();
            boolean signalled = senderLatch.await(10, TimeUnit.SECONDS);
            assertTrue("Message Sent", signalled);
            logger.info("***** Onward message sent for message 1");

		} catch (Exception e) {
			logger.error("Caught ", e);
		}

        
        
        ProcessingRequest message2 = new ProcessingRequest();
        message2.setRequestId("request-2");
        message2.setFileId("file-2");
        message2.setSource("test-source");
        message2.setDestination("test-destination");

      
        logger.info("***** Sending message 2");
        kafkaTemplate.send(INPUT_TOPIC, message2);
        
        try {
        	    
        	CountDownLatch senderLatch = new CountDownLatch(1);
        	sender.setLatch(senderLatch);        
            boolean signalled = senderLatch.await(10, TimeUnit.SECONDS);
            assertTrue("Message Sent", signalled);
            logger.info("***** Onward message sent for message 2");

		} catch (Exception e) {
			logger.error("Caught ", e);
		}

    }

    
    private void waitForKafka() {
		try {
        	logger.info("Waiting for Kafka to bootstrap...");
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// Ignore
		}
	}
    
    
}


