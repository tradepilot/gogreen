package com.deltacapita.trade.pilot.core.processor.receiver;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.processor.sender.SenderI;

@Component
@Profile("test")
public class TestSender implements SenderI<ProcessingRequest> {

    private static final Logger logger = LoggerFactory.getLogger(TestSender.class);
    
    private CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void send(ProcessingRequest message) {
        logger.info("Sending test message {}", message);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

}
