package com.deltacapita.trade.pilot.core.processor.receiver;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
public class TestTaskFactory implements TaskFactoryI {

    private static final Logger logger = LoggerFactory.getLogger(TestTaskFactory.class);

    private String destination = "dest";
    
    private Long delay = 100L;
    
    private boolean success = true;
    
    private int failureEvery = 2;
    
    private AtomicInteger count = new AtomicInteger(1);

    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
    	
    	if( success ) {
    		
			logger.warn("Creating task with success=true as success is true" );
            return new TestProcessorTask(request, destination, delay, true);

    	} else {
    		
    		if( count.getAndIncrement() % failureEvery == 0) {
    			
    			logger.warn("Creating task with success=false" );
                return new TestProcessorTask(request, destination, delay, false);

    		} else {
    			
    			logger.warn("Creating task with success=true" );
                return new TestProcessorTask(request, destination, delay, true);
    		}
    		
    	}
    	
    }

	public void setSuccess(boolean success) {
		this.success = success;
		logger.info("Set success=" + success);
	}


	public void setFailureEvery(int failureEvery) {
		this.failureEvery = failureEvery;
	}

}
