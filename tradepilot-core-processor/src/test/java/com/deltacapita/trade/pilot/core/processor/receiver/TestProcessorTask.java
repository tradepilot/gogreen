package com.deltacapita.trade.pilot.core.processor.receiver;

import java.util.UUID;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;

public class TestProcessorTask implements Callable<ProcessingRequest> { 

    private static final Logger logger = LoggerFactory.getLogger(TestProcessorTask.class);

    private final ProcessingRequest request;
    
    private final String destination;
    
    private final Long delay;
    
    private final boolean success;
    
    public TestProcessorTask(ProcessingRequest request, String destination, Long delay, boolean success) {
        this.request = request;
        this.destination = destination;
        this.delay = delay;
        this.success = success;
    }

    
    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest();
        response.setRequestId(UUID.randomUUID().toString());
        response.setFileId(request.getFileId());
        response.setSource(request.getDestination());
        response.setDestination(destination);
        
        logger.info("Running test task for {}", request);
        if( success ) {
            
            Thread.sleep(delay);
            logger.info("Returning response {}", response);
            return response;
        
        } else {
            logger.info("Throwing exception for {}", request);
            throw new Exception(request.toString());
        }
    }

}
