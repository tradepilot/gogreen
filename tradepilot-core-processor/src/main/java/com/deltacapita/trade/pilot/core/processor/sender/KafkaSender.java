/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.core.processor.sender;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.TOPIC_END_POINT;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;


@Component
@Profile("!test")
public final class KafkaSender implements SenderI<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(KafkaSender.class);

    @Autowired
    private KafkaTemplate<String, ProcessingRequest> kafkaTemplate;

    @Value("${kafka.topic.output}")
    private String topic;

    @PostConstruct
    public void initialise() throws Exception {

        if( !topic.equals(TOPIC_END_POINT) ) {
            logger.info("Initialised with topic {}", topic);
        } else {
            logger.info("Initialised as an end-point");            
        }
    }

    @Override
    public void send(ProcessingRequest message) {

        try {

            String destinationTopic = message.getDestinationTopic();
            if( destinationTopic != null ) {
                
                logger.info("Routing message to topic {} {} ", destinationTopic,  message);
                kafkaTemplate.send(destinationTopic, message.getRequestId(), message);

            } else {
                
                if( !topic.equals(TOPIC_END_POINT) ) {

                    logger.info("Sending message to topic {} {} ", topic,  message);
                    kafkaTemplate.send(topic, message.getRequestId(), message);
                    
                } else {
                    
                    logger.info("Message Sink consuming message {} ",  message);
                }
            }

        } catch (Exception e) {
            logger.warn("Error sending message " + message, e);
        }
    }
}
