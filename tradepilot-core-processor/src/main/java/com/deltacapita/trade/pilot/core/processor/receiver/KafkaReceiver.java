/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.receiver;

import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerAwareListenerErrorHandler;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.processor.sender.SenderI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
@ComponentScan(basePackages = { "com.deltacapita.trade.pilot.core.data", "com.deltacapita.trade.pilot.core.data.repo", "com.deltacapita.trade.pilot.core.data.types" })
public class KafkaReceiver {

    private static final Logger logger = LoggerFactory.getLogger(KafkaReceiver.class);

    @Value("${processor.name}")
    private String processorName;
    
    @Autowired
    private SenderI<ProcessingRequest> sender;

    @Autowired
    private TaskFactoryI taskFactory;


    @PostConstruct
    public void initialise() throws Exception {

    }

    @KafkaListener(topics = "${kafka.topic.input}", errorHandler="listenerRetryErrorHandler")
    public void receive(ProcessingRequest message, Acknowledgment ack) throws Exception {

        logger.info("Received message '{}'", message);
        Callable<ProcessingRequest> task = taskFactory.newTask(message);
        ProcessingRequest result = task.call();
        
        logger.info("Completed processing request {}", message);
        ack.acknowledge();            
        logger.info("Sent ack='{}'", ack.toString());
        
        
        // Send the result which the task will have provided to us
        //
        sender.send(result);
    }
    
    @Bean
    public ConsumerAwareListenerErrorHandler listenerRetryErrorHandler() {
    	
        return (m, e, c) -> {
            
        	logger.warn("Handling error m={}, e={}, c={}", m, e, c);
            MessageHeaders headers = m.getHeaders();
            
            c.seek(new org.apache.kafka.common.TopicPartition(
                    headers.get(KafkaHeaders.RECEIVED_TOPIC, String.class),
                    headers.get(KafkaHeaders.RECEIVED_PARTITION_ID, Integer.class)),
                    headers.get(KafkaHeaders.OFFSET, Long.class));
            
            return null;
        };
    }


    @Override
    public String toString() {
        return "KafkaReceiver [" 
                + "processorName=" + processorName 
                + "]";
    }
}


