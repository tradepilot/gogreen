/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.task;

import java.util.concurrent.Callable;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;

public abstract class AbstractProcessingTask implements Callable<ProcessingRequest> {

    protected final ProcessingRequest request;
    
    protected final String destination;   
    
    protected FileRepository fileRepository;

    protected InstructionRepository instructionRepository;
    
    protected MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory;
    
    public AbstractProcessingTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory ) {
        this.request = request;
        this.destination = destination;
        this.fileRepository = fileRepository;
        this.instructionRepository = instructionRepository;
        this.monitoringAgentFactory = monitoringAgentFactory;
    }

}
