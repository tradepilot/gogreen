package com.deltacapita.trade.pilot.core.processor.task;

import java.util.concurrent.Callable;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;

public interface TaskFactoryI {

    Callable<ProcessingRequest> newTask(ProcessingRequest request);
}
