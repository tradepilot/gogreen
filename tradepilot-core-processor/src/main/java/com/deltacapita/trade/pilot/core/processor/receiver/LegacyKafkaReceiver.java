/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.receiver;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.processor.sender.SenderI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
@Profile("legacy-receiver")
@ComponentScan(basePackages = { "com.deltacapita.trade.pilot.core.data", "com.deltacapita.trade.pilot.core.data.repo", "com.deltacapita.trade.pilot.core.data.types" })
public class LegacyKafkaReceiver {

    private static final Logger logger = LoggerFactory.getLogger(LegacyKafkaReceiver.class);

    private ExecutorService executor;

    private AsyncListenableTaskExecutor taskExecutor;

    private int threadNo = 0;

    @Value("${processor.threads}")
    private Integer numThreads;

    @Value("${processor.name}")
    private String processorName;
    
    @Autowired
    private SenderI<ProcessingRequest> sender;

    @Autowired
    private TaskFactoryI taskFactory;

    private static class RequestCallback implements ListenableFutureCallback<ProcessingRequest> {
        
        public RequestCallback(SenderI<ProcessingRequest> sender, ProcessingRequest request, Acknowledgment ack) {
            this.sender = sender;
            this.request = request;
            this.ack = ack;
        }

        private final SenderI<ProcessingRequest> sender;

        private final ProcessingRequest request;
        
        private final Acknowledgment ack;
        
        
        @Override
        public void onFailure(Throwable ex) {
            
            logger.error("Failed to process request {} {}", request, ex);
        }

        @Override
        public void onSuccess(ProcessingRequest result) {
            
            // Only ack the input message of the task succeeds
            //
            logger.debug("Completed processing request {}", request);
            ack.acknowledge();            
            logger.debug("Sent ack='{}'", ack.toString());
            
            // Send the result which the task will have provided to us
            //
            sender.send(result);
        }
    }

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising " + processorName + " with " + numThreads + " threads");
        executor = Executors.newFixedThreadPool(numThreads, new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {

                return new Thread(r, processorName + "-" + threadNo++);
            }
        });

        taskExecutor = new ConcurrentTaskExecutor(executor);
    }

    @KafkaListener(topics = "${kafka.topic.input}")
    public void receive(ProcessingRequest message, Acknowledgment ack) {

        logger.debug("Received message '{}'", message);

        Callable<ProcessingRequest> task = taskFactory.newTask(message);
        ListenableFuture<ProcessingRequest> result = taskExecutor.submitListenable(task);
        result.addCallback(new RequestCallback(sender, message, ack));
    }

    @Override
    public String toString() {
        return "KafkaReceiver [" 
                + "processorName=" + processorName 
                + ", numThreads=" + numThreads 
                + "]";
    }
}


