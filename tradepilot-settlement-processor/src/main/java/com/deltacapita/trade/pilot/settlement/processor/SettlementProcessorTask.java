/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.settlement.processor;

import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;

public class SettlementProcessorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(SettlementProcessorTask.class); 
    
    private String region;

    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;

    public SettlementProcessorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory, String region ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        this.region = region;
        
        String agentId = MonitoringAgentConstants.SETTLEMENT_PROCESSOR + region.toUpperCase();
        fileInAgent = monitoringAgentFactory.getAgent(agentId);

    }

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);   
        
        String fileId = request.getFileId();         
        Optional<File> result = fileRepository.findById(fileId);
        
        if( result.isPresent() ) {
            
            File file = result.get();   
            logger.info("Injecting {} to {}", response, fileInAgent);        
            fileInAgent.inject(response, file);

        }
        
        logger.info("Running request in region {} for file {}", region, request.getFileId());    
        return response;
    }
 
}
