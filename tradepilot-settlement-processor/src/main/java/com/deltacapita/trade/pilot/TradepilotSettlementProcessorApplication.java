package com.deltacapita.trade.pilot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradepilotSettlementProcessorApplication {

	public static void main(String[] args) {
	    
	    try {
	        
	        SpringApplication.run(TradepilotSettlementProcessorApplication.class, args);
            
        } catch (Exception ex) {
            
            ex.printStackTrace();
        }
	}
}
