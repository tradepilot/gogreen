#!/bin/sh

REGION=$1
SERVICE_PORT=$2
JMX_PORT=$3
PROFILE=$4
VERSION=$5
DATABASE=$6

if [ "$DATABASE" == "couchbase" ]; then
	export SERVICE=tp-settlement-processor-$REGION
else
	export SERVICE=tp-settlement-processor-$DATABASE-$REGION
fi

docker rm ${SERVICE}
mvn clean package -Dmaven.test.skip=true -Ddatabase.type=$DATABASE

docker build -t ${SERVICE} --build-arg SPRING_PROFILE=${PROFILE} --build-arg SERVICE_PORT=${SERVICE_PORT} --build-arg JMX_PORT=${JMX_PORT} --build-arg REGION=${REGION} -f Dockerfile .
docker tag ${SERVICE} dcdockerregistry.azurecr.io/${SERVICE}:v${VERSION}
docker push dcdockerregistry.azurecr.io/${SERVICE}:v${VERSION}

