/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.routing.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.Organisation;
import com.deltacapita.trade.pilot.RoutingConfiguration;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
public class RoutingProcessorTaskFactory implements TaskFactoryI {

    private static final Logger logger = LogManager.getLogger(RoutingProcessorTaskFactory.class);

    @Value("${processor.destination}")
    private String destination; 

    @Autowired
    private RoutingConfiguration config; 

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;
    
    
    private Map<String,String> countryMappings = new HashMap<>();


    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised with destination={}, mappings={}",  destination, config.getRoutings());
        logger.info("Initialised with organistation mappings={}", config.getOrganisationsMap());
        
        Map<String, Organisation> organisationsMap = config.getOrganisationsMap();
        for (Entry<String, Organisation> entry : organisationsMap.entrySet()) {
            
            String orgId = entry.getKey();
            Organisation org = entry.getValue();            
            countryMappings.put("B" + orgId, org.getCountry());
        }
        
        for (Entry<String, String> entry : countryMappings.entrySet()) {
            logger.info("Mapping buyer {} to country {}", entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
        
        logger.info("Creating new task for " + request);
        return new RoutingProcessorTask(request, destination, fileRepository, instructionRepository, agentFactory, config.getRoutings(), countryMappings);
    }
}
