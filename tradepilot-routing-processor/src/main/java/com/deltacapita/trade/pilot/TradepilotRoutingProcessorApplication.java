package com.deltacapita.trade.pilot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradepilotRoutingProcessorApplication {

	public static void main(String[] args) {
	    
	    try {
	        
	        SpringApplication.run(TradepilotRoutingProcessorApplication.class, args);
            
        } catch (Exception ex) {
            
            ex.printStackTrace();
        }
	}
}
