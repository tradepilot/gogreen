/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "routing-service")
public class RoutingConfiguration  {

    private static final Logger logger = LogManager.getLogger(RoutingConfiguration.class);
    
    private List<String> routingList;

    private Map<String,String> routings = new HashMap<>();

    private Collection<Organisation> organisations;
    
    private Map<String, Organisation> organisationsMap = new HashMap<String, Organisation>();

    
    public Map<String, Organisation> getOrganisationsMap() {
        return organisationsMap;
    }

    public Collection<Organisation> getOrganisations() {
        return organisations;
    }

    public void setOrganisations(Collection<Organisation> organisations) {
        
        for (Organisation org : organisations) {
            this.organisationsMap.put(org.getOrgId(), org);
        }

        this.organisations = organisations;
    }
    
    public List<String> getRoutingList() {
        return routingList;
    }

    public void setRoutingList(List<String> routingList) {
        this.routingList = routingList;
        
        if( routingList != null ) {
            for (String routing : routingList) {
                
                String[] parts = routing.split(":");
                if( parts != null && parts.length == 2) {
                    
                    String key = parts[0];
                    String topic = parts[1];
                    routings.put(key, topic);
                    
                    logger.info("Mapping country {} to topic {}", key, topic);
                }
            }
        }
    }

    public Map<String,String> getRoutings() {
        return routings;
    }

}
