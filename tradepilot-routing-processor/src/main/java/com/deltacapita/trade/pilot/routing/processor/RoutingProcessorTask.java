/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.routing.processor;

import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;

public class RoutingProcessorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(RoutingProcessorTask.class); 
    
    private Map<String,String> topicMappings;
    
    private Map<String,String> countryMappings;

    public RoutingProcessorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory, Map<String,String> topicMappings, Map<String,String> countryMappings ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        this.topicMappings = topicMappings;
        this.countryMappings = countryMappings;
    }

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);
        
        String routingId = request.getRoutingId();   
        String country = countryMappings.get(routingId);
        String destinationTopic = topicMappings.get(country);
        
        if( destinationTopic != null ) {
            
            logger.info("Mapped routing ID {} to topic {} for file {}", routingId, destinationTopic, request.getFileId());
            response.setDestinationTopic(destinationTopic);
            
        } else {
            
            logger.info("No mapping for routing ID {} for file {} - using default topic", routingId, request.getFileId());      
            
        }
        
        logger.info("Returning {}", response);        
        return response;
    }
 
}
