#!/bin/sh -x

#docker stop tp-routing-processor
docker rm tp-routing-processor
mvn clean package -Dmaven.test.skip=true

docker build -t tp-routing-processor --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .
docker tag tp-routing-processor dcdockerregistry.azurecr.io/tp-routing-processor:v3
#docker push dcdockerregistry.azurecr.io/tp-routing-processor:v3

docker run --name tp-routing-processor --hostname tp-routing-processor -p 1260:1260 -p 9260:9260 --network trade-pilot-swarm -e SPRING_PROFILE=local-swarm tp-routing-processor

