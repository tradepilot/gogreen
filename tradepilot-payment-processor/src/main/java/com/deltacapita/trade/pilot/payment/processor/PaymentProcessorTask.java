/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.payment.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.LOAN_CREATED;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.NEW;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENTS_EXECUTING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENTS_PROCESSING;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;

public class PaymentProcessorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(PaymentProcessorTask.class); 
    
    private final LocalTime cutoffTime;
    
    private final PaymentRequestRepository paymentRequestRepository;    
    
    private final LocalDate today = LocalDate.now();    
    
    private final PaymentProcessor processor;
    
    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileOutAgent;


    public PaymentProcessorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, PaymentRequestRepository paymentRequestRepository, LocalTime cutoffTime, PaymentProcessor processor, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        this.paymentRequestRepository = paymentRequestRepository;
        this.cutoffTime = cutoffTime;
        this.processor = processor;
        
        fileInAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.PAYMENT_PROCESSOR_IN);
        fileOutAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.PAYMENT_PROCESSOR_OUT);
    }

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);
        
        long start = System.currentTimeMillis();
        logger.info("Handling {}", request);        
                  
        String fileId = request.getFileId();         
        if( processor.isBeforeCutoff(today) ) {
           
            Optional<File> result = fileRepository.findById(fileId);
            if( result.isPresent() ) {
                
                File file = result.get();   
                String fileState = file.getState();
                                
                if( fileState.equals(LOAN_CREATED) || fileState.equals(PAYMENTS_PROCESSING) ) {
                
                    logger.info("Injecting {} to {}", request, fileInAgent);        
                    fileInAgent.inject(request, file);

                    logger.info("Searching for payment requests for {} for file  {}", today, fileId);  
                    List<PaymentRequest> paymentRequests = paymentRequestRepository.findByFileIdAndDueDateAndState(fileId, today, NEW);
                    
                    List<PaymentRequest> executedPayments = processor.executePayments(today, paymentRequests);                                                                               
                    long end = System.currentTimeMillis();
                    logger.info("Executed {} payment requests out of {} for file {} for {} in {} ms", executedPayments.size(), paymentRequests.size(), fileId, today, end - start);  
                    
                    if( fileState.equals(LOAN_CREATED) ) {
                        
                        // The processor will have already done this & persisted it so just generate the event
                        // the first time the file is seen here
                        //
                        if( !executedPayments.isEmpty() ) {
                            file.setState(PAYMENTS_EXECUTING);
                        } else {                            
                            file.setState(PAYMENTS_PROCESSING);
                        }
                        
                        logger.info("Injecting {} to {}", response, fileOutAgent);        
                        fileOutAgent.inject(response, file);
                    }
                    
                } else {
                    
                    logger.warn("Ignoring request for file {} in state {}", fileId, fileState);
                } 

            } else {
                logger.info("No entry found for {}", fileId);            
            }
            
        } else {
            
            logger.warn("Deferring processing of file {} as past cutoff time {}", fileId, cutoffTime);
        }               
                
        logger.info("Returning {}", response);        
        return response;
    }
 
}
