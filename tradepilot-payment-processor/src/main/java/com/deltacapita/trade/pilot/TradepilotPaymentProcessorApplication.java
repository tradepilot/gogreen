/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.core.processor.receiver.KafkaReceiver;

@SpringBootApplication
public class TradepilotPaymentProcessorApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotPaymentProcessorApplication.class);

    @Autowired
    private KafkaReceiver receiver;


    public static void main(String[] args) {
        
        try {
            
            ServiceUtils.logIpAddresses();
            SpringApplication.run(TradepilotPaymentProcessorApplication.class, args);                      

        } catch (Exception e) {
            logger.warn("Error initialising TradepilotPaymentProcessorApplication", e);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        
        logger.info("Initialised with receiver {}", receiver);              
    }   
}
