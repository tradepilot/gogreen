/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.payment.processor;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
public class PaymentProcessorTaskFactory implements TaskFactoryI {

    private static final Logger logger = LogManager.getLogger(PaymentProcessorTaskFactory.class);

    @Value("${processor.destination}")
    private String destination;
    
    @Value("${processor.cutoff-time}")
    private String cutoff;
    
    private LocalTime cutoffTime;
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentProcessor processor;

    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;

    @PostConstruct
    public void initialise() {

        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        cutoffTime = LocalTime.parse(cutoff, formatter);
        
        logger.info("Initialised with destination={}, repo={}, cutoffTime={}",  destination, paymentRequestRepository, cutoffTime);
    }
    
    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
        
        logger.info("Creating new task for " + request);
        return new PaymentProcessorTask(request, destination, fileRepository, instructionRepository, paymentRequestRepository, cutoffTime, processor, agentFactory);
    }
}
