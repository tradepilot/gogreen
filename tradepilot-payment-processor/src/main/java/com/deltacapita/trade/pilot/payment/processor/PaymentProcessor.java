/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.payment.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.COMPLETE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENTS_PROCESSING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENT_EXECUTED;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;


@Component
public class PaymentProcessor {

    private static final String PAYMENT_PROCESSORS_TASK_NAME = "payment-processors-task";

    private static final Logger logger = LogManager.getLogger(PaymentProcessor.class); 
    
    private LocalTime cutoffTime;
    
    @Value("${processor.cutoff-time}")
    private String cutoff;
       
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;
    
    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;

    private MonitoringAgentI<ProcessingRequestI, Object> invoicePaymentExecutedAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> paymentExecutedAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileCompleteAgent;
    
    @PostConstruct
    public void initialise() {
      
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        cutoffTime = LocalTime.parse(cutoff, formatter);  
        
        invoicePaymentExecutedAgent = agentFactory.getAgent(MonitoringAgentConstants.PAYMENT_EXECUTED);
        paymentExecutedAgent = agentFactory.getAgent(MonitoringAgentConstants.PAYMENT_EXECUTED);
        fileCompleteAgent = agentFactory.getAgent(MonitoringAgentConstants.FILE_COMPLETE);
    }
    
    public boolean isBeforeCutoff(LocalDate today) {
             
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime todaysCutoff = today.atTime(cutoffTime);

        return now.isBefore(todaysCutoff);
    }
    
    public List<PaymentRequest> executePayments(LocalDate today, List<PaymentRequest> paymentRequests) {
        
        List<PaymentRequest> executedPayments = new ArrayList<>();
        Map<String,Integer> paymentsByFile = new HashMap<>();
        Map<String,Set<String>> instructionsByFile = new HashMap<>();
        
        if( paymentRequests != null && !paymentRequests.isEmpty() ) {
            
            for (PaymentRequest request : paymentRequests) {
                
                String fileId = request.getFileId();
                LocalDate dueDate = request.getDueDate();                            
                if( today.isEqual(dueDate) || today.isAfter(dueDate) ) {
                                       
                    executePaymentRequest(today, executedPayments, paymentsByFile, instructionsByFile, request, fileId, dueDate);
                }
            }
        } 
        
        // Update the payments
        //
        int updateCount = executedPayments.size();
        List<Instruction> updatedInvoices = new ArrayList<>();
        if( updateCount > 0 ) {
            
            // Update the file & instruction details
            //
            Set<String> fileIds = paymentsByFile.keySet();
            Iterable<File> files = fileRepository.findAllById(fileIds);
            
            for (File file : files) {
                
                String fileId = file.getId();                
                Long outstanding = file.getOutstandingPaymentRequests();
                Long existingCount = (outstanding != null ? outstanding : 0L );
                Integer count = paymentsByFile.get(fileId);  
                Set<String> instructionIds = instructionsByFile.get(fileId);               
                
                Long newCount = existingCount - count;     
                newCount = Math.max(0, newCount);
                file.setOutstandingPaymentRequests(newCount);
                logger.info("Updating outstanding payments for file {}, old={}, new={}", fileId, existingCount, newCount);

                if( newCount.intValue() == 0 ) {
                    
                    file.setState(COMPLETE);
                    logger.info("File {} is now {}", fileId, file.getState());   
                    
                    ProcessingRequest request = new ProcessingRequest();
                    request.setFileId(file.getId());
                    request.setSource(PAYMENT_PROCESSORS_TASK_NAME);
                    request.setDestination(PAYMENT_PROCESSORS_TASK_NAME);
                    request.setRequestId(UUID.randomUUID().toString());

                    fileCompleteAgent.inject(request, file);
                    
                } else {
                    
                    if( !file.getState().equals(PAYMENTS_PROCESSING) ) {
                        
                        file.setState(PAYMENTS_PROCESSING);
                        logger.info("Updating status for {} to {}", fileId, file.getState());                                   
                    }
                }

                
                // Update the instructions
                //
                Iterable<Instruction> invoices = instructionRepository.findAllById(instructionIds);
                for (Instruction invoice : invoices) {     
                    
                    logger.info("Marking instruction {} as {}", invoice.getId(), PAYMENT_EXECUTED);
                    invoice.setState(PAYMENT_EXECUTED);
                    updatedInvoices.add(invoice);                 
                    
                    ProcessingRequest request = new ProcessingRequest();
                    request.setFileId(fileId);
                    request.setSource(PAYMENT_PROCESSORS_TASK_NAME);
                    request.setDestination(PAYMENT_PROCESSORS_TASK_NAME);
                    request.setRequestId(UUID.randomUUID().toString());
                    invoicePaymentExecutedAgent.inject(request, invoice);
                }               
            }
            
            logger.info("Saving files={}, instructions={}, paymentRequests={}", paymentsByFile.keySet().size(), updatedInvoices.size(), executedPayments.size() );
            paymentRequestRepository.saveAll(executedPayments);
            instructionRepository.saveAll(updatedInvoices);
            
            fileRepository.saveAll(files);           
            logger.info("Update complete");
        }
        
        return executedPayments;
    }

    
    private void executePaymentRequest(LocalDate today, List<PaymentRequest> executedPayments,
            Map<String, Integer> paymentsByFile, Map<String, Set<String>> instructionsByFile, PaymentRequest request,
            String fileId, LocalDate dueDate) {
        
        logger.info("Executing payment request {} for file {} as today {} is on or after due date {}", request, fileId, today, dueDate);  
        request.setState(PAYMENT_EXECUTED);
        executedPayments.add(request);
        
        Integer count = paymentsByFile.get(fileId);
        if( count == null ) {
            paymentsByFile.put(fileId, 1);
        } else {
            paymentsByFile.put(fileId, count + 1);                        
        }  
        
        Set<String> ids = instructionsByFile.computeIfAbsent(fileId, k -> new HashSet<>());
        ids.add(request.getInstructionId());
        
        ProcessingRequest pr = new ProcessingRequest();
        pr.setFileId(fileId);
        pr.setSource(PAYMENT_PROCESSORS_TASK_NAME);
        pr.setDestination(PAYMENT_PROCESSORS_TASK_NAME);
        pr.setRequestId(UUID.randomUUID().toString());
        paymentExecutedAgent.inject(pr, request);
    }
}



