/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.payment.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.NEW;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;

@Component
public class PaymentProcessorStartupProcessor  {

    private static final Logger logger = LogManager.getLogger(PaymentProcessorStartupProcessor.class);

    @Value("${processor.name}")
    private String name;

    @Value("${processor.destination}")
    private String destination;
    
    @Value("${processor.threads}")
    private Integer numThreads;

    @Value("${processor.initial-delay}")
    private Long initialDelay;

    @Value("${processor.cutoff-time}")
    private String cutoff;

    @Value("${processor.batch-expression}")
    private String batchExpression;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentProcessor processor;

    private ConcurrentTaskScheduler taskScheduler;

    private int threadNo = 0;



    @PostConstruct
    public void initialise()  {
      
        String processorName = "startup-" + name;

        LocalTime cutoffTime = LocalTime.parse(cutoff, DateTimeFormatter.ISO_LOCAL_TIME);
        LocalDate today = LocalDate.now();  
       
        logger.info("Initialising {} with {} threads and destination {}", processorName, numThreads, destination);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(numThreads,  (Runnable r) ->  
            new Thread(r, processorName + "-" + threadNo++)           
        );
        
        taskScheduler = new ConcurrentTaskScheduler(executor);      

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime todaysCutoff = today.atTime(cutoffTime);        
        if( now.isBefore(todaysCutoff) ) {
            
            logger.info("Scheduling startup task");
            runInitialJobAndSetupBatchJob();
            
        } else {
                
            logger.info("Not scheduling startup task as startup is after cutoff time {}", cutoffTime); 
            setupBatchJob();
        }                
    }  
    
    private void runInitialJobAndSetupBatchJob() {
        
        taskScheduler.execute( () -> {
            
            try {
                
                logger.info("Startup task waiting for {} ms", initialDelay);
                Thread.sleep(initialDelay);
                
            } catch (Exception e) {
                // Ignore
            }

            processOutstandingPayments();
            setupBatchJob();
        });               
    }

    private void setupBatchJob() {
        
        logger.info("Scheduling batch task with expression {}", batchExpression); 
        CronTrigger trigger = new CronTrigger(batchExpression);
        taskScheduler.schedule( () -> {
            
            logger.info("Running scheduled batch job");
            processOutstandingPayments();
            
        }, trigger);  
    }
    
    private void processOutstandingPayments() {
        
        try {
                      
            long start = System.currentTimeMillis();
            LocalDate today = LocalDate.now();       
            logger.info("Checking for outstanding payments due on or before today {}", today);
            List<PaymentRequest> paymentRequests = paymentRequestRepository.findByDueDateIsLessThanEqualAndState(today, NEW);
            
            logger.info("Found {} outstanding payments due on or before today {}", paymentRequests.size(), today);            
            List<PaymentRequest> executedPayments = processor.executePayments(today, paymentRequests);
         
            long end = System.currentTimeMillis();
            logger.info("Executed {} payment requests out of {} for {} in {} ms", executedPayments.size(), paymentRequests.size(), today, end - start);  

        } catch (Exception e) {
            
            logger.error("Error executing initial task", e);
        }
    }
}

