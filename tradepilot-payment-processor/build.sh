#!/bin/sh

#docker stop tp-payment-processor
docker rm tp-payment-processor
mvn clean package -Dmaven.test.skip=true

docker build -t tp-payment-processor --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-payment-processor dcdockerregistry.azurecr.io/tp-payment-processor:v1
#docker push dcdockerregistry.azurecr.io/tp-payment-processor:v1

docker run --name tp-payment-processor --hostname tp-payment-processor -p 1240:1240 -p 9240:9240 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-payment-processor

