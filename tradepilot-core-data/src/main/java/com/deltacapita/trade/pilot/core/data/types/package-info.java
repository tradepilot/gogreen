/**
 * This package defines the types used by all the components in the trade pilot application
 * 
 * @author simonw
 *
 */
package com.deltacapita.trade.pilot.core.data.types;