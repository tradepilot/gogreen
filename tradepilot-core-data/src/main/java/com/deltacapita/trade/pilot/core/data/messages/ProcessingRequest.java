/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.messages;

import java.util.UUID;

public class ProcessingRequest implements ProcessingRequestI {

    private String requestId;

    private String source;

    private String destination;

    private String destinationTopic;

    private String fileId;
    
    private String routingId;
    
    private Long timestamp = System.currentTimeMillis();
        
    
    public ProcessingRequest() {
        
        requestId = UUID.randomUUID().toString();
    }
    
    public ProcessingRequest( ProcessingRequest request ) {
               
        // Copy attributes except destination topic
        // as this should be set explicitly by a router
        //
        setRequestId(UUID.randomUUID().toString());
        setFileId(request.getFileId());
        setSource(request.getDestination());
        setRoutingId(request.getRoutingId());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    @Override
    public String getFileId() {
        return fileId;
    }
    
    @Override
    public String getDestinationTopic() {
        return destinationTopic;
    }
    
    @Override
    public String getRoutingId() {
        return routingId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setRoutingId(String routingId) {
        this.routingId = routingId;
    }
    
    public void setDestinationTopic(String destinationTopic) {
        this.destinationTopic = destinationTopic;
    }
    
    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((requestId == null) ? 0 : requestId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        ProcessingRequest other = (ProcessingRequest) obj;
        if (requestId == null) {
            if (other.requestId != null) {
                return false;
            }
        } else if (!requestId.equals(other.requestId)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "ProcessingRequest ["  
                + "requestId=" + requestId 
                + ", source=" + source 
                + ", destination=" + destination
                + ", fileId=" + fileId 
                + ", routingId=" + routingId 
                + ", destinationTopic=" + destinationTopic
                + ", timestamp=" + timestamp 
                + "]";
    }

}
