/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;

@Repository
@ViewIndexed(designDoc = "paymentRequest", viewName = "all")
public interface PaymentRequestRepository extends CouchbaseRepository<PaymentRequest, String> {

    List<PaymentRequest> findByState(String state);

    List<PaymentRequest> findByDueDate(LocalDate dueDate);

    List<PaymentRequest> findByDueDateAndState(LocalDate dueDate, String state);

    List<PaymentRequest> findByDueDateIsLessThanEqualAndState(LocalDate dueDate, String state);

    List<PaymentRequest> findByFileIdAndDueDateAndState(String fileId, LocalDate dueDate, String state);
}
