/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data;

import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.Loan;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;

public class IdBuilder {

    public static String buildId( Instruction instruction ) {
        
        String fileId = instruction.getFileId();
        String invoiceNumber = instruction.getInvoiceNumber();

        String id = fileId + "-" + invoiceNumber;
        return id;
    }


    public static String buildId( PaymentRequest paymentRequest ) {
        
        String fileId = paymentRequest.getFileId();
        String supplierId = paymentRequest.getSupplierId();
        String paymentReference = paymentRequest.getPaymentReference();
        long dueDateDays = paymentRequest.getDueDate().toEpochDay();

        String id = fileId + "-" + supplierId + "-" + dueDateDays + "-" + paymentReference;
        return id;
    }


    public static String buildId( Loan loan ) {
        
        String fileId = loan.getFileId();
        String currency = loan.getCurrency();
        long maturityDateDays = loan.getMaturityDate().toEpochDay();

        String id = fileId + "-" + currency + "-" + maturityDateDays ;
        return id;
    }

}

