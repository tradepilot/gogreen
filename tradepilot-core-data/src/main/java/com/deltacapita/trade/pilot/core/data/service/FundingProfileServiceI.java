/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deltacapita.trade.pilot.core.data.types.FundingProfile;

public interface FundingProfileServiceI {

    @RequestMapping(value = "/fundingprofile/{buyerId}/{supplierId}/{currency}", method = RequestMethod.GET, produces = "application/json")
    FundingProfile getFundingProfile(@PathVariable("buyerId") String buyerId, @PathVariable("supplierId") String supplierId, @PathVariable("currency") String currency);
}
