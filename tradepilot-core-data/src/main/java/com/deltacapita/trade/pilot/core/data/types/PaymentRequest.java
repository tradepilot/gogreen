/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document
public class PaymentRequest {

    @Id
    private String id;

    @Field
    private String fileId;

    @Field
    private String buyerId;

    @Field
    private String instructionId;

    @Field
    private String paymentReference;

    @Field
    private String supplierId;

    @Field
    private String currency;
    
    @Field
    private LocalDate dueDate;

    @Field
    private Double amount;

    @Field
    private String state;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }    
    
    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    
    @Override
    public int hashCode() {
        
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        PaymentRequest other = (PaymentRequest) obj;
        if (id == null) {
            
            if (other.id != null) {
                return false;
            }
            
        } else if (!id.equals(other.id)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        
        return "PaymentRequest [" 
                + "id=" + id 
                + ", fileId=" + fileId 
                + ", instructionId=" + instructionId 
                + ", paymentReference=" + paymentReference 
                + ", buyerId=" + buyerId 
                + ", supplierId=" + supplierId 
                + ", currency=" + currency 
                + ", dueDate=" + dueDate 
                + ", amount=" + amount 
                + ", state=" + state 
                + "]";
    }


}
