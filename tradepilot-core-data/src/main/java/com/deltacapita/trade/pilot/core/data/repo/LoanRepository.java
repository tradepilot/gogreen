/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.Loan;

@Repository
@ViewIndexed(designDoc = "loan", viewName = "all")
public interface LoanRepository extends CouchbaseRepository<Loan, String> {

    List<Loan> findByState(String state);

    List<Loan> findByMaturityDate(LocalDate maturityDate);

    List<Loan> findByMaturityDateAndState(LocalDate maturityDate, String state);

    List<Loan> findByFileIdAndMaturityDateAndState(String fileId, LocalDate maturityDate, String state);
}
