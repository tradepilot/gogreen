/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

/**
 * Represents a profile that defines the terms for the loan to
 * a buyer from the bank to fund early paid invoices
 * 
 * @author simonw
 *
 */
@Document
public class LoanProfile {

    @Id
    private String id;

    @Field
    private String buyerId;

    @Field
    private String currency;

    @Field
    private Long maxTenor;

    @Field
    private Long maxValue;

    @Field
    private Long maxInvoiceValue;

    @Field
    private Double rate;
    
    @Field
    private String paymentFrequency;
    
    @Field
    private String daycountConvention;


    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getMaxTenor() {
        return maxTenor;
    }

    public void setMaxTenor(Long maxTenor) {
        this.maxTenor = maxTenor;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public Long getMaxInvoiceValue() {
        return maxInvoiceValue;
    }

    public void setMaxInvoiceValue(Long maxInvoiceValue) {
        this.maxInvoiceValue = maxInvoiceValue;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
    
    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public String getDaycountConvention() {
        return daycountConvention;
    }

    public void setDaycountConvention(String daycountConvention) {
        this.daycountConvention = daycountConvention;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LoanProfile other = (LoanProfile) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LoanProfile [" 
                + " id=" + id 
                + ", buyerId=" + buyerId 
                + ", currency=" + currency 
                + ", maxTenor=" + maxTenor 
                + ", maxValue=" + maxValue 
                + ", maxInvoiceValue=" + maxInvoiceValue 
                + ", rate="  + rate 
                + ", paymentFrequency=" + paymentFrequency 
                + ", daycountConvention=" + daycountConvention 
                + "]";
    }




}
