/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deltacapita.trade.pilot.core.data.types.LoanProfile;

public interface LoanServiceI {

    @RequestMapping(value = "/loan/{buyerId}/{currency}", method = RequestMethod.GET, produces = "application/json")
    LoanProfile getLoanProfile(@PathVariable("buyerId") String buyerId, @PathVariable("currency") String currency);
}
