/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;
import java.util.Map;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;


/**
 * Represents am instruction object
 * 
 * @author simonw
 *
 */
@Document
public class Instruction {

	@Id
	private String id;

	@Field
	private String fileId;

	@Field
	private String type;
	
	@Field
	private String bprn;	

	@Field
	private String buyerId;
	
	@Field
	private String supplierId;
	
	@Field
	private String supplierName;
	
	@Field
	private String currency;	

	@Field
	private String invoiceNumber;

	@Field
	private LocalDate dueDate;

	@Field
	private Double amount;

    @Field
    private String state;

	@Field
	private Map<String,Object> properties;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBprn() {
		return bprn;
	}

	public void setBprn(String bprn) {
		this.bprn = bprn;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }   

    public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		return "Instruction [id=" + id 
				+ ", fileId=" + fileId 
                + ", state=" + state 
				+ ", type=" + type 
				+ ", bprn=" + bprn 
				+ ", buyerId=" + buyerId 
				+ ", supplierId=" + supplierId 
				+ ", supplierName=" + supplierName 
				+ ", currency=" + currency
				+ ", invoiceNumber=" + invoiceNumber 
				+ ", dueDate=" + dueDate 
				+ ", amount=" + amount 
				+ ", properties=" + properties 
				+ "]";
	}

}
