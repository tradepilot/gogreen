/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "core-data")
@EnableCouchbaseRepositories(basePackages = {"com.deltacapita.trade.pilot.core.data","com.deltacapita.trade.pilot.core.data.repo"})
public class CoreDataAppConfig {
    
    private List<String> currencies;
    
    private List<CustomerConfig> customerConfigs;

    public List<CustomerConfig> getCustomerConfigs() {
        return customerConfigs;
    }

    public void setCustomerConfigs(List<CustomerConfig> customerConfigs) {
        this.customerConfigs = customerConfigs;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

}
