/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Enumeration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ServiceUtils {
    
    private static final Logger logger = LogManager.getLogger(ServiceUtils.class);

    public static void logIpAddresses() {

        try {
            
            InetAddress localhost = InetAddress.getLocalHost();
            logger.info("Host Name   : " + localhost.getCanonicalHostName() );
            logger.info("IP Address  : " + localhost.getHostAddress());
            
            // Just in case this host has multiple IP addresses....
            //
            InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (allMyIps != null && allMyIps.length > 1) {
                logger.info(" Full list of IP addresses : ");
                for (int i = 0; i < allMyIps.length; i++) {
                    logger.info("IP  " + allMyIps[i]);
                }
            }
        } catch (UnknownHostException e) {
            logger.info("Error retrieving server host name");
        }

        try {
            
            logger.info("Network Interfaces : ");
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                
                NetworkInterface intf = en.nextElement();
                logger.info("    " + intf.getName() + " " + intf.getDisplayName());
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    logger.info("        " + enumIpAddr.nextElement().toString());
                }
            }
        } catch (SocketException e) {
            logger.info("Error retrieving network interface list");
        }
    }
    

    public static long toEpochMillis( LocalDateTime localDateTime ) {
        
        Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
        long epochMs = instant.toEpochMilli();

        return epochMs;
    }
    

    public static LocalDateTime fromEpochMillis( long epochMs ) {
            
        Instant instant = Instant.ofEpochMilli(epochMs);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Z"));

        return localDateTime;
    }

    
}




