/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.BUYER;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.CREDIT_NOTE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DC_30_360;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DC_30_365;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DC_ACT_360;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DC_ACT_365;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DC_ACT_ACT;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DEFAULT;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.FREQ_BIANNUAL;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.FREQ_MONTHLY;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.FREQ_QUARTERLY;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.INVOICE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.SUPPLIER;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.UTC;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import com.deltacapita.trade.pilot.core.data.repo.CustomerRepository;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.FundingProfileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanProfileRepository;
import com.deltacapita.trade.pilot.core.data.types.Customer;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.FundingProfile;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.LoanProfile;

@SpringBootApplication
@Profile("local-setup")
public class CoreDataApp implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(CoreDataApp.class);

    private static final long [] TENORS = new long [] { 90, 120, 180, 270, 360 };
    
    private static final long [] INVOICE_VALUES = new long [] { 100000, 250000, 500000, 1000000, 2000000};
    
    private static final long [] PROFILE_VALUES = new long [] { 10000000, 25000000, 50000000, 100000000, 200000000};
    
    private static final double [] RATES = new double [] { 0.05, 0.10, 0.15, 0.20 };

    private static final String [] DAYCOUNTS = new String [] { DC_30_360, DC_30_365, DC_ACT_360, DC_ACT_365, DC_ACT_ACT };

    private static final String [] PAYMENT_FREQUENCIES = new String [] { FREQ_MONTHLY, FREQ_QUARTERLY, FREQ_BIANNUAL };
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;
    
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FundingProfileRepository profileRepository;

    @Autowired
    private LoanProfileRepository loanProfileRepository;

    @Autowired
    private CoreDataAppConfig config;



    
    public static void main(String[] args) {
        SpringApplication.run(CoreDataApp.class);
    }

    @Override
    public void run(String... args) throws Exception {

        logger.info("Running Core Data setup...");
        //setupCustomers();
        //setupFundingProfiles();
        //setupLoanProfiles();
        logger.info("Done.");
    }
    
    
    public void setupCustomers() {
        
        List<CustomerConfig> configs = config.getCustomerConfigs();
        List<Customer> buyers = new ArrayList<>(configs.size());
        List<Customer> suppliers = new ArrayList<>(configs.size());    
        
        for (CustomerConfig config : configs) {
            
            Customer buyer = new Customer();
            buyer.setId("B" + config.getOrgId());
            buyer.setName(config.getOrgName());
            buyer.setType(BUYER);
            buyer.setAccountNumber(config.getAccountNumber());
            
            Customer supplier = new Customer();
            supplier.setId("S" + config.getOrgId());
            supplier.setName(config.getOrgName());
            supplier.setType(SUPPLIER);
            supplier.setAccountNumber(config.getAccountNumber());

            buyers.add(buyer);
            suppliers.add(supplier);
        }
        
        customerRepository.deleteAll();
        customerRepository.saveAll(buyers);
        customerRepository.saveAll(suppliers);    
        
        Iterable<Customer> customers = customerRepository.findAll();
        for (Customer customer : customers) {
            logger.info("Found {}", customer);
        }
    }

    public void setupFundingProfiles() {
        
        List<Customer> buyers = customerRepository.findByType(BUYER);
        List<Customer> suppliers = customerRepository.findByType(SUPPLIER);
        List<String> currencies = config.getCurrencies();
        List<FundingProfile> profiles = new ArrayList<>();
        Random r = new Random();
        
           
        for (Customer buyer : buyers) {
            
            logger.info("Creating entries for buyer {}", buyer);
            for (Customer supplier : suppliers) {
                
                for (String currency : currencies) {
                                     
                    // Add a default for the buyer/supplier/currency combination
                    //
                    FundingProfile profile = createProfile(r, buyer.getId(), supplier.getId(), currency);
                    profiles.add(profile);
                }                

                // Add a default for the buyer/supplier combination
                //
                FundingProfile profile = createProfile(r, buyer.getId(), supplier.getId(), DEFAULT);
                profiles.add(profile);            
            } 
            
            // Add a default for the buyer 
            //
            FundingProfile profile = createProfile(r, buyer.getId(), DEFAULT, DEFAULT);
            profiles.add(profile);            
        }

      
        // Add a catch-all default
        //
        FundingProfile profile = createProfile(r, DEFAULT, DEFAULT, DEFAULT);
        profiles.add(profile);

        
        logger.info("Saving profiles....");        
        long start = System.currentTimeMillis();
        
        profileRepository.deleteAll();
        profileRepository.saveAll(profiles);
        
        long end = System.currentTimeMillis();
        logger.info("Saved {} profiles in {} ms", profiles.size(), (end - start));        
    }

    public void setupLoanProfiles() {
        
        List<Customer> buyers = customerRepository.findByType(BUYER);
        List<String> currencies = config.getCurrencies();
        List<LoanProfile> profiles = new ArrayList<>();
        Random r = new Random();
                  
        String daycount = DAYCOUNTS[r.nextInt(DAYCOUNTS.length)];
        String paymentFrequency = PAYMENT_FREQUENCIES[r.nextInt(PAYMENT_FREQUENCIES.length)];
        
        for (Customer buyer : buyers) {

            daycount = DAYCOUNTS[r.nextInt(DAYCOUNTS.length)];
            paymentFrequency = PAYMENT_FREQUENCIES[r.nextInt(PAYMENT_FREQUENCIES.length)];

            logger.info("Creating loan profile entries for buyer {}", buyer);                
            for (String currency : currencies) {
                                 
                // Add a default for the buyer/supplier/currency combination
                //
                LoanProfile profile = createLoanProfile(r, buyer.getId(), currency, daycount, paymentFrequency);
                profiles.add(profile);
            }                

            
            // Add a default for the buyer 
            //
            LoanProfile profile = createLoanProfile(r, buyer.getId(), DEFAULT, daycount, paymentFrequency);
            profiles.add(profile);            
        }

      
        // Add a catch-all default
        //
        LoanProfile profile = createLoanProfile(r, DEFAULT, DEFAULT, daycount, paymentFrequency);
        profiles.add(profile);

        
        logger.info("Saving loan profiles....");        
        long start = System.currentTimeMillis();
        
        loanProfileRepository.deleteAll();
        loanProfileRepository.saveAll(profiles);
        
        long end = System.currentTimeMillis();
        logger.info("Saved {} profiles in {} ms", profiles.size(), (end - start));        
    }
    
    private FundingProfile createProfile(Random r, String buyerId, String supplierId, String currency) {
        
        double rate = RATES[r.nextInt(RATES.length)];
        long tenor = TENORS[r.nextInt(TENORS.length)];
        long invoiceValue = INVOICE_VALUES[r.nextInt(INVOICE_VALUES.length)];
        long profileValue = PROFILE_VALUES[r.nextInt(PROFILE_VALUES.length)];
        
        FundingProfile profile = new FundingProfile();
        profile.setId(UUID.randomUUID().toString());
        profile.setBuyerId(buyerId);
        profile.setSupplierId(supplierId);
        profile.setCurrency(currency);
        profile.setMaxInvoiceValue(invoiceValue);
        profile.setMaxValue(profileValue);
        profile.setRate(rate);
        profile.setMaxTenor(tenor);

        return profile;
    }
    
    private LoanProfile createLoanProfile(Random r, String buyerId, String currency, String daycount, String paymentFrequency) {
        
        double rate = RATES[r.nextInt(RATES.length)];
        long tenor = TENORS[r.nextInt(TENORS.length)];
        long invoiceValue = INVOICE_VALUES[r.nextInt(INVOICE_VALUES.length)];
        long profileValue = PROFILE_VALUES[r.nextInt(PROFILE_VALUES.length)];
        
        
        LoanProfile profile = new LoanProfile();
        profile.setId(UUID.randomUUID().toString());
        profile.setBuyerId(buyerId);
        profile.setCurrency(currency);
        profile.setMaxInvoiceValue(invoiceValue);
        profile.setMaxValue(profileValue);
        profile.setRate(rate);
        profile.setMaxTenor(tenor);
        profile.setDaycountConvention(daycount);
        profile.setPaymentFrequency(paymentFrequency);

        return profile;
    }

    public void runQuery(String... args) throws Exception {

        fileRepository.deleteAll();
        instructionRepository.deleteAll();

        File file1 = saveFile("buyer1", "request1");
        File file2 = saveFile("buyer1", "request2");

        String id1 = file1.getId();
        String id2 = file2.getId();

        String file1Id = file1.getId();
        String file2Id = file2.getId();

        String buyerId = file1.getBuyerId();

        System.out.println("***** searching for file 1");
        System.out.println(fileRepository.findById(id1));

        System.out.println("***** searching for file 2");
        System.out.println(fileRepository.findById(id2));

        System.out.println("***** searching for files for buyer " + buyerId);
        List<File> files = fileRepository.findByBuyerId(buyerId);
        for (File file : files) {
            System.out.println("  Found " + file);
        }

        Instruction instruction1 = saveInstruction(buyerId, file1Id, INVOICE);
        Instruction instruction2 = saveInstruction(buyerId, file1Id, INVOICE);
        Instruction instruction3 = saveInstruction(buyerId, file1Id, CREDIT_NOTE);

        System.out.println("***** searching for instruction 1");
        System.out.println(instructionRepository.findById(instruction1.getId()));

        System.out.println("***** searching for instruction 2");
        System.out.println(instructionRepository.findById(instruction2.getId()));

        System.out.println("***** searching for instruction 3");
        System.out.println(instructionRepository.findById(instruction3.getId()));

        Instruction instruction4 = saveInstruction(buyerId, file2Id, INVOICE);
        Instruction instruction5 = saveInstruction(buyerId, file2Id, INVOICE);
        Instruction instruction6 = saveInstruction(buyerId, file2Id, CREDIT_NOTE);

        System.out.println("***** searching for instruction 4");
        System.out.println(instructionRepository.findById(instruction4.getId()));

        System.out.println("***** searching for instruction 5");
        System.out.println(instructionRepository.findById(instruction5.getId()));

        System.out.println("***** searching for instruction 6");
        System.out.println(instructionRepository.findById(instruction6.getId()));

        System.out.println("***** searching for instructions for buyer " + buyerId);
        List<Instruction> instructions = instructionRepository.findByBuyerId(buyerId);
        for (Instruction instruction : instructions) {
            System.out.println("  Found " + instruction);
        }

        System.out.println("***** searching for credit notes for file " + file1Id);
        List<Instruction> creditNotes = instructionRepository.findByFileIdAndType(file1Id, CREDIT_NOTE);
        for (Instruction instruction : creditNotes) {
            System.out.println("  Found " + instruction);
        }
        System.out.println("*****");
    }

    private File saveFile(String buyerId, String requestId) {

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDateTime dueDateTime = LocalDateTime.from(dateTime);

        File file = new File();
        file.setId(UUID.randomUUID().toString());
        file.setNumberOfInstructions(10L);
        file.setBuyerId(buyerId);
        file.setRequestId(requestId);
        file.setTotalValue(2000.0);
        file.setReceivedDateTime(dueDateTime);

        return this.fileRepository.save(file);
    }

    private Instruction saveInstruction(String buyerId, String fileId, String type) {

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDateTime dueDateTime = LocalDateTime.from(dateTime).plusDays(10);
        LocalDate dueDate = dueDateTime.toLocalDate();

        Instruction instruction = new Instruction();
        instruction.setId(UUID.randomUUID().toString());

        instruction.setBuyerId(buyerId);
        instruction.setFileId(fileId);
        instruction.setType(type);
        instruction.setAmount(1000.0);
        instruction.setBprn("bprn-1");
        instruction.setCurrency("GBP");
        instruction.setDueDate(dueDate);

        return this.instructionRepository.save(instruction);
    }
}
