/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.types.Instruction;

@Component
@ViewIndexed(designDoc = "instruction", viewName = "all")
public interface InstructionRepository extends CouchbaseRepository<Instruction, String> {

    List<Instruction> findByBuyerId(String buyerId);

    List<Instruction> findByBuyerIdAndType(String buyerId, String type);

    List<Instruction> findByFileId(String fileId);

    List<Instruction> findByFileIdAndType(String fileId, String type);
}
