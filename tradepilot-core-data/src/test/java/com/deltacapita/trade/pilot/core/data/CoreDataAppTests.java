/*
 * Copyright 2012-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.deltacapita.trade.pilot.core.data;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.ConnectException;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.core.NestedCheckedException;

public class CoreDataAppTests {

	@Rule
	public OutputCapture outputCapture = new OutputCapture();

	@Test
	@Ignore
	public void testDefaultSettings() throws Exception {
		
		try {
			new SpringApplicationBuilder(CoreDataApp.class).run("--server.port=0");
		}
		catch (RuntimeException ex) {
			if (serverNotRunning(ex)) {
				return;
			}
		}
		
		String output = this.outputCapture.toString();
		assertThat(output).contains("buyerId='buyer1'");
	}

	private boolean serverNotRunning(RuntimeException ex) {
		
		@SuppressWarnings("serial")
		NestedCheckedException nested = new NestedCheckedException("failed", ex) {
		};
		
		if (nested.contains(ConnectException.class)) {
			Throwable root = nested.getRootCause();
			if (root.getMessage().contains("Connection refused")) {
				return true;
			}
		}
		
		return false;
	}

}
