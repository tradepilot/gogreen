package com.deltacapita.trade.pilot.core.data.repo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
//
//@SpringBootApplication
//@PropertySource({ "classpath:application.yml" })
//@EnableAutoConfiguration
//@ComponentScan(basePackages = { "com.deltacapita.trade.pilot.core.data", "com.deltacapita.trade.pilot.core.data.repo", "com.deltacapita.trade.pilot.core.data.types" })

//@ContextConfiguration(classes = {SpringBootCouchbaseDataConfiguration.class})

public class AbstractRepositoryTest {

	@Autowired
	protected FileRepository fileRepository;
	
	@Autowired
	protected InstructionRepository instructionRepository;
	
		
	protected File saveFile(String buyerId, String requestId) {
		
		Date now = new Date();
		ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of("UTC"));
		LocalDateTime dueDateTime = LocalDateTime.from(dateTime);
				
		File file = new File();
		file.setId(UUID.randomUUID().toString());
		file.setNumberOfInstructions(10L);
		file.setBuyerId(buyerId);
		file.setRequestId(requestId);
		file.setTotalValue(2000.0);
		file.setReceivedDateTime(dueDateTime);
		
		return this.fileRepository.save(file);
	}	

	
	protected Instruction saveInstruction(String buyerId, String fileId, String type) {
		
		Date now = new Date();
		ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of("UTC"));
		LocalDateTime dueDateTime = LocalDateTime.from(dateTime).plusDays(10);
		LocalDate dueDate = dueDateTime.toLocalDate();		
		
		Instruction instruction = new Instruction();
		instruction.setId(UUID.randomUUID().toString());

		instruction.setBuyerId(buyerId);
		instruction.setFileId(fileId);
		instruction.setType(type);
		instruction.setAmount(1000.0);
		instruction.setBprn("bprn-1");
		instruction.setCurrency("GBP");
		instruction.setDueDate(dueDate);
		
		return this.instructionRepository.save(instruction);
	}
	
}
