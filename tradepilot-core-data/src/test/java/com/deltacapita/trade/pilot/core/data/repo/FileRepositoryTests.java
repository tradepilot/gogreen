package com.deltacapita.trade.pilot.core.data.repo;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;



@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FileRepository.class, InstructionRepository.class})
/*
(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = CoreDataApp.class)

@TestPropertySource(
  locations = "classpath:application.yml")
  */
public class FileRepositoryTests /*extends AbstractRepositoryTest*/ {

    @Autowired
    protected FileRepository fileRepository;
    
    @Autowired
    protected InstructionRepository instructionRepository;
    
        
    protected File saveFile(String buyerId, String requestId) {
        
        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of("UTC"));
        LocalDateTime dueDateTime = LocalDateTime.from(dateTime);
                
        File file = new File();
        file.setId(UUID.randomUUID().toString());
        file.setNumberOfInstructions(10L);
        file.setBuyerId(buyerId);
        file.setRequestId(requestId);
        file.setTotalValue(2000.0);
        file.setReceivedDateTime(dueDateTime);
        
        return this.fileRepository.save(file);
    }   

    
    protected Instruction saveInstruction(String buyerId, String fileId, String type) {
        
        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of("UTC"));
        LocalDateTime dueDateTime = LocalDateTime.from(dateTime).plusDays(10);
        LocalDate dueDate = dueDateTime.toLocalDate();      
        
        Instruction instruction = new Instruction();
        instruction.setId(UUID.randomUUID().toString());

        instruction.setBuyerId(buyerId);
        instruction.setFileId(fileId);
        instruction.setType(type);
        instruction.setAmount(1000.0);
        instruction.setBprn("bprn-1");
        instruction.setCurrency("GBP");
        instruction.setDueDate(dueDate);
        
        return this.instructionRepository.save(instruction);
    }
	
	@Before
	public void initialise() {
		fileRepository.deleteAll();
	}
	
	@After
	public void tearDown() {
		fileRepository.deleteAll();
	}

	
	@Test
	public void testSaveFile() {
		
		File file1 = saveFile("buyer1", "request1");
		File file2 = saveFile("buyer1", "request2");
		
		String id1 = file1.getId();
		String id2 = file2.getId();
		assertNotNull(id1);
        assertNotNull(id2);
        
		String file1Id = file1.getId();
		String file2Id = file2.getId();
		
	    assertNotNull(file1Id);
        assertNotNull(file2Id);

		String buyerId = file1.getBuyerId();
	    assertNotNull(buyerId);

		System.out.println("***** searching for file 1");
		Optional<File> file1Result = fileRepository.findById(id1);
		assertNotNull(file1Result);
		
		File file1a = file1Result.get();
		assertNotNull(file1a);
	}

}
