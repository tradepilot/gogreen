#!/bin/sh

docker rm tp-data-setup
mvn clean package -Dmaven.test.skip=true

docker build -t tp-data-setup --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-data-setup dcdockerregistry.azurecr.io/tp-data-setup:v1
docker push dcdockerregistry.azurecr.io/tp-data-setup:v1

docker run --name tp-data-setup --hostname tp-data-setup -p 1400:1400 -p 9400:9400  --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-data-setup

