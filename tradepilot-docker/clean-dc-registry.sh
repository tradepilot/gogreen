#!/bin/bash


declare -a services=("config-server" "service-registry" "hystrix-service" "funding-profile-service" "loan-service" "file-processor" "file-validator" "funding-request-generator" "loan-generator" "payment-processor" "request-generator" "monitor-service" "ui")
declare -a versions=("2" "4.0" "4.01" )

numservices=${#services[@]}
numversions=${#versions[@]}


for (( i=0; i<${numservices}; i++ ));
do
	
	
    service=tp-${services[$i]}    
	for (( j=0; j<${numversions}; j++ ));
	do
		
    		version=${versions[$j]}
    		
    		echo Deleting $service:v$version -y
    		az acr repository delete -n dcDockerRegistry --image $service:v$version -y
	done    
    
    echo " "
   
done




