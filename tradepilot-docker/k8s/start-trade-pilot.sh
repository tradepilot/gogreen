#!/bin/bash

echo Starting Spring Cloud Services......
kubectl create -f tp-cloud/
sleep 5

echo ""
echo Starting Trade Pilot Services......
kubectl create -f tp-services/
sleep 5

echo ""
echo Starting Trade Pilot Processors......
kubectl create -f tp-processors/

echo ""
echo Starting Trade Pilot Simulator......
kubectl create -f tp-demo/tp-request-generator-service.yaml
