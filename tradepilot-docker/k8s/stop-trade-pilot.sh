#!/bin/bash

echo Stopping Trade Pilot Simulator......
kubectl delete -f tp-demo/tp-request-generator-service.yaml

echo ""
echo Stopping Trade Pilot Processors......
kubectl delete -f tp-processors/
sleep 5

echo ""
echo Stopping Trade Pilot Services......
kubectl delete -f tp-services/
sleep 5

echo ""
echo Stopping Spring Cloud Services......
kubectl delete -f tp-cloud/


