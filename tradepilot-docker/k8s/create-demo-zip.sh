#!/bin/bash

usage() { echo "Usage: $0  -f <zipfile>  " 1>&2; exit 1; }

while getopts ":f:" o; do
    case "${o}" in
        f)
            export ZIP_FILE_NAME=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if  [ -z "${ZIP_FILE_NAME}" ]; then
    usage
fi



pushd cockroach
zip -x.git* -r ${ZIP_FILE_NAME} .
zip -x../build* -uj ${ZIP_FILE_NAME} ../*-trade-pilot.sh
zip -uj ${ZIP_FILE_NAME} ../*-kafka.sh
zip -uj ${ZIP_FILE_NAME} ../get*.sh
zip -uj ${ZIP_FILE_NAME} ../patch*.sh

mv ${ZIP_FILE_NAME} ..
popd
unzip -l ${ZIP_FILE_NAME}
