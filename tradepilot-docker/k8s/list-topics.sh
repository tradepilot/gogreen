#!/bin/bash

ZOOKEEPER=$1
kubectl run topic-list --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --list -zookeeper ${ZOOKEEPER}:2181
