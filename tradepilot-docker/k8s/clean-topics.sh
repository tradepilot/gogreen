#!/bin/bash
ZOOKEEPER=$1

# Delete & (Re)Create the topics that the services use to communicate here
#
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic file-processor-input
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic file-validator-input
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic loan-input
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic payment-input
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic settlement-input
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic settlement-output-eu
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic settlement-output-us
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic settlement-output-ap
#kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --delete -zookeeper ${ZOOKEEPER}:2181 --topic monitor-data


kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic file-processor-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic file-validator-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic funding-request-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic loan-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic payment-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic settlement-input
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic settlement-output-eu
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic settlement-output-us
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic settlement-output-ap
kubectl run topic-setup --env="ALLOW_PLAINTEXT_LISTENER=yes" --env="KAFKA_ZOOKEEPER_CONNECT=${ZOOKEEPER}:2181" --image=dcdockerregistry.azurecr.io/tp-kafka:v1 --attach --rm -- kafka-topics.sh --create -zookeeper ${ZOOKEEPER}:2181 --replication-factor 1 --partitions 3 --topic monitor-data





