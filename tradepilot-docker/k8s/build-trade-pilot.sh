#!/bin/bash


usage() { echo "Usage: $0  -v <version> [-d <database>] " 1>&2; exit 1; }

while getopts ":d:v:" o; do
    case "${o}" in
        d)
            export DATABASE_NAME=${OPTARG}
            ;;
        v)
            export VERSION=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if  [ -z "${VERSION}" ]; then
    usage
fi


pushd ..
echo Building Spring Cloud Services......

./build-container.sh -s config-server -v $VERSION -p azure-k8s -r yes -u yes
./build-container.sh -s service-registry -v $VERSION -p azure-k8s -r yes -u yes

echo ""
echo Building Trade Pilot Services......
./build-container.sh -s funding-profile-service -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s loan-service -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes

./build-container.sh -s metrics-service -v $VERSION -p azure-k8s -r yes -u yes
./build-container.sh -s monitor-service -v $VERSION -p azure-k8s -r yes -u yes
./build-container.sh -s hystrix-service -v $VERSION -p azure-k8s -r yes -u yes
./build-container.sh -s ui -v $VERSION -p azure-k8s -r yes -u yes

echo ""
echo Building Trade Pilot Processors......
./build-container.sh -s file-processor -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s file-validator -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s funding-request-generator -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s loan-generator -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s payment-processor -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes
./build-container.sh -s routing-processor -v $VERSION -p azure-k8s -d $DATABASE_NAME -r yes -u yes

pushd ../tradepilot-settlement-processor
./build.sh azure-k8s $VERSION $DATABASE_NAME

popd 
popd



