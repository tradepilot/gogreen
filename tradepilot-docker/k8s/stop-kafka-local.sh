#!/bin/bash

echo Stopping Kafka Brokers......
kubectl delete -f kafka/local/tp-kafka-1-template.yaml
sleep 2

echo ""
echo Stopping Zookeeper......
kubectl delete -f kafka/local/tp-zookeeper-deployment.yaml
sleep 2

echo ""
echo Stopping k8s services......
kubectl delete -f kafka/local/tp-kafka-service.yaml
kubectl delete -f kafka/local/tp-zookeeper-service.yaml

echo ""
echo Running pods......
kubectl get pods

echo ""
echo Running services......
kubectl get svc


