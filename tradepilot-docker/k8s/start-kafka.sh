#!/bin/bash

echo Starting Zookeeper......
kubectl create -f kafka/tp-zookeeper-service.yaml
kubectl create -f kafka/tp-zookeeper-deployment.yaml
echo Waiting for Zookeeper......
sleep 2

echo ""
echo Starting Kafka services......
kubectl create -f kafka/tp-kafka-service.yaml
sleep 2

# We need to find the IP addresses that have been assigned to the services and
# replace the tokens in the template files to create the real deployment files
#
KAFKA_1_IP=`kubectl get service tp-kafka-1 -oyaml | grep clusterIP | awk '{print $2}'`
KAFKA_2_IP=`kubectl get service tp-kafka-2 -oyaml | grep clusterIP | awk '{print $2}'`

echo ""
echo Starting Kafka Broker 1 on $KAFKA_1_IP......
cat kafka/tp-kafka-1-template.yaml | sed "s/KAFKA_1_IP/$KAFKA_1_IP/g" > kafka/tp-kafka-1-deployment.yaml
kubectl create -f kafka/tp-kafka-1-deployment.yaml
echo Waiting for Kafka Broker 1.......
sleep 10

echo ""
echo Starting Kafka Broker 2 on $KAFKA_2_IP......
cat kafka/tp-kafka-2-template.yaml | sed "s/KAFKA_2_IP/$KAFKA_2_IP/g" > kafka/tp-kafka-2-deployment.yaml
kubectl create -f kafka/tp-kafka-2-deployment.yaml

echo ""
echo Starting Kafka Manager......
kubectl create -f kafka/tp-kafka-manager.yaml

echo ""
echo Running pods......
kubectl get pods

echo ""
echo Running services......
kubectl get svc

# Clean up
#
rm kafka/tp-kafka-1-deployment.yaml
rm kafka/tp-kafka-2-deployment.yaml

