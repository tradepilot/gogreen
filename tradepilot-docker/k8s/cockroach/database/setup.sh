#!/bin/bash
kubectl exec -i cockroachdb-0 -- ./cockroach.sh user set tradepilot --insecure
kubectl exec -i cockroachdb-0 -- ./cockroach sql --insecure < cockroach-setup.sql
