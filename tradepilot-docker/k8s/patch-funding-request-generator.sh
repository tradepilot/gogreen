#!/bin/bash

kubectl set image deployment tp-funding-request-generator tp-funding-request-generator=dcdockerregistry.azurecr.io/tp-funding-request-generator-cockroach:v2.0.1-patch --record=true
kubectl rollout status deployment tp-funding-request-generator
kubectl rollout history deployment tp-funding-request-generator
