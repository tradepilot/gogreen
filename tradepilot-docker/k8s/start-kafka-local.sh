#!/bin/bash

echo Starting Zookeeper......
kubectl create -f kafka/local/tp-zookeeper-service.yaml
kubectl create -f kafka/local/tp-zookeeper-deployment.yaml
echo Waiting for Zookeeper......
sleep 2

echo ""
echo Starting Kafka services......
kubectl create -f kafka/local/tp-kafka-service.yaml
sleep 2

# We need to find the IP addresses that have been assigned to the services and
# replace the tokens in the template files to create the real deployment files
#
KAFKA_1_IP=`kubectl get service tp-kafka-1 -oyaml | grep clusterIP | awk '{print $2}'`

echo ""
echo Starting Kafka Broker 1 on $KAFKA_1_IP......
cat kafka/local/tp-kafka-1-template.yaml | sed "s/KAFKA_1_IP/$KAFKA_1_IP/g" > kafka/local/tp-kafka-1-deployment.yaml
kubectl create -f kafka/local/tp-kafka-1-deployment.yaml
echo Waiting for Kafka Broker 1.......

echo ""
echo Running pods......
kubectl get pods

echo ""
echo Running services......
kubectl get svc

# Clean up
#
rm kafka/local/tp-kafka-1-deployment.yaml

