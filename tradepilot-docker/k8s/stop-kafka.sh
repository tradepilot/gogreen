#!/bin/bash

echo ""
echo Stopping Kafka Manager......
kubectl delete -f kafka/tp-kafka-manager.yaml


echo Stopping Kafka Brokers......
kubectl delete -f kafka/tp-kafka-1-template.yaml
kubectl delete -f kafka/tp-kafka-2-template.yaml
sleep 2

echo ""
echo Stopping Zookeeper......
kubectl delete -f kafka/tp-zookeeper-deployment.yaml
sleep 2

echo ""
echo Stopping k8s services......
kubectl delete -f kafka/tp-kafka-service.yaml
kubectl delete -f kafka/tp-zookeeper-service.yaml

echo ""
echo Running pods......
kubectl get pods

echo ""
echo Running services......
kubectl get svc


