#!/bin/bash


declare -a services=("config-server" "service-registry" "hystrix-service" "funding-profile-service" "loan-service" "file-processor" "file-validator" "funding-request-generator" "loan-generator" "payment-processor" "request-generator" "monitor-service" "ui" "routing-processor" "settlement-processor-eu" "settlement-processor-us" "settlement-processor-ap" )
declare -a versions=("3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3")

numservices=${#services[@]}
for (( i=0; i<${numservices}; i++ ));
do
    service=tp-${services[$i]}
    version=${versions[$i]}

    image=dcdockerregistry.azurecr.io/$service:v$version
    echo "Pulling " $image
    docker pull $image        
    echo " "
   
done

docker image ls


