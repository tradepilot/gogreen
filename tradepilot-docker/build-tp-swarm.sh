#!/bin/bash

if [ $# -ge 1 ]
then
    BUILD_PROFILE=$1
else
    BUILD_PROFILE=local-swarm
fi

if [ $# -ge 2 ]
then
    PUSH=yes
else
    PUSH=no
fi

declare -a services=("config-server" "service-registry" "hystrix-service" "funding-profile-service" "loan-service" "file-processor" "file-validator" "funding-request-generator" "loan-generator" "payment-processor" "request-generator" "monitor-service" "ui")
declare -a versions=("3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3" "3")

#declare -a services=("config-server" "service-registry" "file-processor" )
#declare -a versions=("2" "2" "2")

numservices=${#services[@]}


for (( i=0; i<${numservices}; i++ ));
do
    dir=tradepilot-${services[$i]}
    service=tp-${services[$i]}
    version=${versions[$i]}
   
    pushd ../$dir
    mvn clean package -Dmaven.test.skip=true
    docker build -t $service --build-arg SPRING_PROFILE=$BUILD_PROFILE -f Dockerfile .
    
    image=dcdockerregistry.azurecr.io/$service:v$version
    docker tag $service $image    
     
    if [ "$PUSH" == "yes" ]; then
        echo "Pushing image" $image
        docker push $image
    fi
    
    popd
    sleep 3
    echo " "
   
done




