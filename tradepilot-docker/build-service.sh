#!/bin/bash

if [ $# -ge 2 ]
then
    SERVICE_NAME=$1
    VERSION=$2
else
    echo "Usage $0 <service name> <version> <profile> <push>"
    exit 1
fi


if [ $# -ge 5 ]
then
    export REPLICAS=$5
else
    export REPLICAS=1
fi

. ./build-container.sh
. ../$SERVICE_DIR/port-config.sh
ENV_VARS="--env KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 --env SPRING_PROFILE=$BUILD_PROFILE"

echo Creating service $SERVICE with $REPLICAS replicas from image $IMAGE
docker service create --name $SERVICE --hostname $SERVICE $PUBLISHED_PORTS --network trade-pilot-swarm $ENV_VARS --replicas $REPLICAS $IMAGE
docker service logs -f $SERVICE


echo "Done. "









