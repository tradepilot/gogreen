#!/bin/sh

SERVER=$1
NETWORK=$2
TOPIC=$3

echo Listing Kafka topic data for $TOPIC on $SERVER on network $NETWORK.....
echo ""

echo ""
docker run -it --rm --network ${NETWORK} -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=${SERVER}:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-console-consumer.sh --zookeeper ${SERVER}:2181 --topic $TOPIC --from-beginning

echo ""
echo Done.

