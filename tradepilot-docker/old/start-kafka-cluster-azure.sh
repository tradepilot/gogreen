
echo Starting Zookeeper Server.....
docker start zookeeper-server-azure
echo ""

echo Waiting for Zookeeper Server.....
sleep 10
echo ""

echo Starting Kafka Server 1.....
docker start kafka-1-azure 
echo ""

echo Starting Kafka Server 2.....
docker start kafka-2-azure 

echo ""
docker ps

echo ""
echo Done.
