
echo Starting TradePilot Containers.....
docker start tp-file-validator tp-file-processor 
sleep 10

echo Starting TradePilot Request Generator.....
docker start tp-request-generator 

echo ""
docker ps

echo ""
echo Done.
