
echo Listing Kafka topics.....
echo ""

echo ""
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --list -zookeeper zookeeper-server-azure:2181
echo ""
echo Done.

