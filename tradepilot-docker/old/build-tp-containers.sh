#!/bin/bash

if [ $# -ge 1 ]
then
    PUSH=yes
else
    PUSH=no
fi

docker service rm tp-request-generator
docker service rm tp-file-processor
docker service rm tp-file-validator
docker service rm tp-funding-profile-service
docker service rm tp-funding-request-generator
docker service rm tp-service-registry
docker service rm tp-config-server

pushd ../tradepilot-request-generator
mvn clean package -Dmaven.test.skip=true
docker build -t tp-request-generator --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-request-generator dcdockerregistry.azurecr.io/tp-request-generator:v2
popd

pushd ../tradepilot-file-processor
mvn clean package -Dmaven.test.skip=true
docker build -t tp-file-processor --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-file-processor dcdockerregistry.azurecr.io/tp-file-processor:v2
popd

pushd ../tradepilot-file-validator
mvn clean package -Dmaven.test.skip=true
docker build -t tp-file-validator --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-file-validator dcdockerregistry.azurecr.io/tp-file-validator:v2
popd

pushd ../tradepilot-funding-profile-service
mvn clean package -Dmaven.test.skip=true
docker build -t tp-funding-profile-service --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-funding-profile-service dcdockerregistry.azurecr.io/tp-funding-profile-service:v2
popd

pushd ../tradepilot-funding-request-generator
mvn clean package -Dmaven.test.skip=true
docker build -t tp-funding-request-generator --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-funding-request-generator dcdockerregistry.azurecr.io/tp-funding-request-generator:v2
popd

pushd ../tradepilot-service-registry
mvn clean package -Dmaven.test.skip=true
docker build -t tp-service-registry --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-service-registry dcdockerregistry.azurecr.io/tp-service-registry:v2
popd

pushd ../tradepilot-config-server
mvn clean package -Dmaven.test.skip=true
docker build -t tp-config-server --build-arg SPRING_PROFILE=local -f Dockerfile .
docker tag tp-config-server dcdockerregistry.azurecr.io/tp-config-server:v2
popd


# Don't do this every build so we keep volumes down in Azure
#
if [ "$PUSH" == "yes" ]; then
  echo "Pushing images"
  docker push dcdockerregistry.azurecr.io/tp-request-generator:v2
  docker push dcdockerregistry.azurecr.io/tp-file-processor:v2
  docker push dcdockerregistry.azurecr.io/tp-file-validator:v2
  docker push dcdockerregistry.azurecr.io/tp-funding-profile-service:v2
  docker push dcdockerregistry.azurecr.io/tp-funding-request-generator:v2
  docker push dcdockerregistry.azurecr.io/tp-service-registry:v2
  docker push dcdockerregistry.azurecr.io/tp-config-server:v2
else  
  echo "NOT Pushing images"
fi


docker ps


