
echo Stopping Kafka Server 1.....
docker stop kafka-1 
echo ""

echo Stopping Kafka Server 2.....
docker stop kafka-2 
echo ""

echo Stopping Zookeeper Server.....
docker stop zookeeper-server

echo ""
echo Done.