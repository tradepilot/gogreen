
echo Starting Couchbase 5.0.0.....
docker start couchbase5-azure
echo ""

echo Waiting for Couchbase 5.0.0 Server.....
sleep 2

echo ""
docker ps

echo ""
echo Done.
