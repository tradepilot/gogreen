#!/bin/bash

if [ $# -ge 1 ]
then
    PUSH=yes
else
    PUSH=no
fi


docker service rm tp-swarm_tp-funding-profile-service
docker service rm tp-swarm_tp-funding-request-generator
docker service rm tp-swarm_tp-service-registry


pushd ../tradepilot-funding-profile-service
mvn clean package -Dmaven.test.skip=true
docker build -t tp-funding-profile-service-swarm --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .
docker tag tp-funding-profile-service-swarm dcdockerregistry.azurecr.io/tp-funding-profile-service-swarm:v2
popd

pushd ../tradepilot-service-registry
mvn clean package -Dmaven.test.skip=true
docker build -t tp-service-registry-swarm --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .
docker tag tp-service-registry-swarm dcdockerregistry.azurecr.io/tp-service-registry-swarm:v2
popd

pushd ../tradepilot-funding-request-generator
mvn clean package -Dmaven.test.skip=true
docker build -t tp-funding-request-generator-swarm --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .
docker tag tp-funding-request-generator-swarm dcdockerregistry.azurecr.io/tp-funding-request-generator-swarm:v2
popd


# Don't do this every build so we keep volumes down in Azure
#
if [ "$PUSH" == "yes" ]; then
  echo "Pushing images"
  docker push dcdockerregistry.azurecr.io/tp-funding-profile-service-swarm:v2
  docker push dcdockerregistry.azurecr.io/tp-funding-request-generator-swarm:v2
  docker push dcdockerregistry.azurecr.io/tp-service-registry-swarm:v2
else  
  echo "NOT Pushing images"
fi

docker stack deploy -c docker-compose-stage-2.yml tp-swarm
docker service ls


