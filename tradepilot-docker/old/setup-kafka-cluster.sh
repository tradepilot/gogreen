#!/bin/bash -x


# Delete any existing containers
#
docker rm zookeeper-server
docker rm kafka-1
docker rm kafka-2
docker network rm trade-pilot


# Create the trade pilot bridge network between the docker containers
#
docker network create trade-pilot --driver bridge


# Start a Zookeeper server on the trade pilot network
#
docker run -d -e ALLOW_ANONYMOUS_LOGIN=yes --name zookeeper-server --network trade-pilot bitnami/zookeeper:latest


# Start 2 Kafka brokers on the trade pilot network
#
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-1 --network trade-pilot -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server:2181 bitnami/kafka:latest
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-2 --network trade-pilot -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server:2181 bitnami/kafka:latest
sleep 15

# Create the topics that the services use to communicate here
#
docker run -it --rm --network trade-pilot -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server:2181 bitnami/kafka:latest kafka-topics.sh --create -zookeeper zookeeper-server:2181 --replication-factor 2 --partitions 4 --topic file-input

