
echo Stopping Kafka Server 1.....
docker stop kafka-1-azure 
echo ""

echo Stopping Kafka Server 2.....
docker stop kafka-2-azure 
echo ""

echo Stopping Zookeeper Server.....
docker stop zookeeper-server-azure

echo ""
echo Done.