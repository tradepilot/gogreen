
echo Starting Kafka REST service.....
docker start kafka-rest
echo ""

echo Waiting for Kafka REST service.....
sleep 10

echo Starting Kafka UI service.....
docker start kafka-ui
echo ""
sleep 2

echo ""
docker ps

echo ""
echo Done.
