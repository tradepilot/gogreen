#!/bin/bash 


# Delete any existing containers
#
docker rm zookeeper-server-azure
docker rm kafka-1-azure
docker rm kafka-2-azure
docker network rm trade-pilot-azure


# Create the trade pilot bridge network between the docker containers
#
docker network create trade-pilot-azure --driver bridge


# Start a Zookeeper server on the trade pilot network
#
docker run -d -e ALLOW_ANONYMOUS_LOGIN=yes --name zookeeper-server-azure --network trade-pilot-azure dcdockerregistry.azurecr.io/tp-zookeeper:v1


# Start 2 Kafka brokers on the trade pilot network
#
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-1-azure --network trade-pilot-azure -e KAFKA_DELETE_TOPIC_ENABLE=true -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-2-azure --network trade-pilot-azure -e KAFKA_DELETE_TOPIC_ENABLE=true -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1
sleep 15


# Create the topics that the services use to communicate here
#
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic file-input
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic file-output
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic funding-request-output
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic file-processor-input
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic file-validator-input
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-azure:2181 --topic funding-request-input

docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-azure:2181 --replication-factor 2 --partitions 3 --topic file-input
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-azure:2181 --replication-factor 2 --partitions 3 --topic file-output
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-azure:2181 --replication-factor 2 --partitions 3 --topic funding-request-input
docker run -it --rm --network trade-pilot-azure -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-azure:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-azure:2181 --replication-factor 2 --partitions 3 --topic funding-request-output



