
echo Stopping TradePilot Containers.....

docker stop tp-request-generator tp-file-processor tp-file-validator tp-funding-request-generator tp-funding-profile-service tp-loan-generator tp-loan-service tp-payment-processor tp-hystrix-service tp-service-registry tp-config-server
echo ""
docker ps

echo ""
echo Done.
