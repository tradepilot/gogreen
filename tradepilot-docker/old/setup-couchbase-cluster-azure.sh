
echo Couchbase 5.0.0.....
docker run -d --name couchbase5-azure --network trade-pilot-azure -p 8091-8094:8091-8094 -p 11210-11211:11210-11211 couchbase/server:5.0.0
echo ""

# Run these when the cluster is up
#
#CREATE INDEX idx_type ON tradepilot(type) USING GSI;
#CREATE INDEX idx_fileId ON tradepilot(fileId) USING GSI;
#CREATE INDEX idx_buyerId ON tradepilot(buyerId) USING GSI;

echo Waiting for Couchbase Server.....
sleep 2

echo ""
docker ps

echo ""
echo Done.
