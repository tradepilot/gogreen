#!/bin/sh

SERVER=$1
NETWORK=$2
BROKER=$3
TOPIC=$4

echo Sending data to Kafka topic $TOPIC on broker ${BROKER} with zookeeper $SERVER on network $NETWORK.....
echo ""

echo ""
docker run -it --rm --network ${NETWORK} -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=${SERVER}:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-console-producer.sh --broker-list ${BROKER}:9092 --topic $TOPIC

echo ""
echo Done.

