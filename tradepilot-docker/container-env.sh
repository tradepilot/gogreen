#!/bin/bash

export BUILD_PROFILE=local-swarm
export PUSH=no
export REBUILD=no
export DOCKERFILE=Dockerfile
export DATABASE=couchbase

usage() { echo "Usage: $0 -s <service name> -v <version> [-p <profile>] [-u <push>] [-r <rebuild>] [-f <dockerfile>] [-d <database>]" 1>&2; exit 1; }

while getopts ":s:v:p:u:r:f:d:" o; do
    case "${o}" in
        s)
            export SERVICE_NAME=${OPTARG}
            ;;
        v)
            export VERSION=${OPTARG}
            ;;
        p)
            export BUILD_PROFILE=${OPTARG}
            ;;
        u)
            export PUSH=${OPTARG}
            ;;
        r)
            export REBUILD=${OPTARG}
            ;;
        f)
            export DOCKERFILE=${OPTARG}
            ;;
        d)
            export DATABASE=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${SERVICE_NAME}" ] || [ -z "${VERSION}" ]; then
    usage
fi

if [ "$DATABASE" == "couchbase" ]; then
	export SERVICE=tp-$SERVICE_NAME
else
	export SERVICE=tp-$SERVICE_NAME-$DATABASE
fi

export SERVICE_DIR=tradepilot-$SERVICE_NAME
export IMAGE=dcdockerregistry.azurecr.io/$SERVICE:v$VERSION










