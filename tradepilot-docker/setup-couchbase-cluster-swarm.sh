
echo Setting up Couchbase 5.0.0.....
docker rm couchbase5-swarm
#docker run -d --name couchbase5-swarm --network trade-pilot-swarm -p 8091-8094:8091-8094 -p 11210-11211:11210-11211 couchbase/server:5.0.0
docker run -d --name couchbase5-swarm --network trade-pilot-swarm -p 8091-8094:8091-8094 -p 11210-11211:11210-11211 dcdockerregistry.azurecr.io/tp-couchbase5:v1
docker run -d --name couchbase5-swarm-1 --network trade-pilot-swarm  dcdockerregistry.azurecr.io/tp-couchbase5:v1


echo ""

# Run these when the cluster is up
#
#CREATE INDEX idx_type ON tradepilot(type) USING GSI;
#CREATE INDEX idx_fileId ON tradepilot(fileId) USING GSI;
#CREATE INDEX idx_buyerId ON tradepilot(buyerId) USING GSI;
#CREATE INDEX idx_class ON tradepilot(_class);
#CREATE INDEX idx_supplierId ON tradepilot(supplierId);

echo Waiting for Couchbase Server.....
sleep 2

echo ""
docker ps

echo ""
echo Done.
