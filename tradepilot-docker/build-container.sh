#!/bin/bash

export SERVICE_NAME=
export VERSION=
export BUILD_PROFILE=
export PUSH=
export REBUILD=
export DOCKERFILE=
export DATABASE=


. ./container-env.sh
echo "Building $SERVICE version $VERSION for profile $BUILD_PROFILE with push $PUSH for image $IMAGE for database $DATABASE"

docker rm $SERVICE
pushd ../$SERVICE_DIR

if [ "$REBUILD" == "yes" ]; then
    echo "Rebuilding service" $SERVICE
	mvn clean package -Dmaven.test.skip=true -Ddatabase.type=$DATABASE
else
	echo "Not rebuilding service"
fi

docker build -t $SERVICE --build-arg SPRING_PROFILE=$BUILD_PROFILE -f $DOCKERFILE .
docker tag $SERVICE $IMAGE    
 
if [ "$PUSH" == "yes" ]; then
    echo "Pushing image" $IMAGE
    docker push $IMAGE
else
	echo "Not pushing image"
fi

popd
echo "Build Complete. "









