#!/bin/sh

SERVER=$1
NETWORK=$2

echo Listing Kafka topics on $SERVER on network $NETWORK.....
echo ""

echo ""
docker run -it --rm --network ${NETWORK} -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=${SERVER}:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --list -zookeeper ${SERVER}:2181

echo ""
echo Done.

