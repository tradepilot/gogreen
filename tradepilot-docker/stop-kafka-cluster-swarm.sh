
echo Stopping Kafka Server 1.....
docker stop kafka-1-swarm 
echo ""

echo Stopping Kafka Server 2.....
docker stop kafka-2-swarm 
echo ""

echo Stopping Zookeeper Server.....
docker stop zookeeper-server-swarm

echo ""
echo Done.