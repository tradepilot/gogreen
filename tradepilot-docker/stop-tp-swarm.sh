#!/bin/sh


echo Stopping Trade Pilot Processors......
docker stack rm tp-swarm-procs
sleep 30

echo ""
echo Stopping Trade Pilot Services......
docker stack rm tp-swarm-svcs
sleep 30

echo ""
echo Stopping Spring Cloud Services......
docker stack rm tp-swarm-cloud
sleep 15

echo ""
docker stack ls
echo ""
echo ""
echo Done
