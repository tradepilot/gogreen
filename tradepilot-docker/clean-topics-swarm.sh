#!/bin/bash -x


# Delete & (Re)Create the topics that the services use to communicate here
#
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic file-processor-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic file-validator-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic funding-request-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic loan-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic payment-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic settlement-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic settlement-output-eu
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic settlement-output-us
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic settlement-output-ap
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic monitor-data

docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic file-processor-metrics
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --delete -zookeeper zookeeper-server-swarm:2181 --topic file-validator-metrics


docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic file-processor-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic file-validator-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic funding-request-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic loan-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic payment-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic settlement-input
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic settlement-output-eu
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic settlement-output-us
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 3 --topic settlement-output-ap
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 2 --topic monitor-data

docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 4 --topic file-processor-metrics
docker run -it --rm --network trade-pilot-swarm -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1 kafka-topics.sh --create -zookeeper zookeeper-server-swarm:2181 --replication-factor 2 --partitions 4 --topic file-validator-metrics






