#!/bin/sh

echo Starting Spring Cloud Services......
docker stack deploy -c docker-compose-cloud.yml tp-swarm-cloud
echo Waiting for Docker containers to start......
echo ""
sleep 90

echo Starting Trade Pilot Services......
docker stack deploy -c docker-compose-services.yml tp-swarm-svcs
echo Waiting for Docker containers to start......
echo ""
sleep 90

echo Starting Trade Pilot Processors......
docker stack deploy -c docker-compose-processors.yml tp-swarm-procs
echo Waiting for Docker containers to start......
echo ""
sleep 120

docker stack ls
echo ""
docker service ls
echo ""
echo ""
echo Done.

