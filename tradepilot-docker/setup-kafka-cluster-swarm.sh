#!/bin/bash -x


# Delete any existing containers
#
#docker rm zookeeper-server-swarm
#docker rm kafka-1-swarm
#docker rm kafka-2-swarm
#docker network rm trade-pilot-swarm


# Create the trade pilot bridge network between the docker containers
#
#docker network create --attachable -d overlay trade-pilot-swarm


# Start a Zookeeper server on the trade pilot network
#
docker run -d -e ALLOW_ANONYMOUS_LOGIN=yes --name zookeeper-server-swarm --network trade-pilot-swarm -p 2181:2181 -p 2888:2888 -p 3888:3888 dcdockerregistry.azurecr.io/tp-zookeeper:v1


# Start 2 Kafka brokers on the trade pilot network
#
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-1-swarm --network trade-pilot-swarm -p 9092:9092 -e KAFKA_DELETE_TOPIC_ENABLE=true -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1
docker run -d -e ALLOW_PLAINTEXT_LISTENER=yes --name kafka-2-swarm --network trade-pilot-swarm -p 9094:9092 -e KAFKA_DELETE_TOPIC_ENABLE=true -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 dcdockerregistry.azurecr.io/tp-kafka:v1
sleep 15


# Create the topics that the services use to communicate here
#
./clean-topics-swarm.sh




