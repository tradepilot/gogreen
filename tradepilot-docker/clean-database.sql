
select count(*) from `tradepilot` where `_class` = 'com.deltacapita.trade.pilot.core.data.types.File';
select count(*) from `tradepilot` where `_class` = 'com.deltacapita.trade.pilot.core.data.types.Instruction';
select count(*) from `tradepilot` where `_class` = 'com.deltacapita.trade.pilot.core.data.types.PaymentRequest';
select count(*) from `tradepilot` where `_class` = 'com.deltacapita.trade.pilot.core.data.types.Loan';

delete from `tradepilot` where `_class` in [
"com.deltacapita.trade.pilot.core.data.types.File",
"com.deltacapita.trade.pilot.core.data.types.Instruction",
"com.deltacapita.trade.pilot.core.data.types.PaymentRequest",
"com.deltacapita.trade.pilot.core.data.types.Loan"];

