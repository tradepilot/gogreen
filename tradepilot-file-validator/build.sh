#!/bin/sh

#docker stop tp-file-validator
docker rm tp-file-validator
mvn clean package -Dmaven.test.skip=true

docker build -t tp-file-validator --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-file-validator dcdockerregistry.azurecr.io/tp-file-validator:v1
#docker push dcdockerregistry.azurecr.io/tp-file-validator:v1

docker run --name tp-file-validator --hostname tp-file-validator -p 1210:1210 -p 9210:9210 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-file-validator

