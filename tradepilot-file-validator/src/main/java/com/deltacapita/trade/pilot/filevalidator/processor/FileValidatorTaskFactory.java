/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.filevalidator.processor;

import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;

@Component
public class FileValidatorTaskFactory implements TaskFactoryI {

    private static final Logger logger = LogManager.getLogger(FileValidatorTaskFactory.class);

    @Value("${processor.destination}")
    private String destination;
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;
    
    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;
    
    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised with destination={}, agentFactory={} ", destination, agentFactory);
    }
    
    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
        
        logger.info("Creating new task for " + request);
        return new FileValidatorTask(request, destination, fileRepository, instructionRepository, agentFactory);
    }
}
