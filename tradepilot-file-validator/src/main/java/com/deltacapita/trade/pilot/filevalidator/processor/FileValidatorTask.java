/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.filevalidator.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.INVOICE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PROCESSING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.REJECTED;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;

public class FileValidatorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(FileValidatorTask.class);

    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;
    
    private MonitoringAgentI<ProcessingRequestI, Object> instructionValidatedAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileOutAgent;
    
    public FileValidatorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        
        fileInAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.FILE_VALIDATOR_IN);
        instructionValidatedAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.INSTRUCTION_VALIDATED);
        fileOutAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.FILE_VALIDATOR_OUT);
    }

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);
        
        long start = System.currentTimeMillis();
        logger.info("Handling {}", request);        
        
        String fileId = request.getFileId();
        Optional<File> result = fileRepository.findById(fileId);
        if( result.isPresent() ) {
            
            File file = result.get();                   
            logger.info("Injecting {} to {}", request, fileInAgent);        
            fileInAgent.inject(request, file);
            
            Double totalValue = file.getTotalValue();
            Long numberOfInstructions = file.getNumberOfInstructions();
            String state = PROCESSING;
            
            List<Instruction> instructions = instructionRepository.findByFileId(fileId);
            if( instructions != null ) {
                
                logger.info("Validating instruction count for {}", fileId);
                int size = instructions.size();
                if( instructions.size() != numberOfInstructions.intValue() ) {
                    logger.warn("File validation failed for {} invalid instruction count - expected={}, actual={}", fileId, numberOfInstructions, size);
                    state = REJECTED;
                }
                
                logger.info("Validating control sum for {}", fileId);
                Double total = 0.0;
                for (Instruction instruction : instructions) {
                    
                    Double amount = instruction.getAmount();
                    String instructionType = instruction.getType();
                    
                    if( instructionType.equals(INVOICE)) {
                        total += amount;
                        instructionValidatedAgent.inject(request, instruction);
                    } else {
                        total -= amount;                        
                    }
                }
                
                // I know this won't work for irregular numbers but we don't have any as this is just a demo....
                //
                if( !totalValue.equals(total) ) {
                    logger.warn("Control Sum validation failed for {} invalid control sum - expected={}, actual={}", fileId, totalValue, total);  
                    state = REJECTED;
                } 
                
                file.setState(state);
                logger.info("Saving file {}", fileId);
                fileRepository.save(file);                
                fileOutAgent.inject(response, file);
                
            } else {
                logger.error("No instructions found for ", fileId );
            }
            
            long end = System.currentTimeMillis();
            logger.info("Validated {} instructions for {} in {} ms", file.getNumberOfInstructions(), fileId, end - start);  

        } else {
            logger.info("No entry found for {}", fileId);            
        }
                
        logger.info("Returning {}", response);        
        return response;
    }
}
