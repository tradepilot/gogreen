/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.deltacapita.trade.pilot.core.metrics.MetricsSource;
import com.deltacapita.trade.pilot.core.processor.receiver.KafkaReceiver;

@SpringBootApplication
@EnableDiscoveryClient
public class TradepilotFileValidatorApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotFileValidatorApplication.class);

    @Autowired
    private KafkaReceiver receiver;
   
    @Autowired
    private MetricsSource metricsSource;
    
    public static void main(String[] args) {
        
        try {
            
            SpringApplication.run(TradepilotFileValidatorApplication.class, args);

        } catch (Exception e) {
            logger.error("Error starting application", e);
        }
     }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Initialised with receiver {} and metrics source", receiver, metricsSource);
    }
}
