#!/bin/sh

#docker stop tp-hystrix-service
docker rm tp-hystrix-service
mvn clean package -Dmaven.test.skip=true 

docker build -t tp-hystrix-service --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-hystrix-service dcdockerregistry.azurecr.io/tp-hystrix-service:v2
#docker push dcdockerregistry.azurecr.io/tp-hystrix-service:v2

docker run --name tp-hystrix-service --hostname tp-hystrix-service -p 1120:1120 -p 9120:9120 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-hystrix-service

