#!/bin/sh

#docker stop tp-funding-profile-service
docker rm tp-funding-profile-service
mvn clean package -Dmaven.test.skip=true -U

docker build -t tp-funding-profile-service --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-funding-profile-service dcdockerregistry.azurecr.io/tp-funding-profile-service:v2
#docker push dcdockerregistry.azurecr.io/tp-funding-profile-service:v2

docker run --name tp-funding-profile-service --hostname tp-funding-profile-service -p 1300:1300 -p 9300:9300 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-funding-profile-service

