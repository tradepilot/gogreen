/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingprofile.service;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Useful for debugging the Hystrix stream in the docker environment
 * 
 * @author simonw
 *
 */
@RestController  
public class TestHystrixController {

    private static final Logger logger = LogManager.getLogger(TestHystrixController.class);

    @Autowired
    private ApplicationContext context;
    
 
    @PostConstruct
    public void initialise() throws Exception {
        logger.info("Initialised with context {}", context );      
    }

    @RequestMapping(value = "/hystrix/{server}/{port}/{path}", method = RequestMethod.GET, produces = "application/json")
    public String getFundingProfile(@PathVariable("server") String server, @PathVariable("port") String port, @PathVariable("path") String path) {
        
        try {

            logger.info("Calling service for server={}, port={}, path={}", server, port, path);
            URL url = new URL("http://" + server + ":" + port + "/actuator/" + path);
            logger.info("URL={}", url.toString());
            
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");  
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            
            int status = con.getResponseCode();
            logger.info("Connection status {}", status);

            
            int i = 0;
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
                i++;
                
                if( i > 10 ) {
                    break;
                }
            }
            in.close();

            String result = content.toString();
            logger.info("Returning result {}", result);
            return result;

        } catch (Exception e) {
            logger.warn("Service call failed {}", e);
        }
        
        return "Error";
    }
}
