/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.fundingprofile.service.FundingProfileService;

@SpringBootApplication
@EnableDiscoveryClient
public class FundingProfileServiceApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(FundingProfileServiceApplication.class);
    
    @Autowired
    private FundingProfileService service;
    
 
    public static void main(String[] args) {
        
        try {
            
            ServiceUtils.logIpAddresses();
            SpringApplication.run(FundingProfileServiceApplication.class, args);
            
        } catch (Throwable t) {
            logger.warn("Error initialising FundingProfileServiceApplication", t);
        }

    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Initialised with {}", service);
    }    
}
