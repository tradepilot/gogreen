/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingprofile.service;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DEFAULT;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deltacapita.trade.pilot.core.data.repo.FundingProfileRepository;
import com.deltacapita.trade.pilot.core.data.service.FundingProfileServiceI;
import com.deltacapita.trade.pilot.core.data.types.FundingProfile;

@RestController  
public class FundingProfileService implements FundingProfileServiceI {

    private static final Logger logger = LogManager.getLogger(FundingProfileService.class);
    
    @Autowired
    private FundingProfileRepository profileRepository;
    
    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void initialise() throws Exception {
        logger.info("Initialised with repository {} and context {}", profileRepository, context );      
    }
    
    @Override
    @RequestMapping(value = "/fundingprofile/{buyerId}/{supplierId}/{currency}", method = RequestMethod.GET, produces = "application/json")
    public FundingProfile getFundingProfile(@PathVariable("buyerId") String buyerId, @PathVariable("supplierId") String supplierId, @PathVariable("currency") String currency) {
        
        FundingProfile profile = findProfile(buyerId, supplierId, currency);
        if( profile == null ) {
            
            logger.info("Falling back to default currency for buyerId={}, supplierId={}", buyerId, supplierId);             
            profile = findProfile(buyerId, supplierId, DEFAULT);
            if( profile == null ) {
                
                logger.info("Falling back to default supplier for buyerId={}", buyerId);             
                profile = findProfile(buyerId, DEFAULT, DEFAULT);
                if( profile == null ) {
                    
                    logger.info("Falling back to default profile");             
                    profile = findProfile(DEFAULT, DEFAULT, DEFAULT);
                } 
            } 
        } 

        return profile;
    }
    
    
    private FundingProfile findProfile( String buyerId, String supplierId, String currency) {
        
        logger.info("Looking for funding profile for buyerId={}, supplierId={}, currency={}", buyerId, supplierId, currency);             
        if( profileRepository != null ) {
            
            List<FundingProfile> profiles = profileRepository.findByBuyerIdAndSupplierIdAndCurrency(buyerId, supplierId, currency);
            if( !profiles.isEmpty() ) {
                
                if( profiles.size() != 1 ) {
                    
                    logger.error("More than one profile found for buyerId={}, supplierId={}, currency={}" , buyerId, supplierId, currency);
                    return null;
                    
                } else {
                    
                    FundingProfile profile = profiles.get(0);
                    logger.info("Found {}" , profile);
                    return profile;
                }
            }
        }        

        return null;
    }
}
