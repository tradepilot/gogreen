/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.LoanProfile;

@Repository
@ViewIndexed(designDoc = "loanProfile", viewName = "all")
public interface LoanProfileRepository extends CouchbaseRepository<LoanProfile, String> {

    List<LoanProfile> findByBuyerIdAndCurrency(String buyerId, String currency);

}
