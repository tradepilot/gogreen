/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

/**
 * Represents a profile that defines the terms by which invoices
 * from a supplier to a buyer are funded before their due date
 * 
 * @author simonw
 *
 */
@Document
public class FundingProfile implements FundingProfileI {

    @Id
    private String id;

    @Field
    private String buyerId;

    @Field
    private String supplierId;

    @Field
    private String currency;

    @Field
    private Long maxTenor;

    @Field
    private Long maxValue;

    @Field
    private Long maxInvoiceValue;

    @Field
    private Double rate;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getMaxTenor() {
        return maxTenor;
    }

    public void setMaxTenor(Long maxTenor) {
        this.maxTenor = maxTenor;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public Long getMaxInvoiceValue() {
        return maxInvoiceValue;
    }

    public void setMaxInvoiceValue(Long maxInvoiceValue) {
        this.maxInvoiceValue = maxInvoiceValue;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FundingProfile other = (FundingProfile) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FundingProfile [" 
                + " id=" + id 
                + ", buyerId=" + buyerId 
                + ", supplierId=" + supplierId 
                + ", currency=" + currency 
                + ", maxTenor=" + maxTenor 
                + ", maxValue=" + maxValue 
                + ", maxInvoiceValue=" + maxInvoiceValue 
                + ", rate=" 
                + rate 
                + "]";
    }




}
