/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;


/**
 * Configuration class to get around incompatibilities between Spring Boot M7 & the
 * Spring Data Couchbase support in 3.0.2
 * 
 * @author simonw
 *
 */
@Configuration
@EnableCouchbaseRepositories
public class TradepilotCouchbaseConfiguration extends AbstractCouchbaseConfiguration {

    private static final Logger logger = LogManager.getLogger(TradepilotCouchbaseConfiguration.class);

    @Value("${spring.couchbase.bucket.name}")
    private String bucketName;

    @Value("${spring.couchbase.bucket.password}")
    private String password;

    @Value("${spring.couchbase.bootstrap-hosts}")
    private List<String> hosts;
    
    // Call this something else to avoid the type conflict
    @Value("${spring.couchbase.env.timeouts.connectMs}")
    private Long connectTimeoutMs;

    // Call this something else to avoid the type conflict
    @Value("${spring.couchbase.env.timeouts.socket-connectMs}")
    private Integer socketConnectTimeoutMs;


    @Override
    protected List<String> getBootstrapHosts() {
        return this.hosts;
    }

    @Override
    protected String getBucketName() {
        return this.bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return this.password;
    }
    

    /**
     * Construct the environment specifically to avoid type clashes
     * between what M7 provides and what the Spring Data classes expect
     */
    @Override
    protected CouchbaseEnvironment getEnvironment() {
              
       logger.info(this);
        
       return DefaultCouchbaseEnvironment.builder()
          .connectTimeout(connectTimeoutMs)
          .socketConnectTimeout(socketConnectTimeoutMs)
          .build();
    }

    @Override
    public String toString() {
        return "TradepilotCouchbaseConfiguration [bucketName=" + bucketName 
                + ", password=" + password 
                + ", hosts=" + hosts 
                + ", connectTimeoutMs=" + connectTimeoutMs 
                + ", socketConnectTimeoutMs=" + socketConnectTimeoutMs 
                + "]";
    }
}
