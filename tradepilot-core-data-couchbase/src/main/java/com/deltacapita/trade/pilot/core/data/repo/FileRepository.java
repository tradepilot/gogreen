/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.types.File;

@Component
@ViewIndexed(designDoc = "file", viewName = "all")
public interface FileRepository extends CouchbaseRepository<File, String> {

    List<File> findByBuyerId(String buyerId);

    List<File> findByRequestId(String requestId);
}
