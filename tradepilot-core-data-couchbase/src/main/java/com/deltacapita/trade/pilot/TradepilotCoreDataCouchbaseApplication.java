/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradepilotCoreDataCouchbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradepilotCoreDataCouchbaseApplication.class, args);
	}
}
