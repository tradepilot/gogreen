/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

public class ConnectionGraphDetails {

    private boolean update;
    
    private ConnectionGraph elements;

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public ConnectionGraph getElements() {
        return elements;
    }

    public void setElements(ConnectionGraph elements) {
        this.elements = elements;
    }
}
