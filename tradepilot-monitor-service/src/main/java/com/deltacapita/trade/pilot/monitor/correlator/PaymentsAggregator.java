/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_EXECUTED;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.service.DetailBuilder;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndCountryDetails;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Component
public class PaymentsAggregator {

    private static final Logger logger = LoggerFactory.getLogger(PaymentsAggregator.class);

    @Value("${correlator.cache.lease}")
    private Long itemLease;

    @Value("${correlator.cache.maxSize}")
    private Long maxSize;

    @Autowired
    private DetailBuilder detailBuilder;
    
    private LoadingCache<String, PaymentAndCountryDetails> itemCache;

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising with cache lease {} and max size {}", itemLease, maxSize);        
        itemCache = newCache();
    }
    
    public void aggregate( List<MonitoringEventI> events ) {
               
        logger.debug("Aggregating {} events ", events.size());
        for (MonitoringEventI event : events) {
            
            Map<String, Object> data = event.getData();            
            String buyerId = (String) data.get(MonitoringConstants.BUYER_ID);
            String supplierId = (String) data.get(MonitoringConstants.SUPPLIER_ID);
            Double amount = (Double) data.get(MonitoringConstants.AMOUNT);
            String currency = (String) data.get(MonitoringConstants.CURRENCY);
            String state = (String) data.get(MonitoringConstants.STATE);
            boolean complete = state.equals(PAYMENT_EXECUTED);
            logger.debug("Processing event {} in state {}", event.getCorrelationId(), state);

            String openKey = buyerId + "-" + supplierId + "-" + currency + "-OPEN" ;
            String completeKey = buyerId + "-" + supplierId + "-" + currency + "-COMPLETE" ;
            
            PaymentAndCountryDetails openDetails = itemCache.getIfPresent(openKey);
            PaymentAndCountryDetails completeDetails = itemCache.getIfPresent(completeKey);
            
            if( complete ) {
                
                if( openDetails != null ) {
                    
                    Double existingAmount = openDetails.getAmount();
                    Double newAmount = Math.abs(existingAmount - amount);
                    
                    openDetails.setAmount(newAmount);
                    itemCache.put(openKey, openDetails);
                }
                
                if( completeDetails != null ) {
                    
                    Double existingAmount = completeDetails.getAmount();
                    Double newAmount = existingAmount + amount;
                    
                    completeDetails.setAmount(newAmount);
                    itemCache.put(completeKey, completeDetails);
                    
                } else {
                 
                    PaymentAndCountryDetails details = new PaymentAndCountryDetails();
                    details.setAmount(amount);
                    details.setCurrency(currency);
                    
                    String country = detailBuilder.getCountry(supplierId);
                    String city = detailBuilder.getCity(supplierId);
                    String clientName = detailBuilder.getClientName(supplierId);
                    
                    details.setClient(clientName);
                    details.setCountry(country);
                    details.setCity(city);
                    details.setComplete(true);
                    itemCache.put(openKey, details);
                }
                
            } else {
                               
                if( openDetails != null ) {
                    
                    Double existingAmount = openDetails.getAmount();
                    Double newAmount = Math.abs(existingAmount + amount);
                    
                    openDetails.setAmount(newAmount);
                    itemCache.put(openKey, openDetails);
                    
                } else {
                    
                    PaymentAndCountryDetails details = new PaymentAndCountryDetails();
                    details.setAmount(amount);
                    details.setCurrency(currency);
                    
                    String country = detailBuilder.getCountry(supplierId);
                    String city = detailBuilder.getCity(supplierId);
                    String clientName = detailBuilder.getClientName(supplierId);
                    
                    details.setClient(clientName);
                    details.setCountry(country);
                    details.setCity(city);
                    details.setComplete(false);
                    itemCache.put(openKey, details);
                }
            }           
        }
    }
    
    private LoadingCache<String, PaymentAndCountryDetails> newCache() {
        
        return CacheBuilder.newBuilder()
            .expireAfterAccess(itemLease, TimeUnit.MILLISECONDS)
            .maximumSize(maxSize)
            .build(new CacheLoader<String, PaymentAndCountryDetails>() {
                
                public PaymentAndCountryDetails load(String correlationId) {

                    PaymentAndCountryDetails details = new PaymentAndCountryDetails();
                    return details;
                }
            }
        );
    }
    
    public Collection<PaymentAndCountryDetails> getAllPayments() {
        
        ConcurrentMap<String, PaymentAndCountryDetails> map = itemCache.asMap();
        return map.values();
    }

}
