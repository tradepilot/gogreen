/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;


public class AgentEventDetails {

    private String id;

    private String agentId;
    
    private String agentName;

    private Long timestampMillis;

    @JsonFormat(pattern = "HH:mm:ss.SSS")
    private LocalDateTime timestamp;

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String value) {
        agentId = value;
    }

    /**
     * @return the timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(LocalDateTime value) {
        timestamp = value;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
    
    public Long getTimestampMillis() {
        return timestampMillis;
    }

    public void setTimestampMillis(Long timestampMillis) {
        this.timestampMillis = timestampMillis;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        AgentEventDetails other = (AgentEventDetails) obj;
        if (id == null) {
            
            if (other.id != null) {
                return false;
            }
            
        } else if (!id.equals(other.id)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "AgentEventDetails [id=" + id + ", agentId=" + agentId + ", agentName=" + agentName
                + ", timestampMillis=" + timestampMillis + ", timestamp=" + timestamp + "]";
    }


}