/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.service;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.COMPLETE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.NEW;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENTS_EXECUTING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PAYMENTS_PROCESSING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PROCESSING;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.REJECTED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_COMPLETE;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_PROCESSOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_PROCESSOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_VALIDATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_VALIDATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUESTED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_COMPLETE;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_VALIDATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_GENERATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_GENERATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_SAVED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_EXECUTED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_PROCESSOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_PROCESSOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_SAVED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.SETTLEMENT_PROCESSOR_AP;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.SETTLEMENT_PROCESSOR_EU;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.SETTLEMENT_PROCESSOR_US;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.AMOUNT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.BUYER_ID;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.MATURITY_DATE;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.NO_OF_INSTRUCTIONS;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.OUTSTANDING_PAYMENT_REQUESTS;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.RECEIVED_DATE_TIME;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.STATE;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.MonitorConfig;
import com.deltacapita.trade.pilot.Organisation;
import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.types.AgentEventDetails;
import com.deltacapita.trade.pilot.monitor.types.FileHistory;
import com.deltacapita.trade.pilot.monitor.types.ItemHistory;


@Component
public class DetailBuilder {   

    private static final Logger logger = LogManager.getLogger(DetailBuilder.class);
    
    private Map<String,String> agentDisplayNames = new HashMap<>();;

    private Map<String,String> stateDisplayNames = new HashMap<>();;

    @Autowired
    private MonitorConfig serviceConfig;
    
    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising ");       

        agentDisplayNames.put(FILE_PROCESSOR_IN, "File In");
        agentDisplayNames.put(FILE_PROCESSOR_OUT, "File Out");
               
        agentDisplayNames.put(FILE_VALIDATOR_IN, "Validator In");
        agentDisplayNames.put(FILE_VALIDATOR_OUT, "Validator Out");
        
        agentDisplayNames.put(FUNDING_REQUEST_GENERATOR_IN, "Funding In");
        agentDisplayNames.put(FUNDING_REQUEST_GENERATOR_OUT, "Funding Out");
        
        agentDisplayNames.put(LOAN_GENERATOR_IN, "Loan In");
        agentDisplayNames.put(LOAN_GENERATOR_OUT, "Loan Out");
        
        agentDisplayNames.put(PAYMENT_PROCESSOR_IN, "Payments In");
        agentDisplayNames.put(PAYMENT_PROCESSOR_OUT, "Payments Out");

        agentDisplayNames.put(FILE_COMPLETE, "Complete");       
        agentDisplayNames.put(INSTRUCTION_COMPLETE, "Complete");
        
        agentDisplayNames.put(INSTRUCTION_CREATED, "Created");
        agentDisplayNames.put(INSTRUCTION_VALIDATED, "Processing");
        agentDisplayNames.put(FUNDING_REQUESTED, "Funding");
       
        agentDisplayNames.put(PAYMENT_CREATED, "Created");
        agentDisplayNames.put(PAYMENT_SAVED, "Pending");
        agentDisplayNames.put(PAYMENT_EXECUTED, "Executed");
        
        agentDisplayNames.put(LOAN_CREATED, "Created");
        agentDisplayNames.put(LOAN_SAVED, "Saved");       

        agentDisplayNames.put(SETTLEMENT_PROCESSOR_EU, "Settlement EU");    
        agentDisplayNames.put(SETTLEMENT_PROCESSOR_US, "Settlement US");    
        agentDisplayNames.put(SETTLEMENT_PROCESSOR_AP, "Settlement AP");            
       
        stateDisplayNames.put(NEW, "New");    
        stateDisplayNames.put(PROCESSING, "Processing");    
        stateDisplayNames.put(REJECTED, "Rejected");    
        stateDisplayNames.put(FUNDING_REQUESTED, "Funding Requested");    
        stateDisplayNames.put(LOAN_CREATED, "Awaiting Payment");    
        stateDisplayNames.put(PAYMENTS_PROCESSING, "Payments Pending");    
        stateDisplayNames.put(PAYMENTS_EXECUTING, "Payments in Progress");    
        stateDisplayNames.put(COMPLETE, "Complete");    
    }
    
  
    public FileHistory buildFileHistory(ItemHistory item) {
        
        FileHistory history = new FileHistory();        
        Map<String, Object> data = item.getData();        
        history.setFileId(item.getCorrelationId());
               
        try {
            
            String buyerId = (String) data.get(BUYER_ID);        
            history.setBuyerId(buyerId);
            
            String clientName = getClientName(buyerId);
            history.setBuyerName(clientName);
            
            String country = getCountry(buyerId);
            history.setCountry(country);   
            
            history.setTotalValue((Double) data.get(AMOUNT));
            
            String lastAgent = getAgentDisplayName(item.getLastAgentPoint());
            String stateName = getStateDisplayName((String) data.get(STATE));

            history.setState(stateName);
            history.setLastAgentPoint(lastAgent);
                
            Long lastAgentTimestampMillis = item.getLastAgentTimestamp();
            if( lastAgentTimestampMillis != null ) {
                history.setLastAgentTimestampMillis(lastAgentTimestampMillis);
                history.setLastAgentTimestamp(ServiceUtils.fromEpochMillis(lastAgentTimestampMillis));                        
            }
                
            Integer num = (Integer) data.get(NO_OF_INSTRUCTIONS);
            if( num != null ) {
                history.setNumberOfInstructions(num.longValue());
            }
            
            num = (Integer) data.get(OUTSTANDING_PAYMENT_REQUESTS);
            if( num != null ) {
                history.setOutstandingPaymentRequests(num.longValue());        
            }
                        
            Number matDate = (Number) data.get(MATURITY_DATE);  
            if( matDate != null ) {
                LocalDate maturityDate = LocalDate.ofEpochDay(matDate.longValue());
                history.setMaturityDate(maturityDate);                
            }
            
            Number recDateTime = (Number) data.get(RECEIVED_DATE_TIME);
            if( recDateTime != null ) {
                LocalDateTime receivedDateTime = ServiceUtils.fromEpochMillis(recDateTime.longValue());
                history.setReceivedDateTime(receivedDateTime);           
            }
            
            List<MonitoringEventI> events = item.getEvents();
            List<AgentEventDetails> chain = buildChain(events);
            
            history.setChain(chain);        
        
        } catch (Exception e) {
            
            logger.error("Unable to build file history for {}", item.getCorrelationId(), e);
        }
        
        return history;
    }
    
    
    private List<AgentEventDetails> buildChain( List<MonitoringEventI> events ) {
        
        List<AgentEventDetails> chain = new ArrayList<>(events.size());
        for (MonitoringEventI event : events) {
            
            AgentEventDetails details = new AgentEventDetails();
            Long timestampMillis = event.getTimestamp();
            LocalDateTime time = ServiceUtils.fromEpochMillis(timestampMillis);

            details.setId(event.getCorrelationId() + "-" + event.getAgentId());
            details.setAgentId(event.getAgentId());
            
            String displayName = getAgentDisplayName(event.getAgentName());
            details.setAgentName(displayName); 
            
            details.setTimestamp(time);
            details.setTimestampMillis(timestampMillis);
            
            chain.add(details);
        }               

        return chain;
    }
    
    
    public String getCountry( String id ) {
        
        logger.debug("Getting country for {}", id);
        if( id != null && id.length() >= 1) {

            String org = id.substring(1);
            Organisation organisation = serviceConfig.getOrganisationsMap().get(org);
            if( organisation != null ) {
                return organisation.getCountry();
            }
        }
        
        return "United Kingdom";
    }

    public String getCity( String id ) {
        
        logger.debug("Getting city for {}", id);
        if( id != null && id.length() >= 1) {

            String org = id.substring(1);
            Organisation organisation = serviceConfig.getOrganisationsMap().get(org);
            if( organisation != null ) {
                return organisation.getCity();
            }
        }
        
        return "London";
    }

    public String getClientName( String id ) {
        
        //logger.debug("Getting client for {}", id);
        if( id != null && id.length() >= 1) {

            String org = id.substring(1);
            Organisation organisation = serviceConfig.getOrganisationsMap().get(org);
            if( organisation != null ) {
                return organisation.getOrgName();
            }
        }
        
        return "Unknown";
    }

    
    public String getAgentDisplayName( String name ) {
        
        if( name != null ) {
            
            String displayName = agentDisplayNames.get(name);
            if( displayName != null ) {
                return displayName;
            }
        }
        
        return name;
    }


    public String getStateDisplayName( String name ) {
        
        if( name != null ) {
            
            String displayName = stateDisplayNames.get(name);
            if( displayName != null ) {
                return displayName;
            }
        }
    
        return name;
    }

    
    public void setServiceConfig(MonitorConfig serviceConfig) {
        this.serviceConfig = serviceConfig;
    }
}


