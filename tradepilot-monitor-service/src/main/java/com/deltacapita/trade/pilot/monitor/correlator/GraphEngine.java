/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.CONTAINER_ID;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.NAME;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.SOURCE;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.TARGET;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.WEIGHT;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.X;
import static com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.Y;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphEdge;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphNode;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraphDetails;
import com.deltacapita.trade.pilot.monitor.types.ContainerDetails;
import com.deltacapita.trade.pilot.monitor.types.ItemHistory;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalCause;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

@Component
public class GraphEngine {

    private static final String CONTAINER = "Container";

    private static final Logger logger = LoggerFactory.getLogger(GraphEngine.class);

    @Value("${correlator.graphEngine.lease}")
    private Long itemLease = 30000L;

    @Value("${correlator.graphEngine.maxSize}")
    private Long maxSize = 1000L;
    
    @Value("${correlator.graphEngine.testProp}")
    private String testProp;

   // @Value("${correlator.graphEngine.fileAgentList}")
    private List<String> fileAgents;
    
    private Map<String,Integer> fileAgentPositions = new HashMap<>();

    private Map<String,String> displayNames = new HashMap<>();

    private Map<String,String> serviceNames = new HashMap<>();

    private Map<String,String> intervals = new HashMap<>();
    
    private Map<String,Integer> pinnedNodes = new HashMap<>();
    
    private LoadingCache<String, GraphNode> nodes;

    private LoadingCache<String, GraphEdge> edges;
    
    private ConnectionGraphDetails details;
    
    
    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising with node lease {} and max size {}", itemLease, maxSize);        

        nodes = newCache(new GraphNode());
        edges = newCache(new GraphEdge());
        
        fileAgents = new ArrayList<>();
        fileAgents.add("File In");
        fileAgents.add("File Out");
        fileAgents.add("Validate In");
        fileAgents.add("Validate Out");
        fileAgents.add("Funding In");
        fileAgents.add("Funding Out");
        fileAgents.add("Loan In");
        fileAgents.add("Loan Out");
        fileAgents.add("Payment In");
        fileAgents.add("Payment Out");
        
        int x = 50;
        for (String agent : fileAgents) {
            fileAgentPositions.put(agent, x);
            x += 50;
        }

        fileAgentPositions.put("Settlement EU", x);
        fileAgentPositions.put("Settlement US", x);
        fileAgentPositions.put("Settlement AP", x);      
        x += 50;
        fileAgentPositions.put("Complete", x);       
        
        pinnedNodes.put("Settlement EU", 50);
        pinnedNodes.put("Settlement US", 100);
        pinnedNodes.put("Settlement AP", 150);
       
        displayNames = new HashMap<>();
        displayNames.put("FILE_PROCESSOR_IN","File In");
        displayNames.put("FILE_PROCESSOR_OUT","File Out");
        displayNames.put("FILE_VALIDATOR_IN","Validate In");
        displayNames.put("FILE_VALIDATOR_OUT","Validate Out");
        displayNames.put("FUNDING_REQUEST_GENERATOR_IN","Funding In");
        displayNames.put("FUNDING_REQUEST_GENERATOR_OUT","Funding Out");
        displayNames.put("LOAN_GENERATOR_IN","Loan In");
        displayNames.put("LOAN_GENERATOR_OUT","Loan Out");
        displayNames.put("PAYMENT_PROCESSOR_IN","Payment In");
        displayNames.put("PAYMENT_PROCESSOR_OUT","Payment Out");
        displayNames.put("SETTLEMENT_PROCESSOR_EU","Settlement EU");
        displayNames.put("SETTLEMENT_PROCESSOR_US","Settlement US");
        displayNames.put("SETTLEMENT_PROCESSOR_AP","Settlement AP");
        displayNames.put("FILE_COMPLETE","Complete");
        
        serviceNames = new HashMap<>();
        serviceNames.put("FILE_PROCESSOR_IN","File Processor");
        serviceNames.put("FILE_PROCESSOR_OUT","File Processor");
        serviceNames.put("FILE_VALIDATOR_IN","File Validator");
        serviceNames.put("FILE_VALIDATOR_OUT","File Validator");
        serviceNames.put("FUNDING_REQUEST_GENERATOR_IN","Funding Request Generator");
        serviceNames.put("FUNDING_REQUEST_GENERATOR_OUT","Funding Request Generator");
        serviceNames.put("LOAN_GENERATOR_IN","Loan Generator");
        serviceNames.put("LOAN_GENERATOR_OUT","Loan Generator");
        serviceNames.put("PAYMENT_PROCESSOR_IN","Payment Processor");
        serviceNames.put("PAYMENT_PROCESSOR_OUT","Payment Processor");
        serviceNames.put("SETTLEMENT_PROCESSOR_EU","Settlement Processor EU");
        serviceNames.put("SETTLEMENT_PROCESSOR_US","Settlement Processor US");
        serviceNames.put("SETTLEMENT_PROCESSOR_AP","Settlement Processor AP");
        serviceNames.put("FILE_COMPLETE","Complete");

        intervals = new HashMap<>();
        intervals.put("FILE_PROCESSOR_OUT","FILE_PROCESSOR_IN");
        intervals.put("FILE_VALIDATOR_IN","FILE_PROCESSOR_OUT");
        intervals.put("FILE_VALIDATOR_OUT","FILE_VALIDATOR_IN");
        intervals.put("FUNDING_REQUEST_GENERATOR_IN","FILE_VALIDATOR_OUT");
        intervals.put("FUNDING_REQUEST_GENERATOR_OUT","FUNDING_REQUEST_GENERATOR_IN");
        intervals.put("LOAN_GENERATOR_IN","FUNDING_REQUEST_GENERATOR_OUT");
        intervals.put("LOAN_GENERATOR_OUT","LOAN_GENERATOR_IN");
        intervals.put("PAYMENT_PROCESSOR_IN","LOAN_GENERATOR_OUT");
        intervals.put("PAYMENT_PROCESSOR_OUT","PAYMENT_PROCESSOR_IN");
        intervals.put("SETTLEMENT_PROCESSOR_EU","PAYMENT_PROCESSOR_OUT");
        intervals.put("SETTLEMENT_PROCESSOR_US","PAYMENT_PROCESSOR_OUT");
        intervals.put("SETTLEMENT_PROCESSOR_AP","PAYMENT_PROCESSOR_OUT");
        intervals.put("FILE_COMPLETE","SETTLEMENT_PROCESSOR_EU");                 
    }
    
    
    public void update( List<ItemHistory> histories ) {
        
        logger.debug("Processing {} events ", histories.size());
        cleanUp();
        
        for (ItemHistory history : histories) {
                       
            List<MonitoringEventI> events = history.getEvents();
            for (MonitoringEventI event : events) {
                
                String agentId = event.getAgentId();
                String agentDisplayName = getDisplayName(event.getAgentName());                
                
                GraphNode node = new GraphNode();
                node.setId(agentId);
                node.setTimestamp(event.getTimestamp());
                node.getData().put(NAME, agentDisplayName); 
                node.setId(agentId);
                
                Map<String, Object> eventData = event.getData();
                Object containerId = eventData.get(CONTAINER_ID);
                node.getProperties().put(CONTAINER, containerId);

                GraphNode existingNode = nodes.getIfPresent(agentId);
                if( existingNode == null ) {
                    logger.info("Adding node {} ", node);
                } else {
                    logger.debug("Updating node {} ", node);    
                }       
                
                nodes.put(agentId, node);   
                createIntervals(events, event);                
            }        
        }  
    }

    private String getDisplayName( String name ) {
        
        if( name != null ) {
            String agentName = displayNames.get(name);
            if( agentName != null ) { 
                return agentName;
            }
        }

        return name;
    }
    
    private <T> LoadingCache<String, T> newCache(T object) {
        
        return CacheBuilder.newBuilder()
            .expireAfterWrite(itemLease, TimeUnit.MILLISECONDS)
            .maximumSize(maxSize)
            .removalListener(new RemovalListener<String, T>() {

                @Override
                public void onRemoval(RemovalNotification<String, T> notification) {
                    
                    RemovalCause cause = notification.getCause();
                    if( cause != RemovalCause.REPLACED ) {                        
                        logger.info("Graph updated due to item removal {} for {}" , cause, notification.getValue());   
                    }
                }
            })
            .build(new CacheLoader<String, T>() {
                
                public T load(String id) {
                    return object;
                }
            }
        );
    }


    private void createIntervals( List<MonitoringEventI> events, MonitoringEventI event ) {
        
        String agentId = event.getAgentId();
        String agentName = event.getAgentName();        
        String upstreamAgentName = intervals.get(agentName);

        if( upstreamAgentName != null ) {
            
            for (MonitoringEventI otherEvent : events) {
                
                String otherAgentName = otherEvent.getAgentName();
                if( otherAgentName.equals(upstreamAgentName) ) {
                    
                    String upstreamAgentId = otherEvent.getAgentId();                                            
                    String id = upstreamAgentId + "-" + agentId;
    
                    GraphEdge edge = new GraphEdge();
                    edge.setId(id);
                    
                    edge.getData().put(SOURCE, upstreamAgentId);
                    edge.getData().put(TARGET, agentId);
                    edge.getData().put(WEIGHT, 1);
                                                         
                    logger.debug("Adding edge {} ", id);        
                    edges.put(id, edge);                         
                }
            }
        }
    }

    
    public ConnectionGraphDetails getGraphDetails() {
        
        return buildGraphDetails();    
    }
    
    public List<ContainerDetails> getContainerDetails() {
              
        Set<ContainerDetails> detailSet = new HashSet<>();        
        Collection<GraphNode> nodeValues = nodes.asMap().values();
        
        for (GraphNode graphNode : nodeValues) {
            
            String id = graphNode.getId();
            if( id != null ) {
                
                // id=LOAN_GENERATOR_IN-559a74c528e4-10.0.0.47
                String [] parts = id.split("-");
                if( parts != null && parts.length == 3) {
                    
                    String agent = parts[0];
                    String container = parts[1];
                    String host = parts[2];
                    String detailId = container + "-" + host;
                    String serviceName = serviceNames.get(agent);
                    
                    ContainerDetails details = new ContainerDetails();
                    details.setId(detailId);
                    details.setContainer(container);
                    details.setHost(host);
                    details.setServiceName(serviceName);
                    details.setState("Running");
                    
                    detailSet.add(details);
                }
            }
        }
        
        List<ContainerDetails> list = new ArrayList<>(detailSet);
        list.sort(new Comparator<ContainerDetails>() {

            @Override
            public int compare(ContainerDetails o1, ContainerDetails o2) {
                
                return o1.getContainer().compareTo(o2.getContainer());
            }
        });
        
        return list;
    }
    
    private void cleanUp() {
        
        nodes.cleanUp();
        edges.cleanUp();       
    }

    public ConnectionGraphDetails buildGraphDetails() {
      
        cleanUp();
        int rows = 1;
        Map<String, List<GraphNode>> nodesByColumn = new HashMap<>();
        Map<Long, List<GraphNode>> sortedNodes = new TreeMap<>();
        ConcurrentMap<String, GraphNode> nodeMap = nodes.asMap();
        Collection<GraphNode> allNodes = nodeMap.values();       

        // Split nodes into columns based on name, with multiple agent IDs for the same name
        // in the same column
        //
        for (GraphNode node : allNodes) {
            
            String nodeName = (String) node.getData().get(NAME);
            List<GraphNode> nodeList = nodesByColumn.get(nodeName);
            
            if( nodeList == null ) {
                nodeList = new ArrayList<>();
                nodesByColumn.put(nodeName, nodeList);
            }
            
            nodeList.add(node);
            int nodeCount = nodeList.size();
            if( nodeCount > rows ) { 
                rows = nodeCount;
            }
        }

        // Now sort the nodes in time order
        //
        for (Entry<String, List<GraphNode>> entry : nodesByColumn.entrySet()) {
            
            List<GraphNode> nodes = entry.getValue();
            nodes.sort(new Comparator<GraphNode>() {

                @Override
                public int compare(GraphNode o1, GraphNode o2) {
                    
                    Long ts1 = o1.getTimestamp();
                    Long ts2 = o2.getTimestamp();
                    
                    return ts1.compareTo(ts2);
                }
            });

            GraphNode node = nodes.get(0);
            long timestamp = node.getTimestamp();
            sortedNodes.put(timestamp, nodes);
        }
    
       
        List<GraphNode> nodeList = new ArrayList<>();
        for (Long key : sortedNodes.keySet()) {
                        
            List<GraphNode> nodes = sortedNodes.get(key);
            int numRows = nodes.size();
            
            PositionData yPositions = getYPositions(numRows);
            if( nodes.size() == 1 ) {
                
                // Put a single node in the middle, unless specifically pinned
                // as per the settlement nodes
                //
                GraphNode node = nodes.get(0);
                int xPos = getXpos(node);
                int yPos = getYpos(node, yPositions);
                
                node.getPosition().put(X, xPos);
                node.getPosition().put(Y, yPos);
                nodeList.add(node);
                
            } else {
                
                // Distribute multiple nodes around the middle
                //
                int i = 0;
                int [] positions = yPositions.getPositions();
                for (GraphNode node : nodes) {
                    
                    int xPos = getXpos(node);
                    int yPos = positions[i++];
                    
                    node.getPosition().put(X, xPos);
                    node.getPosition().put(Y, yPos);
                    nodeList.add(node);                    
                }
            }
        }
        
        List<GraphEdge> edgeList = new ArrayList<>();
        edgeList.addAll(edges.asMap().values());

        ConnectionGraph graph = new ConnectionGraph();
        graph.setNodes(nodeList);
        graph.setEdges(edgeList);
        
        details = new ConnectionGraphDetails();     
        details.setUpdate(true);
        details.setElements(graph);

        return details;
    }
    
    private int getXpos( GraphNode node ) {
        
        String agentName = (String) node.getData().get(NAME);
        Integer xpos = fileAgentPositions.get(agentName);
        
        if( xpos != null ) {
            return xpos;
        }
        
        return 50;
    }
    
    private int getYpos( GraphNode node, PositionData yPositions ) {
        
        String agentName = (String) node.getData().get(NAME);
        Integer yPos = pinnedNodes.get(agentName);
       
        if( yPos != null ) {
            return yPos;
        } 
        
        return yPositions.getMidPos();
    }

    
    private static class PositionData {
        
        private final int midPos;
        
        private final int [] positions;
        
        PositionData( int mid, int [] pos ) {
            midPos = mid;
            positions = pos;
        }

        public int getMidPos() {
            return midPos;
        }

        public int [] getPositions() {
            return positions;
        }
    }
    
    /**
     * Very simple layout algorithm tries to put the 'main line' of points in the middle of the graph
     * 
     * @param rows Max number of rows in the graph
     * @return
     */
    private PositionData getYPositions( int rows ) {
        
        switch (rows) {
            
            case 1:
                return new PositionData(100, new int [] { 100 });

            case 2:
                return new PositionData(100, new int [] { 50, 150 });

            case 3:
                return new PositionData(100, new int [] { 50, 100, 150 });

            case 4:
                return new PositionData(125, new int [] { 50, 100, 150, 200 });

            case 5:
                return new PositionData(150, new int [] { 50, 100, 150, 200, 250 });

            case 6:
                return new PositionData(175, new int [] { 50, 100, 150, 200, 250, 300 });

            case 7:
                return new PositionData(200, new int [] { 50, 100, 150, 200, 250, 300, 350 });

            case 8:
                return new PositionData(225, new int [] { 50, 100, 150, 200, 250, 300, 350, 400 });

            case 9:
                return new PositionData(250, new int [] { 50, 100, 150, 200, 250, 300, 350, 400, 450 });

            case 10:
                return new PositionData(275, new int [] { 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 });

            default:
                return new PositionData(100, new int [] { 100 });
        }
    }

}

