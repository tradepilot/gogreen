/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.metrics;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DISABLED;
import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.metrics.MetricUpdate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalCause;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;


@Component
@Profile("!test")
public final class KafkaMetricsSender {

    private static final Logger logger = LogManager.getLogger(KafkaMetricsSender.class);
   
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.producer.key-serializer}")
    private String keySerializer;
    
    @Value("${spring.kafka.producer.value-serializer}")
    private String valueSerializer;

    private LoadingCache<String, String> activeClients;        

    private KafkaProducer<String, MetricUpdate> kafkaProducer;

    @PostConstruct
    public void initialise() throws Exception {

        Properties producerProperties = new Properties();
        producerProperties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.put(KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
        producerProperties.put(VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
        
        logger.info("Initialising metrics sender with properties {}", producerProperties);
        kafkaProducer = new KafkaProducer<String, MetricUpdate>(producerProperties);
        logger.info("Metrics sender initialised with producer {}", kafkaProducer);
        
        activeClients = newCache();
    }
    
    public void registerActiveClient(String agentId) {
        
        String existing = activeClients.getIfPresent(agentId);
        if( existing == null ) {
            logger.info("Registering new client {}", agentId);
            activeClients.put(agentId, agentId);
        }        
    }
    
    
    private LoadingCache<String, String> newCache() {
        
        return CacheBuilder.newBuilder()
            .expireAfterAccess(15000, TimeUnit.MILLISECONDS)
            .removalListener(new RemovalListener<String, String>() {

                @Override
                public void onRemoval(RemovalNotification<String, String> notification) {
                    
                    RemovalCause cause = notification.getCause();
                    if( cause == RemovalCause.EXPIRED ) {
                        logger.info("Removing client {}", notification.getKey());       
                    }
                }
            })
            .build(new CacheLoader<String, String>() {
                
                public String load(String agentId) {

                    return agentId;
                }
            }
        );
    }


    public void send(String metricName, String topic, double value ) {
        
        try {

            if( !topic.equals(DISABLED) ) {

                long messageCount = 4;                
                logger.info("Sending monitoring metrics {} with value {} to topic {} for {} partitions",  metricName, value, topic, messageCount);
                MetricUpdate update = new MetricUpdate();
                update.setMetricName(metricName);
                update.setMetricValue(value);
                
                for (int i = 0; i < 4; i++) {
                    ProducerRecord<String, MetricUpdate> message = new ProducerRecord<String, MetricUpdate>(topic, i, update.getRequestId(), update);
                    kafkaProducer.send(message);                    
                }
                
            } 

        } catch (Exception e) {
            logger.warn("Error sending metric " + metricName, e);
        }        
    }
}
