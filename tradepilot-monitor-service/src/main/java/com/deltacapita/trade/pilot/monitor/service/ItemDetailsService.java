/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deltacapita.trade.pilot.monitor.correlator.GraphEngine;
import com.deltacapita.trade.pilot.monitor.correlator.InvoiceAggregator;
import com.deltacapita.trade.pilot.monitor.correlator.PaymentsAggregator;
import com.deltacapita.trade.pilot.monitor.correlator.SimpleCorrelator;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraphDetails;
import com.deltacapita.trade.pilot.monitor.types.ContainerDetails;
import com.deltacapita.trade.pilot.monitor.types.FileHistory;
import com.deltacapita.trade.pilot.monitor.types.ItemHistory;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndCountryDetails;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndSystemDetails;


@RestController 
public class ItemDetailsService implements ItemDetailsServiceI {   

    private static final Logger logger = LogManager.getLogger(ItemDetailsService.class);
    
    @Autowired
    private SimpleCorrelator correlator;    
    
    @Autowired
    private DetailBuilder detailBuilder;

    @Autowired
    private PaymentsAggregator paymentsAggregator;  
    
    @Autowired
    private InvoiceAggregator invoiceAggregator;  

    @Autowired
    private GraphEngine graphEngine;  

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising ");      
      }
    
    
    @Override
    @CrossOrigin
    @RequestMapping(value = "/filedetails/{system}/{client}/{currency}", method = RequestMethod.GET, produces = "application/json")
    public List<FileHistory> getFileDetails(@PathVariable("system") String system, @PathVariable("client") String client, @PathVariable("currency") String currency) {
        
        logger.debug("Getting file details for system={}, client={}, currency={}", system, client, currency);
        Collection<ItemHistory> files = correlator.getAllFiles();        
        List<FileHistory> histories = new ArrayList<>(files.size());
        
        for (ItemHistory file : files) {
            FileHistory history = detailBuilder.buildFileHistory(file);
            histories.add(history);
        }
        
        return histories;
    }
    
    @Override
    @CrossOrigin
    @RequestMapping(value = "/paymentsbycountry/{client}/{currency}/{country}", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentAndCountryDetails> getPaymentsByCountryAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("country") String country) {

        logger.debug("Getting payment details for client={}, currency={}, country={}", client, currency, country);
        return new ArrayList<>(paymentsAggregator.getAllPayments());      
           
    }
    
    @CrossOrigin
    @RequestMapping(value = "/paymentsbysystem/{client}/{currency}/{system}", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentAndSystemDetails> getPaymentsBySystemAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("system") String system) {
    
        logger.debug("Getting system details for client={}, currency={}, system={}", client, currency, system);
        ArrayList<PaymentAndSystemDetails> results = new ArrayList<>(invoiceAggregator.getAllInvoices());    
        return results;
    }
    
    @CrossOrigin
    @RequestMapping(value = "/connectiongraph", method = RequestMethod.GET, produces = "application/json")
    public ConnectionGraphDetails getConnectionGraph() {
                
        logger.debug("Getting connection graph");
        ConnectionGraphDetails graphDetails = graphEngine.getGraphDetails();
        return graphDetails;
    }
    
    @CrossOrigin
    @RequestMapping(value = "/containerdetails", method = RequestMethod.GET, produces = "application/json")
    public List<ContainerDetails> getContainerDetails() {
        
        logger.debug("Getting container details");
        List<ContainerDetails> details = graphEngine.getContainerDetails();
        return details;
    }
}


