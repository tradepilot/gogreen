/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.FILE_FLOW;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;

public class ItemHistory {

    private static final Logger logger = LoggerFactory.getLogger(ItemHistory.class);

    private String correlationId;
    
    private String flow;
    
    private String lastAgentPoint;    
    
    private Long lastAgentTimestamp;

    private List<MonitoringEventI> events;
        
    private Map<String,Object> data = new HashMap<>();
    
    
    public void addEvent(MonitoringEventI event) {
        
        events.add(event);
        lastAgentPoint = event.getAgentName();
        lastAgentTimestamp = event.getTimestamp();
    }
    
    public void addEvents(List<MonitoringEventI> newEvents) {
       
        if( flow.equals(FILE_FLOW)) {
            logger.debug("Updating item {}", correlationId);
        }
        
        events.addAll(newEvents);
        for (MonitoringEventI event : newEvents) {
            if( flow.equals(FILE_FLOW)) {
                logger.debug("Adding data for {} from {}", event.getCorrelationId(), event.getAgentName());
            }
            
            data.putAll(event.getData());
        }
        
        sortEvents();
        MonitoringEventI lastEvent = events.get(events.size() - 1);
        lastAgentPoint = lastEvent.getAgentName();
        lastAgentTimestamp = lastEvent.getTimestamp();

        if( flow.equals(FILE_FLOW)) {
            logger.debug("Updated item {}", correlationId);
        }

    }
    
    public String getLastAgentPoint() {
        
        return lastAgentPoint;
    }    

    public Long getLastAgentTimestamp() {
        return lastAgentTimestamp;
    }  

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public List<MonitoringEventI> getEvents() {
        return events;
    }

    public void setEvents(List<MonitoringEventI> events) {
        
        if( logger.isDebugEnabled() ) {
            
            if( flow.equals(FILE_FLOW)) {
                logger.debug("Creating item {}", correlationId);
            }

            for (MonitoringEventI event : events) {
                if( flow.equals(FILE_FLOW)) {
                    logger.debug("Adding data for {} from {}", event.getCorrelationId(), event.getAgentName());
                }                        
            }
            
        }

        this.events = events;    
        sortEvents();
        correlationId = events.get(0).getCorrelationId();

        if( logger.isDebugEnabled() ) {
            
            if( flow.equals(FILE_FLOW)) {
                logger.debug("Created item {}", correlationId);
            }
            
        }
    }

    public Map<String,Object> getData() {
        return data;
    }

    public void setData(Map<String,Object> data) {
        this.data = data;
    }  

    private void sortEvents() {
        
        events.sort(new Comparator<MonitoringEventI>() {

            @Override
            public int compare(MonitoringEventI o1, MonitoringEventI o2) {
                
                Long ts1 = o1.getTimestamp();
                Long ts2 = o2.getTimestamp();
                
                return ts1.compareTo(ts2);
            }
        });
        
    }
}
