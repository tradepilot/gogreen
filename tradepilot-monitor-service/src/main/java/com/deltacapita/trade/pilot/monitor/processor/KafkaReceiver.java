/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.processor;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.correlator.SimpleCorrelator;


@Component
@ComponentScan
public class KafkaReceiver {

    private static final Logger logger = LoggerFactory.getLogger(KafkaReceiver.class);
    
    @Autowired
    private SimpleCorrelator correlator;
    

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising with correlator " + correlator);
    }

    @KafkaListener(topics = "${kafka.topic.input}")
    public void receive(MonitoringEventI event, Acknowledgment ack) {

        if( logger.isTraceEnabled() ) {
            
            logger.trace("Received event {}", event);     
            
        } else if( logger.isDebugEnabled() ) {
            
            logger.debug("Received event {} from {}", event.getCorrelationId(), event.getAgentName() );                            
        }        
        
        correlator.onEvent(event);       
        ack.acknowledge();
    }
}


