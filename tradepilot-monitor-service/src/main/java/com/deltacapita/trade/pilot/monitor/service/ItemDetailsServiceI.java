/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deltacapita.trade.pilot.monitor.types.ConnectionGraphDetails;
import com.deltacapita.trade.pilot.monitor.types.ContainerDetails;
import com.deltacapita.trade.pilot.monitor.types.FileHistory;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndCountryDetails;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndSystemDetails;


public interface ItemDetailsServiceI {

    @RequestMapping(value = "/filedetails/{system}/{client}/{currency}", method = RequestMethod.GET, produces = "application/json")
    List<FileHistory> getFileDetails(@PathVariable("system") String system, @PathVariable("client") String client, @PathVariable("currency") String currency);

    @RequestMapping(value = "/paymentsbycountry/{client}/{currency}/{country}", method = RequestMethod.GET, produces = "application/json")
    List<PaymentAndCountryDetails> getPaymentsByCountryAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("country") String country);
    
    @RequestMapping(value = "/paymentsbysystem/{client}/{currency}/{system}", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentAndSystemDetails> getPaymentsBySystemAndCurrency(@PathVariable("client") String client, @PathVariable("currency") String currency, @PathVariable("system") String system);

    @RequestMapping(value = "/connectiongraph", method = RequestMethod.GET, produces = "application/json")
    public ConnectionGraphDetails getConnectionGraph();

    @RequestMapping(value = "/containerdetails", method = RequestMethod.GET, produces = "application/json")
    public List<ContainerDetails> getContainerDetails();
}
