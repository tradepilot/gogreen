/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

import java.util.List;

public class ItemDetails {
    
    private String correlationId;
    
    private String currency;
        
    private String creditorName;
    
    private String debtorName;

    private String creditorAccount;
    
    private String debtorAccount;
    
    private String creditorCountry;

    private String debtorCountry;
    
    private String cutoffDate;
    
    private String lastAgentPoint;    

    private Double amount;
    
    private Boolean complete;
    
    private List<AgentEventDetails> chain;   
    


    /**
     * @return the correlationId
     */
    public String getCorrelationId()
    {
        return correlationId;
    }

    /**
     * @param correlationId the correlationId to set
     */
    public void setCorrelationId(String value)
    {
        correlationId = value;
    }



    /**
     * @return the currency
     */
    public String getCurrency()
    {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String value)
    {
        currency = value;
    }

    /**
     * @return the value
     */
    public Double getAmount()
    {
        return amount;
    }

    /**
     * @param value the value to set
     */
    public void setAmount(Double value)
    {
        amount = value;
    }


    /**
     * @return the lastAgentPoint
     */
    public String getLastAgentPoint()
    {
        return lastAgentPoint;
    }

    /**
     * @param lastAgentPoint the lastAgentPoint to set
     */
    public void setLastAgentPoint(String value)
    {
        lastAgentPoint = value;
    }


    /**
     * @return the complete
     */
    public Boolean getComplete()
    {
        return complete;
    }

    /**
     * @param complete the complete to set
     */
    public void setComplete(Boolean value)
    {
        complete = value;
    }

    /**
     * @return the creditorName
     */
    public String getCreditorName()
    {
        return creditorName;
    }

    /**
     * @param creditorName the creditorName to set
     */
    public void setCreditorName(String value)
    {
        creditorName = value;
    }

    /**
     * @return the debtorName
     */
    public String getDebtorName()
    {
        return debtorName;
    }

    /**
     * @param debtorName the debtorName to set
     */
    public void setDebtorName(String value)
    {
        debtorName = value;
    }

    /**
     * @return the creditorAccount
     */
    public String getCreditorAccount()
    {
        return creditorAccount;
    }

    /**
     * @param creditorAccount the creditorAccount to set
     */
    public void setCreditorAccount(String value)
    {
        creditorAccount = value;
    }

    /**
     * @return the debtorAccount
     */
    public String getDebtorAccount()
    {
        return debtorAccount;
    }

    /**
     * @param debtorAccount the debtorAccount to set
     */
    public void setDebtorAccount(String value)
    {
        debtorAccount = value;
    }

    /**
     * @return the creditorCountry
     */
    public String getCreditorCountry()
    {
        return creditorCountry;
    }

    /**
     * @param creditorCountry the creditorCountry to set
     */
    public void setCreditorCountry(String value)
    {
        creditorCountry = value;
    }

    /**
     * @return the debtorCountry
     */
    public String getDebtorCountry()
    {
        return debtorCountry;
    }

    /**
     * @param debtorCountry the debtorCountry to set
     */
    public void setDebtorCountry(String value)
    {
        debtorCountry = value;
    }

    /**
     * @return the cutoffDate
     */
    public String getCutoffDate()
    {
        return cutoffDate;
    }

    /**
     * @param cutoffDate the cutoffDate to set
     */
    public void setCutoffDate(String value)
    {
        cutoffDate = value;
    }

    /**
     * @return the chain
     */
    public List<AgentEventDetails> getChain()
    {
        return chain;
    }

    /**
     * @param chain the chain to set
     */
    public void setChain(List<AgentEventDetails> value)
    {
        chain = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((correlationId == null) ? 0 : correlationId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        ItemDetails other = (ItemDetails) obj;
        if (correlationId == null) {
            
            if (other.correlationId != null) {
                return false;
            }
            
        } else if (!correlationId.equals(other.correlationId)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "ItemDetails [" 
                + "correlationId=" + correlationId 
                + ", currency=" + currency 
                + ", creditorName=" + creditorName 
                + ", debtorName=" + debtorName 
                + ", creditorAccount=" + creditorAccount
                + ", debtorAccount=" + debtorAccount 
                + ", creditorCountry=" + creditorCountry 
                + ", debtorCountry=" + debtorCountry 
                + ", cutoffDate=" + cutoffDate 
                + ", lastAgentPoint=" + lastAgentPoint 
                + ", amount="+ amount 
                + ", complete=" + complete 
                + ", chain=" + chain 
                + "]";
    }  
}

