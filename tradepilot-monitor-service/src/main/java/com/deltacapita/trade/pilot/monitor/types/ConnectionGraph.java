/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ConnectionGraph {
    
    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String SOURCE = "source";
    
    public static final String TARGET = "target";

    public static final String WEIGHT = "weight";

    public static final String X = "x";

    public static final String Y = "y";
 
    
    public static class GraphNode {
        
        @JsonIgnore
        private String id;
        
        @JsonIgnore
        private long timestamp;
                     
        private Map<String,Object> data = new HashMap<>();

        private Map<String,Object> properties = new HashMap<>();

        private Map<String,Integer> position = new HashMap<>();
        
        public GraphNode() {
            data.put("properties", properties);
        }

        
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
            data.put(ID, id);
        }

        public Map<String,Object> getData() {
            return data;
        }

        public void setData(Map<String,Object> data) {
            this.data = data;
        }

        public Map<String,Integer> getPosition() {
            return position;
        }

        public void setPosition(Map<String,Integer> position) {
            this.position = position;
        }
        
        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        
        public Map<String,Object> getProperties() {
            return properties;
        }

        public void setProperties(Map<String,Object> properties) {
            this.properties = properties;
        }


        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            
            if (this == obj) {
                return true;
            }
            
            if (obj == null) {
                return false;
            }
            
            if (getClass() != obj.getClass()) {
                return false;
            }
            
            GraphNode other = (GraphNode) obj;
            if (id == null) {
                if (other.id != null) {
                    return false;
                }
            } else if (!id.equals(other.id)) {
                return false;
            }
            
            return true;
        }

        @Override
        public String toString() {
            return "GraphNode [id=" + id + ", timestamp=" + timestamp + ", data=" + data + ", properties=" + properties + ", position=" + position + "]";
        }
    }
    
    
    public static class GraphEdge {
        
        private String id;
        
        private Map<String,Object> data = new HashMap<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
            data.put(ID, id);
        }

        public Map<String,Object> getData() {
            return data;
        }

        public void setData(Map<String,Object> data) {
            this.data = data;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            
            if (this == obj) {
                return true;
            }
            
            if (obj == null) {
                return false;
            }
            
            if (getClass() != obj.getClass()) {
                return false;
            }
            
            GraphEdge other = (GraphEdge) obj;
            if (id == null) {
                if (other.id != null) {
                    return false;
                }
            } else if (!id.equals(other.id)) {
                return false;
            }
            
            return true;
        }

        @Override
        public String toString() {
            return "GraphEdge [id=" + id + ", data=" + data + "]";
        }
    }    
    
    private List<GraphNode> nodes;

    private List<GraphEdge> edges;

    public List<GraphNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<GraphNode> nodes) {
        this.nodes = nodes;
    }

    public List<GraphEdge> getEdges() {
        return edges;
    }

    public void setEdges(List<GraphEdge> edges) {
        this.edges = edges;
    }

    @Override
    public String toString() {
        return "ConnectionGraph [nodes=" + nodes + ", edges=" + edges + "]";
    }
}
