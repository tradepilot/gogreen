/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.FILE_FLOW;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.INSTRUCTION_FLOW;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.LOAN_FLOW;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.PAYMENT_FLOW;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.types.ItemHistory;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Component
public class SimpleCorrelator {
    
    private static final Logger logger = LoggerFactory.getLogger(SimpleCorrelator.class);

    @Value("${correlator.cache.lease}")
    private Long itemLease;

    @Value("${correlator.cache.maxSize}")
    private Long maxSize;

    @Value("${correlator.waitTime}")
    private Long waitTime;    
    
    @Autowired
    private PaymentsAggregator paymentsAggregator;
    
    @Autowired
    private InvoiceAggregator invoiceAggregator;
    
    @Autowired
    private GraphEngine graphEngine;
    
    private Map<String,EventHandler> eventHandlers;
    
    private LoadingCache<String, ItemHistory> fileItemCache;
    
    private LoadingCache<String, ItemHistory> instructionItemCache;
    
    private LoadingCache<String, ItemHistory> loanItemCache;

    private LoadingCache<String, ItemHistory> paymentItemCache;


    private class EventHandler {
       
        private LinkedBlockingQueue<MonitoringEventI> inputQueue;
        
        private final String flowName;
        
        private final String threadName;
        
        private final Long waitTime;   
        
        private final LoadingCache<String, ItemHistory> itemCache;
        
        public EventHandler(String flow, String name, LoadingCache<String, ItemHistory> cache, Long delay) {
            
            flowName = flow;
            waitTime = delay;
            threadName = name + "EventHandler";
            itemCache = cache;
            inputQueue = new LinkedBlockingQueue<>();
        }

        public void onEvent(MonitoringEventI event) {
            
            boolean added = inputQueue.offer(event);
            if( !added ) {
                logger.error("Unable to add event {}", event);
            }
        }
        
        public void start() {
            
            Thread t = new Thread(() -> {
                
                while(true) {
                    
                    try {
                        
                        MonitoringEventI event = inputQueue.poll(waitTime, TimeUnit.MILLISECONDS);                                                
                        if( event != null ) {   
                            
                            // Drain the queue on each take as events are likely to relate to the same entities
                            //
                            List<MonitoringEventI> events = new ArrayList<>(inputQueue.size() + 5);
                            events.add(event);
                            int count = inputQueue.drainTo(events);
                            logger.info("Added {} events ", count);

                            correlate(flowName, events, itemCache);
                        }
                        
                    } catch (InterruptedException e) {
                        // Ignore
                        logger.warn("Interrupted {}", e);
                        Thread.currentThread().interrupt();
                    }
                }
                
            }, threadName);
            
            t.start();
        }        
    }
    
    
    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising with cache lease {} and max size {}", itemLease, maxSize);        
        fileItemCache = newCache();
        instructionItemCache = newCache();
        loanItemCache = newCache();
        paymentItemCache = newCache();        

        eventHandlers = new HashMap<>();        
        eventHandlers.put(FILE_FLOW, new EventHandler(FILE_FLOW, "File", fileItemCache, waitTime));
        eventHandlers.put(INSTRUCTION_FLOW, new EventHandler(INSTRUCTION_FLOW, "Instruction", instructionItemCache, waitTime));
        eventHandlers.put(LOAN_FLOW, new EventHandler(LOAN_FLOW, "Loan", loanItemCache, waitTime));
        eventHandlers.put(PAYMENT_FLOW, new EventHandler(PAYMENT_FLOW, "Payment", paymentItemCache, waitTime));       
        
        for (EventHandler handler : eventHandlers.values()) {
            handler.start();
        }
    }
    
    
    private LoadingCache<String, ItemHistory> newCache() {
        
        return CacheBuilder.newBuilder()
            .expireAfterAccess(itemLease, TimeUnit.MILLISECONDS)
            .maximumSize(maxSize)
            .build(new CacheLoader<String, ItemHistory>() {
                
                public ItemHistory load(String correlationId) {

                    ItemHistory history = new ItemHistory();
                    history.setCorrelationId(correlationId);
                    
                    return history;
                }
            }
        );
    }
    
    public void onEvent(MonitoringEventI event) {
        
        logger.debug("Handing event {}", event);
        String flow = event.getFlow();
        EventHandler eventHandler = eventHandlers.get(flow);
        
        eventHandler.onEvent(event);
    }
    
    private void correlate( String flow, List<MonitoringEventI> events, LoadingCache<String, ItemHistory> cache ) {
        
        for (MonitoringEventI event : events) {
            logger.debug("Received event {} from {}", event.getCorrelationId(), event.getAgentName());
        }
               
        Map<String, List<MonitoringEventI>> eventsById = new HashMap<>();
        for (MonitoringEventI event : events) {
            
            String correlationId = event.getCorrelationId();
            if( correlationId != null ) {
                
                List<MonitoringEventI> list = eventsById.get(correlationId);                
                if(list == null) {
                    
                    list = new ArrayList<>();
                    eventsById.put(correlationId, list);
                }
                
                list.add(event);
                
            } else {
                logger.warn("Ignoring uncorrelated event {}", event);
            }
        }
              
        List<ItemHistory> updatedHistories = new ArrayList<>();
        for (Entry<String, List<MonitoringEventI>> entry : eventsById.entrySet()) {
            
            String correlationId = entry.getKey();
            List<MonitoringEventI> itemEvents = entry.getValue();

            try {
                                
                ItemHistory itemHistory = cache.getIfPresent(correlationId);               
                if( itemHistory == null ) {
                    
                    if( flow.equals(MonitoringConstants.FILE_FLOW)) {
                        logger.debug("Creating new item history in flow {} for {} with {} events", flow, correlationId, itemEvents.size());
                    }
                    
                    itemHistory = new ItemHistory();
                    itemHistory.setCorrelationId(correlationId);
                    itemHistory.setFlow(flow);
                    itemHistory.setEvents(itemEvents);
                    
                    cache.put(correlationId, itemHistory);
                    
                } else {
                    
                    if( flow.equals(MonitoringConstants.FILE_FLOW)) {
                        logger.debug("Updating item history in flow {} for {} with {} new events", flow, correlationId, itemEvents.size());
                    }
                    
                    itemHistory.addEvents(itemEvents);
                }
                
                updatedHistories.add(itemHistory);
                
            } catch (Exception e) {
                
                logger.warn("Unable to add or update history for {}", correlationId, e);
            }
        }
        
        if( flow.equals(MonitoringConstants.FILE_FLOW)) {
            graphEngine.update(updatedHistories);
        }

        if( flow.equals(MonitoringConstants.PAYMENT_FLOW)) {
            paymentsAggregator.aggregate(events);
        }

        if( flow.equals(MonitoringConstants.INSTRUCTION_FLOW)) {
            invoiceAggregator.aggregate(events);
        }

    }   
    
    public Collection<ItemHistory> getAllFiles() {
        
        ConcurrentMap<String, ItemHistory> map = fileItemCache.asMap();
        return map.values();
    }
}


