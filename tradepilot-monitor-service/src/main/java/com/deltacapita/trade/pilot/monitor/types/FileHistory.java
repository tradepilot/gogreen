/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FileHistory {
    
    private String fileId;
    
    private String lastAgentPoint;    
    
    private Long lastAgentTimestampMillis;

    private LocalDateTime lastAgentTimestamp;

    private String buyerId;

    private String buyerName;

    private String state;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receivedDateTime;

    private LocalDate maturityDate;

    private Long numberOfInstructions;

    private Long outstandingPaymentRequests;

    private Double totalValue;
    
    private String country;

    private List<AgentEventDetails> chain;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }


    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDateTime getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(LocalDateTime receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }

    public LocalDate getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(LocalDate maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Long getNumberOfInstructions() {
        return numberOfInstructions;
    }

    public void setNumberOfInstructions(Long numberOfInstructions) {
        this.numberOfInstructions = numberOfInstructions;
    }

    public Long getOutstandingPaymentRequests() {
        return outstandingPaymentRequests;
    }

    public void setOutstandingPaymentRequests(Long outstandingPaymentRequests) {
        this.outstandingPaymentRequests = outstandingPaymentRequests;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public String getLastAgentPoint() {
        return lastAgentPoint;
    }

    public void setLastAgentPoint(String lastAgentPoint) {
        this.lastAgentPoint = lastAgentPoint;
    }

    public LocalDateTime getLastAgentTimestamp() {
        return lastAgentTimestamp;
    }

    public void setLastAgentTimestamp(LocalDateTime lastAgentTimestamp) {
        this.lastAgentTimestamp = lastAgentTimestamp;
    }

    public Long getLastAgentTimestampMillis() {
        return lastAgentTimestampMillis;
    }

    public void setLastAgentTimestampMillis(Long lastAgentTimestampMillis) {
        this.lastAgentTimestampMillis = lastAgentTimestampMillis;
    }
    
    public List<AgentEventDetails> getChain() {
        return chain;
    }

    public void setChain(List<AgentEventDetails> chain) {
        this.chain = chain;
    }
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileHistory other = (FileHistory) obj;
        if (fileId == null) {
            if (other.fileId != null) {
                return false;
            }
        } else if (!fileId.equals(other.fileId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileHistory [fileId=" + fileId + ", lastAgentPoint=" + lastAgentPoint + ", lastAgentTimestampMillis="
                + lastAgentTimestampMillis + ", lastAgentTimestamp=" + lastAgentTimestamp + ", buyerId=" + buyerId
                + ", buyerName=" + buyerName + ", state=" + state + ", receivedDateTime=" + receivedDateTime
                + ", maturityDate=" + maturityDate + ", numberOfInstructions=" + numberOfInstructions
                + ", outstandingPaymentRequests=" + outstandingPaymentRequests + ", totalValue=" + totalValue
                + ", country=" + country + ", chain=" + chain + "]";
    }

}

