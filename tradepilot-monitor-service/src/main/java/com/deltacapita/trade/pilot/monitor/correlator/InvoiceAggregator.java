/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUESTED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_COMPLETE;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_VALIDATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_EXECUTED;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.MetricConfig;
import com.deltacapita.trade.pilot.MonitorConfig;
import com.deltacapita.trade.pilot.core.data.TradePilotConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.metrics.KafkaMetricsSender;
import com.deltacapita.trade.pilot.monitor.service.DetailBuilder;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndSystemDetails;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Component
public class InvoiceAggregator {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceAggregator.class);

    @Value("${correlator.cache.lease}")
    private Long itemLease;

    @Value("${correlator.cache.maxSize}")
    private Long maxSize;

    @Value("${correlator.metrics.delay}")
    private long delay;

    @Value("${correlator.metrics.window}")
    private long window;

    @Autowired
    private MonitorConfig serviceConfig;

    @Autowired
    private DetailBuilder detailBuilder;

    @Autowired
    private KafkaMetricsSender metricsSender;

    private List<SimpleAggregator> aggregators = new ArrayList<>();    
        
    private List<IntervalAggregator> intervalAggregators = new ArrayList<>();    

    private Map<String,SimpleAggregator> aggregatorsBySystem = new HashMap<>();    

    private Map<String,IntervalAggregator> aggregatorsByInterval = new HashMap<>();    

    private ScheduledExecutorService executorService;

    
    
    private static class SimpleAggregator {
        
        private String agentName;
        
        private String systemName;
        
        private Long itemLease;
        
        private Long maxSize;

        private DetailBuilder detailBuilder;
        
        private Set<String> ids = new HashSet<>();
        
        private LoadingCache<String, PaymentAndSystemDetails> totalsByKey;        

        public SimpleAggregator( String name, String system, Long lease, Long size, DetailBuilder builder ) {
            agentName = name;
            systemName = system;
            itemLease = lease;
            maxSize = size;
            detailBuilder = builder;
            totalsByKey = newCache();     
        }
        
        public void onEvent( MonitoringEventI event ) {
            
            Map<String, Object> data = event.getData();            
            Double amount = (Double) data.get(MonitoringConstants.AMOUNT);

            logger.debug("{} processing event {} for {}", systemName, event.getCorrelationId(), event.getAgentName());
            if( amount != null ) {
                
                String correlationId = event.getCorrelationId();
                String supplierId = (String) data.get(MonitoringConstants.SUPPLIER_ID);
                String currency = (String) data.get(MonitoringConstants.CURRENCY);
                String clientName = detailBuilder.getClientName(supplierId);
                String key = clientName + "-" + systemName + "-" + currency;

                if( event.getAgentName().equals(agentName) ) {
                                    
                    if( !ids.contains(correlationId)  ) {
                        
                        PaymentAndSystemDetails details = totalsByKey.getIfPresent(key);
                        if( details == null ) {
                            
                            details = new PaymentAndSystemDetails();
                            details.setSystem(systemName);
                            details.setClient(clientName);
                            details.setCurrency(currency);
                            details.setInFlightCount(1);
                            details.setInFlightValue(amount);

                            totalsByKey.put(key, details);
                            
                            if( systemName.equals("File Processor")) {
                                logger.debug("{} creating new entry {} for {} with value {}", systemName, event.getCorrelationId(), event.getAgentName(), details.getInFlightValue());
                            }
                            
                        } else {
                            
                            long inFlightCount = details.getInFlightCount();
                            double inFlightValue = details.getInFlightValue();
                            
                            details.setInFlightCount(inFlightCount + 1);
                            details.setInFlightValue(inFlightValue + amount);
                            
                            if( systemName.equals("File Processor")) {
                                logger.debug("{} updating entry {} for {} with value {}", systemName, event.getCorrelationId(), event.getAgentName(), details.getInFlightValue());
                            }

                        }

                        ids.add(correlationId);
                    }
                    
                } else {
                    
                    if( ids.contains(correlationId) ) {

                        PaymentAndSystemDetails details = totalsByKey.getIfPresent(key);
                        if( details != null ) {
                            
                            long inFlightCount = details.getInFlightCount();
                            double inFlightValue = details.getInFlightValue();
                            
                            long newCount = inFlightCount - 1;
                            double newValue = inFlightValue - amount;
                            
                            newCount = (newCount < 1 ? 0 : newCount);
                            newValue = (newValue < 1 ? 0 : newValue);                            
                            
                            details.setInFlightCount(newCount);
                            details.setInFlightValue(newValue);

                            if( systemName.equals("File Processor")) {
                                logger.debug("{} decrementing entry {} for {} to value {}", systemName, event.getCorrelationId(), event.getAgentName(), newValue);
                            }

                            totalsByKey.put(key, details);
                        } 

                        ids.remove(correlationId);
                    }
                }
            }
        }

        public Collection<PaymentAndSystemDetails> getDetails() {
            ConcurrentMap<String, PaymentAndSystemDetails> map = totalsByKey.asMap();
            return map.values();
        }   
        
        private LoadingCache<String, PaymentAndSystemDetails> newCache() {
            
            return CacheBuilder.newBuilder()
                .expireAfterAccess(itemLease, TimeUnit.MILLISECONDS)
                .maximumSize(maxSize)
                .build(new CacheLoader<String, PaymentAndSystemDetails>() {
                    
                    public PaymentAndSystemDetails load(String correlationId) {

                        PaymentAndSystemDetails details = new PaymentAndSystemDetails();
                        return details;
                    }
                }
            );
        }

        @Override
        public String toString() {
            return "SimpleAggregator [agentName=" + agentName + ", systemName=" + systemName + ", itemLease="
                    + itemLease + ", maxSize=" + maxSize + "]";
        }
    }
    
    
    private static class IntervalAggregator {       

        private final Long itemLease;
        
        private final Long maxSize;

        private final Long movingAvgWindow;

        private final String sourceAgentName;
        
        private final String targetAgentName;
        
        private final String intervalName;
                
        private final Set<String> ids = new HashSet<>();
        
        private final LoadingCache<String, MonitoringEventI> sourceEvents;
 
        private final LoadingCache<String, MonitoringEventI> targetEvents;
        
        private final ScheduledExecutorService executor;
        
        private final long movingAvgWindowSeconds;
        
        private volatile double movingAverageFlowRate = 0.0;
        
        private volatile double movingAverageLatency = 0.0;
        
        
        public IntervalAggregator(String sourceAgentName, String targetAgentName, Long lease, Long size, Long window) {
 
            this.sourceAgentName = sourceAgentName;
            this.targetAgentName = targetAgentName;
            this.intervalName = sourceAgentName + "-" + targetAgentName;
            
            itemLease = lease;
            maxSize = size;
            movingAvgWindow = window;
            movingAvgWindowSeconds = window / 1000;
           
            sourceEvents = newCache();
            targetEvents = newCache();
            
            executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
                
                @Override
                public Thread newThread(Runnable r) {
                    return new Thread(r, intervalName + "-aggregator");
                }
            });
            
            
            executor.scheduleWithFixedDelay( () -> {
                            
                try {
                    
                    int count = 0;
                    long total = 0;
                    
                    Set<String> correlationIds = targetEvents.asMap().keySet();
                    for (String correlationId : correlationIds) {
                        
                        MonitoringEventI target = targetEvents.getIfPresent(correlationId);
                        MonitoringEventI source = sourceEvents.getIfPresent(correlationId);
                        
                        if( target != null && source != null ) {
                            
                            Long sourceTimestamp = source.getTimestamp();
                            Long targetTimestamp = target.getTimestamp();                        
                            Long interval = targetTimestamp - sourceTimestamp;
                            
                            total += interval;
                            count++;
                            
                            sourceEvents.invalidate(correlationId);
                            targetEvents.invalidate(correlationId);                        
                        }
                    }

                    if( total > 0 && count > 0 ) {                        
                        movingAverageLatency = (double) total / count;        
                        movingAverageFlowRate = (double) count / movingAvgWindowSeconds;
                    } else {
                        movingAverageLatency = 0.0;
                        movingAverageFlowRate = 0.0;
                    }
                    
                    logger.info("Moving Avg. Latency for {} is {} and flow rate is {}", intervalName, movingAverageLatency, movingAverageFlowRate);
                    
                    sourceEvents.cleanUp();
                    targetEvents.cleanUp();                

                } catch (Throwable t) {
                    logger.error("Error calculating moving avg. latency for " + intervalName, t);
                }
                
            }, movingAvgWindow, movingAvgWindow, TimeUnit.MILLISECONDS);
        }

        public void onEvent( MonitoringEventI event ) {
            
            Map<String, Object> data = event.getData();            
            String invoiceType = (String) data.get(MonitoringConstants.TYPE);

            if( invoiceType != null && invoiceType.equals(TradePilotConstants.INVOICE) ) {
                
                String correlationId = event.getCorrelationId();
                String agentName = event.getAgentName();
                
                if( agentName.equals(sourceAgentName) ) {
                    
                    ids.add(correlationId);
                    sourceEvents.put(correlationId, event);
                    
                } else if( agentName.equals(targetAgentName) ) {
                    
                    ids.remove(correlationId);
                    targetEvents.put(correlationId, event);
                }
            }
        }


        public String getIntervalName() {
            return intervalName;
        }

        public int getCurrentDepth() {
            return ids.size();
        }
        
        private LoadingCache<String, MonitoringEventI> newCache() {
            
            return CacheBuilder.newBuilder()
                .expireAfterAccess(itemLease, TimeUnit.MILLISECONDS)
                .maximumSize(maxSize)
                .build(new CacheLoader<String, MonitoringEventI>() {
                    
                    public MonitoringEventI load(String correlationId) {

                        MonitoringEventI details = new MonitoringEvent();
                        return details;
                    }
                }
            );
        }

        public double getMovingAverageLatency() {
            return movingAverageLatency;
        }

        @Override
        public String toString() {
            return "IntervalAggregator [" 
                    + "intervalName=" + intervalName 
                    + ", movingAvgWindow=" + movingAvgWindow 
                    + ", sourceAgentName=" + sourceAgentName 
                    + ", targetAgentName=" + targetAgentName                     
                    + ", itemLease=" + itemLease 
                    + ", maxSize=" + maxSize 
                    + "]";
        }
    }
   
    @PostConstruct
    public void initialise() throws Exception {
        
        logger.info("Initialising with cache lease {} and max size {}", itemLease, maxSize);        
        aggregators.add(new SimpleAggregator(INSTRUCTION_CREATED, "File Processor", itemLease, maxSize, detailBuilder));
        aggregators.add(new SimpleAggregator(INSTRUCTION_VALIDATED, "File Validator", itemLease, maxSize, detailBuilder));
        aggregators.add(new SimpleAggregator(FUNDING_REQUESTED, "Funding Request", itemLease, maxSize, detailBuilder));
        aggregators.add(new SimpleAggregator(PAYMENT_CREATED, "Payment Processor", itemLease, maxSize, detailBuilder));
        aggregators.add(new SimpleAggregator(PAYMENT_EXECUTED, "Payment Processor", itemLease, maxSize, detailBuilder));
        aggregators.add(new SimpleAggregator(INSTRUCTION_COMPLETE, "Settlement", itemLease, maxSize, detailBuilder));    
        
        intervalAggregators.add(new IntervalAggregator(INSTRUCTION_CREATED, INSTRUCTION_VALIDATED, itemLease, maxSize, window));    
        intervalAggregators.add(new IntervalAggregator(INSTRUCTION_VALIDATED, FUNDING_REQUESTED, itemLease, maxSize, window));    
        
        
        for (SimpleAggregator aggregator : aggregators) {
            logger.info("Configured aggregator {}", aggregator);
            aggregatorsBySystem.put(aggregator.agentName, aggregator);
        }

        for (IntervalAggregator aggregator : intervalAggregators) {
            logger.info("Configured interval aggregator {}", aggregator);
            aggregatorsByInterval.put(aggregator.getIntervalName(), aggregator);
        }
        
        logger.info("Will send metrics every {}ms", delay);                
        Collection<MetricConfig> metrics = serviceConfig.getMetrics();
        for (MetricConfig config : metrics) {
            
            logger.info("Found configured metric {}", config);        
        }

        executorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
            
            @Override
            public Thread newThread(Runnable r) {

                return new Thread(r, "InvoiceMetricsSender");
            }
        });

        
        executorService.scheduleWithFixedDelay(() -> {
                        
            sendMetrics();
            
        }, delay, delay, TimeUnit.MILLISECONDS);
    }
    
    public void aggregate( List<MonitoringEventI> events ) {
               
        logger.debug("Aggregating {} events ", events.size());
        for (MonitoringEventI event : events) {
                        
            for (SimpleAggregator aggregator : aggregators) {
                aggregator.onEvent(event);
            }    
            
            for (IntervalAggregator aggregator : intervalAggregators) {
                aggregator.onEvent(event);
            }
        }
        
        registerActiveCLients(events);
    }    

    public Collection<PaymentAndSystemDetails> getAllInvoices() {
        
        List<PaymentAndSystemDetails> totals = new ArrayList<>();
        for (SimpleAggregator aggregator : aggregators) {
            Collection<PaymentAndSystemDetails> details = aggregator.getDetails();
            totals.addAll(details);
        }     
        
        return totals;
    }
    
    
    private void registerActiveCLients(List<MonitoringEventI> events ) {
        
        try {
            
            for (MonitoringEventI event : events) {
                
                String agentName = event.getAgentName();
                Collection<MetricConfig> metrics = serviceConfig.getMetrics();
                for (MetricConfig config : metrics) {  
                    
                    String aggregatorName = config.getAggregatorName();
                    if( aggregatorName.equals(agentName) ) {
                        
                        String agentId = event.getAgentId();
                        metricsSender.registerActiveClient(agentId);
                    }
                }
            }

        } catch (Throwable t) {
            logger.error("Error registering active client ", t);
        }
    }
    
    
    public void sendMetrics() {
        
        Collection<MetricConfig> metrics = serviceConfig.getMetrics();
        for (MetricConfig config : metrics) {            
            sendMetrics(config);
        }
    }
    
    public void sendMetrics(MetricConfig config) {
            
        try {
            
            String aggregatorName = config.getAggregatorName();
            String metricName = config.getMetricName();
            String topicName = config.getTopicName();     

            SimpleAggregator aggregator = aggregatorsBySystem.get(aggregatorName);
            if( aggregator != null ) {
                
                long totalItems = 0;
                Collection<PaymentAndSystemDetails> details = aggregator.getDetails();
                
                for (PaymentAndSystemDetails paymentAndSystemDetails : details) {
                    totalItems += paymentAndSystemDetails.getInFlightCount();
                }
                
                metricsSender.send(metricName, topicName, totalItems);
            }
            
            IntervalAggregator intervalAggregator = aggregatorsByInterval.get(aggregatorName);
            if( intervalAggregator != null ) {
                
                double metricValue = 0.0;
                if( metricName.contains("depth")) {
                    metricValue = intervalAggregator.getCurrentDepth();                                    
                } else if( metricName.contains("latency") ) {
                    metricValue = intervalAggregator.getMovingAverageLatency();                                                 
                }
                
                metricsSender.send(metricName, topicName, metricValue);
            }

        } catch (Throwable t) {
            
            logger.error("Error sending metrics for " + config, t);
        }
    }

    public void setItemLease(Long itemLease) {
        this.itemLease = itemLease;
    }

    public void setMaxSize(Long maxSize) {
        this.maxSize = maxSize;
    }

    public void setDetailBuilder(DetailBuilder detailBuilder) {
        this.detailBuilder = detailBuilder;
    }
}
