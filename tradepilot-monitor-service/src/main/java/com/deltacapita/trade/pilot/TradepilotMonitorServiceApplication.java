/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradepilotMonitorServiceApplication {

    private static final Logger logger = LogManager.getLogger(TradepilotMonitorServiceApplication.class);

	public static void main(String[] args) {
	    
	    try {
	        SpringApplication app = new SpringApplication(TradepilotMonitorServiceApplication.class);
	        app.run(args);    
        } catch (Exception e) {
            logger.error(e);
        }
		
	}
}
