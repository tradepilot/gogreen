/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.types;

public class PaymentAndSystemDetails {
    
    private String system;
    
    private String currency;
    
    private String client;
    
    private long inFlightCount;
    
    private long atRiskCount;

    private long highRiskCount;

    private long recoveredCount ;

    private double inFlightValue;
    
    private double atRiskValue;
    
    private double highRiskValue;

    private double recoveredValue;

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public long getInFlightCount() {
        return inFlightCount;
    }

    public void setInFlightCount(long inFlightCount) {
        this.inFlightCount = inFlightCount;
    }

    public long getAtRiskCount() {
        return atRiskCount;
    }

    public void setAtRiskCount(long atRiskCount) {
        this.atRiskCount = atRiskCount;
    }

    public long getHighRiskCount() {
        return highRiskCount;
    }

    public void setHighRiskCount(long highRiskCount) {
        this.highRiskCount = highRiskCount;
    }

    public long getRecoveredCount() {
        return recoveredCount;
    }

    public void setRecoveredCount(long recoveredCount) {
        this.recoveredCount = recoveredCount;
    }

    public double getInFlightValue() {
        return inFlightValue;
    }

    public void setInFlightValue(double inFlightValue) {
        this.inFlightValue = inFlightValue;
    }

    public double getAtRiskValue() {
        return atRiskValue;
    }

    public void setAtRiskValue(double atRiskValue) {
        this.atRiskValue = atRiskValue;
    }

    public double getHighRiskValue() {
        return highRiskValue;
    }

    public void setHighRiskValue(double highRiskValue) {
        this.highRiskValue = highRiskValue;
    }

    public double getRecoveredValue() {
        return recoveredValue;
    }

    public void setRecoveredValue(double recoveredValue) {
        this.recoveredValue = recoveredValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((client == null) ? 0 : client.hashCode());
        result = prime * result + ((currency == null) ? 0 : currency.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PaymentAndSystemDetails other = (PaymentAndSystemDetails) obj;
        if (client == null) {
            if (other.client != null) {
                return false;
            }
        } else if (!client.equals(other.client)) {
            return false;
        }
        if (currency == null) {
            if (other.currency != null) {
                return false;
            }
        } else if (!currency.equals(other.currency)) {
            return false;
        }
        if (system == null) {
            if (other.system != null) {
                return false;
            }
        } else if (!system.equals(other.system)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PaymentAndSystemDetails [system=" + system + ", currency=" + currency + ", client=" + client
                + ", inFlightCount=" + inFlightCount + ", atRiskCount=" + atRiskCount + ", highRiskCount="
                + highRiskCount + ", recoveredCount=" + recoveredCount + ", inFlightValue=" + inFlightValue
                + ", atRiskValue=" + atRiskValue + ", highRiskValue=" + highRiskValue + ", recoveredValue="
                + recoveredValue + "]";
    }

    
}
