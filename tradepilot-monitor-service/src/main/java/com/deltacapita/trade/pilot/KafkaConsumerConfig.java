/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties.AckMode;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;


@EnableKafka
@Configuration
@Profile("!test")
public class KafkaConsumerConfig {
 
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServer;
    
    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;
    
    @Value("${spring.kafka.consumer.enable-auto-commit}")
    private Boolean autoCommit;

    @Bean
    public ConsumerFactory<String, MonitoringEvent> consumerFactory() {
        
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, autoCommit);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        
        return new DefaultKafkaConsumerFactory<>(props,
                                          new StringDeserializer(), 
                                          new JsonDeserializer<>(MonitoringEvent.class));
    }
     
    
    /**
     * Override the default container factory in order to set the ACK mode to manual.
     * This mans that messages will only be ACK'd if the processor handles them without
     * any exceptions - if an exception is thrown then the message will be available for
     * reprocessing
     * 
     * @return
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MonitoringEvent> kafkaListenerContainerFactory() {
        
        ConcurrentKafkaListenerContainerFactory<String, MonitoringEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.getContainerProperties().setAckMode(AckMode.MANUAL);
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
