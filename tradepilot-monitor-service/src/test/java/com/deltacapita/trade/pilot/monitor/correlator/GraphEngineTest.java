/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_PROCESSOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_PROCESSOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_VALIDATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FILE_VALIDATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_GENERATOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.LOAN_GENERATOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_PROCESSOR_IN;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.PAYMENT_PROCESSOR_OUT;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants.FILE_FLOW;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphEdge;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraph.GraphNode;
import com.deltacapita.trade.pilot.monitor.types.ConnectionGraphDetails;
import com.deltacapita.trade.pilot.monitor.types.ItemHistory;

public class GraphEngineTest {

    private GraphEngine engine;
    
    @Before
    public void setUp() throws Exception {
        
        engine = new GraphEngine();
        engine.initialise();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetGraphDetails() {
        
        ItemHistory history1 = new ItemHistory();
        history1.setCorrelationId("correlationId1");      
        history1.setFlow(FILE_FLOW);

        String correlationId1 = "correlationId-1";
        Map<String, Object> data = new HashMap<>();
        data.put(MonitoringConstants.SUPPLIER_ID, "SORG1");
        data.put(MonitoringConstants.CURRENCY, "GBP");        
        data.put(MonitoringConstants.AMOUNT, 1000.0);     

        
        MonitoringEvent event1 = new MonitoringEvent();
        event1.setAgentId(FILE_PROCESSOR_IN + "-1");
        event1.setAgentName(FILE_PROCESSOR_IN);
        event1.setCorrelationId(correlationId1);
        event1.setFlow("FILE_FLOW");                
        event1.setData(data);
        event1.setTimestamp(1000L);
        
        MonitoringEvent event1a = new MonitoringEvent();
        event1a.setAgentId(FILE_PROCESSOR_OUT + "-1");
        event1a.setAgentName(FILE_PROCESSOR_OUT);
        event1a.setCorrelationId(correlationId1);
        event1a.setFlow("FILE_FLOW");                
        event1a.setData(data);
        event1a.setTimestamp(1000L);

        MonitoringEvent event2 = new MonitoringEvent();
        event2.setAgentId(FILE_VALIDATOR_IN + "-1");
        event2.setAgentName(FILE_VALIDATOR_IN);
        event2.setCorrelationId(correlationId1);
        event2.setFlow("FILE_FLOW");
        event2.setData(data);
        event2.setTimestamp(1001L);

        MonitoringEvent event2a = new MonitoringEvent();
        event2a.setAgentId(FILE_VALIDATOR_OUT + "-1");
        event2a.setAgentName(FILE_VALIDATOR_OUT);
        event2a.setCorrelationId(correlationId1);
        event2a.setFlow("FILE_FLOW");
        event2a.setData(data);
        event2a.setTimestamp(1001L);

        MonitoringEvent event3a = new MonitoringEvent();
        event3a.setAgentId(FUNDING_REQUEST_GENERATOR_IN + "-1");
        event3a.setAgentName(FUNDING_REQUEST_GENERATOR_IN);
        event3a.setCorrelationId(correlationId1);
        event3a.setFlow("FILE_FLOW");
        event3a.setData(data);
        event3a.setTimestamp(1002L);

        MonitoringEvent event3b = new MonitoringEvent();
        event3b.setAgentId(FUNDING_REQUEST_GENERATOR_IN + "-2");
        event3b.setAgentName(FUNDING_REQUEST_GENERATOR_IN);
        event3b.setCorrelationId(correlationId1);
        event3b.setFlow("FILE_FLOW");
        event3b.setData(data);
        event3b.setTimestamp(1003L);

        MonitoringEvent event3c = new MonitoringEvent();
        event3c.setAgentId(FUNDING_REQUEST_GENERATOR_IN + "-3");
        event3c.setAgentName(FUNDING_REQUEST_GENERATOR_IN);
        event3c.setCorrelationId(correlationId1);
        event3c.setFlow("FILE_FLOW");
        event3c.setData(data);
        event3c.setTimestamp(1003L);

        MonitoringEvent event3d = new MonitoringEvent();
        event3d.setAgentId(FUNDING_REQUEST_GENERATOR_IN + "-4");
        event3d.setAgentName(FUNDING_REQUEST_GENERATOR_IN);
        event3d.setCorrelationId(correlationId1);
        event3d.setFlow("FILE_FLOW");
        event3d.setData(data);
        event3d.setTimestamp(1003L);
        
        MonitoringEvent event3e = new MonitoringEvent();
        event3e.setAgentId(FUNDING_REQUEST_GENERATOR_IN + "-5");
        event3e.setAgentName(FUNDING_REQUEST_GENERATOR_IN);
        event3e.setCorrelationId(correlationId1);
        event3e.setFlow("FILE_FLOW");
        event3e.setData(data);
        event3e.setTimestamp(1003L);

        MonitoringEvent event3a2 = new MonitoringEvent();
        event3a2.setAgentId(FUNDING_REQUEST_GENERATOR_OUT + "-1");
        event3a2.setAgentName(FUNDING_REQUEST_GENERATOR_OUT);
        event3a2.setCorrelationId(correlationId1);
        event3a2.setFlow("FILE_FLOW");
        event3a2.setData(data);
        event3a2.setTimestamp(1002L);

        MonitoringEvent event3b2 = new MonitoringEvent();
        event3b2.setAgentId(FUNDING_REQUEST_GENERATOR_OUT + "-2");
        event3b2.setAgentName(FUNDING_REQUEST_GENERATOR_OUT);
        event3b2.setCorrelationId(correlationId1);
        event3b2.setFlow("FILE_FLOW");
        event3b2.setData(data);
        event3b2.setTimestamp(1003L);

        MonitoringEvent event3c2 = new MonitoringEvent();
        event3c2.setAgentId(FUNDING_REQUEST_GENERATOR_OUT + "-3");
        event3c2.setAgentName(FUNDING_REQUEST_GENERATOR_OUT);
        event3c2.setCorrelationId(correlationId1);
        event3c2.setFlow("FILE_FLOW");
        event3c2.setData(data);
        event3c2.setTimestamp(1003L);

        MonitoringEvent event3d2 = new MonitoringEvent();
        event3d2.setAgentId(FUNDING_REQUEST_GENERATOR_OUT + "-4");
        event3d2.setAgentName(FUNDING_REQUEST_GENERATOR_OUT);
        event3d2.setCorrelationId(correlationId1);
        event3d2.setFlow("FILE_FLOW");
        event3d2.setData(data);
        event3d2.setTimestamp(1003L);
        
        MonitoringEvent event3e2 = new MonitoringEvent();
        event3e2.setAgentId(FUNDING_REQUEST_GENERATOR_OUT + "-5");
        event3e2.setAgentName(FUNDING_REQUEST_GENERATOR_OUT);
        event3e2.setCorrelationId(correlationId1);
        event3e2.setFlow("FILE_FLOW");
        event3e2.setData(data);
        event3e2.setTimestamp(1003L);

        MonitoringEvent event4 = new MonitoringEvent();
        event4.setAgentId(LOAN_GENERATOR_IN + "-1");
        event4.setAgentName(LOAN_GENERATOR_IN);
        event4.setCorrelationId(correlationId1);
        event4.setFlow("FILE_FLOW");
        event4.setData(data);
        event4.setTimestamp(1004L);

        MonitoringEvent event4a = new MonitoringEvent();
        event4a.setAgentId(LOAN_GENERATOR_OUT+ "-1");
        event4a.setAgentName(LOAN_GENERATOR_OUT);
        event4a.setCorrelationId(correlationId1);
        event4a.setFlow("FILE_FLOW");
        event4a.setData(data);
        event4a.setTimestamp(1004L);

        MonitoringEvent event5 = new MonitoringEvent();
        event5.setAgentId(PAYMENT_PROCESSOR_IN + "-1");
        event5.setAgentName(PAYMENT_PROCESSOR_IN);
        event5.setCorrelationId(correlationId1);
        event5.setFlow("FILE_FLOW");
        event5.setData(data);
        event5.setTimestamp(1005L);

        MonitoringEvent event5a = new MonitoringEvent();
        event5a.setAgentId(PAYMENT_PROCESSOR_OUT + "-1");
        event5a.setAgentName(PAYMENT_PROCESSOR_OUT);
        event5a.setCorrelationId(correlationId1);
        event5a.setFlow("FILE_FLOW");
        event5a.setData(data);
        event5a.setTimestamp(1005L);



        List<MonitoringEventI> events = new ArrayList<>();
        events.add(event1);
        events.add(event1a);
        events.add(event2);
        events.add(event2a);
        events.add(event3a);
        events.add(event3b);
        events.add(event3c);
        events.add(event3d);
        events.add(event3e);
        events.add(event3a2);
        events.add(event3b2);
        events.add(event3c2);
        events.add(event3d2);
        events.add(event3e2);
        events.add(event4);
        events.add(event4a);
        events.add(event5);
        events.add(event5a);        
        history1.setEvents(events);
              
        List<ItemHistory> histories = new ArrayList<>();
        histories.add(history1);
        
        engine.update(histories);
        ConnectionGraphDetails graphDetails = engine.getGraphDetails();
        assertNotNull(graphDetails);
        
        ConnectionGraph graph = graphDetails.getElements();
        assertNotNull(graph);
        
        List<GraphNode> nodes = graph.getNodes();
        assertNotNull(nodes);

        List<GraphEdge> edges = graph.getEdges();
        assertNotNull(edges);
        
        //assertEquals(7, nodes.size());
        //assertEquals(7, edges.size());        
        
        System.out.println(graph);

        /*
          
        assertEquals(event1.getAgentId(),  nodes.get(0).getId());
        assertEquals(event2.getAgentId(),  nodes.get(1).getId());
        assertEquals(event3a.getAgentId(), nodes.get(2).getId());
        assertEquals(event3a.getAgentId(), nodes.get(3).getId());
        assertEquals(event4.getAgentId(),  nodes.get(4).getId());
        assertEquals(event5.getAgentId(),  nodes.get(5).getId());
        assertEquals(event6.getAgentId(),  nodes.get(6).getId());

         */
                

    }



}
