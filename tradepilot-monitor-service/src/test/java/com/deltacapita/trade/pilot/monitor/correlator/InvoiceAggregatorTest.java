/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.monitor.correlator;

import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_CREATED;
import static com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants.INSTRUCTION_VALIDATED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.deltacapita.trade.pilot.MonitorConfig;
import com.deltacapita.trade.pilot.Organisation;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;
import com.deltacapita.trade.pilot.monitor.service.DetailBuilder;
import com.deltacapita.trade.pilot.monitor.types.PaymentAndSystemDetails;

public class InvoiceAggregatorTest {
    
    private InvoiceAggregator aggregator;
    
    private DetailBuilder detailBuilder;

    private MonitorConfig serviceConfig;
    
    @Before
    public void setUp() throws Exception {
        
        Organisation org1 = new Organisation();
        org1.setOrgId("ORG1");
        org1.setCity("London");
        org1.setOrgName("ASDA Stores Ltd");
        org1.setCountry("United Kingdom");
        
        Organisation org2 = new Organisation();
        org2.setOrgId("ORG2");
        org2.setCity("New York");
        org2.setOrgName("Home Depot");
        org2.setCountry("United States");
        
        Collection<Organisation> organisations = new ArrayList<>();
        organisations.add(org1);
        organisations.add(org2);
        
        serviceConfig = new MonitorConfig();
        serviceConfig.setOrganisations(organisations);
        
        detailBuilder = new DetailBuilder();
        detailBuilder.setServiceConfig(serviceConfig);
        aggregator = new InvoiceAggregator();
        aggregator.setItemLease(100000L);
        aggregator.setMaxSize(10000L);
        aggregator.setDetailBuilder(detailBuilder);
        aggregator.initialise();   
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetAllInvoices() throws Exception {
              
        Map<String, Object> data = new HashMap<>();
        data.put(MonitoringConstants.SUPPLIER_ID, "SORG1");
        data.put(MonitoringConstants.CURRENCY, "GBP");        
        data.put(MonitoringConstants.AMOUNT, 1000.0);        
        
        List<MonitoringEventI> events = new ArrayList<>();        
        MonitoringEvent event1 = new MonitoringEvent();
        event1.setAgentId(INSTRUCTION_CREATED + "-1");
        event1.setAgentName(INSTRUCTION_CREATED);
        event1.setCorrelationId("correlationId-1");
        event1.setFlow("INVOICE_FLOW");                
        event1.setData(data);
        
        events.add(event1);        
        aggregator.aggregate(events);
        
        Collection<PaymentAndSystemDetails> invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        PaymentAndSystemDetails details = invoices.iterator().next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);

        MonitoringEvent event2 = new MonitoringEvent();
        event2.setAgentId(INSTRUCTION_CREATED + "-1");
        event2.setAgentName(INSTRUCTION_CREATED);
        event2.setCorrelationId("correlationId-2");
        event2.setFlow("INVOICE_FLOW");
        event2.setData(data);

        events.clear();
        events.add(event2);
        aggregator.aggregate(events);
       
        invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        details = invoices.iterator().next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals("File Processor", details.getSystem());
        assertEquals(2000.0, details.getInFlightValue(), 0.001);
        
        
        MonitoringEvent event3 = new MonitoringEvent();
        event3.setAgentId(INSTRUCTION_VALIDATED + "-1");
        event3.setAgentName(INSTRUCTION_VALIDATED);
        event3.setCorrelationId("correlationId-1");
        event3.setFlow("INVOICE_FLOW");
        event3.setData(data);

        events.clear();
        events.add(event3);
        aggregator.aggregate(events);

        invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        Iterator<PaymentAndSystemDetails> it = invoices.iterator();
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());
        
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Validator", details.getSystem());
        
        MonitoringEvent event4 = new MonitoringEvent();
        event4.setAgentId(INSTRUCTION_VALIDATED + "-1");
        event4.setAgentName(INSTRUCTION_VALIDATED);
        event4.setCorrelationId("correlationId-2");
        event4.setFlow("INVOICE_FLOW");
        event4.setData(data);

        events.clear();
        events.add(event4);
        aggregator.aggregate(events);

        invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        it = invoices.iterator();
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        
        assertEquals(0.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());
        
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(2000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Validator", details.getSystem());        
    }



    @Test
    public void testGetAllInvoices2() throws Exception {
              
        Map<String, Object> data = new HashMap<>();
        data.put(MonitoringConstants.SUPPLIER_ID, "SORG1");
        data.put(MonitoringConstants.CURRENCY, "GBP");        
        data.put(MonitoringConstants.AMOUNT, 1000.0);        
        
        List<MonitoringEventI> events = new ArrayList<>();        
        MonitoringEvent event1 = new MonitoringEvent();
        event1.setAgentId(INSTRUCTION_CREATED + "-1");
        event1.setAgentName(INSTRUCTION_CREATED);
        event1.setCorrelationId("correlationId-1");
        event1.setFlow("INVOICE_FLOW");                
        event1.setData(data);

        Map<String, Object> data2 = new HashMap<>();
        data2.put(MonitoringConstants.SUPPLIER_ID, "SORG2");
        data2.put(MonitoringConstants.CURRENCY, "GBP");        
        data2.put(MonitoringConstants.AMOUNT, 1000.0);        
        
        MonitoringEvent event2 = new MonitoringEvent();
        event2.setAgentId(INSTRUCTION_CREATED + "-1");
        event2.setAgentName(INSTRUCTION_CREATED);
        event2.setCorrelationId("correlationId-2");
        event2.setFlow("INVOICE_FLOW");                
        event2.setData(data2);
        
        events.add(event1);        
        events.add(event2);        
        events.add(event2);        
        aggregator.aggregate(events);
        
        Collection<PaymentAndSystemDetails> invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        Iterator<PaymentAndSystemDetails> it = invoices.iterator();
        PaymentAndSystemDetails details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());

        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());
        
        
        MonitoringEvent event3 = new MonitoringEvent();
        event3.setAgentId(INSTRUCTION_VALIDATED + "-1");
        event3.setAgentName(INSTRUCTION_VALIDATED);
        event3.setCorrelationId("correlationId-1");
        event3.setFlow("INVOICE_FLOW");                
        event3.setData(data);
        
        
        MonitoringEvent event4 = new MonitoringEvent();
        event4.setAgentId(INSTRUCTION_VALIDATED + "-1");
        event4.setAgentName(INSTRUCTION_VALIDATED);
        event4.setCorrelationId("correlationId-2");
        event4.setFlow("INVOICE_FLOW");                
        event4.setData(data2);
        
        events.clear();
        events.add(event3);        
        events.add(event3);        
        events.add(event4);        
        aggregator.aggregate(events);
        
        invoices = aggregator.getAllInvoices();
        assertNotNull(invoices);
        it = invoices.iterator();
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(0.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());

        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(0.0, details.getInFlightValue(), 0.001);
        assertEquals("File Processor", details.getSystem());

        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Validator", details.getSystem());
        
        details = it.next();
        assertNotNull(details);
        System.out.println(details);
        assertEquals(1000.0, details.getInFlightValue(), 0.001);
        assertEquals("File Validator", details.getSystem());
    }



}
