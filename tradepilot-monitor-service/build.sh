#!/bin/sh

#docker stop tp-monitor-service
docker rm tp-monitor-service
mvn clean package -Dmaven.test.skip=true -Ddatabase.type=cockroach

docker build -t tp-monitor-service --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-monitor-service dcdockerregistry.azurecr.io/tp-monitor-service:v4.5
#docker push dcdockerregistry.azurecr.io/tp-monitor-service:v1

docker run --name tp-monitor-service --hostname tp-monitor-service -p 1130:1130 -p 9130:9130 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-monitor-service

