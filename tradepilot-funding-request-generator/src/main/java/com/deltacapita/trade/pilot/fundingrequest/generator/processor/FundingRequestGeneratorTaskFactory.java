/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingrequest.generator.processor;

import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;
import com.deltacapita.trade.pilot.fundingrequest.generator.client.FundingProfileServiceClient;

@Component
public class FundingRequestGeneratorTaskFactory implements TaskFactoryI {

    private static final Logger logger = LogManager.getLogger(FundingRequestGeneratorTaskFactory.class);

    @Value("${processor.destination}")
    private String destination;
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;
    
    @Autowired
    private FundingProfileServiceClient fundingProfileServiceClient;
    
    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised with destination={}, client={}, repo={} ",  destination, fundingProfileServiceClient, paymentRequestRepository);
    }
    
    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
        
        logger.info("Creating new task for " + request);
        return new FundingRequestGeneratorTask(request, destination, fileRepository, instructionRepository, fundingProfileServiceClient, paymentRequestRepository, agentFactory);
    }
}
