/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingrequest.generator.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.types.FundingProfile;

@Component
public class FundingProfileServiceFallback implements FundingProfileServiceClient {

    private static final Logger logger = LogManager.getLogger(FundingProfileServiceFallback.class);

    @Override
    public FundingProfile getFundingProfile(String buyerId, String supplierId, String currency) {
        
        logger.warn("Funding Profile Service call failed for buyer=" + buyerId + ", supplier=" + supplierId + ", ccy=" + currency);
        
        return null;
    }

}
