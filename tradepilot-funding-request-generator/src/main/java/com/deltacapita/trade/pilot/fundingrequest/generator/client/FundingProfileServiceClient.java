/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingrequest.generator.client;



import org.springframework.cloud.openfeign.FeignClient;

import com.deltacapita.trade.pilot.core.data.service.FundingProfileServiceI;


@FeignClient(name = "tp-funding-profile-service", fallback = FundingProfileServiceFallback.class)
public interface FundingProfileServiceClient extends FundingProfileServiceI {
}
