/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.fundingrequest.generator.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DEFAULT;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.FUNDING_REQUESTED;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.INVOICE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.NEW;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.PROCESSING;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.IdBuilder;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.FundingProfileI;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;
import com.deltacapita.trade.pilot.fundingrequest.generator.client.FundingProfileServiceClient;

import feign.RetryableException;

public class FundingRequestGeneratorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(FundingRequestGeneratorTask.class); 

    private FundingProfileServiceClient fundingProfileServiceClient;
    
    private PaymentRequestRepository paymentRequestRepository;
    
    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileOutAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fundingRequestedAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> paymentCreatedAgent;

    public FundingRequestGeneratorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, FundingProfileServiceClient fundingProfileServiceClient, PaymentRequestRepository paymentRequestRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        this.fundingProfileServiceClient = fundingProfileServiceClient;
        this.paymentRequestRepository = paymentRequestRepository;
        
        fileInAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_IN);
        fileOutAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.FUNDING_REQUEST_GENERATOR_OUT);
        fundingRequestedAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.FUNDING_REQUESTED);
        paymentCreatedAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.PAYMENT_CREATED);
    }

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);
        
        long start = System.currentTimeMillis();
        logger.info("Handling {}", request);        
        
        String fileId = request.getFileId();
        Optional<File> result = fileRepository.findById(fileId);
        if( result.isPresent() ) {
            
            File file = result.get();   
            String fileState = file.getState();
                       
            logger.info("Injecting {} to {}", request, fileInAgent);        
            fileInAgent.inject(request, file);

            if( fileState.equals(PROCESSING)) {
                
                List<PaymentRequest> paymentRequests = new ArrayList<>();
                List<Instruction> updatedInvoices = new ArrayList<>();

                List<Instruction> instructions = instructionRepository.findByFileId(fileId);
                if( instructions != null ) {                
                    
                    logger.info("Generating funding requests for {}", fileId);
                    for (Instruction instruction : instructions) {
                        
                        String instructionId = instruction.getId();
                        String instructionType = instruction.getType();
                        if( instructionType.equals(INVOICE)) {

                            String buyerId = (instruction.getBuyerId() != null ? instruction.getBuyerId() : DEFAULT );
                            String supplierId = (instruction.getSupplierId() != null ? instruction.getSupplierId() : DEFAULT );
                            String currency = (instruction.getCurrency() != null ? instruction.getCurrency() : DEFAULT );
                            
                            logger.info("Generating funding request for invoice {}, buyer={}, supplier={}, ccy={}", instruction.getId(), buyerId, supplierId, currency);
                            FundingProfileI fundingProfile = getFundingProfile(buyerId, supplierId, currency);
                            
                            if( fundingProfile != null ) {

                                // Very simple calculation as this is just a pilot
                                //
                                logger.info("Got funding profile for {}, {}", instructionId, fundingProfile);
                                
                                double originalRate = fundingProfile.getRate();
                                double rate = originalRate + 0.10;
                                logger.warn("Patching spread for {} to {} from {}", instructionId, rate, originalRate);
                                
                                double invoiceAmount = instruction.getAmount();
                                double fee = invoiceAmount * rate;
                                double paymentAmount = invoiceAmount - fee;
                                
                                PaymentRequest paymentRequest = new PaymentRequest();
                                paymentRequest.setFileId(fileId);
                                paymentRequest.setInstructionId(instructionId);
                                
                                String invoiceNumber = instruction.getInvoiceNumber();
                                //String paymentReference = invoiceNumber + "-patched";
                                String paymentReference = invoiceNumber;
                                logger.warn("Updating payment reference for {} to {} from {}", instructionId, paymentReference, invoiceNumber);
                                paymentRequest.setPaymentReference(paymentReference);
                                
                                paymentRequest.setBuyerId(buyerId);
                                paymentRequest.setSupplierId(supplierId);
                                paymentRequest.setCurrency(currency);
                                paymentRequest.setAmount(paymentAmount);
                                paymentRequest.setDueDate(instruction.getDueDate());
                                paymentRequest.setState(NEW);
                                
                                String id = IdBuilder.buildId(paymentRequest);
                                paymentRequest.setId(id);
                                
                                instruction.setState(FUNDING_REQUESTED);
                                updatedInvoices.add(instruction);
                                
                                logger.info("Created payment request for {}, {}", instructionId, paymentRequest);
                                paymentRequests.add(paymentRequest);
                                
                            } else {
                                
                                logger.info("No funding profile found for {}", instructionId);                                
                            }
                        } 
                    }
                                    
                    logger.info("Saving payment requests for {}", fileId);
                    paymentRequestRepository.saveAll(paymentRequests);
                    paymentCreatedAgent.inject(response, paymentRequests);
                    
                    logger.info("Saving updated instruction states for {}", fileId);
                    instructionRepository.saveAll(updatedInvoices);                    
                    fundingRequestedAgent.inject(response, updatedInvoices);
                    
                    logger.info("Saved {} instructions for file with ID {}", instructions.size(), file.getId());                   
                    file.setState(FUNDING_REQUESTED);
                    logger.info("Updating file status for {}", fileId);
                    fileRepository.save(file);                    
                    
                } else {
                    logger.error("No instructions found for ", fileId );
                }
                
                long end = System.currentTimeMillis();
                logger.info("Generated {} funding requests for {} in {} ms", paymentRequests.size(), fileId, end - start);  
                
            } else {
                
                logger.warn("Ignoring request for file {} in state {}", fileId, fileState);
            } 

            logger.info("Injecting {} to {}", response, fileOutAgent);        
            fileOutAgent.inject(response, file);

        } else {
            logger.info("No entry found for {}", fileId);            
        }
                
        logger.info("Returning {}", response);        
        return response;
    }
    
    private FundingProfileI getFundingProfile(String buyerId, String supplierId, String currency) {
              
        int max = 5;
        Throwable ex = null;
        
        for (int i = 0; i < max; i++) {
            try {
                
                FundingProfileI fundingProfile = fundingProfileServiceClient.getFundingProfile(buyerId, supplierId, currency);
                return fundingProfile;
                
            } catch (RetryableException e) {
                
                logger.info("Call {} of {} for buyer={}, supplier={}, currency={} failed - {}, retrying...", i, max, buyerId, supplierId, currency, e);    
                ex = e;
            }
            
        }
        
        throw new RuntimeException("Service call failed for buyer=" + buyerId + ", supplier=" + supplierId + ", ccy=" + currency, ex);
    }
}
