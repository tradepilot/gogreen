#!/bin/sh

#docker stop tp-funding-request-generator
docker rm tp-funding-request-generator
mvn clean package -Dmaven.test.skip=true

docker build -t tp-funding-request-generator --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-funding-request-generator dcdockerregistry.azurecr.io/tp-funding-request-generator:v1
#docker push dcdockerregistry.azurecr.io/tp-funding-request-generator:v1

docker run --name tp-funding-request-generator --hostname tp-funding-request-generator -p 1220:1220 -p 9220:9220 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-funding-request-generator

