#!/bin/sh -x

#docker stop tp-config-server
docker rm tp-config-server
mvn clean package -Dmaven.test.skip=true

docker build -t tp-config-server -f Dockerfile .
docker tag tp-config-server dcdockerregistry.azurecr.io/tp-config-server:v3
#docker push dcdockerregistry.azurecr.io/tp-config-server:v3

docker run --name tp-config-server --hostname tp-config-server -p 1100:1100 -p 9100:9100 --network trade-pilot-swarm -e SPRING_PROFILE=native tp-config-server

