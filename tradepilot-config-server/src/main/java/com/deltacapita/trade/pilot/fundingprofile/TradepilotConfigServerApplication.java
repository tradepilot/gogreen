package com.deltacapita.trade.pilot.fundingprofile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableConfigServer
@EnableWebMvc
@EnableDiscoveryClient
public class TradepilotConfigServerApplication {

    private static final Logger logger = LogManager.getLogger(TradepilotConfigServerApplication.class);

	public static void main(String[] args) {
	    try {
	        SpringApplication.run(TradepilotConfigServerApplication.class, args);
            
        } catch (Throwable t) {
            logger.warn("Error initialising TradepilotConfigServerApplication", t);
        }
	}
}
