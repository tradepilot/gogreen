package com.deltacapita.trade.pilot.metrics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.TradepilotMetricsServiceApplication;

import io.prometheus.client.Gauge;

@Component
public class TestMetricsSource {

    private static final Logger logger = LogManager.getLogger(TradepilotMetricsServiceApplication.class);
         
    private static final Gauge absoluteGauge = Gauge.build().name("tp_requests_absolute").help("Total request count").register();
    
    private static final Gauge rateGauge = Gauge.build().name("tp_requests_total").help("Total request rate").register();

    
    public String processRequest(String name) {
        
        double beforeRate = rateGauge.get();
        double beforeAbsolute = absoluteGauge.get();
        
        rateGauge.inc();
        absoluteGauge.inc();
        
        double afterRate = rateGauge.get();       
        double afterAbsolute = absoluteGauge.get();       
     
        logger.info("Incremented rate request count from {} to {}", beforeRate, afterRate);
        logger.info("Incremented absolute request count from {} to {}", beforeAbsolute, afterAbsolute);
        
        return String.format("Gauge %s incremented from %2.0f to %2.0f", name, beforeRate, afterRate);
     }
    
    
    public String reset(String name) {
        
        double beforeRate = rateGauge.get();
        double beforeAbsolute = absoluteGauge.get();
        
        rateGauge.set(0);
        absoluteGauge.set(0);
        
        logger.info("Reset rateGauge from {} to 0", beforeRate);
        logger.info("Reset absoluteGauge from {} to 0", beforeAbsolute);

        return String.format("Reset gauge %s from %2.0f to 0", name, beforeRate);
}
    
}
