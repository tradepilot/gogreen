package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.deltacapita.trade.pilot.metrics.TestMetricsSource;

import io.prometheus.client.exporter.MetricsServlet;

@SpringBootApplication
public class TradepilotMetricsServiceApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotMetricsServiceApplication.class);

    
    @Autowired
    private TestMetricsSource metricsSource;
    
    
	public static void main(String[] args) {
		SpringApplication.run(TradepilotMetricsServiceApplication.class, args);
	}
	
    @Override
    public void run(String... args) throws Exception {
        
        logger.info("Initialised with metrics source {}", metricsSource);     
	}
    
    @Bean
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ServletRegistrationBean servletRegistrationBean(){
        return new ServletRegistrationBean(new MetricsServlet(),"/metrics/*");
    }
}
