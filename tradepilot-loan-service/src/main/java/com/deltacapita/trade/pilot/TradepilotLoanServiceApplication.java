/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.loanservice.LoanService;

@SpringBootApplication
@EnableDiscoveryClient
public class TradepilotLoanServiceApplication  implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotLoanServiceApplication.class);

    @Autowired
    private LoanService service;

	public static void main(String[] args) {
	    
        ServiceUtils.logIpAddresses();
		SpringApplication.run(TradepilotLoanServiceApplication.class, args);
	}
	
    @Override
    public void run(String... args) throws Exception {
        logger.info("Initialised with {}", service);
    }  
}
