/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.loanservice;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DEFAULT;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deltacapita.trade.pilot.core.data.repo.LoanProfileRepository;
import com.deltacapita.trade.pilot.core.data.service.LoanServiceI;
import com.deltacapita.trade.pilot.core.data.types.LoanProfile;


@RestController 
public class LoanService implements LoanServiceI {

    private static final Logger logger = LogManager.getLogger(LoanService.class);

    
    @Autowired
    private LoanProfileRepository profileRepository;
    

    @PostConstruct
    public void initialise() throws Exception {
        logger.info("Initialised with repository {} ", profileRepository );      
    }
    
    @Override
    @RequestMapping(value = "/loan/{buyerId}/{currency}", method = RequestMethod.GET, produces = "application/json")
    public LoanProfile getLoanProfile(@PathVariable("buyerId") String buyerId, @PathVariable("currency") String currency) {
        
        LoanProfile profile = findProfile(buyerId, currency);
        if( profile == null ) {
            
            logger.info("Falling back to default currency for buyerId={}", buyerId);             
            profile = findProfile(buyerId, DEFAULT);
            if( profile == null ) {
                
                logger.info("Falling back to default profile");             
                profile = findProfile( DEFAULT, DEFAULT);
            } 
        } 

        return profile;
    }
    
    private LoanProfile findProfile( String buyerId, String currency) {
        
        logger.info("Looking for loan profile for buyerId={}, currency={}", buyerId, currency);             
        if( profileRepository != null ) {
            
            List<LoanProfile> profiles = profileRepository.findByBuyerIdAndCurrency(buyerId, currency);
            if( !profiles.isEmpty() ) {
                
                if( profiles.size() != 1 ) {
                    
                    logger.error("More than one profile found for buyerId={}, currency={}" , buyerId, currency);
                    return null;
                    
                } else {
                    
                    LoanProfile profile = profiles.get(0);
                    logger.info("Found {}" , profile);
                    return profile;
                }
            }
        }        

        return null;
    }
}
