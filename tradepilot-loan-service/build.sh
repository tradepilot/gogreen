#!/bin/sh

#docker stop tp-loan-service
docker rm tp-loan-service
mvn clean package -Dmaven.test.skip=true -U

docker build -t tp-loan-service --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-loan-service dcdockerregistry.azurecr.io/tp-loan-service:v2
#docker push dcdockerregistry.azurecr.io/tp-loan-service:v2

docker run --name tp-loan-service --hostname tp-loan-service -p 1310:1310 -p 9310:9310 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-loan-service

