#!/bin/sh

docker rm tp-request-generator
mvn clean package -Dmaven.test.skip=true

NEW_MEM="--build-arg NEW_MEM_ARG=128m"
MIN_MEM="--build-arg MIN_MEM_ARG=256m"
MAX_MEM="--build-arg MAX_MEM_ARG=256m"
SPRING_PROFILE="--build-arg SPRING_PROFILE_ARG=azure-k8s"

NUM_DOCS="--build-arg NUM_DOCS_ARG=10"
NUM_BPRNS="--build-arg NUM_BPRNS_ARG=2"
NUM_INVOICES="--build-arg NUM_INVOICES_ARG=2"
NUM_CREDIT_NOTES="--build-arg NUM_CREDIT_NOTES_ARG=0"
DOCS_PER_HOUR="--build-arg DOCS_PER_HOUR_ARG=3600"
PRINT_MESSAGES="--build-arg PRINT_MESSAGES_ARG=true"

COMMON_ARGS="${NEW_MEM} ${MIN_MEM} ${MAX_MEM} ${SPRING_PROFILE}"
COMMON_OPTIONS="${NUM_DOCS} ${NUM_BPRNS} ${NUM_INVOICES} ${NUM_CREDIT_NOTES} ${DOCS_PER_HOUR} ${PRINT_MESSAGES}"

docker build -t tp-request-generator ${COMMON_ARGS} ${COMMON_OPTIONS} -f Dockerfile .
#docker tag tp-request-generator dcdockerregistry.azurecr.io/tp-request-generator:v4.5
#docker push dcdockerregistry.azurecr.io/tp-request-generator:v1

docker run --name tp-request-generator --hostname tp-request-generator -p 1400:1400 -p 9400:9400  --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-request-generator

