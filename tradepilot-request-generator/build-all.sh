#!/bin/bash

pushd ../tradepilot-docker

./build-container.sh request-generator 100 azure-k8s yes no Dockerfile
./build-container.sh request-generator 200 azure-k8s yes no Dockerfile-200
./build-container.sh request-generator 500 azure-k8s yes no Dockerfile-500
./build-container.sh request-generator 1000 azure-k8s yes no Dockerfile-1000
./build-container.sh request-generator 100000 azure-k8s yes no Dockerfile-100000

popd
