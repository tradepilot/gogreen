/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.adapter;

import java.io.UnsupportedEncodingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("file")
public final class FileAdapter implements AdapterI {

    private static final Logger logger = LogManager.getLogger(FileAdapter.class);

    private boolean printMessages;

    @Override
    public void send(byte[] data) {

        try {
            String doc = new String(data, "UTF-8");
            if( printMessages) {
                logger.info("Generated document " + doc + "");
            }
        } catch (UnsupportedEncodingException e) {
            logger.warn("Error adapting data " + data, e);
        }
    }
    
    @Override
    public void setPrintMessages(boolean printMessages) {
        this.printMessages = printMessages;     
    }
}
