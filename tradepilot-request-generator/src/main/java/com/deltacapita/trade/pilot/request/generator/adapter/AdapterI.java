/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.adapter;

public interface AdapterI {

    void setPrintMessages( boolean printMessages);
    
    /**
     *
     * @param data
     */
    void send(byte[] data);
}
