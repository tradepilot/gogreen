/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.deltacapita.trade.pilot.request.generator.config.RequestGeneratorConfig;

/**
 * Abstract base class for all Factories which generate Client Funding Requests
 * in various formats
 */
@EnableConfigurationProperties
public abstract class RequestGeneratorFactory {

    @Autowired
    private RequestGeneratorConfig config;

    protected boolean randomiseDate = true;

    /**
     * Create a new Client Funding Request containing "random" data
     *
     * @param numInstructions
     *            the number of instructions the CFR is to contain
     * @return array of bytes containing CFR representation
     */
    public abstract byte[] generate(final int numInstructions, final int numBprns, final int numCreditNotes);

    public RequestGeneratorConfig getConfig() {
        return config;
    }

    public void setConfig(RequestGeneratorConfig config) {
        this.config = config;
    }
    
    public void setRandomiseDate(boolean randomiseDate) {
        this.randomiseDate = randomiseDate;
    }
}
