/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.adapter;

import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.request.generator.config.RequestGeneratorConfig;

@Component
@Profile("!file")
public final class KafkaAdapter implements AdapterI {

    private static final Logger logger = LogManager.getLogger(KafkaAdapter.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private RequestGeneratorConfig config;

    private String topic;
    
    private boolean printMessages;

    @PostConstruct
    public void initialise() throws Exception {

        topic = config.getTopic();
        logger.info("Initialised with topic " + topic);
    }

    @Override
    public void send(byte[] data) {

        try {

            String message = new String(data, "UTF-8");
            if( printMessages ) {
                logger.debug("Sending message to topic " + topic + " " + message);  
            } else {
                logger.debug("Sending message to topic " + topic );                  
            }
            
            kafkaTemplate.send(topic, UUID.randomUUID().toString(), message);
            
        } catch (Exception e) {
            logger.warn("Error adapting data " + data, e);
        }
    }

    @Override
    public void setPrintMessages(boolean printMessages) {
        this.printMessages = printMessages;     
    }

}
