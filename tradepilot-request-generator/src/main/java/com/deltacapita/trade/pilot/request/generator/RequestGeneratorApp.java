
/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/*
 * Run with "--interactive=<true|false> --numDocuments=<2> --numBprns=<2> --numInvoices=<4> --numCreditNotes=<2> --documentsPerHour=<3600>"
 */
@SpringBootApplication
@ComponentScan("com.deltacapita.trade.pilot")
public class RequestGeneratorApp implements ApplicationRunner {

    private static final Logger logger = LogManager.getLogger(RequestGeneratorApp.class);

    private static final String MAX_DOCUMENTS_ARG = "numDocuments";

    private static final String DOCS_PER_HOUR_ARG = "documentsPerHour";

    private static final String NUM_INVOICES_ARG = "numInvoices";

    private static final String NUM_CREDIT_NOTES_ARG = "numCreditNotes";

    private static final String NUM_BPRNS_ARG = "numBprns";

    private static final String PRINT_MESSAGES_ARG = "printMessages";

    private static final String INTERACTIVE_ARG = "interactive";

    private static final int MILLISECONDS_PER_HOUR = 60 * 60 * 1000;

    private static final int MAX_DOCUMENTS = 10000000;

    private static final int MAX_NUMBER_INSTRUCTIONS = 100000;

    private static final int MAX_DOCS_PER_HOUR = 9999999;

    private static final int DEFAULT_DOC_COUNT = 4;

    private static final int DEFAULT_DOCS_PER_HOUR = 3600;

    private static final int DEFAULT_INVOICE_COUNT = 2;

    private static final int DEFAULT_CREDIT_NOTE_COUNT = 0;

    private static final int DEFAULT_BPRN_COUNT = 1;

    private static ConfigurableApplicationContext appContext;

    private static boolean interactive = false;

    @Autowired
    private RequestGenerator generator;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(RequestGeneratorApp.class);
        appContext = app.run(args);

        if( !interactive ) {            
            SpringApplication.exit(appContext);
        }
        
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        try {

            logger.info("Starting with args " + Arrays.toString(args.getSourceArgs()));
            interactive = getBooleanValueOrDefault(args, INTERACTIVE_ARG, false);
            
            if( !interactive ) {
                
                final int maxDocs = getValueOrDefault(args, MAX_DOCUMENTS_ARG, DEFAULT_DOC_COUNT);
                final int docsPerHour = getValueOrDefault(args, DOCS_PER_HOUR_ARG, DEFAULT_DOCS_PER_HOUR);
                final int numInvoices = getValueOrDefault(args, NUM_INVOICES_ARG, DEFAULT_INVOICE_COUNT);
                final int numCreditNotes = getValueOrDefault(args, NUM_CREDIT_NOTES_ARG, DEFAULT_CREDIT_NOTE_COUNT);
                final int numBprns = getValueOrDefault(args, NUM_BPRNS_ARG, DEFAULT_BPRN_COUNT);
                final boolean printMessages = getBooleanValueOrDefault(args, PRINT_MESSAGES_ARG, true);           
                
                if ( (maxDocs < 0 || maxDocs > MAX_DOCUMENTS) || (docsPerHour < 1 || docsPerHour > MAX_DOCS_PER_HOUR)
                || (numInvoices < 0 || numInvoices > MAX_NUMBER_INSTRUCTIONS)) {

                    usage();

                } else {
                    
                    Integer delay = MILLISECONDS_PER_HOUR / docsPerHour;
                    logger.info("Generating {} documents at a rate of {} documents per hour with a delay of {} ms with {} printMessages {}", maxDocs, docsPerHour, delay, printMessages);
                    generator.generate(numInvoices, numCreditNotes, numBprns, maxDocs, docsPerHour, true, printMessages);
                    
                    generator.waitForCompletion();
                    logger.info("Shutting down....");            
                    generator.shutdown();  
                        
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            usage();
        }
    }

    private int getValueOrDefault(ApplicationArguments args, String optionName, int defaultValue) {

        if (args.containsOption(optionName)) {

            List<String> optionValues = args.getOptionValues(optionName);
            if (optionValues != null && optionValues.size() == 1) {
                return Integer.valueOf(optionValues.get(0));
            }
        }
        
        logger.info("No argument found for {}, using defailt value {}", optionName, defaultValue);
        return defaultValue;
    }

    private boolean getBooleanValueOrDefault(ApplicationArguments args, String optionName, boolean defaultValue) {

        if (args.containsOption(optionName)) {

            List<String> optionValues = args.getOptionValues(optionName);
            if (optionValues != null && optionValues.size() == 1) {
                return Boolean.parseBoolean(optionValues.get(0));
            }
        }

        logger.info("No argument found for {}, using defailt value {}", optionName, defaultValue);
        return defaultValue;
    }

    private void usage() {
        System.err.println("Usage: " + getClass().getName() + "--numDocuments=<2> --numBprns=<2> --numInvoices=<4> --numCreditNotes=<2> --documentsPerHour=<3600> --printMessages=<true|false>");
        System.exit(-1);
    }

}
