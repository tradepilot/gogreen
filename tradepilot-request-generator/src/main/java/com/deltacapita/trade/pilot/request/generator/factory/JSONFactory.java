/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.factory;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.CREDIT_NOTE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.INVOICE;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.UTC;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import com.deltacapita.trade.pilot.request.generator.config.Organisation;

@SuppressWarnings("deprecation")
@Service
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "request-generator.templates")
public class JSONFactory extends RequestGeneratorFactory {

    private static final Logger logger = LogManager.getLogger(JSONFactory.class);

    private Random random = new Random();

    private List<Organisation> supplierList;

    private List<String> currencies;

    private List<String> amounts;

    private List<String> creditNoteAmounts;

    private List<String> tenors;

    private List<LocalDate> dueDates;

    private String headerTemplateFile;

    private String instructionTemplateFile;

    private String footerTemplateFile;

    private String headerTemplate;

    private String instructionTemplate;

    private String footerTemplate;

    private long requestId = 0;

    private LocalDate currentDate;


    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialising with header template " + headerTemplateFile);
        logger.info("Initialising with instruction template " + instructionTemplateFile);
        logger.info("Initialising with footer template " + footerTemplateFile);

        headerTemplate = loadTemplate(headerTemplateFile);
        instructionTemplate = loadTemplate(instructionTemplateFile);
        footerTemplate = loadTemplate(footerTemplateFile);
        
        supplierList = new ArrayList<>(getConfig().getOrganisations());
        currencies = getConfig().getCurrencies();
        amounts = getConfig().getAmounts();
        creditNoteAmounts = getConfig().getCreditNoteAmounts();
        tenors = getConfig().getTenors();

        createSettlementDates();
    }

    @Override
    public byte[] generate(final int numInvoices, final int numBprns, final int numCreditNotes) {

        List<String> bprns = new ArrayList<>(numBprns);
        for (int i = 0; i < numBprns; i++) {
            String bprn = "BPRN-" + i;
            bprns.add(bprn);
        }

        requestId++;
        int totalInstructions = numInvoices + numCreditNotes;

        Map<String, String> valueMap = new HashMap<>();
        StrSubstitutor sub = new StrSubstitutor(valueMap);
        valueMap.put("numInstructions", String.valueOf(numInvoices));

        int index = random.nextInt(supplierList.size());
        Organisation buyer = supplierList.get(index);

        Long controlSum = 0L;
        StringBuilder sb1 = new StringBuilder();

        for (int i = 0; i < numBprns; i++) {

            String bprn = bprns.get(i);
            for (int j = 0; j < numInvoices; j++) {

                Organisation supplier = getSupplier(buyer);

                index = random.nextInt(amounts.size());
                String amount = amounts.get(index);

                index = random.nextInt(currencies.size());
                String ccy = currencies.get(index);

                LocalDate dueDate = getDueDate();
                String instructionId = String.format("%s-%01d-%010d", currentDate, i, j);
                
                valueMap.put("invoiceNumber", instructionId);
                valueMap.put("bprn", bprn);

                valueMap.put("buyerId", "B" + buyer.getOrgId());
                valueMap.put("supplierId", "S" + supplier.getOrgId());
                valueMap.put("supplierName", supplier.getOrgName());
                valueMap.put("amount", amount);
                valueMap.put("ccy", ccy);
                valueMap.put("dueDate", dueDate.toString());
                valueMap.put("instructionType", INVOICE);

                controlSum += Long.valueOf(amount);
                String instruction = sub.replace(instructionTemplate);
                
                if (j != numInvoices - 1 ) {
                    instruction = instruction.replace("}", "},");
                } else {
                    
                    if( i != numBprns - 1 || numCreditNotes > 0) {
                        instruction = instruction.replace("}", "},");
                    }
                }

                sb1.append(instruction);
            }

            if( numCreditNotes > 0 ) {
                for (int k = numInvoices; k < totalInstructions; k++) {

                    Organisation supplier = getSupplier(buyer);

                    index = random.nextInt(creditNoteAmounts.size());
                    String amount = creditNoteAmounts.get(index);

                    index = random.nextInt(currencies.size());
                    String ccy = currencies.get(index);

                    index = random.nextInt(dueDates.size());
                    LocalDate dueDate = getDueDate();

                    String instructionId = String.format("%s-%01d-%010d", currentDate, i, k);
                    valueMap.put("invoiceNumber", instructionId);
                    valueMap.put("bprn", bprn);

                    valueMap.put("buyerId", "B" + buyer.getOrgId());
                    valueMap.put("supplierId", "S" + supplier.getOrgId());
                    valueMap.put("supplierName", supplier.getOrgName());
                    valueMap.put("amount", amount);
                    valueMap.put("ccy", ccy);
                    valueMap.put("dueDate", dueDate.toString());
                    valueMap.put("instructionType", CREDIT_NOTE);

                    controlSum -= Long.valueOf(amount);
                    String instruction = sub.replace(instructionTemplate);
                    if (k != totalInstructions - 1 || i != numBprns - 1) {
                        instruction = instruction.replace("}", "},");
                    }

                    sb1.append(instruction);
                }                
            }
        }

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        String receivedTime = dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        long currentTimeMillis = System.currentTimeMillis();
        valueMap.put("requestId", String.valueOf(currentTimeMillis + requestId));
        valueMap.put("buyerId", "B" + buyer.getOrgId());
        valueMap.put("receivedTime", receivedTime);
        valueMap.put("controlSum", String.valueOf(controlSum));
        valueMap.put("numInstructions", String.valueOf(totalInstructions * numBprns));

        String header = sub.replace(headerTemplate);
        String footer = sub.replace(footerTemplate);

        StringBuilder sb = new StringBuilder();
        sb.append(header);
        sb.append(sb1);
        sb.append(footer);

        return sb.toString().getBytes();
    }

    public String getHeaderTemplateFile() {
        return headerTemplateFile;
    }

    public void setHeaderTemplateFile(String headerTemplateFile) {
        this.headerTemplateFile = headerTemplateFile;
    }

    public String getInstructionTemplateFile() {
        return instructionTemplateFile;
    }

    public void setInstructionTemplateFile(String instructionTemplateFile) {
        this.instructionTemplateFile = instructionTemplateFile;
    }

    public String getFooterTemplateFile() {
        return footerTemplateFile;
    }

    public void setFooterTemplateFile(String footerTemplateFile) {
        this.footerTemplateFile = footerTemplateFile;
    }
    
    private String loadTemplate(String templateName) throws Exception {

        logger.info("Loading template " + templateName);
        StringBuilder sb = new StringBuilder();

        try (InputStream in = getClass().getResourceAsStream(templateName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));) {

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }

        return sb.toString();
    }

    private void createSettlementDates() {

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        currentDate = dateTime.toLocalDate();
        dueDates = new ArrayList<>(tenors.size());

        for (String tenor : tenors) {

            Long days = Long.valueOf(tenor);
            LocalDateTime dueDateTime = LocalDateTime.from(dateTime).plusDays(days);
            LocalDate dueDate = dueDateTime.toLocalDate();

            dueDates.add(dueDate);
        }
    }

    /*
     * Gets a random supplier, making sure its always different from the buyer
     */
    private Organisation getSupplier(Organisation buyer) {

        Organisation supplier = null;

        do {

            int index = random.nextInt(supplierList.size());
            supplier = supplierList.get(index);

        } while (buyer.equals(supplier));

        return supplier;
    }


    private LocalDate getDueDate() {
        
        if( randomiseDate ) {
            
            int index = random.nextInt(dueDates.size());
            LocalDate dueDate = dueDates.get(index);
    
            return dueDate;
        
        } else {
            return dueDates.get(0);
        }        
    }
}
