/*
 * Copyright (c) 2019. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.request.generator.adapter.AdapterI;
import com.deltacapita.trade.pilot.request.generator.config.RequestGeneratorConfig;
import com.deltacapita.trade.pilot.request.generator.factory.RequestGeneratorFactory;

/*
 * 
 */
@Component
public class RequestGenerator {
    
    private static final Logger logger = LogManager.getLogger(RequestGenerator.class);

    private static final int MILLISECONDS_PER_HOUR = 60 * 60 * 1000;

    @Autowired
    private RequestGeneratorFactory factory;

    @Autowired
    private AdapterI adapter;

    @Autowired
    private RequestGeneratorConfig config;
    
    private ScheduledExecutorService executor;
    
    private volatile AtomicBoolean isRunning = new AtomicBoolean(false);

    private ScheduledFuture<?> future;
    
    private AtomicInteger documentCount = new AtomicInteger(0);

    @PostConstruct
    public void initialise() throws Exception {

        int poolSize = config.getPoolSize();
        executor = Executors.newScheduledThreadPool(poolSize, new ThreadFactory() {

            private int threadNo = 0;

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "GeneratorThread-" + threadNo++);
            }
        });
    }


    public void generate(final int numInvoices, final int numCreditNotes, final int numBprns, final int maxDocs, final int docsPerHour, boolean randomiseDates, boolean printMessages) throws Exception {

        try {
                isRunning.set(true);
                Integer delay = MILLISECONDS_PER_HOUR / docsPerHour;
                final long start = System.currentTimeMillis();

                int poolSize = config.getPoolSize();
                logger.info("Generating {} documents at a rate of {} documents per hour with a delay of {} ms using {} threads with randomise dates {} print messages {}", maxDocs, docsPerHour, delay, poolSize, randomiseDates, printMessages);
                factory.setRandomiseDate(randomiseDates);
                adapter.setPrintMessages(printMessages);
                
                documentCount.set(0);
                logger.info("Creating " + (numInvoices + numCreditNotes) + " instructions per BPRN across " + numBprns + " BPRNs");
                future = executor.scheduleWithFixedDelay(() -> {

                    if (documentCount.get() < maxDocs) {

                        logger.info("Generating document {} of {}", (documentCount.get() + 1), maxDocs);
                        byte[] cfrBytes = factory.generate(numInvoices, numBprns, numCreditNotes);
                        adapter.send(cfrBytes);
                        
                        int totalFiles = documentCount.incrementAndGet();
                        long totalInvoices = totalFiles * numInvoices * numBprns;
                        long totalCreditNotes = totalFiles * numCreditNotes * numBprns;                        
                        logger.info("Generated {} files, {} invoices, {} credit notes", totalFiles, totalInvoices, totalCreditNotes);

                    } else {

                        long end = System.currentTimeMillis();
                        logger.info("Generated {} documents in {} ms", documentCount.get(), (end - start));
                        
                        future.cancel(true);
                        isRunning.set(false);
                        future = null;
                    }

                }, 1000, delay, TimeUnit.MILLISECONDS);
            
        } catch (Exception e) {
            logger.error("Error generating data", e);
        } 
    }
    
    public void shutdown() {

        try {
            executor.shutdown();
            executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            // Ignore           
        }
        
    }
    
    public void waitForCompletion() {
        
        try {
            
            if( future != null ) {
                future.get(1, TimeUnit.MINUTES);
            }    
            
        } catch (Exception e) {
            // Ignore           
        }
   }
    
    public int stop() {
        
        if( future != null ) {
            
            logger.info("Stopping generator");
            future.cancel(true);
            future = null;
            isRunning.set(false);
        }
        
        logger.info("Stopped generator after {} files", documentCount.get());        
        return documentCount.get();
    }    
    
    public boolean isRunning() {
        return isRunning.get();
    }
    
    public int getDocumentCount() {
        return documentCount.get();
    }
}
