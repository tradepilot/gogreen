
/*
 * Copyright (c) 2019. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.request.generator.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deltacapita.trade.pilot.core.data.repo.CustomerRepository;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.Customer;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.Loan;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;
import com.deltacapita.trade.pilot.request.generator.RequestGenerator;

@RestController
public class RequestGeneratorService {
        
    private static final Logger logger = LogManager.getLogger(RequestGeneratorService.class);
    
    @Autowired
    private RequestGenerator generator;
    
    @Autowired
    private CustomerRepository customerRepository;    

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private PaymentRequestRepository paymentRepository;
    
    @Autowired
    private LoanRepository loanRepository;
    
    
    @RequestMapping(value = "/start/{numInvoices}/{numCreditNotes}/{numBprns}/{maxDocs}/{docsPerHour}/{randomiseDates}/{printMessages}", method = RequestMethod.GET, produces = "application/json")
    public String start( 
            @PathVariable("numInvoices") final int numInvoices, 
            @PathVariable("numCreditNotes") final int numCreditNotes, 
            @PathVariable("numBprns") final int numBprns, 
            @PathVariable("maxDocs") final int maxDocs, 
            @PathVariable("docsPerHour") final int docsPerHour,
            @PathVariable("randomiseDates") final boolean randomiseDates,
            @PathVariable("printMessages") final boolean printMessages
            ) throws Exception {
        
        if( !isRunning() ) {
            
            logger.info("Starting request generator for {} files with {} invoices and {} credit notes across {} BPRNS at a rate of {} documents per hour with random dates {}", maxDocs, numInvoices, numCreditNotes, numBprns, docsPerHour, randomiseDates );
            generator.generate(numInvoices, numCreditNotes, numBprns, maxDocs, docsPerHour, randomiseDates, printMessages);
            return "Generator Started";
            
        } else {
            logger.info("Generator already running");            
            return "Generator Already Running";
        }
    }
    
    @RequestMapping(value = "/stop", method = RequestMethod.GET, produces = "application/json")
    public RestResponse stop() {
        
        RestResponse response = new RestResponse();
        int count = generator.stop();

        response.setMessage("Stopped after " + count + " files");
        return response;
    }
    
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    public boolean isRunning() {
        return generator.isRunning();
    }
    
    @RequestMapping(value = "/count", method = RequestMethod.GET, produces = "application/json")
    public int getDocumentCount() {
        return generator.getDocumentCount();
    }

    @RequestMapping(value = "/files", method = RequestMethod.GET, produces = "application/json")
    public List<File> getFiles() {
        
        List<File> files = new ArrayList<>();
        Iterable<File> it = fileRepository.findAll();
        
        for (File file : it) {
            files.add(file);
        }
        
        return files;
    }

    @RequestMapping(value = "/files/{fileId}", method = RequestMethod.GET, produces = "application/json")
    public File getFileById(@PathVariable("fileId") final String fileId) {
        
        Optional<File> found = fileRepository.findById(fileId);        
        if( found.isPresent() ) {
            return found.get();
        }
        
        return null;
    }
    
    @RequestMapping(value = "/files/buyer/{buyerId}", method = RequestMethod.GET, produces = "application/json")
    public List<File> getFilesForBuyer(@PathVariable("buyerId") final String buyerId) {
        
        List<File> files = new ArrayList<>();
        Iterable<File> it = fileRepository.findByBuyerId(buyerId);
        
        for (File file : it) {
            files.add(file);
        }
        
        return files;
    }
    
    
    @RequestMapping(value = "/instructions", method = RequestMethod.GET, produces = "application/json")
    public List<Instruction> getInstructions() {
        
        List<Instruction> instructions = new ArrayList<>();
        Iterable<Instruction> it = instructionRepository.findAll();
        
        for (Instruction instruction : it) {
            instructions.add(instruction);
        }
        
        return instructions;
    }

    
    @RequestMapping(value = "/instructions/{instructionId}", method = RequestMethod.GET, produces = "application/json")
    public Instruction getInstructionById(@PathVariable("instructionId") final String instructionId) {
        
        Optional<Instruction> found = instructionRepository.findById(instructionId);        
        if( found.isPresent() ) {
            return found.get();
        }
        
        return null;
    }

    @RequestMapping(value = "/instructions/buyer/{buyerId}", method = RequestMethod.GET, produces = "application/json")
    public List<Instruction> getInstructionByBuyerId(@PathVariable("buyerId") final String buyerId) {
        
        List<Instruction> instructions = new ArrayList<>();
        Iterable<Instruction> it = instructionRepository.findByBuyerId(buyerId);
        
        for (Instruction instruction : it) {
            instructions.add(instruction);
        }
        
        return instructions;
    }

    @RequestMapping(value = "/instructions/buyer/type/{buyerId}/{instructionType}", method = RequestMethod.GET, produces = "application/json")
    public List<Instruction> getInstructionByBuyerIdAndType(@PathVariable("buyerId") final String buyerId, @PathVariable("instructionType") final String instructionType) {
        
        List<Instruction> instructions = new ArrayList<>();
        Iterable<Instruction> it = instructionRepository.findByBuyerIdAndType(buyerId, instructionType);
        
        for (Instruction instruction : it) {
            instructions.add(instruction);
        }
        
        return instructions;
    }
    
    @RequestMapping(value = "/instructions/file/{fileId}", method = RequestMethod.GET, produces = "application/json")
    public List<Instruction> getInstructionByFileId(@PathVariable("fileId") final String fileId) {
        
        List<Instruction> instructions = new ArrayList<>();
        Iterable<Instruction> it = instructionRepository.findByFileId(fileId);
        
        for (Instruction instruction : it) {
            instructions.add(instruction);
        }
        
        return instructions;
    }

    @RequestMapping(value = "/instructions/file/type/{fileId}/{instructionType}", method = RequestMethod.GET, produces = "application/json")
    public List<Instruction> getInstructionByFileAndType(@PathVariable("fileId") final String fileId, @PathVariable("instructionType") final String instructionType) {
        
        List<Instruction> instructions = new ArrayList<>();
        Iterable<Instruction> it = instructionRepository.findByFileIdAndType(fileId, instructionType);
        
        for (Instruction instruction : it) {
            instructions.add(instruction);
        }
        
        return instructions;
    }
    
    @RequestMapping(value = "/customers", method = RequestMethod.GET, produces = "application/json")
    public List<Customer> getCustomers() {
        
        List<Customer> customers = new ArrayList<>();
        Iterable<Customer> it = customerRepository.findAll();
        
        for (Customer customer : it) {
            customers.add(customer);
        }
        
        return customers;
    }


    @RequestMapping(value = "/payments", method = RequestMethod.GET, produces = "application/json")
    public List<PaymentRequest> getPaymentRequests() {
        
        List<PaymentRequest> payments = new ArrayList<>();
        Iterable<PaymentRequest> it = paymentRepository.findAll();
        
        for (PaymentRequest customer : it) {
            payments.add(customer);
        }
        
        return payments;
    }


    @RequestMapping(value = "/loans", method = RequestMethod.GET, produces = "application/json")
    public List<Loan> getLoans() {
        
        List<Loan> loans = new ArrayList<>();
        Iterable<Loan> it = loanRepository.findAll();
        
        for (Loan loan : it) {
            loans.add(loan);
        }
        
        return loans;
    }

    private static class RestResponse {
        
        private String message;

        @SuppressWarnings("unused")
		public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
    
    private static class ClearData {
        
        private long files; 
        
        private long instructions; 
        
        private long payments; 
        
        private long loans;

        @SuppressWarnings("unused")
        public long getFiles() {
            return files;
        }

        public void setFiles(long files) {
            this.files = files;
        }

        @SuppressWarnings("unused")
        public long getInstructions() {
            return instructions;
        }

        public void setInstructions(long instructions) {
            this.instructions = instructions;
        }

        @SuppressWarnings("unused")
        public long getPayments() {
            return payments;
        }

        public void setPayments(long payments) {
            this.payments = payments;
        }

        @SuppressWarnings("unused")
        public long getLoans() {
            return loans;
        }

        public void setLoans(long loans) {
            this.loans = loans;
        }
        
    }

    @RequestMapping(value = "/clear", method = RequestMethod.GET, produces = "application/json")
    public ClearData clear() {
        
        ClearData results = new ClearData();

        try {            
            
            long files = fileRepository.count();
            long instructions = instructionRepository.count();
            long payments = paymentRepository.count();
            long loans = loanRepository.count();

            results.setFiles(files);
            results.setInstructions(instructions);
            results.setPayments(payments);
            results.setLoans(loans);

            fileRepository.deleteAll();
            instructionRepository.deleteAll();
            paymentRepository.deleteAll();
            loanRepository.deleteAll();

            String message = "Cleared " + files + " files, " + instructions + " instructions, " + payments + " payments, " + loans + " loans";
            logger.info(message);

        } catch (Exception e) {
            logger.error("Error clearing database " + e.getMessage());
        }

        return results;
    }

}
