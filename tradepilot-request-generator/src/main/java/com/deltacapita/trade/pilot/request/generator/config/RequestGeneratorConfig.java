/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.request.generator.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "request-generator")
public class RequestGeneratorConfig {

    private String appName;

    private String topic;

    private int poolSize;

    private Collection<Organisation> organisations = new ArrayList<Organisation>();

    private List<String> currencies;

    private List<String> amounts;

    private List<String> creditNoteAmounts;

    private List<String> tenors;

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setOrganisations(List<Organisation> organisations) {
        this.organisations = organisations;

    }

    public Collection<Organisation> getOrganisations() {
        return organisations;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

    public List<String> getAmounts() {
        return amounts;
    }

    public void setAmounts(List<String> amounts) {
        this.amounts = amounts;
    }

    public List<String> getCreditNoteAmounts() {
        return creditNoteAmounts;
    }

    public void setCreditNoteAmounts(List<String> creditNoteAmounts) {
        this.creditNoteAmounts = creditNoteAmounts;
    }
    
    public List<String> getTenors() {
        return tenors;
    }

    public void setTenors(List<String> tenors) {
        this.tenors = tenors;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
