!function() {
	
	var loans = {
		version : "1.0.0"
	};
	
	var loansApi = remoteHost +  "/loans/"
	var customerApi = remoteHost +  "/customers/"
    var chartGroup = "loansChartGroup";
	
	var filteredData;
	var fileIdDimension;
	var fileIdDimensionGroup;
	var buyerNameDimension;
	var buyerNameDimensionGroup;
	var paymentFrequencyDimension;
	var paymentFrequencyDimensionGroup;
	var loanStateDimension;
	var loanStateDimensionGroup;
	var daycountConventionDimension;
	var daycountConventionDimensionGroup;
	var loanCcyDimension;
	var loanCcyDimensionGroup;
	var maturityDateDimension;
	var maturityDateDimensionGroup;

	var loanFileId = '...';
	var loanBuyerName = '...';
	var paymentFrequency = '...';
	var daycountConvention = '...';
	var loanCcy = '...';
	var loanState = '...';
	var loanMaturityDate = '...';

	var all;				
	var allDim;
	var initialised = false;	
	var customerMap = new Map();
	var pctFormat = d3.format(".4p");

	var loanDetailsTable = dc.dataTable("#loan-details-grid", chartGroup);
	
	var data = 
	[
		{
			"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0-GBP-17933",
			"fileId": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
			"buyerId": "BCUST000011",
			"currency": "GBP",
			"maturityDate": "2019-02-06",
			"amount": 2607500,
			"rate": 0.2,
			"paymentFrequency": "6M",
			"daycountConvention": "30_365",
			"state": "LOAN_CREATED"
		},
		{
			"id": "1549456021128-BCUST000012-1549456020106-10-1816000.0-GBP-17933",
			"fileId": "1549456021128-BCUST000012-1549456020106-10-1816000.0",
			"buyerId": "BCUST000012",
			"currency": "GBP",
			"maturityDate": "2019-02-06",
			"amount": 1816000,
			"rate": 0.15,
			"paymentFrequency": "1M",
			"daycountConvention": "ACT_360",
			"state": "LOAN_CREATED"
		}
	]
		

	loans.resetFilter = loans_resetFilter;
	function loans_resetFilter() {
		
		dc.filterAll('loansChartGroup'); 
		dc.renderAll('loansChartGroup');
	}

	loans.resetData = loans_resetData;
	function loans_resetData() {
		
		filteredData.remove();
		dc.filterAll('loansChartGroup'); 
		dc.renderAll('loansChartGroup');
	}


	loans.initialiseWidgets = loans_initialise_Widgets;
	function loans_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});

		$('#lblTotalLoanCount').text(filteredData.size());
		$('#lblSelectedFileCount').text(all.length);
				
		fileIdDimension = filteredData.dimension(function (d) {
			return d.fileId;
		});

		buyerNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.buyerId);
		});
			
		paymentFrequencyDimension = filteredData.dimension(function (d) {
			return d.paymentFrequency;
		});

		loanStateDimension = filteredData.dimension(function (d) {
			return d.state;
		});
	
		daycountConventionDimension = filteredData.dimension(function (d) {
			return d.daycountConvention;
		});

		loanCcyDimension = filteredData.dimension(function (d) {
			return d.currency;
		});

		maturityDateDimension = filteredData.dimension(function (d) {
			return d.maturityDate;
		});

		fileIdDimensionGroup = fileIdDimension.group().reduceCount();
		buyerNameDimensionGroup = buyerNameDimension.group().reduceCount();		
		paymentFrequencyDimensionGroup = paymentFrequencyDimension.group().reduceCount();
		loanStateDimensionGroup = loanStateDimension.group().reduceCount();
		daycountConventionDimensionGroup = daycountConventionDimension.group().reduceCount();
		loanCcyDimensionGroup = loanCcyDimension.group().reduceCount();
		maturityDateDimensionGroup = maturityDateDimension.group().reduceCount();
		
		
		loanDetailsTable.width(600).height(420)
	    	.dimension(allDim)
	    	.group(function(d) { return "Selected Loans" })
	    	.size(10000)
	    	.columns([
	    		function(d) { return d.fileId },
    	        function(d) { return customerMap.get(d.buyerId); },
		    function(d) { return d.currency; },
			function(d) { return d3.format(",.4r")(d.amount); },
			function(d) { return pctFormat(d.rate); },
		    function(d) { return d.state; },
   	        function(d) { return d.maturityDate; },
	        function(d) { return d.paymentFrequency; },
   	        function(d) { return d.daycountConvention; },
	    ])
	    .sortBy(function(d) { return d.receivedDateTime; })
	    .order(d3.descending)   
	    .on('renderlet', function (table) {    		
		    	
		    	table.select('tr.dc-table-group').remove();		    	
		    	table.selectAll('tr.dc-table-row').on('click', function (row) { 
		    		
		    		console.log("selected =" + JSON.stringify(row));		    			    		
		    	});
    		});			  
	}
	
	
	loans.updateOverlayData = loans_updateOverlayData;
	function loans_updateOverlayData(system, client, ccy, row) {
		
		if( loansTabActive ) {

			waitingDialog.show();
			var restUrl = loansApi;
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				waitingDialog.hide();
				loans_resetData();
				filteredData.add(data);
				//console.log(JSON.stringify(data));
				
				$('#lblTotalLoanCount').text(filteredData.size());
				updateSelectedCount();

				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {
										
					var activeMap = new Map();
					populateDropDown(data, "loanFileId", activeMap, loanFileId, fileIdDimension, function(i) {
						
						activeMap.set(i.fileId, i.fileId);
					})
					
					populateDropDown(data, "loanBuyerName", activeMap, loanBuyerName, buyerNameDimension, function(i) {
						
						customerName = customerMap.get(i.buyerId);
						activeMap.set(customerName, customerName);
					})
				
					populateDropDown(data, "paymentFrequency", activeMap, paymentFrequency, paymentFrequencyDimension, function(i) {
						
						activeMap.set(i.paymentFrequency, i.paymentFrequency);
					})

					populateDropDown(data, "daycountConvention", activeMap, daycountConvention, daycountConventionDimension, function(i) {
						
						activeMap.set(i.daycountConvention, i.daycountConvention);
					})

					populateDropDown(data, "loanCcy", activeMap, loanCcy, loanCcyDimension, function(i) {
						
						activeMap.set(i.currency, i.currency);
					})

					populateDropDown(data, "loanState", activeMap, loanState, loanStateDimension, function(i) {
						
						activeMap.set(i.state, i.state);
					})

					populateDropDown(data, "loanMaturityDate", activeMap, loanMaturityDate, maturityDateDimension, function(i) {
						
						activeMap.set(i.maturityDate, i.maturityDate);
					})				
				}		
								

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}					
			});				
		}
	}
	
	loans.loadCustomerData = loans_loadCustomerData;
	function loans_loadCustomerData() {
		
		var restUrl = customerApi;
		d3.json(restUrl, function(error, data) {
			
			if (error) {
				console.log(error);
				return;
			}

			if( data == null || data.length == 0) {
				messageDialog.show("No items found");					
			} else {
				
				data.forEach(function(i) {
					customerMap.set(i.id, i.name);
				});
			}					
		});				
	}

	loans.loanFileIdChanged = loans_loanFileIdChanged;
	function loans_loanFileIdChanged() {		
		loanFileId = filterValueChanged('#loanFileId', fileIdDimension, chartGroup);			
	}
		
	loans.loanBuyerNameChanged = loans_loanBuyerNameChanged;
	function loans_loanBuyerNameChanged() {
		
		loanBuyerName = filterValueChanged('#loanBuyerName', buyerNameDimension, chartGroup);						
	}
	
	loans.paymentFrequencyChanged = loans_paymentFrequencyChanged;
	function loans_paymentFrequencyChanged() {
		
		paymentFrequency = filterValueChanged('#paymentFrequency', paymentFrequencyDimension, chartGroup);						
	}

	loans.daycountConventionChanged = loans_daycountConventionChanged;
	function loans_daycountConventionChanged() {
		
		daycountConvention = filterValueChanged('#daycountConvention', daycountConventionDimension, chartGroup);						
	}
	
	loans.loanCcyChanged = loans_loanCcyChanged;
	function loans_loanCcyChanged() {
		
		loanCcy = filterValueChanged('#loanCcy', loanCcyDimension, chartGroup);						
	}
	
	loans.loanStateChanged = loans_loanStateChanged;
	function loans_loanStateChanged() {
		
		loanState = filterValueChanged('#loanState', loanStateDimension, chartGroup);						
	}
	
	loans.loanMaturityDateChanged = loans_loanMaturityDateChanged;
	function loans_loanMaturityDateChanged() {
		
		loanMaturityDate = filterValueChanged('#loanMaturityDate', maturityDateDimension, chartGroup);						
	}
		
	
	function updateSelectedCount() {
		$('#lblSelectedFileCount').text(filteredData.size());		
	}
	
	
	if (typeof define === "function" && define.amd)
		define(loans);
	else if (typeof module === "object" && module.exports)
		module.exports = loans;
	
	this.loans = loans;
}();



function loanFileIdChanged() {
	
	loans.loanFileIdChanged();			
}

function loanBuyerNameChanged() {
	
	loans.loanBuyerNameChanged();			
}

function paymentFrequencyChanged() {
	
	loans.paymentFrequencyChanged();			
}

function daycountConventionChanged() {
	
	loans.daycountConventionChanged();			
}

function loanCcyChanged() {
	
	loans.loanCcyChanged();			
}

function loanStateChanged() {
	
	loans.loanStateChanged();			
}

function loanMaturityDateChanged() {
	
	loans.loanMaturityDateChanged();			
}



function loansUpdateData() {
	loans.updateOverlayData();	
};

loansViewTabActiveListener = function() {
	loansTabActive = true;
	loans.initialiseWidgets();
	loans.updateOverlayData();
}

loans.loadCustomerData();
loans.initialiseWidgets();
loansUpdateData();



