!function() {
	
	var payments = {
		version : "1.0.0"
	};
	
	var paymentsApi = remoteHost +  "/payments/"
	var customerApi = remoteHost +  "/customers/"
    var chartGroup = "paymentsChartGroup";
	
	var filteredData;
	var fileIdDimension;
	var fileIdDimensionGroup;
	var buyerNameDimension;
	var buyerNameDimensionGroup;
	var supplierNameDimension;
	var supplierNameDimensionGroup;
	var paymentStateDimension;
	var paymentStateDimensionGroup;
	var paymentReferenceDimension;
	var paymentReferenceDimensionGroup;
	var paymentCcyDimension;
	var paymentCcyDimensionGroup;
	var paymentDateDimension;
	var paymentDateDimensionGroup;

	var paymentFileId = '...';
	var paymentBuyerName = '...';
	var paymentSupplierName = '...';
	var paymentReference = '...';
	var paymentCcy = '...';
	var paymentState = '...';
	var paymentPaymentDate = '...';
	
	var all;				
	var allDim;
	var initialised = false;	
	var customerMap = new Map();


	var paymentDetailsTable = dc.dataTable("#payment-details-grid", chartGroup);
	
	var data = 
	[
		{
			"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0-SCUST000003-17933-2019-02-06-0-0000000004",
			"fileId": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
			"buyerId": "BCUST000011",
			"paymentId": "1549456020122-BCUST000011-1549456019101-10-2607500.0-2019-02-06-0-0000000004",
			"paymentReference": "2019-02-06-0-0000000004",
			"supplierId": "SCUST000003",
			"currency": "GBP",
			"dueDate": "2019-02-06",
			"amount": 425,
			"state": "PAYMENT_EXECUTED"
		},
		{
			"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0-SCUST000004-17933-2019-02-06-0-0000000009",
			"fileId": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
			"buyerId": "BCUST000011",
			"paymentId": "1549456020122-BCUST000011-1549456019101-10-2607500.0-2019-02-06-0-0000000009",
			"paymentReference": "2019-02-06-0-0000000009",
			"supplierId": "SCUST000004",
			"currency": "USD",
			"dueDate": "2019-02-06",
			"amount": 4750,
			"state": "PAYMENT_EXECUTED"
		}
	]
		

	payments.resetFilter = payments_resetFilter;
	function payments_resetFilter() {
		
		dc.filterAll('paymentsChartGroup'); 
		dc.renderAll('paymentsChartGroup');
	}

	payments.resetData = payments_resetData;
	function payments_resetData() {
		
		filteredData.remove();
		dc.filterAll('paymentsChartGroup'); 
		dc.renderAll('paymentsChartGroup');
	}


	payments.initialiseWidgets = payments_initialise_Widgets;
	function payments_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});

		$('#lblTotalPaymentCount').text(filteredData.size());
		$('#lblSelectedFileCount').text(all.length);
				
		fileIdDimension = filteredData.dimension(function (d) {
			return d.fileId;
		});

		buyerNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.buyerId);
		});
			
		supplierNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.supplierId);
		});

		paymentStateDimension = filteredData.dimension(function (d) {
			return d.state;
		});
	
		paymentReferenceDimension = filteredData.dimension(function (d) {
			return d.paymentReference;
		});

		paymentCcyDimension = filteredData.dimension(function (d) {
			return d.currency;
		});

		paymentDateDimension = filteredData.dimension(function (d) {
			return d.dueDate;
		});

		fileIdDimensionGroup = fileIdDimension.group().reduceCount();
		buyerNameDimensionGroup = buyerNameDimension.group().reduceCount();		
		supplierNameDimensionGroup = supplierNameDimension.group().reduceCount();
		paymentStateDimensionGroup = paymentStateDimension.group().reduceCount();
		paymentReferenceDimensionGroup = paymentReferenceDimension.group().reduceCount();
		paymentCcyDimensionGroup = paymentCcyDimension.group().reduceCount();
		paymentDateDimensionGroup = paymentDateDimension.group().reduceCount();
		
		
		paymentDetailsTable.width(600).height(420)
	    	.dimension(allDim)
	    	.group(function(d) { return "Selected Payments" })
	    	.size(10000)
	    	.columns([
	    		function(d) { return d.fileId },
    	        function(d) { return customerMap.get(d.buyerId); },
    	        function(d) { return customerMap.get(d.supplierId); },
		    function(d) { return d.currency; },
		    function(d) { return d.state; },
   	        function(d) { return d.paymentReference; },
   	        function(d) { return d.dueDate; },,
			function(d) { return d3.format(",.2r")(d.amount); },
	    ])
	    .sortBy(function(d) { return d.receivedDateTime; })
	    .order(d3.descending)   
	    .on('renderlet', function (table) {    		
		    	
		    	table.select('tr.dc-table-group').remove();		    	
		    	table.selectAll('tr.dc-table-row').on('click', function (row) { 
		    		
		    		console.log("selected =" + JSON.stringify(row));		    			    		
		    	});
    		});		
	  
	}
	
	
	payments.updateOverlayData = payments_updateOverlayData;
	function payments_updateOverlayData(system, client, ccy, row) {
		
		if( paymentsTabActive ) {

			waitingDialog.show();
			var restUrl = paymentsApi;
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				waitingDialog.hide();
				payments_resetData();
				filteredData.add(data);
				console.log(JSON.stringify(data));
				
				$('#lblTotalPaymentCount').text(filteredData.size());
				updateSelectedCount();

				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {
					
					var activeMap = new Map();
					populateDropDown(data, "paymentFileId", activeMap, paymentFileId, fileIdDimension, function(i) {
						
						activeMap.set(i.fileId, i.fileId);
					})
					
					populateDropDown(data, "paymentBuyerName", activeMap, paymentBuyerName, buyerNameDimension, function(i) {
						
						customerName = customerMap.get(i.buyerId);
						activeMap.set(customerName, customerName);
					})
				
					populateDropDown(data, "paymentSupplierName", activeMap, paymentSupplierName, supplierNameDimension, function(i) {
						
						customerName = customerMap.get(i.supplierId);
						activeMap.set(customerName, customerName);
					})

					populateDropDown(data, "paymentReference", activeMap, paymentReference, paymentReferenceDimension, function(i) {
						
						activeMap.set(i.paymentReference, i.paymentReference);
					})

					populateDropDown(data, "paymentCcy", activeMap, paymentCcy, paymentCcyDimension, function(i) {
						
						activeMap.set(i.currency, i.currency);
					})

					populateDropDown(data, "paymentState", activeMap, paymentState, paymentStateDimension, function(i) {
						
						activeMap.set(i.state, i.state);
					})

					populateDropDown(data, "paymentPaymentDate", activeMap, paymentPaymentDate, paymentDateDimension, function(i) {
						
						activeMap.set(i.dueDate, i.dueDate);
					})				
				}		
								

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}					
			});				
		}
	}

	payments.loadCustomerData = payments_loadCustomerData;
	function payments_loadCustomerData() {
		
		var restUrl = customerApi;
		d3.json(restUrl, function(error, data) {
			
			if (error) {
				console.log(error);
				return;
			}

			if( data == null || data.length == 0) {
				messageDialog.show("No items found");					
			} else {
				
				data.forEach(function(i) {
					customerMap.set(i.id, i.name);
				});
			}					
		});				
	}
	
	payments.paymentFileIdChanged = payments_paymentFileIdChanged;
	function payments_paymentFileIdChanged() {		
		paymentFileId = filterValueChanged('#paymentFileId', fileIdDimension, chartGroup);			
	}
		
	payments.paymentBuyerNameChanged = payments_paymentBuyerNameChanged;
	function payments_paymentBuyerNameChanged() {
		
		paymentBuyerName = filterValueChanged('#paymentBuyerName', buyerNameDimension, chartGroup);						
	}
	
	payments.paymentSupplierNameChanged = payments_paymentSupplierNameChanged;
	function payments_paymentSupplierNameChanged() {
		
		paymentSupplierName = filterValueChanged('#paymentSupplierName', supplierNameDimension, chartGroup);						
	}

	payments.paymentReferenceChanged = payments_paymentReferenceChanged;
	function payments_paymentReferenceChanged() {
		
		paymentReference = filterValueChanged('#paymentReference', paymentReferenceDimension, chartGroup);						
	}
	
	payments.paymentCcyChanged = payments_paymentCcyChanged;
	function payments_paymentCcyChanged() {
		
		paymentCcy = filterValueChanged('#paymentCcy', paymentCcyDimension, chartGroup);						
	}
	
	payments.paymentStateChanged = payments_paymentStateChanged;
	function payments_paymentStateChanged() {
		
		paymentState = filterValueChanged('#paymentState', paymentStateDimension, chartGroup);						
	}
	
	payments.paymentPaymentDateChanged = payments_paymentPaymentDateChanged;
	function payments_paymentPaymentDateChanged() {
		
		paymentPaymentDate = filterValueChanged('#paymentPaymentDate', paymentDateDimension, chartGroup);						
	}
		
	
	function updateSelectedCount() {
		$('#lblSelectedFileCount').text(filteredData.size());		
	}
	
	
	if (typeof define === "function" && define.amd)
		define(payments);
	else if (typeof module === "object" && module.exports)
		module.exports = payments;
	
	this.payments = payments;
}();



function paymentFileIdChanged() {
	
	payments.paymentFileIdChanged();			
}

function paymentBuyerNameChanged() {
	
	payments.paymentBuyerNameChanged();			
}

function paymentSupplierNameChanged() {
	
	payments.paymentSupplierNameChanged();			
}

function paymentReferenceChanged() {
	
	payments.paymentReferenceChanged();			
}

function paymentCcyChanged() {
	
	payments.paymentCcyChanged();			
}

function paymentStateChanged() {
	
	payments.paymentStateChanged();			
}

function paymentPaymentDateChanged() {
	
	payments.paymentPaymentDateChanged();			
}



function paymentsUpdateData() {
	payments.updateOverlayData();	
};

paymentsViewTabActiveListener = function() {
	paymentsTabActive = true;
	payments.initialiseWidgets();
	payments.updateOverlayData();
}

payments.loadCustomerData();
payments.initialiseWidgets();
paymentsUpdateData();



