!function() {
	
	var instructions = {
		version : "1.0.0"
	};
	
	var instructionsApi = remoteHost +  "/instructions/"
	var customerApi = remoteHost +  "/customers/"
    var chartGroup = "instructionsChartGroup";
	

	var filteredData;
	var fileIdDimension;
	var fileIdDimensionGroup;
	var buyerNameDimension;
	var buyerNameDimensionGroup;
	var supplierNameDimension;
	var supplierNameDimensionGroup;
	var instructionStateDimension;
	var instructionStateDimensionGroup;
	var instructionTypeDimension;
	var instructionTypeDimensionGroup;
	var instructionCcyDimension;
	var instructionCcyDimensionGroup;
	var paymentDateDimension;
	var paymentDateDimensionGroup;
	
	var instructionFileId = '...';
	var instructionBuyerName = '...';
	var instructionSupplierName = '...';
	var instructionType = '...';
	var instructionCcy = '...';
	var instructionState = '...';
	var instructionPaymentDate = '...';

	var all;				
	var allDim;
	var initialised = false;	
	var customerMap = new Map();


	var instructionDetailsTable = dc.dataTable("#invoice-details-grid", chartGroup);
	
	var data = 
	[
		{
			"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0-2019-02-06-0-0000000001",
			"fileId": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
			"type": "INVOICE",
			"bprn": "BPRN-0",
			"buyerId": "BCUST000011",
			"supplierId": "SCUST000013",
			"supplierName": "Puma Inc",
			"currency": "EUR",
			"invoiceNumber": "2019-02-06-0-0000000001",
			"dueDate": "2019-02-06",
			"amount": 50000,
			"state": "PAYMENT_EXECUTED",
			"properties": null
		},
		{
			"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0-2019-02-06-0-0000000002",
			"fileId": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
			"type": "INVOICE",
			"bprn": "BPRN-0",
			"buyerId": "BCUST000011",
			"supplierId": "SCUST000010",
			"supplierName": "New Balance Athletic Shoe Inc",
			"currency": "CHF",
			"invoiceNumber": "2019-02-06-0-0000000002",
			"dueDate": "2019-02-06",
			"amount": 500,
			"state": "PAYMENT_EXECUTED",
			"properties": null
		}
	]
		

	instructions.resetFilter = instructions_resetFilter;
	function instructions_resetFilter() {
		
		dc.filterAll('instructionsChartGroup'); 
		dc.renderAll('instructionsChartGroup');
	}

	instructions.resetData = instructions_resetData;
	function instructions_resetData() {
		
		filteredData.remove();
		dc.filterAll('instructionsChartGroup'); 
		dc.renderAll('instructionsChartGroup');
	}


	instructions.initialiseWidgets = instructions_initialise_Widgets;
	function instructions_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});

		$('#lblTotalInstructionCount').text(filteredData.size());
		$('#lblSelectedFileCount').text(all.length);
				
		fileIdDimension = filteredData.dimension(function (d) {
			return d.fileId;
		});

		buyerNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.buyerId);
		});
			
		supplierNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.supplierId);
		});

		instructionStateDimension = filteredData.dimension(function (d) {
			return d.state;
		});
	
		instructionTypeDimension = filteredData.dimension(function (d) {
			return d.type;
		});

		instructionCcyDimension = filteredData.dimension(function (d) {
			return d.currency;
		});

		paymentDateDimension = filteredData.dimension(function (d) {
			return d.dueDate;
		});

		fileIdDimensionGroup = fileIdDimension.group().reduceCount();
		buyerNameDimensionGroup = buyerNameDimension.group().reduceCount();		
		supplierNameDimensionGroup = supplierNameDimension.group().reduceCount();
		instructionStateDimensionGroup = instructionStateDimension.group().reduceCount();
		instructionTypeDimensionGroup = instructionTypeDimension.group().reduceCount();
		instructionCcyDimensionGroup = instructionCcyDimension.group().reduceCount();
		paymentDateDimensionGroup = paymentDateDimension.group().reduceCount();
		
		
		instructionDetailsTable.width(600).height(420)
	    	.dimension(allDim)
	    	.group(function(d) { return "Selected Instructions" })
	    	.size(10000)
	    	.columns([
	    		function(d) { return d.fileId },
    	        function(d) { return customerMap.get(d.buyerId); },
    	        function(d) { return customerMap.get(d.supplierId); },
		    function(d) { return d.currency; },
		    function(d) { return d.state; },
   	        function(d) { return d.bprn; },
   	        function(d) { return d.dueDate; },
		    function(d) { return d.invoiceNumber; },
	        	function(d) { return d.type; },
			function(d) { return d3.format(",.2r")(d.amount); },
	    ])
	    .sortBy(function(d) { return d.receivedDateTime; })
	    .order(d3.descending)   
	    .on('renderlet', function (table) {    		
		    	
		    	table.select('tr.dc-table-group').remove();		    	
		    	table.selectAll('tr.dc-table-row').on('click', function (row) { 
		    		
		    		console.log("selected =" + JSON.stringify(row));		    			    		
		    	});
    		});		
	  
	}
	
	
	instructions.updateOverlayData = instructions_updateOverlayData;
	function instructions_updateOverlayData(system, client, ccy, row) {
		
		if( instructionsTabActive ) {

			waitingDialog.show();
			var restUrl = instructionsApi;
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				waitingDialog.hide();
				instructions_resetData();
				filteredData.add(data);
				//console.log(JSON.stringify(data));
				
				$('#lblTotalInstructionCount').text(filteredData.size());
				updateSelectedCount();

				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {
					
					var activeMap = new Map();
					populateDropDown(data, "instructionFileId", activeMap, instructionFileId, fileIdDimension, function(i) {
						
						activeMap.set(i.fileId, i.fileId);
					})
					
					populateDropDown(data, "instructionBuyerName", activeMap, instructionBuyerName, buyerNameDimension, function(i) {
						
						customerName = customerMap.get(i.buyerId);
						activeMap.set(customerName, customerName);
					})
				
					populateDropDown(data, "instructionSupplierName", activeMap, instructionSupplierName, supplierNameDimension, function(i) {
						
						customerName = customerMap.get(i.supplierId);
						activeMap.set(customerName, customerName);
					})

					populateDropDown(data, "instructionType", activeMap, instructionType, instructionTypeDimension, function(i) {
						
						activeMap.set(i.type, i.type);
					})

					populateDropDown(data, "instructionCcy", activeMap, instructionCcy, instructionCcyDimension, function(i) {
						
						activeMap.set(i.currency, i.currency);
					})

					populateDropDown(data, "instructionState", activeMap, instructionState, instructionStateDimension, function(i) {
						
						activeMap.set(i.state, i.state);
					})

					populateDropDown(data, "instructionPaymentDate", activeMap, instructionPaymentDate, paymentDateDimension, function(i) {
						
						activeMap.set(i.dueDate, i.dueDate);
					})				
				}		
								

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}					
			});				
		}
	}

	instructions.loadCustomerData = instructions_loadCustomerData;
	function instructions_loadCustomerData() {
		
		var restUrl = customerApi;
		d3.json(restUrl, function(error, data) {
			
			if (error) {
				console.log(error);
				return;
			}

			if( data == null || data.length == 0) {
				messageDialog.show("No items found");					
			} else {
				
				data.forEach(function(i) {
					customerMap.set(i.id, i.name);
				});
			}					
		});				
	}

	instructions.instructionFileIdChanged = instructions_instructionFileIdChanged;
	function instructions_instructionFileIdChanged() {		
		instructionFileId = filterValueChanged('#instructionFileId', fileIdDimension, chartGroup);			
	}
		
	instructions.instructionBuyerNameChanged = instructions_instructionBuyerNameChanged;
	function instructions_instructionBuyerNameChanged() {
		
		instructionBuyerName = filterValueChanged('#instructionBuyerName', buyerNameDimension, chartGroup);						
	}
	
	instructions.instructionSupplierNameChanged = instructions_instructionSupplierNameChanged;
	function instructions_instructionSupplierNameChanged() {
		
		instructionSupplierName = filterValueChanged('#instructionSupplierName', supplierNameDimension, chartGroup);						
	}

	instructions.instructionTypeChanged = instructions_instructionTypeChanged;
	function instructions_instructionTypeChanged() {
		
		instructionType = filterValueChanged('#instructionType', instructionTypeDimension, chartGroup);						
	}
	
	instructions.instructionCcyChanged = instructions_instructionCcyChanged;
	function instructions_instructionCcyChanged() {
		
		instructionCcy = filterValueChanged('#instructionCcy', instructionCcyDimension, chartGroup);						
	}
	
	instructions.instructionStateChanged = instructions_instructionStateChanged;
	function instructions_instructionStateChanged() {
		
		instructionState = filterValueChanged('#instructionState', instructionStateDimension, chartGroup);						
	}
	
	instructions.instructionPaymentDateChanged = instructions_instructionPaymentDateChanged;
	function instructions_instructionPaymentDateChanged() {
		
		instructionPaymentDate = filterValueChanged('#instructionPaymentDate', paymentDateDimension, chartGroup);						
	}
		
	
	function updateSelectedCount() {
		$('#lblSelectedFileCount').text(filteredData.size());		
	}
	
	
	if (typeof define === "function" && define.amd)
		define(instructions);
	else if (typeof module === "object" && module.exports)
		module.exports = instructions;
	
	this.instructions = instructions;
}();



function instructionFileIdChanged() {
	
	instructions.instructionFileIdChanged();			
}

function instructionBuyerNameChanged() {
	
	instructions.instructionBuyerNameChanged();			
}

function instructionSupplierNameChanged() {
	
	instructions.instructionSupplierNameChanged();			
}

function instructionTypeChanged() {
	
	instructions.instructionTypeChanged();			
}

function instructionCcyChanged() {
	
	instructions.instructionCcyChanged();			
}

function instructionStateChanged() {
	
	instructions.instructionStateChanged();			
}

function instructionPaymentDateChanged() {
	
	instructions.instructionPaymentDateChanged();			
}


function instructionsUpdateData() {
	instructions.updateOverlayData();	
};

instructionsViewTabActiveListener = function() {
	instructionsTabActive = true;
	instructions.initialiseWidgets();
	instructions.updateOverlayData();
}

instructions.loadCustomerData();
instructions.initialiseWidgets();
instructionsUpdateData();



