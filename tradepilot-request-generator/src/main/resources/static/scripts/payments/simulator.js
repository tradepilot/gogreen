!function() {
	
	var simulatorView = {
		version : "1.0.0"
	};
	
	
	var simulatorViewStartApi = remoteHost +  "/start/"
	var simulatorViewProgressApi = remoteHost +  "/count"
	var simulatorViewStatusApi = remoteHost +  "/status"
	var simulatorViewStopApi = remoteHost +  "/stop"
	var simulatorViewClearApi = remoteHost +  "/clear"
	
	var initialised = false;	
	var running = false; 
	var numFiles = 10;
	var numInvoices = 4;
	var numCreditNotes = 2;
	var numBprns = 1;
	var filesPerHour = 3600;
	var randomiseDates = true;
	var printMessages = false;	
	var timeoutHandle = null;

	simulatorView.initialiseWidgets = simulatorView_initialise_Widgets;
	function simulatorView_initialise_Widgets() {

		$('#numFilesInput').val(numFiles);
		$('#numInvoicesInput').val(numInvoices);
		$('#numCreditNotesInput').val(numCreditNotes);
		$('#numBprnsInput').val(numBprns);
		$('#filesPerHourInput').val(filesPerHour);
	}
	
	
	simulatorView.startSimulator = simulatorView_startSimulator;
	function simulatorView_startSimulator() {
		
		//debugger
		running = true;
		$('#simulator-start-btn').prop('disabled', true);
		$('#simulator-stop-btn').prop('disabled', false);
				
		numFiles = $('#numFilesInput').val();
		numInvoices = $('#numInvoicesInput').val();
		numCreditNotes = $('#numCreditNotesInput').val();
		numBprns = $('#numBprnsInput').val();
		filesPerHour = $('#filesPerHourInput').val();
		randomiseDates = $('#randomiseDatesInputCheck').is(':checked')
		
		var url = "/start/" + numInvoices + "/" + numCreditNotes + "/" + numBprns + "/" + numFiles + "/" + filesPerHour + "/" + randomiseDates + "/true";	
		console.log("URL value " + url );
		
		var progressValue = 0;
		$('.progress-bar').attr('aria-valuemax', numFiles);
		$('.progress-bar').attr('aria-valuenow', progressValue);
		
		$.get( url )
		  .done(function( data ) {			  		
			  console.log("Start value " + data );
		  }
		);	
		
		startAutoRefresh();
	}
	
	simulatorView.stopSimulator = simulatorView_stopSimulator;
	function simulatorView_stopSimulator() {

		var msg = 'Stopping simulator....';
		//waitingDialog.show(msg);
		console.log(msg);
		
		$.get( simulatorViewStopApi )
		  .done(function( data ) {	
			  
			    //waitingDialog.hide();			 
				if( data == null || data.length == 0) {
					messageDialog.show("No response from simulator");					
				} else {			
					messageDialog.show(data.message);	
					console.log("Simulator Response value " + data.message );	
				}								  
		  }
		);
	}	

	
	
	simulatorView.clearData = simulatorView_clearData;
	function simulatorView_clearData() {		

		var msg = 'Clearing data....';
		waitingDialog.show(msg);
		console.log(msg);	
		var restUrl = simulatorViewClearApi;
		
		$.get( simulatorViewClearApi )
		  .done(function( data ) {	
			  
				waitingDialog.hide();
				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {			
					var message = "Cleared " + data.files + " files, " + data.instructions + " instructions, " + data.payments + " payments, " + data.loans + " loans";
					console.log(message);
					messageDialog.show(message);	
				}					
		  }
		);

	}	

	var progressSize = 0;
	var progressValue = 0;
	
	simulatorView.updateProgress = simulatorView_updateProgress;
	function simulatorView_updateProgress() {		
		
		//console.log("simulatorView_updateProgress()");
		$.get( simulatorViewStatusApi )
		  .done(function( isRunning ) {		
			 			  
		     //console.log("Status value " + isRunning );
			 if( isRunning ) {
				 
				 $('#lblStatus').css("color","darkgreen");
				 $('#lblStatus').text("Running...");
				 
				 $('#simulator-start-btn').prop('disabled', true);
				 $('#simulator-stop-btn').prop('disabled', false);

				 $.get( simulatorViewProgressApi )
				  .done(function( progressValue ) {							 			  
					 progressSize = (progressValue / numFiles) * 100;
					 $('.progress-bar').css('width', progressSize+'%').attr('aria-valuenow', progressValue);
					 $('#lblCount').text(progressValue);
				});
				
				
			 } else {
				 
				// Show it as complete for a brief period before resetting
				//
				$('.progress-bar').css('width', '100%').attr('aria-valuenow', numFiles);				
				$('#lblCount').text(numFiles);
					
				$(this).delay(1000).queue(function() {

					progressSize = 0;
					progressValue = 0;
					
					$('#simulator-start-btn').prop('disabled', false);
					$('#simulator-stop-btn').prop('disabled', true);

					$('#lblStatus').css("color","darkred");
					$('#lblStatus').text("Stopped");
					$('#lblCount').text(progressValue);

					$('.progress-bar').css('width', progressSize+'%').attr('aria-valuenow', progressValue);				
					stopAutoRefresh();

				    $(this).dequeue();
				});
			 }
		  });		
	}
	
	
	function startAutoRefresh() {
		
		if( timeoutHandle == null ) {			
			console.log("Starting auto-refresh");
			timeoutHandle = setInterval( function () { 
				simulatorViewUpdateProgress() 
			}, REFRESH_PERIOD); 					
		} 	    
	};

	function stopAutoRefresh() {
		
		if( timeoutHandle != null ) {
			console.log("Stopping auto-refresh");
			clearInterval(timeoutHandle); 
			timeoutHandle = null; 		
		}
	};
		
	
	if (typeof define === "function" && define.amd)
		define(simulatorView);
	else if (typeof module === "object" && module.exports)
		module.exports = simulatorView;
	
	this.simulatorView = simulatorView;
}();


function simulatorViewUpdateProgress() {
	simulatorView.updateProgress();	
};

simulatorViewTabActiveListener = function() {
	simulatorTabActive = true;
}

simulatorView.initialiseWidgets();

