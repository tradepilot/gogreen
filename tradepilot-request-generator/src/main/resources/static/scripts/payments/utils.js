
// Global constants
//
var REFRESH_PERIOD = 2000;
var LONG_REFRESH_PERIOD = 20000;
var BILLION = 1000000000.0;


var SIMULATOR_TAB_NAME="Simulator";
var FILES_TAB_NAME="Files";
var INSTRUCTIONS_TAB_NAME="Instructions";
var PAYMENTS_TAB_NAME="Payments";
var LOANS_TAB_NAME="Loans";

var remoteHost=""

// Get any variables from the URL
//
$.extend({
	getUrlVars : function() {
		
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	},

	getUrlVar : function(name) {
		return $.getUrlVars()[name];
	}
});

var allVars = $.getUrlVars();
var currentTab;
var lastTab;

var commaFormat = d3.format(",");
var percentFormat = d3.format("%");
var timeFormat = d3.time.format("%H:%M:%S");

var simulatorTabActive = true;
var filesTabActive = false;
var instructionsTabActive = false;
var paymentsTabActive = false;
var loansTabActive = false;

var simulatorViewTabActiveListener;
var filesViewTabActiveListener;
var instructionsViewTabActiveListener;
var paymentsViewTabActiveListener;
var loansViewTabActiveListener;


// Tab listener to allow us to start & stop scripts
// for each tab when the become active or inactive
//
$(document).ready(function() {
	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		currentTab = $(e.target).text(); 
		lastTab = $(e.relatedTarget).text(); 
		
		switch( currentTab ) {

			case SIMULATOR_TAB_NAME:
				simulatorTabActive = true;
				filesTabActive = false;
				instructionsTabActive = false;
				paymentsTabActive = false;
				loansTabActive = false;
				console.log("Simulator View Tab Active");	
				simulatorViewTabActiveListener();
				break;	

			case FILES_TAB_NAME:
				simulatorTabActive = false;
				filesTabActive = true;
				instructionsTabActive = false;
				paymentsTabActive = false;
				loansTabActive = false;
				console.log("Files View Tab Active");	
				filesViewTabActiveListener();
				break;		

			case INSTRUCTIONS_TAB_NAME:
				simulatorTabActive = false;
				filesTabActive = false;
				instructionsTabActive = true;
				paymentsTabActive = false;
				loansTabActive = false;
				console.log("Instructions View Tab Active");	
				instructionsViewTabActiveListener();
				break;		

			case PAYMENTS_TAB_NAME:
				simulatorTabActive = false;
				filesTabActive = false;
				instructionsTabActive = false;
				paymentsTabActive = true;
				loansTabActive = false;
				console.log("Payments View Tab Active");	
				paymentsViewTabActiveListener();
				break;		
				
			case LOANS_TAB_NAME:
				simulatorTabActive = false;
				filesTabActive = false;
				instructionsTabActive = false;
				paymentsTabActive = false;
				loansTabActive = true;
				console.log("Loans View Tab Active");	
				loansViewTabActiveListener();
				break;		
		}
	});
});



function populateDropDown(data, controlId, activeMap, selectedValue, dimension, propertyExtractor) {
	
	activeMap.clear();
	data.forEach(propertyExtractor);
	
	var select = document.getElementById(controlId); 
	select.innerHTML = "";

	var dropDownValues = sortMapKeys(activeMap);				
    select.innerHTML += "<option value=\"...\">...</option>";

    for(var i = 0; i < dropDownValues.length; i++) {
	    var opt = dropDownValues[i];
	    select.innerHTML += "<option value=\"" + opt + "\">" + opt + "</option>";
	}		
    
    select.value = selectedValue;	    
	if( selectedValue != '...') {
		dimension.filter(selectedValue);
	} else {
		dimension.filterAll();
	}
}



function filterValueChanged(controlId, dimension, chartGroup) {
	
	var filterValue = $(controlId).val();
	if( filterValue != '...') {
		dimension.filter(filterValue);
	} else {
		dimension.filterAll();
	}
	
	dc.redrawAll(chartGroup);	
	return filterValue;
}


function pad2(number) {
	return (number < 10 ? '0' : '') + number
}

function formatAmount(amount) {

	return formatAmountAndScale(amount,1000000.0);
}

function formatAmountAndScale(amount, multiplier) {
	
	var notional = d3.round(amount * multiplier, 2)
	var formatted = commaFormat(notional);
	
	if( notional > 1000 ) {
		// Extra scaling for bn
		//
		notional = d3.round((amount / 1000) * multiplier, 2)
		formatted = commaFormat(notional);
		
		return formatted + "bn";
	} else if( notional > 100 ) {
		return formatted + "mm";
	} else if( notional > 10 ) {
		return formatted + "mm";
	} else if( notional > 1 ) {
		return formatted + "mm";
	} else {
		return formatted + "mm";
	}
}

function formatAmountAndScale2(amount, multiplier) {
	
	var notional = 0;
	var scaled = amount * multiplier;
	
	if( scaled < 0 ) {		
		notional = d3.round(scaled, 4)
	} else if( scaled < 1000 ) {
		notional = d3.round(scaled, 3)
	} else if( scaled < 100000 ) {
		notional = d3.round(scaled, 2)
	} else {
		notional = d3.round(scaled, 0)		
	}

	var formatted = commaFormat(notional);
	
	if( scaled > 10000 ) {
		return formatted + "bn";
	} else if( notional > 100 ) {
		return formatted + "mm";
	} else if( notional > 10 ) {
		return formatted + "mm";
	} else if( notional > 1 ) {
		return formatted + "mm";
	} else {
		return formatted + "mm";
	}
}
		


function sortMapKeys(map) {
	
    var keyArray = [];
    map.forEach(
		function (value, key, map) {
			  keyArray.push(key);
		}
	);
		   
    keyArray.sort();
    return keyArray;
}

function formatValueAndScale(amount, multiplier) {
	
	var notional = d3.round(amount * multiplier, 2)
	var formatted = commaFormat(notional);
	
	if( notional > 1000 ) {
		// Extra scaling for bn
		//
		notional = d3.round((amount / 1000) * multiplier, 2)
		formatted = commaFormat(notional);		
	} 
	
	return formatted;
}
	

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);


var messageDialog = messageDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-footer">' +
				'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
//			$dialog.find('.progress-bar').attr('class', 'progress-bar');
//			if (settings.progressType) {
//				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
//			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);


