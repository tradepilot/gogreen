!function() {
	
	var files = {
		version : "1.0.0"
	};
	
	var filesApi = remoteHost +  "/files/"
	var customerApi = remoteHost +  "/customers/"
    var chartGroup = "filesChartGroup";
	

	var width = 990;
	var height = 570;
	
	var svgWidth = 4000;
	var svgHeight = 200;
	
	var filteredData;
	var fileIdDimension;
	var fileIdDimensionGroup;
	var buyerNameDimension;
	var buyerNameDimensionGroup;
	var buyerIdDimension;
	var buyerIdDimensionGroup;
	var fileStateDimension;
	var fileStateDimensionGroup;
	var paymentDateDimension;
	var paymentDateDimensionGroup;
	
	var inputFileId = '...';
	var inputBuyerId = '...';
	var inputBuyerName = '...';
	var inputFileState = '...';
	var inputPaymentDate = '...';
	
	var customerMap = new Map();

	var all;				
	var allDim;
	var initialised = false;	


	var fileDetailsTable = dc.dataTable("#file-details-grid", chartGroup);
	
	var data = 
	[
		{
		"id": "1549456020122-BCUST000011-1549456019101-10-2607500.0",
		"requestId": "1549456020122",
		"buyerId": "BCUST000011",
		"state": "COMPLETE",
		"receivedDateTime": "2019-02-06T12:26:59.101",
		"maturityDate": "2019-02-06",
		"numberOfInstructions": 10,
		"outstandingPaymentRequests": 0,
		"totalValue": 2607500
		},
		{
		"id": "1549456021128-BCUST000012-1549456020106-10-1816000.0",
		"requestId": "1549456021128",
		"buyerId": "BCUST000012",
		"state": "COMPLETE",
		"receivedDateTime": "2019-02-06T12:27:00.106",
		"maturityDate": "2019-02-06",
		"numberOfInstructions": 10,
		"outstandingPaymentRequests": 0,
		"totalValue": 1816000
		}
	]
		

	files.resetFilter = files_resetFilter;
	function files_resetFilter() {
		
		dc.filterAll('filesChartGroup'); 
		dc.renderAll('filesChartGroup');
	}

	files.resetData = files_resetData;
	function files_resetData() {
		
		filteredData.remove();		
		$("#file-details-grid tr").remove(); 
		
		dc.filterAll('filesChartGroup'); 
		dc.renderAll('filesChartGroup');
	}


	files.initialiseWidgets = files_initialise_Widgets;
	function files_initialise_Widgets() {
		
		filteredData = crossfilter(data);
		all = filteredData.groupAll();				
		allDim = filteredData.dimension(function(d) {return d;});

		$('#lblTotalFileCount').text(filteredData.size());
		$('#lblSelectedFileCount').text(all.length);
				
		fileIdDimension = filteredData.dimension(function (d) {
			return d.id;
		});

		buyerIdDimension = filteredData.dimension(function (d) {
			return d.buyerId;
		});
		
		buyerNameDimension = filteredData.dimension(function (d) {
			return customerMap.get(d.buyerId);
		});
			
		fileStateDimension = filteredData.dimension(function (d) {
			return d.state;
		});

		paymentDateDimension = filteredData.dimension(function (d) {
			return d.maturityDate;
		});

		fileIdDimensionGroup = fileIdDimension.group().reduceCount();
		buyerIdDimensionGroup = buyerIdDimension.group().reduceCount();
		buyerNameDimensionGroup = buyerNameDimension.group().reduceCount();		
		fileStateDimensionGroup = fileStateDimension.group().reduceCount();
		paymentDateDimensionGroup = paymentDateDimension.group().reduceCount();
		
		
		fileDetailsTable.width(600).height(420)
		    	.dimension(allDim)
		    	.group(function(d) { return "Selected Files" })
		    	.size(10000)
		    	.columns([
				function(d) { return d.id },
	    	        function(d) { return customerMap.get(d.buyerId); },
			    function(d) { return d.buyerId },
			    function(d) { return d.state; },
	   	        function(d) { return d.maturityDate; },
	   	        function(d) { return d.receivedDateTime; },
			    function(d) { return d.numberOfInstructions; },
		        	function(d) { return d.outstandingPaymentRequests; },
				function(d) { return d3.format(",.2r")(d.totalValue); },
		    ])
		    .sortBy(function(d) { return d.receivedDateTime; })
		    .order(d3.descending)   
		    .on('renderlet', function (table) {    		
			    	
			    	table.select('tr.dc-table-group').remove();		    	
			    	table.selectAll('tr.dc-table-row').on('click', function (row) { 
			    		
			    		console.log("selected =" + JSON.stringify(row));		    			    		
			    	});
	    		});		
	  
	}
	
	
	files.updateOverlayData = files_updateOverlayData;
	function files_updateOverlayData(system, client, ccy, row) {
		
		if( filesTabActive ) {

			files_resetData();
			waitingDialog.show();
			var restUrl = filesApi;
			
			d3.json(restUrl, function(error, data) {
				
				if (error) {
					console.log(error);
					return;
				}
				
				waitingDialog.hide();
				filteredData.add(data);
				//console.log(JSON.stringify(data));

				$('#lblTotalFileCount').text(filteredData.size());
				updateSelectedCount();

				if( data == null || data.length == 0) {
					messageDialog.show("No items found");					
				} else {
					
					var activeMap = new Map();
					populateDropDown(data, "inputFileId", activeMap, inputFileId, fileIdDimension, function(i) {
						
						activeMap.set(i.id, i.id);
					})

					populateDropDown(data, "inputBuyerName", activeMap, inputBuyerName, buyerNameDimension, function(i) {
						
						customerName = customerMap.get(i.buyerId);
						activeMap.set(customerName, customerName);
					})
					
					populateDropDown(data, "inputBuyerId", activeMap, inputBuyerId, buyerIdDimension, function(i) {
						
						activeMap.set(i.buyerId, i.buyerId);
					})
				
					populateDropDown(data, "inputFileState", activeMap, inputFileState, fileStateDimension, function(i) {
						
						activeMap.set(i.state, i.state);
					})

					populateDropDown(data, "inputPaymentDate", activeMap, inputPaymentDate, paymentDateDimension, function(i) {
						
						activeMap.set(i.maturityDate, i.maturityDate);
					})				
				}		
								

			  	// Render the Charts
			    //
				if( initialised == false ) {
					dc.renderAll(chartGroup);		
					initialised = true;
				} else {
					dc.redrawAll(chartGroup);				
				}					
			});				
		}
	}

	
	files.loadCustomerData = files_loadCustomerData;
	function files_loadCustomerData() {
		
		var restUrl = customerApi;
		d3.json(restUrl, function(error, data) {
			
			if (error) {
				console.log(error);
				return;
			}

			if( data == null || data.length == 0) {
				messageDialog.show("No items found");					
			} else {
				
				data.forEach(function(i) {
					customerMap.set(i.id, i.name);
				});
			}					
		});				
	}
	
	

	files.inputBuyerIdChanged = files_inputBuyerIdChanged;
	function files_inputBuyerIdChanged() {		
		inputBuyerId = filterValueChanged('#inputBuyerId', buyerIdDimension, chartGroup);						
	}
	
	files.inputFileIdChanged = files_inputFileIdChanged;
	function files_inputFileIdChanged() {
		inputFileId = filterValueChanged('#inputFileId', fileIdDimension, chartGroup);						
	}
	
	files.inputBuyerNameChanged = files_inputBuyerNameChanged;
	function files_inputBuyerNameChanged() {
		inputBuyerName = filterValueChanged('#inputBuyerName', buyerNameDimension, chartGroup);							
	}
	
	files.inputFileStateChanged = files_inputFileStateChanged;
	function files_inputFileStateChanged() {
		inputFileState = filterValueChanged('#inputFileState', fileStateDimension, chartGroup);				
	}
	
	files.inputPaymentDateChanged = files_inputPaymentDateChanged;
	function files_inputPaymentDateChanged() {
		inputPaymentDate = filterValueChanged('#inputPaymentDate', paymentDateDimension, chartGroup);						
	}
	
	
	function updateSelectedCount() {
		$('#lblSelectedFileCount').text(filteredData.size());		
	}
	
	
	if (typeof define === "function" && define.amd)
		define(files);
	else if (typeof module === "object" && module.exports)
		module.exports = files;
	
	this.files = files;
}();



function inputFileIdChanged() {
	
	files.inputFileIdChanged();			
}

function inputBuyerNameChanged() {
	
	files.inputBuyerNameChanged();			
}

function inputBuyerIdChanged() {
	
	files.inputBuyerIdChanged();			
}

function inputFileStateChanged() {
	
	files.inputFileStateChanged();			
}

function inputPaymentDateChanged() {
	
	files.inputPaymentDateChanged();			
}

function filesUpdateData() {
	files.updateOverlayData();	
};

filesViewTabActiveListener = function() {
	filesTabActive = true;
	files.initialiseWidgets();
	files.updateOverlayData();
}

files.loadCustomerData();
files.initialiseWidgets();
filesUpdateData();



