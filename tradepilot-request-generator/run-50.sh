#!/bin/bash

NEW_MEM="-e NEW_MEM=-Xms128m"
MIN_MEM="-e MIN_MEM=-Xms256m"
MAX_MEM="-e MAX_MEM=-Xms256m"
SPRING_PROFILE="-e SPRING_PROFILE=local-swarm"
KAFKA_ZOOKEEPER_CONNECT="-e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181"

NUM_DOCS="-e NUM_DOCS=5"
NUM_BPRNS="-e NUM_BPRNS=2"
NUM_INVOICES="-e NUM_INVOICES=2"
NUM_CREDIT_NOTES="-e NUM_CREDIT_NOTES=0"
DOCS_PER_HOUR="-e DOCS_PER_HOUR=1800"
PRINT_MESSAGES="-e PRINT_MESSAGES=true"

#COMMON_ARGS="${SPRING_PROFILE} ${KAFKA_ZOOKEEPER_CONNECT}"
COMMON_ARGS="${NEW_MEM} ${MIN_MEM} ${MAX_MEM} ${SPRING_PROFILE} ${KAFKA_ZOOKEEPER_CONNECT}"
COMMON_OPTIONS="${NUM_DOCS} ${NUM_BPRNS} ${NUM_INVOICES} ${NUM_CREDIT_NOTES} ${DOCS_PER_HOUR} ${PRINT_MESSAGES}"

echo docker run --name tp-request-generator --hostname tp-request-generator -p 1400:1400 -p 9400:9400  --network trade-pilot-swarm ${COMMON_ARGS} ${COMMON_OPTIONS} dcdockerregistry.azurecr.io/tp-request-generator:v4.5
docker run --name tp-request-generator --hostname tp-request-generator -p 1400:1400 -p 9400:9400  --network trade-pilot-swarm ${COMMON_ARGS} ${COMMON_OPTIONS} dcdockerregistry.azurecr.io/tp-request-generator:v4.5

