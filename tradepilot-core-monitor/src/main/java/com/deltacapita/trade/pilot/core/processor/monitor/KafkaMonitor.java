/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */

package com.deltacapita.trade.pilot.core.processor.monitor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.DISABLED;
import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("!monitor-service")
public final class KafkaMonitor {

    private static final Logger logger = LogManager.getLogger(KafkaMonitor.class);

    @Value("${monitor.agentId}")
    private String agentId;

    @Value("${monitor.topic}")
    private String topic;
    
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.producer.key-serializer}")
    private String keySerializer;
    
    @Value("${spring.kafka.producer.value-serializer}")
    private String valueSerializer;

    private KafkaProducer<String, MonitoringEvent> kafkaProducer;

    @PostConstruct
    public void initialise() throws Exception {

        if( !topic.equals(DISABLED) ) {
            logger.info("Monitor initialised with agent ID {}", agentId);
        } else {
            logger.info("Monitoring is disabled for {}", agentId);            
        }       
        
        Properties producerProperties = new Properties();
        producerProperties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.put(KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
        producerProperties.put(VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
        
        logger.info("Initialising producer for {} with properties {}", agentId, producerProperties);
        kafkaProducer = new KafkaProducer<String, MonitoringEvent>(producerProperties);
        logger.info("Monitor initialised with producer {}", kafkaProducer);
    }

    public void inject(MonitoringEventI event) {
        
        try {

            if( !topic.equals(DISABLED) ) {

                if( logger.isDebugEnabled() ) {
                    logger.debug("Sending monitoring message from {} to topic {}, data={}", agentId,  topic, event);
                }

                MonitoringEvent monitoringEvent = (MonitoringEvent) event;
                ProducerRecord<String, MonitoringEvent> message = new ProducerRecord<String, MonitoringEvent>(topic, event.getRequestId(), monitoringEvent);
                kafkaProducer.send(message);
            } 

        } catch (Exception e) {
            logger.warn("Error sending message " + event, e);
        }        
    }

}
