/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

public class MonitoringAgentConstants {

    public static final String FILE_PROCESSOR_IN = "FILE_PROCESSOR_IN";

    public static final String FILE_PROCESSOR_OUT = "FILE_PROCESSOR_OUT";
      
    public static final String FILE_VALIDATOR_IN = "FILE_VALIDATOR_IN";

    public static final String FILE_VALIDATOR_OUT = "FILE_VALIDATOR_OUT";

    public static final String FUNDING_REQUEST_GENERATOR_IN = "FUNDING_REQUEST_GENERATOR_IN";

    public static final String FUNDING_REQUEST_GENERATOR_OUT = "FUNDING_REQUEST_GENERATOR_OUT";

    public static final String LOAN_GENERATOR_IN = "LOAN_GENERATOR_IN";

    public static final String LOAN_GENERATOR_OUT = "LOAN_GENERATOR_OUT";

    public static final String PAYMENT_PROCESSOR_IN = "PAYMENT_PROCESSOR_IN";

    public static final String PAYMENT_PROCESSOR_OUT = "PAYMENT_PROCESSOR_OUT";

    public static final String SETTLEMENT_PROCESSOR = "SETTLEMENT_PROCESSOR_";

    public static final String SETTLEMENT_PROCESSOR_EU = SETTLEMENT_PROCESSOR + "EU";

    public static final String SETTLEMENT_PROCESSOR_US = SETTLEMENT_PROCESSOR + "US";

    public static final String SETTLEMENT_PROCESSOR_AP = SETTLEMENT_PROCESSOR + "AP";

    public static final String FILE_COMPLETE = "FILE_COMPLETE";

    public static final String INSTRUCTION_COMPLETE = "INSTRUCTION_COMPLETE";

    public static final String INSTRUCTION_CREATED = "INSTRUCTION_CREATED";
    
    public static final String INSTRUCTION_VALIDATED = "INSTRUCTION_VALIDATED";    

    public static final String FUNDING_REQUESTED = "FUNDING_REQUESTED";    

    public static final String PAYMENT_CREATED = "PAYMENT_CREATED";
    
    public static final String PAYMENT_SAVED = "PAYMENT_SAVED";

    public static final String PAYMENT_EXECUTED = "PAYMENT_EXECUTED";

    public static final String LOAN_CREATED = "LOAN_CREATED";
    
    public static final String LOAN_SAVED = "LOAN_SAVED";

}
