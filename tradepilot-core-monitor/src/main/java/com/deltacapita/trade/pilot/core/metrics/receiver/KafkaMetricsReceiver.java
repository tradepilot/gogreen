/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.metrics.receiver;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.metrics.MetricUpdate;
import com.deltacapita.trade.pilot.core.metrics.MetricsSourceI;

@Component
@Profile("!monitor-service")
public class KafkaMetricsReceiver {

    private static final Logger logger = LogManager.getLogger(KafkaMetricsReceiver.class);

    @Autowired
    private MetricsSourceI metricsSource;
    
    @Value("${monitor.metrics.topic}")
    private String metricsTopic;
 
    @Value("${monitor.metrics.enabled}")
    private boolean metricsEnabled;
    
    @PostConstruct
    public void initialise() {
        
        logger.info("Initialised with topic {} and source {}", metricsTopic, metricsSource);
    }
    
    @KafkaListener(topics = "${monitor.metrics.topic}", containerFactory = "kafkaMetricListenerContainerFactory")
    public void receive( MetricUpdate update, Acknowledgment ack ) {
                          
        if( metricsEnabled ) {
            
            if( logger.isDebugEnabled() ) {
                logger.debug("Received " + update);
            }
            
            metricsSource.metricUpdate(update);      
            
        } else {
            
            if( logger.isDebugEnabled() ) {
                logger.debug("Metrics disabled - Ignoring " + update);
            }            
        }
        
        ack.acknowledge();
    }
}
