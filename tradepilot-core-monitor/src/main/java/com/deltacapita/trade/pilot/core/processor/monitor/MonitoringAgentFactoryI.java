/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

public interface MonitoringAgentFactoryI<T,D> {

    MonitoringAgentI<T,D> getAgent( String agentName );
}
