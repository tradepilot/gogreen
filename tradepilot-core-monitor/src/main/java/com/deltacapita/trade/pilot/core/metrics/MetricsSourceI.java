/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.metrics;

public interface MetricsSourceI {

    void metricUpdate( MetricUpdate update );
}
