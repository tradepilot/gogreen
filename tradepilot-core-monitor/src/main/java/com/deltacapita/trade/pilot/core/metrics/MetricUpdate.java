/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.metrics;

import java.util.UUID;

public class MetricUpdate {

    private String requestId;

    private String metricName;
    
    private Double metricValue;

    public MetricUpdate() {
        
        requestId = UUID.randomUUID().toString();
    }   
    
    public String getRequestId() {
        return requestId;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public Double getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(Double metricValue) {
        this.metricValue = metricValue;
    }


    @Override
    public String toString() {
        return "MetricUpdate [requestId=" + requestId + ", metricName=" + metricName + ", metricValue=" + metricValue + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((requestId == null) ? 0 : requestId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        MetricUpdate other = (MetricUpdate) obj;
        if (requestId == null) {
            if (other.requestId != null) {
                return false;
            }
        } else if (!requestId.equals(other.requestId)) {
            return false;
        }
        
        return true;
    }
}
