/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

import java.util.Map;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;

public interface MonitoringEventI extends ProcessingRequestI {

    public String getCorrelationId();
    
    public String getAgentId();
    
    public String getAgentName();
    
    public String getFlow();

    public Map<String,Object> getData();
}
