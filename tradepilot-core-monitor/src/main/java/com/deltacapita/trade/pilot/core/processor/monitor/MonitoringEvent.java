/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

import java.util.Map;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;

public class MonitoringEvent extends ProcessingRequest implements MonitoringEventI {

    private String correlationId;
    
    private String agentId;
    
    private String agentName;

    private String flow;

    private Map<String, Object> data;
       
    @Override
    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    
    @Override
    public Map<String, Object> getData() {

        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    @Override
    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    @Override
    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    @Override
    public String toString() {
        return "MonitoringEvent [" 
                + "correlationId=" + correlationId 
                + ", agentId=" + agentId 
                + ", agentName=" + agentName 
                + ", flow=" + flow 
                + ", requestId=" + getRequestId() 
                + ", source=" + getSource() 
                + ", destination=" + getDestination() 
                + ", fileId=" + getFileId() 
                + ", timestamp=" + getTimestamp()
                + ", data=" + data
                + "]";
    }
}
