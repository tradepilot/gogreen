/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

import java.util.Collection;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;

public interface MonitoringEventBuilderI {

    public MonitoringEventI build( ProcessingRequestI request, Object data, Long timestamp );
    
    public Collection<MonitoringEventI> buildAll( ProcessingRequestI request, Collection<Object> data, Long timestamp );
    
}
