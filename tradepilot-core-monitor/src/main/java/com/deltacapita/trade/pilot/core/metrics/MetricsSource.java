/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.metrics;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;


@Component
@Profile("!monitor-service")
public class MetricsSource implements MetricsSourceI {

    private static final Logger logger = LogManager.getLogger(MetricsSource.class);
         
    @Autowired
    private MeterRegistry registry;
    
    private Map<String,AtomicInteger> metrics;
    
    
    @PostConstruct
    public void initialise() {
        
        logger.info("Initialised with registry {}", registry);
        metrics = new ConcurrentHashMap<>();
        
        List<Meter> meters = registry.getMeters();
        for (Meter meter : meters) {
            logger.info("Found registered meter {}", meter.getId());                
        }
    }
    
    
    @Override
    public void metricUpdate(MetricUpdate update) {
        
        if( logger.isDebugEnabled() ) {
            logger.debug("Received " + update);
        }
                
        String metricName = update.getMetricName();
        Double metricValue = update.getMetricValue();
               
        if( metricName != null && metricValue != null ) {
            
            int value = metricValue.intValue();
            AtomicInteger currentValue = metrics.get(metricName);  
            
            if( currentValue == null ) {
                
                logger.info("Creating new metric for {} with initial value {}", metricName, metricValue);
                currentValue = registry.gauge(metricName, new AtomicInteger(value));
                metrics.put(metricName, currentValue);
                
            } else {
                
                logger.info("Updating metric for {} with new value {}", metricName, metricValue);
                currentValue.set(value);
            }            
            
        } else {
            logger.warn("Ignoring update with null fields {}", update);
        }

    }    
}
