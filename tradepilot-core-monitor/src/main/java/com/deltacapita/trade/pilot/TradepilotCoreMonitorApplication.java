package com.deltacapita.trade.pilot;

import org.springframework.boot.SpringApplication;

//@SpringBootApplication
public class TradepilotCoreMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradepilotCoreMonitorApplication.class, args);
	}
}
