/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor.simple;

import java.net.InetAddress;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.processor.monitor.KafkaMonitor;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;

@Component
@Profile("!monitor-service")
public class MonitoringAgentFactory implements MonitoringAgentFactoryI<ProcessingRequestI, Object> {

    private static final Logger logger = LogManager.getLogger(MonitoringAgentFactory.class);

    @Value("${monitor.enabled}")
    private Boolean enabled;
    
    @Autowired
    private KafkaMonitor kafkaMonitor;
    
    private String jvmName = UUID.randomUUID().toString();

    private Map<String, MonitoringAgent> agents = new ConcurrentHashMap<>();
        
    
    @PostConstruct
    public void initialise() throws Exception {

        InetAddress localhost = InetAddress.getLocalHost();
        String hostName = localhost.getCanonicalHostName();
        String hostAddress = localhost.getHostAddress();
        
        if( hostName != null && hostAddress != null ) {
            
            jvmName = hostName + "-" + hostAddress;
        }

        logger.info("Initialised with enabled={}, jvmName={}", enabled, jvmName);
    }
    
    
    @Override
    public MonitoringAgentI<ProcessingRequestI, Object> getAgent(String agentName) {
                
        String agentId = getAgentId(agentName);
        MonitoringAgent agent = agents.get(agentId);
        
        if( agent == null ) {
            
            logger.info("Constructing new agent for {}", agentId);            
            agent = new MonitoringAgent(getAgentId(agentName), agentName, kafkaMonitor, enabled);
            agents.put(agentId, agent);
            
        } else {
            
            logger.debug("Returning agent {}", agentId);
        }
        
        return agent;
    }
    
    private String getAgentId( String agentName ) {
        
        return agentName + "-" + jvmName;
    }
}
