/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor.simple;

import java.net.InetAddress;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.processor.monitor.KafkaMonitor;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;

@Profile("!monitor-service")
public class MonitoringAgent implements MonitoringAgentI<ProcessingRequestI, Object> {
    
    private static final Logger logger = LogManager.getLogger(MonitoringAgent.class);

    private String agentId;
    
    private String agentName;
    
    private MonitoringEventBuilder eventBuilder;
    
    private KafkaMonitor kafkaMonitor;
    
    private boolean enabled;
    
    private String containerId;

    
    public MonitoringAgent(String id, String name, KafkaMonitor monitor, boolean isEnabled)  {
        
        agentId = id;
        agentName = name;
        kafkaMonitor = monitor;
        enabled = isEnabled;
        
        try {
            
            InetAddress localhost = InetAddress.getLocalHost();
            String hostName = localhost.getCanonicalHostName();
            String hostAddress = localhost.getHostAddress();
            
            if( hostName != null && hostAddress != null ) {
                containerId = hostName + "-" + hostAddress;
                
            }
           
        } catch (Exception ex) {
            logger.warn("Unable to get container ID", ex);
        }
        
        logger.info("Agent {} initialised with ID {}, enabled={}, containerId={}", agentName, agentId, enabled, containerId);
        if( enabled ) {
            eventBuilder = new MonitoringEventBuilder();
            eventBuilder.setContainerId(containerId);
        }
    }  

    @Override
    public void inject(ProcessingRequestI request, Object data) {

        long now = System.currentTimeMillis();
        if( data instanceof Collection ) {
            
            @SuppressWarnings("unchecked")
            Collection<Object> objects = (Collection<Object>) data;
            for (Object object : objects) {
                doInject(request, object, now);
            }
            
        } else {
            doInject(request, data, now);            
        }        
    }

    @Override
    public void inject(ProcessingRequestI request, Object data, long timestamp) {
        doInject(request, data, timestamp);
    }

  
    public void doInject(ProcessingRequestI request, Object data, long timestamp) {
        
        if( enabled ) {
            
            MonitoringEvent event = (MonitoringEvent) eventBuilder.build(request, data, timestamp);            
            if( event != null ) {
                
                event.setAgentId(agentId);        
                event.setAgentName(agentName);        
                logger.info("Agent {} injecting event {} ", agentId, event);                
                kafkaMonitor.inject(event);     
                
            } else {
                
                logger.warn("Unable to build event for {}, data={}", request, data.getClass().getSimpleName() );                
            }
            
        } else {
            
            logger.debug("Agent {} is disabled", agentName);
        }
    }


    public void injectAll(ProcessingRequestI request, Collection<Object> data) {
        
        if( enabled ) {
            
            long now = System.currentTimeMillis();
            for (Object object : data) {
                inject(request, object, now);
            } 
            
        } else {
            
            logger.debug("Agent {} is disabled", agentName);
        }
    }

    @Override
    public String toString() {
        return "MonitoringAgent [" 
                + "agentId=" + agentId 
                + ", agentName=" + agentName 
                + ", enabled=" + enabled 
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((agentId == null) ? 0 : agentId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        MonitoringAgent other = (MonitoringAgent) obj;
        if (agentId == null) {
            if (other.agentId != null) {
                return false;
            }
        } else if (!agentId.equals(other.agentId)) {
            return false;
        }
        
        return true;
    }
}
