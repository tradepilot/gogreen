/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

public interface MonitoringAgentI<T,D> {

    public void inject(T request, D data);
    
    public void inject(T request, D data, long timestamp);
    
}
