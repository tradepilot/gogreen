/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor.simple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.Loan;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEvent;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventBuilderI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringEventI;

public class MonitoringEventBuilder implements MonitoringEventBuilderI {

    private static final Logger logger = LogManager.getLogger(MonitoringEventBuilder.class);

    private static final String FILE = "com.deltacapita.trade.pilot.core.data.types.File";
    
    private static final String INSTRUCTION = "com.deltacapita.trade.pilot.core.data.types.Instruction";
    
    private static final String LOAN = "com.deltacapita.trade.pilot.core.data.types.Loan";
    
    private static final String PAYMENT_REQUEST = "com.deltacapita.trade.pilot.core.data.types.PaymentRequest";

    private String containerId;


    @Override
    public MonitoringEventI build(ProcessingRequestI request, Object data, Long timestamp) {
        
        String className = data.getClass().getName();
        switch(className) {
            
            case FILE:               
                return buildFileEvent(request, (File) data, timestamp);
                
            case INSTRUCTION:               
                return buildInstructionEvent(request, (Instruction) data, timestamp);
                
            case LOAN:               
                return buildLoanEvent(request, (Loan) data, timestamp);

            case PAYMENT_REQUEST:               
                return buildPaymentRequestEvent(request, (PaymentRequest) data, timestamp);

            default:
                logger.warn("No mapping set up for {}", className);
        }
        
        // Return null if unknown type
        //
        return null;
    }

    @Override
    public Collection<MonitoringEventI> buildAll(ProcessingRequestI request, Collection<Object> data, Long timestamp) {
        
        Collection<MonitoringEventI> events = new ArrayList<>();
        for (Object object : data) {
            events.add(build(request, object, timestamp));
        }
       
        return events;
    }
    
    
    public MonitoringEventI buildFileEvent(ProcessingRequestI request, File file, Long timestamp) {
        
        MonitoringEvent event = buildEvent(request, timestamp);        
        Map<String, Object> data = event.getData();
               
        data.put(MonitoringConstants.BUYER_ID, file.getBuyerId());
        data.put(MonitoringConstants.MATURITY_DATE, file.getMaturityDate().toEpochDay());
        data.put(MonitoringConstants.AMOUNT, file.getTotalValue());
        data.put(MonitoringConstants.NO_OF_INSTRUCTIONS, file.getNumberOfInstructions());
        data.put(MonitoringConstants.OUTSTANDING_PAYMENT_REQUESTS, file.getOutstandingPaymentRequests());
        data.put(MonitoringConstants.RECEIVED_DATE_TIME, ServiceUtils.toEpochMillis(file.getReceivedDateTime()));
        data.put(MonitoringConstants.STATE, file.getState());
        
        event.setCorrelationId(file.getId());
        event.setFlow(MonitoringConstants.FILE_FLOW);
 
        if( logger.isDebugEnabled() ) {
            logger.debug("Built file event {}", event);            
        }
        
        return event;
    }

    public MonitoringEventI buildInstructionEvent(ProcessingRequestI request, Instruction instruction, Long timestamp) {
        
        MonitoringEvent event = buildEvent(request, timestamp);        
        Map<String, Object> data = event.getData();
        
        data.put(MonitoringConstants.FILE_ID, instruction.getFileId());
        data.put(MonitoringConstants.BUYER_ID, instruction.getBuyerId());
        data.put(MonitoringConstants.SUPPLIER_ID, instruction.getSupplierId());
        data.put(MonitoringConstants.BPRN, instruction.getBprn());
        data.put(MonitoringConstants.AMOUNT, instruction.getAmount());
        data.put(MonitoringConstants.CURRENCY, instruction.getCurrency());
        data.put(MonitoringConstants.DUE_DATE, instruction.getDueDate());
        data.put(MonitoringConstants.INVOICE_NO, instruction.getInvoiceNumber());
        data.put(MonitoringConstants.STATE, instruction.getState());
        data.put(MonitoringConstants.TYPE, instruction.getType());
        
        event.setCorrelationId(instruction.getId());
        event.setFlow(MonitoringConstants.INSTRUCTION_FLOW);
 
        if( logger.isDebugEnabled() ) {
            logger.debug("Built instruction event {}", event);            
        }
        
        return event;
    }

    public MonitoringEventI buildLoanEvent(ProcessingRequestI request, Loan loan, Long timestamp) {
        
        MonitoringEvent event = buildEvent(request, timestamp);        
        Map<String, Object> data = event.getData();
        
        data.put(MonitoringConstants.FILE_ID, loan.getFileId());
        data.put(MonitoringConstants.BUYER_ID, loan.getBuyerId());
        data.put(MonitoringConstants.AMOUNT, loan.getAmount());
        data.put(MonitoringConstants.CURRENCY, loan.getCurrency());
        data.put(MonitoringConstants.RATE, loan.getRate());
        data.put(MonitoringConstants.STATE, loan.getState());
        
        event.setCorrelationId(loan.getId());
        event.setFlow(MonitoringConstants.LOAN_FLOW);

        if( logger.isDebugEnabled() ) {
            logger.debug("Built loan event {}", event);            
        }
        
        return event;
    }

    public MonitoringEventI buildPaymentRequestEvent(ProcessingRequestI request, PaymentRequest paymentRequest, Long timestamp) {
        
        MonitoringEvent event = buildEvent(request, timestamp);        
        Map<String, Object> data = event.getData();
        
        data.put(MonitoringConstants.FILE_ID, paymentRequest.getFileId());
        data.put(MonitoringConstants.BUYER_ID, paymentRequest.getBuyerId());
        data.put(MonitoringConstants.SUPPLIER_ID, paymentRequest.getSupplierId());
        data.put(MonitoringConstants.AMOUNT, paymentRequest.getAmount());
        data.put(MonitoringConstants.CURRENCY, paymentRequest.getCurrency());
        data.put(MonitoringConstants.DUE_DATE, paymentRequest.getDueDate());
        data.put(MonitoringConstants.PAYMENT_REFERENCE, paymentRequest.getPaymentReference());
        data.put(MonitoringConstants.STATE, paymentRequest.getState());
        
        event.setCorrelationId(paymentRequest.getId());
        event.setFlow(MonitoringConstants.PAYMENT_FLOW);
 
        if( logger.isDebugEnabled() ) {
            logger.debug("Built payment event {}", event);            
        }
        
        return event;
    }
    
    public MonitoringEvent buildEvent(ProcessingRequestI request, Long timestamp) {
        
        MonitoringEvent event = new MonitoringEvent();
        event.setRequestId(UUID.randomUUID().toString());

        event.setDestination(request.getDestination());
        event.setFileId(request.getFileId());
        event.setSource(request.getSource());
        event.setTimestamp(timestamp);
        
        Map<String, Object> data = new HashMap<>();
        data.put(MonitoringConstants.CONTAINER_ID, containerId);
        event.setData(data);
        
        return event;
    }


    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }
}
