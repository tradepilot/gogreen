/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.processor.monitor;

public class MonitoringConstants {

    public static final String FILE_FLOW = "FILES";
    
    public static final String INSTRUCTION_FLOW = "INSTRUCTIONS";
    
    public static final String LOAN_FLOW = "LOANS";

    public static final String PAYMENT_FLOW = "PAYMENTS";

    public static final String BUYER_ID = "BUYER_ID";

    public static final String SUPPLIER_ID = "SUPPLIER_ID";

    public static final String MATURITY_DATE = "MATURITY_DATE";

    public static final String NO_OF_INSTRUCTIONS = "NO_OF_INSTRUCTIONS";

    public static final String OUTSTANDING_PAYMENT_REQUESTS = "OUTSTANDING_PAYMENT_REQUESTS";

    public static final String RECEIVED_DATE_TIME = "RECEIVED_DATE_TIME";

    public static final String STATE = "STATE";   
    
    public static final String BPRN = "BPRN";   

    public static final String FILE_ID = "FILE_ID";
    
    public static final String TYPE = "TYPE";   

    public static final String CURRENCY = "CURRENCY";   
    
    public static final String RATE = "RATE";   

    public static final String AMOUNT = "AMOUNT";   

    public static final String DUE_DATE = "DUE_DATE";   
    
    public static final String PAYMENT_REFERENCE = "PAYMENT_REFERENCE";   

    public static final String INVOICE_NO = "INVOICE_NO";   

    public static final String CONTAINER_ID = "CONTAINER_ID";   
}
