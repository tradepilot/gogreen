/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.deltacapita.trade.pilot.core.data.types.LoanI;



@Entity
@Table(name = "loan")
public class Loan implements LoanI {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="fileid")
    private String fileId;

    @Column(name="buyerid")
    private String buyerId;

    @Column(name="currency")
    private String currency;
    
    @Column(name="maturitydate")
    private LocalDate maturityDate;

    @Column(name="amount")
    private Double amount;

    @Column(name="rate")
    private Double rate;

    @Column(name="paymentfrequency")
    private String paymentFrequency;
    
    @Column(name="daycountconvention")
    private String daycountConvention;

    @Column(name="state")
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public LocalDate getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(LocalDate maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }    
    
    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public String getDaycountConvention() {
        return daycountConvention;
    }

    public void setDaycountConvention(String daycountConvention) {
        this.daycountConvention = daycountConvention;
    }

    
    @Override
    public int hashCode() {
        
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        Loan other = (Loan) obj;
        if (id == null) {
            
            if (other.id != null) {
                return false;
            }
            
        } else if (!id.equals(other.id)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        
        return "Loan [" 
                + "id=" + id 
                + ", fileId=" + fileId 
                + ", buyerId=" + buyerId 
                + ", currency=" + currency 
                + ", maturityDate=" + maturityDate 
                + ", amount=" + amount 
                + ", rate=" + rate 
                + ", paymentFrequency=" + paymentFrequency 
                + ", daycountConvention=" + daycountConvention 
                + ", state=" + state 
                + "]";
    }
}
