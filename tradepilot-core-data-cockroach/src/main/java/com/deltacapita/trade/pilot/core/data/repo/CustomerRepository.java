/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String> {

    List<Customer> findByType(String type);

    List<Customer> findByName(String name);
}
