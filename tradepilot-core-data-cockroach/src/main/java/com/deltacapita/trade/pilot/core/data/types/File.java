/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.deltacapita.trade.pilot.core.data.types.FileI;

/**
 * Represents a file object
 * 
 * @author simonw
 *
 */
@Entity
@Table(name = "file")
public class File implements FileI {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="requestid")
    private String requestId;

    @Column(name="buyerid")
    private String buyerId;

    @Column(name="state")
    private String state;

    @Column(name="receiveddatetime")
    private LocalDateTime receivedDateTime;

    @Column(name="maturitydate")
    private LocalDate maturityDate;

    @Column(name="numberofinstructions")
    private Long numberOfInstructions;

    @Column(name="outstandingpaymentrequests")
    private Long outstandingPaymentRequests;

    @Column(name="totalvalue")
    private Double totalValue;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public LocalDateTime getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(LocalDateTime receivedTime) {
        this.receivedDateTime = receivedTime;
    }

    public Long getNumberOfInstructions() {
        return numberOfInstructions;
    }

    public void setNumberOfInstructions(Long numberOfInstructions) {
        this.numberOfInstructions = numberOfInstructions;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }
    
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDate getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(LocalDate maturityDate) {
        this.maturityDate = maturityDate;
    }    

    public Long getOutstandingPaymentRequests() {
        return outstandingPaymentRequests;
    }

    public void setOutstandingPaymentRequests(Long outstandingPaymentRequests) {
        this.outstandingPaymentRequests = outstandingPaymentRequests;
    }
    
    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        File other = (File) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "File [id=" + id 
                + ", requestId=" + requestId 
                + ", state=" + state 
                + ", buyerId=" + buyerId 
                + ", receivedDateTime=" + receivedDateTime 
                + ", numberOfInstructions=" + numberOfInstructions 
                + ", outstandingPaymentRequests=" + outstandingPaymentRequests 
                + ", totalValue=" + totalValue
                + ", maturityDate=" + maturityDate
                + "]";
    }

}
