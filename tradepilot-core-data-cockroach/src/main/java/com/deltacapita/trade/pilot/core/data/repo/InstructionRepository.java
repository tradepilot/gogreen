/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.Instruction;

@Repository
public interface InstructionRepository extends CrudRepository<Instruction, String> {

    List<Instruction> findByBuyerId(String buyerId);

    List<Instruction> findByBuyerIdAndType(String buyerId, String type);

    List<Instruction> findByFileId(String fileId);

    List<Instruction> findByFileIdAndType(String fileId, String type);
}
