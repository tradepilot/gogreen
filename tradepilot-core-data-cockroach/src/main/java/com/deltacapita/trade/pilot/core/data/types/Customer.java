/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.deltacapita.trade.pilot.core.data.types.CustomerI;

/**
 * Represents a buyer or supplier
 * 
 * @author simonw
 *
 */
@Entity
@Table(name = "customer")
public class Customer implements CustomerI {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="type")
    private String type;

    @Column(name="name")
    private String name;

    @Column(name="addressline1")
    private String addressLine1;

    @Column(name="addressline2")
    private String addressLine2;

    @Column(name="addressline3")
    private String addressLine3;

    @Column(name="city")
    private String city;

    @Column(name="state")
    private String state;

    @Column(name="postcode")
    private String postCode;

    @Column(name="country")
    private String country;

    @Column(name="email")
    private String email;

    @Column(name="telephone")
    private String telephoneNo;
    
    @Column(name="accountnumber")
    private String accountNumber;
    

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Customer other = (Customer) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Customer [" 
                + " id=" + id 
                + ", type=" + type 
                + ", name=" + name 
                + ", accountNumber=" + accountNumber 
                + ", addressLine1=" + addressLine1
                + ", addressLine2=" + addressLine2 
                + ", addressLine3=" + addressLine3 
                + ", city=" + city 
                + ", state=" + state 
                + ", postCode=" + postCode 
                + ", country=" + country 
                + ", email=" + email 
                + ", telephoneNo=" + telephoneNo 
                + "]";
    }
}
