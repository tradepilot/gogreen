/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.deltacapita.trade.pilot.core.data.types.FundingProfileI;

/**
 * Represents a profile that defines the terms by which invoices
 * from a supplier to a buyer are funded before their due date
 * 
 * @author simonw
 *
 */
@Entity
@Table(name = "fundingprofile")
public class FundingProfile implements FundingProfileI {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="buyerid")
    private String buyerId;

    @Column(name="supplierid")
    private String supplierId;

    @Column(name="currency")
    private String currency;

    @Column(name="maxtenor")
    private Long maxTenor;

    @Column(name="maxtotalvalue")
    private Long maxValue;

    @Column(name="maxinvoicevalue")
    private Long maxInvoiceValue;

    @Column(name="rate")
    private Double rate;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getMaxTenor() {
        return maxTenor;
    }

    public void setMaxTenor(Long maxTenor) {
        this.maxTenor = maxTenor;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public Long getMaxInvoiceValue() {
        return maxInvoiceValue;
    }

    public void setMaxInvoiceValue(Long maxInvoiceValue) {
        this.maxInvoiceValue = maxInvoiceValue;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FundingProfile other = (FundingProfile) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FundingProfile [" 
                + " id=" + id 
                + ", buyerId=" + buyerId 
                + ", supplierId=" + supplierId 
                + ", currency=" + currency 
                + ", maxTenor=" + maxTenor 
                + ", maxValue=" + maxValue 
                + ", maxInvoiceValue=" + maxInvoiceValue 
                + ", rate=" + rate 
                + "]";
    }
}
