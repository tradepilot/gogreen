/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.File;

@Repository
public interface FileRepository extends CrudRepository<File, String> {

    List<File> findByBuyerId(String buyerId);

    List<File> findByRequestId(String requestId);
}
