/*
 * Copyright (c) 2017. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.types;

import java.time.LocalDate;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Represents am instruction object
 * 
 * @author simonw
 *
 */
@Entity
@Table(name = "instruction")
public class Instruction implements InstructionI {

    @Id
    @Column(name="id")
	private String id;

    @Column(name="fileid")
	private String fileId;

    @Column(name="type")
	private String type;
	
    @Column(name="bprn")
	private String bprn;	

    @Column(name="buyerid")
	private String buyerId;
	
    @Column(name="supplierid")
	private String supplierId;
	
    @Column(name="suppliername")
	private String supplierName;
	
    @Column(name="currency")
	private String currency;	

    @Column(name="invoicenumber")
	private String invoiceNumber;

    @Column(name="duedate")
	private LocalDate dueDate;

    @Column(name="amount")
	private Double amount;

    @Column(name="state")
    private String state;

    @Transient
	private Map<String,Object> properties;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBprn() {
		return bprn;
	}

	public void setBprn(String bprn) {
		this.bprn = bprn;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }   

    public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        Instruction other = (Instruction) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        
        return true;
    }


	@Override
	public String toString() {
		return "Instruction [id=" + id 
				+ ", fileId=" + fileId 
                + ", state=" + state 
				+ ", type=" + type 
				+ ", bprn=" + bprn 
				+ ", buyerId=" + buyerId 
				+ ", supplierId=" + supplierId 
				+ ", supplierName=" + supplierName 
				+ ", currency=" + currency
				+ ", invoiceNumber=" + invoiceNumber 
				+ ", dueDate=" + dueDate 
				+ ", amount=" + amount 
				+ ", properties=" + properties 
				+ "]";
	}


}
