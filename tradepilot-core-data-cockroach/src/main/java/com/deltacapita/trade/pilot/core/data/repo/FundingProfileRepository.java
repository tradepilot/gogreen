/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.FundingProfile;

@Repository
public interface FundingProfileRepository extends CrudRepository<FundingProfile, String> {

    List<FundingProfile> findByBuyerIdAndSupplierId(String buyerId, String supplierId);

    List<FundingProfile> findByBuyerIdAndSupplierIdAndCurrency(String buyerId, String supplierId, String currency);
}
