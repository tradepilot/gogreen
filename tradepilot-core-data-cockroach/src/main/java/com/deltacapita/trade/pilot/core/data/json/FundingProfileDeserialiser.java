/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.json;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.jackson.JsonComponent;

import com.deltacapita.trade.pilot.core.data.types.FundingProfile;
import com.deltacapita.trade.pilot.core.data.types.FundingProfileI;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@JsonComponent
public class FundingProfileDeserialiser extends JsonDeserializer<FundingProfileI> {

    private static final Logger logger = LogManager.getLogger(FundingProfileDeserialiser.class);

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised CockroachDB custom deserialiser for {}", FundingProfile.class);
    }
    
    @Override
    public FundingProfileI deserialize(JsonParser jp, DeserializationContext context) throws IOException {
        
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode root = mapper.readTree(jp);
        
        FundingProfile fundingProfile = mapper.readValue(root.toString(), FundingProfile.class);
        return fundingProfile;
    }    
}