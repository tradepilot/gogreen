/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.core.data.repo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;

@Repository
public interface PaymentRequestRepository extends CrudRepository<PaymentRequest, String> {

    List<PaymentRequest> findByState(String state);

    List<PaymentRequest> findByDueDate(LocalDate dueDate);

    List<PaymentRequest> findByDueDateAndState(LocalDate dueDate, String state);

    List<PaymentRequest> findByDueDateIsLessThanEqualAndState(LocalDate dueDate, String state);

    List<PaymentRequest> findByFileIdAndDueDateAndState(String fileId, LocalDate dueDate, String state);
}
