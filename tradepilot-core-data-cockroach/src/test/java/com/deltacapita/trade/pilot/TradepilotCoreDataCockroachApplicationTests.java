package com.deltacapita.trade.pilot;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.UTC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.deltacapita.trade.pilot.core.data.repo.CustomerRepository;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.FundingProfileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanProfileRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanRepository;
import com.deltacapita.trade.pilot.core.data.repo.PaymentRequestRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Instruction;
import com.deltacapita.trade.pilot.core.data.types.Loan;
import com.deltacapita.trade.pilot.core.data.types.PaymentRequest;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = TradepilotCoreDataCockroachApplication.class)
public class TradepilotCoreDataCockroachApplicationTests {

    @Autowired
    private FileRepository fileRepository;
        
    @Autowired
    private InstructionRepository instructionRepository;
        
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FundingProfileRepository profileRepository;

    @Autowired
    private LoanProfileRepository loanProfileRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private LoanRepository loanRepository;
    

    @Test
	public void contextLoads() {
	    
	    assertNotNull(fileRepository);
        assertNotNull(instructionRepository);
        assertNotNull(customerRepository);
        assertNotNull(profileRepository);
        assertNotNull(loanProfileRepository);
        assertNotNull(loanRepository);
        assertNotNull(paymentRequestRepository);
	}
    
	
	@Test
    public void testFiles() throws Exception {

        fileRepository.deleteAll();

        File file1 = saveFile("buyer1", "request1");
        File file2 = saveFile("buyer1", "request2");

        String id1 = file1.getId();
        String id2 = file2.getId();
        String buyerId = file1.getBuyerId();

        Optional<File> o1 = fileRepository.findById(id1);
        File f1 = o1.get();
        assertNotNull(f1);
        assertEquals(file1, f1);
        assertEquals(file1.getRequestId(), f1.getRequestId());
        assertEquals(file1.getBuyerId(), f1.getBuyerId());
        assertEquals(file1.getNumberOfInstructions(), f1.getNumberOfInstructions());
        assertEquals(file1.getOutstandingPaymentRequests(), f1.getOutstandingPaymentRequests());
        assertEquals(file1.getTotalValue(), f1.getTotalValue());
        //assertEquals(file1.getMaturityDate(), f1.getMaturityDate());        
        //assertEquals(file1.getReceivedDateTime(), f1.getReceivedDateTime());
        

        Optional<File> o2 = fileRepository.findById(id2);
        File f2 = o2.get();
        assertNotNull(f2);
        assertEquals(file2, f2);
        assertEquals(file2.getRequestId(), f2.getRequestId());
        assertEquals(file2.getBuyerId(), f2.getBuyerId());
        assertEquals(file2.getNumberOfInstructions(), f2.getNumberOfInstructions());
        assertEquals(file2.getOutstandingPaymentRequests(), f2.getOutstandingPaymentRequests());
        assertEquals(file2.getTotalValue(), f2.getTotalValue());
       

        List<File> files = fileRepository.findByBuyerId(buyerId);        
        assertTrue(files.contains(file1));
        assertTrue(files.contains(file2));


        fileRepository.deleteAll();
    }
    
	@Test
    public void testInstructions() throws Exception {

        instructionRepository.deleteAll();

        Instruction instruction1 = saveInstruction("buyer1", "supplier1", "A-00001");
        Instruction instruction2 = saveInstruction("buyer1", "supplier2", "A-00002");

        String id1 = instruction1.getId();
        String id2 = instruction2.getId();
        String buyerId = instruction1.getBuyerId();

        Optional<Instruction> o1 = instructionRepository.findById(id1);
        Instruction i1 = o1.get();
        assertNotNull(i1);
        assertEquals(instruction1, i1);
        assertEquals(instruction1.getBuyerId(), i1.getBuyerId());
        assertEquals(instruction1.getSupplierId(), i1.getSupplierId());
        assertEquals(instruction1.getAmount(), i1.getAmount());
        assertEquals(instruction1.getBprn(), i1.getBprn());
        assertEquals(instruction1.getCurrency(), i1.getCurrency());
        assertEquals(instruction1.getFileId(), i1.getFileId());
        assertEquals(instruction1.getInvoiceNumber(), i1.getInvoiceNumber());
        assertEquals(instruction1.getState(), i1.getState());

        Optional<Instruction> o2 = instructionRepository.findById(id2);
        Instruction i2 = o2.get();
        assertNotNull(i2);
        assertEquals(instruction2, i2);
        assertEquals(instruction2.getBuyerId(), i2.getBuyerId());
        assertEquals(instruction2.getSupplierId(), i2.getSupplierId());
        assertEquals(instruction2.getAmount(), i2.getAmount());
        assertEquals(instruction2.getBprn(), i2.getBprn());
        assertEquals(instruction2.getCurrency(), i2.getCurrency());
        assertEquals(instruction2.getFileId(), i2.getFileId());
        assertEquals(instruction2.getInvoiceNumber(), i2.getInvoiceNumber());
        assertEquals(instruction2.getState(), i2.getState());

        List<Instruction> instructions = instructionRepository.findByBuyerId(buyerId);
        assertTrue(instructions.contains(instruction1));
        assertTrue(instructions.contains(instruction2));

        instructionRepository.deleteAll();
    }


	
    @Test
    public void testPaymentRequests() throws Exception {

        paymentRequestRepository.deleteAll();

        PaymentRequest request1 = savePaymentRequest("buyer1", "supplier1", "A-00001");
        PaymentRequest request2 = savePaymentRequest("buyer1", "supplier2", "A-00002");

        String id1 = request1.getId();
        String id2 = request2.getId();

        Optional<PaymentRequest> o1 = paymentRequestRepository.findById(id1);
        PaymentRequest i1 = o1.get();
        assertNotNull(i1);
        assertEquals(request1, i1);
        assertEquals(request1.getBuyerId(), i1.getBuyerId());
        assertEquals(request1.getSupplierId(), i1.getSupplierId());
        assertEquals(request1.getAmount(), i1.getAmount());
        assertEquals(request1.getInstructionId(), i1.getInstructionId());
        assertEquals(request1.getCurrency(), i1.getCurrency());
        assertEquals(request1.getFileId(), i1.getFileId());
        assertEquals(request1.getPaymentReference(), i1.getPaymentReference());
        assertEquals(request1.getState(), i1.getState());

        Optional<PaymentRequest> o2 = paymentRequestRepository.findById(id2);
        PaymentRequest i2 = o2.get();
        assertNotNull(i2);
        assertEquals(request2, i2);
        assertEquals(request2.getBuyerId(), i2.getBuyerId());
        assertEquals(request2.getSupplierId(), i2.getSupplierId());
        assertEquals(request2.getAmount(), i2.getAmount());
        assertEquals(request2.getInstructionId(), i2.getInstructionId());
        assertEquals(request2.getCurrency(), i2.getCurrency());
        assertEquals(request2.getFileId(), i2.getFileId());
        assertEquals(request2.getPaymentReference(), i2.getPaymentReference());
        assertEquals(request2.getState(), i2.getState());

        List<PaymentRequest> requests = paymentRequestRepository.findByState("PROCESSING");
        assertTrue(requests.contains(request1));
        assertTrue(requests.contains(request2));

        paymentRequestRepository.deleteAll();
    }
    
    
    
    @Test
    public void testLoans() throws Exception {

        loanRepository.deleteAll();

        Loan request1 = saveLoan("buyer1", "supplier1", "A-00001");
        Loan request2 = saveLoan("buyer1", "supplier2", "A-00002");

        String id1 = request1.getId();
        String id2 = request2.getId();

        Optional<Loan> o1 = loanRepository.findById(id1);
        Loan i1 = o1.get();
        assertNotNull(i1);
        assertEquals(request1, i1);
        assertEquals(request1.getBuyerId(), i1.getBuyerId());
        assertEquals(request1.getDaycountConvention(), i1.getDaycountConvention());
        assertEquals(request1.getAmount(), i1.getAmount());
        assertEquals(request1.getRate(), i1.getRate());
        assertEquals(request1.getCurrency(), i1.getCurrency());
        assertEquals(request1.getFileId(), i1.getFileId());
        assertEquals(request1.getPaymentFrequency(), i1.getPaymentFrequency());
        assertEquals(request1.getState(), i1.getState());

        Optional<Loan> o2 = loanRepository.findById(id2);
        Loan i2 = o2.get();
        assertNotNull(i2);
        assertEquals(request2, i2);
        assertEquals(request2.getBuyerId(), i2.getBuyerId());
        assertEquals(request2.getDaycountConvention(), i2.getDaycountConvention());
        assertEquals(request2.getAmount(), i2.getAmount());
        assertEquals(request2.getRate(), i2.getRate());
        assertEquals(request2.getCurrency(), i2.getCurrency());
        assertEquals(request2.getFileId(), i2.getFileId());
        assertEquals(request2.getPaymentFrequency(), i2.getPaymentFrequency());
        assertEquals(request2.getState(), i2.getState());

        List<Loan> requests = loanRepository.findByState("PROCESSING");
        assertTrue(requests.contains(request1));
        assertTrue(requests.contains(request2));

        loanRepository.deleteAll();
    }
    
	
    private File saveFile(String buyerId, String requestId) {

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDateTime dueDateTime = LocalDateTime.from(dateTime);
        LocalDate maturityDate = LocalDate.from(dateTime).plusDays(30);        

        File file = new File();
        file.setId(UUID.randomUUID().toString());
        file.setNumberOfInstructions(10L);
        file.setBuyerId(buyerId);
        file.setRequestId(requestId);
        file.setTotalValue(2000.0);
        file.setReceivedDateTime(dueDateTime);
        file.setMaturityDate(maturityDate);

        return this.fileRepository.save(file);
    }
    
    private Instruction saveInstruction(String buyerId, String supplierId, String invoiceNo) {

        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDate maturityDate = LocalDate.from(dateTime).plusDays(30);        

        Instruction instruction = new Instruction();
     
        instruction.setId(UUID.randomUUID().toString());
        instruction.setBuyerId(buyerId);
        instruction.setSupplierId(supplierId);
        instruction.setSupplierName(supplierId);
        instruction.setAmount(1234.5);
        instruction.setBprn("BPRN-1");
        instruction.setCurrency("GBP");
        instruction.setDueDate(maturityDate);
        instruction.setFileId("file-1");
        instruction.setInvoiceNumber(invoiceNo);
        instruction.setState("PROCESSING");
        instruction.setType("INVOICE");
        
        return instructionRepository.save(instruction);
    }
    
    private PaymentRequest savePaymentRequest(String buyerId, String supplierId, String invoiceNo) {
        
        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDate maturityDate = LocalDate.from(dateTime).plusDays(30);        

        PaymentRequest request = new PaymentRequest();
        request.setId(UUID.randomUUID().toString());
        request.setBuyerId(buyerId);
        request.setSupplierId(supplierId);
        request.setAmount(1234.5);
        request.setCurrency("GBP");
        request.setDueDate(maturityDate);
        request.setFileId("file-1");
        request.setInstructionId("I-" + buyerId + "-" + supplierId + "-" + invoiceNo);
        request.setState("PROCESSING");
        request.setPaymentReference("P-" + buyerId + "-" + supplierId + "-" + invoiceNo);
        
        return paymentRequestRepository.save(request);
    }
    
    
    private Loan saveLoan(String buyerId, String supplierId, String invoiceNo) {
        
        Date now = new Date();
        ZonedDateTime dateTime = now.toInstant().atZone(ZoneId.of(UTC));
        LocalDate maturityDate = LocalDate.from(dateTime).plusDays(30);        

        Loan request = new Loan();
        request.setId(UUID.randomUUID().toString());
        request.setBuyerId(buyerId);
        request.setDaycountConvention("ACT_ACT");
        request.setAmount(1234.5);
        request.setCurrency("GBP");
        request.setMaturityDate(maturityDate);
        request.setFileId("file-1");
        request.setRate(1.025);
        request.setState("PROCESSING");
        request.setPaymentFrequency("MONTHLY");
        
        return loanRepository.save(request);
    }
    
}
