/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.loan.generator.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.types.LoanProfile;

@Component
public class LoanServiceFallback implements LoanServiceClient {

    private static final Logger logger = LogManager.getLogger(LoanServiceFallback.class);


    @Override
    public LoanProfile getLoanProfile(String buyerId, String currency) {
        
        logger.warn("Loan Service call failed for buyer={}, ccy={}", buyerId, currency);       
        return null;
    }

}
