/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.deltacapita.trade.pilot.core.data.ServiceUtils;
import com.deltacapita.trade.pilot.core.data.types.LoanProfileI;
import com.deltacapita.trade.pilot.core.processor.receiver.KafkaReceiver;
import com.deltacapita.trade.pilot.loan.generator.client.LoanServiceClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
public class TradepilotLoanGeneratorApplication implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(TradepilotLoanGeneratorApplication.class);

    @Autowired
    private KafkaReceiver receiver;
   
    @Autowired
    private LoanServiceClient loanServiceClient;


    public static void main(String[] args) {
        
        try {
            
            ServiceUtils.logIpAddresses();
            SpringApplication.run(TradepilotLoanGeneratorApplication.class, args);                      

        } catch (Throwable t) {
            logger.warn("Error initialising TradepilotLoanGeneratorApplication", t);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        
        logger.info("Initialised with receiver {}, client {}", receiver, loanServiceClient);              
        Runnable r = new Runnable() {
            
            @Override
            public void run() {
                
                try {
                    
                    Random rand = new Random();

                    int i = 0;
                    int max = 100;
                    while(i++ < max) {
                         
                        long delay = 10000L + rand.nextInt(3000) + rand.nextInt(1000);                        
                        Thread.sleep(delay);
                        
                        String id = "54321";
                        String buyerId = "BCUST000002";
                        String currency = "EUR";
                        logger.info("Getting loan profile for {}", id);
                        LoanProfileI loanProfile = loanServiceClient.getLoanProfile(buyerId, currency);
                        if( loanProfile != null ) {
    
                            logger.info("Got loan profile for {}, {}", id, loanProfile);
    
                        } else {
                            
                            logger.info("No loan profile found for {}", id);
                            
                        }
                    }
                    
                } catch (Throwable e) {
                    logger.info("No loan profile found ", e);
                }
                
            }
        };

        Thread t = new Thread(r);
        t.start();

    }	
}
