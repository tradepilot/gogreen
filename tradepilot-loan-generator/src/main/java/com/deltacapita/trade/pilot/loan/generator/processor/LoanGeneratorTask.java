/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.loan.generator.processor;

import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.ACCOUNT_BASE_CURRENCY;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.FUNDING_REQUESTED;
import static com.deltacapita.trade.pilot.core.data.TradePilotConstants.LOAN_CREATED;

import java.time.LocalDate;
import java.util.Optional;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.deltacapita.trade.pilot.core.data.IdBuilder;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanRepository;
import com.deltacapita.trade.pilot.core.data.types.File;
import com.deltacapita.trade.pilot.core.data.types.Loan;
import com.deltacapita.trade.pilot.core.data.types.LoanProfileI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentConstants;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentI;
import com.deltacapita.trade.pilot.core.processor.task.AbstractProcessingTask;
import com.deltacapita.trade.pilot.loan.generator.client.LoanServiceClient;

import feign.RetryableException;

public class LoanGeneratorTask extends AbstractProcessingTask implements Callable<ProcessingRequest> {

    private static final Logger logger = LogManager.getLogger(LoanGeneratorTask.class); 

    private LoanServiceClient loanProfileServiceClient;
    
    private LoanRepository loanRepository;
    
    private MonitoringAgentI<ProcessingRequestI, Object> fileInAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> fileOutAgent;

    private MonitoringAgentI<ProcessingRequestI, Object> loanCreatedAgent;


    public LoanGeneratorTask(ProcessingRequest request, String destination, FileRepository fileRepository, InstructionRepository instructionRepository, LoanServiceClient loanServiceClient, LoanRepository loanRepository, MonitoringAgentFactoryI<ProcessingRequestI, Object> monitoringAgentFactory ) {
        
        super(request, destination, fileRepository, instructionRepository, monitoringAgentFactory);
        this.loanProfileServiceClient = loanServiceClient;
        this.loanRepository = loanRepository;
        
        fileInAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.LOAN_GENERATOR_IN);
        fileOutAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.LOAN_GENERATOR_OUT);
        loanCreatedAgent = monitoringAgentFactory.getAgent(MonitoringAgentConstants.LOAN_CREATED);
    }
    

    @Override
    public ProcessingRequest call() throws Exception {
        
        ProcessingRequest response = new ProcessingRequest(request);
        response.setDestination(destination);
        
        long start = System.currentTimeMillis();
        logger.info("Handling {}", request);        
        
        String fileId = request.getFileId();
        Optional<File> result = fileRepository.findById(fileId);
        if( result.isPresent() ) {
            
            File file = result.get();   
            String fileState = file.getState();
            
            logger.info("Injecting {} to {}", request, fileInAgent);        
            fileInAgent.inject(request, file);

            if( fileState.equals(FUNDING_REQUESTED)) {
                
                String buyerId = file.getBuyerId();
                LoanProfileI loanProfile = getLoanProfile(buyerId, "GBP");
                
                if (loanProfile != null) {

                    Loan loan = createLoan(file, loanProfile);
                    loanRepository.save(loan);                    
                    loanCreatedAgent.inject(request, loan);
                    
                    file.setState(LOAN_CREATED);
                    logger.info("Updating file status for {} to {}", fileId, file.getState());
                    fileRepository.save(file);
                }                 
                
                long end = System.currentTimeMillis();
                logger.info("Generated loan request for {} in {} ms", fileId, end - start);  
                
            } else {
                
                logger.warn("Ignoring request for file {} in state {}", fileId, fileState);
            } 
            
            logger.info("Injecting {} to {}", response, fileOutAgent);        
            fileOutAgent.inject(response, file);

        } else {
            logger.info("No entry found for {}", fileId);            
        }
                
        logger.info("Returning {}", response);        
        return response;
    }
    
    
    private LoanProfileI getLoanProfile(String buyerId, String currency) {
              
        int max = 5;
        Throwable ex = null;
        
        for (int i = 0; i < max; i++) {
            try {
                
                LoanProfileI loanProfile = loanProfileServiceClient.getLoanProfile(buyerId, currency);
                return loanProfile;
                
            } catch (RetryableException e) {
                
                logger.info("Call {} of {} for buyer={}, currency={} failed - {}, retrying...", i, max, buyerId, currency, e);    
                ex = e;
            }
            
        }
        
        throw new RuntimeException("Service call failed for buyer=" + buyerId  + ", ccy=" + currency, ex);
    }
    
    
    private Loan createLoan(File file, LoanProfileI loanProfile ) {
        
        Loan loan = new Loan();        
        String fileId = file.getId();
        
        String buyerId = file.getBuyerId();
        Double amount = file.getTotalValue();  
        LocalDate maturityDate = file.getMaturityDate();
        Double rate = loanProfile.getRate();
        String paymentFrequency = loanProfile.getPaymentFrequency();
        String daycountConvention = loanProfile.getDaycountConvention();
        
        loan.setRate(rate);
        loan.setAmount(amount);
        loan.setBuyerId(buyerId);
        loan.setFileId(fileId);
        loan.setPaymentFrequency(paymentFrequency);
        loan.setDaycountConvention(daycountConvention);
        loan.setMaturityDate(maturityDate);
        
        loan.setCurrency(ACCOUNT_BASE_CURRENCY);
        loan.setState(LOAN_CREATED);   
        
        String id = IdBuilder.buildId(loan);
        loan.setId(id);
    
        return loan;
    }
}
