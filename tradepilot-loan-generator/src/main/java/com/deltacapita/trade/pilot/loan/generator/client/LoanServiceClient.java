/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.loan.generator.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.deltacapita.trade.pilot.core.data.service.LoanServiceI;


@FeignClient(name = "tp-loan-service", fallback = LoanServiceFallback.class)
public interface LoanServiceClient extends LoanServiceI {
}
