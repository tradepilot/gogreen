/*
 * Copyright (c) 2018. Delta Capita Ltd. All Rights Reserved.
 *
 */
package com.deltacapita.trade.pilot.loan.generator.processor;

import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequest;
import com.deltacapita.trade.pilot.core.data.messages.ProcessingRequestI;
import com.deltacapita.trade.pilot.core.data.repo.FileRepository;
import com.deltacapita.trade.pilot.core.data.repo.InstructionRepository;
import com.deltacapita.trade.pilot.core.data.repo.LoanRepository;
import com.deltacapita.trade.pilot.core.processor.monitor.MonitoringAgentFactoryI;
import com.deltacapita.trade.pilot.core.processor.task.TaskFactoryI;
import com.deltacapita.trade.pilot.loan.generator.client.LoanServiceClient;

@Component
public class LoanGeneratorTaskFactory implements TaskFactoryI {

    private static final Logger logger = LogManager.getLogger(LoanGeneratorTaskFactory.class);

    @Value("${processor.destination}")
    private String destination;
    
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private InstructionRepository instructionRepository;
    
    @Autowired
    private LoanServiceClient loanServiceClient;
    
    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private MonitoringAgentFactoryI<ProcessingRequestI, Object> agentFactory;

    @PostConstruct
    public void initialise() throws Exception {

        logger.info("Initialised with destination={}, client={}, repo={} ",  destination, loanRepository, loanRepository);
    }
    
    @Override
    public Callable<ProcessingRequest> newTask(ProcessingRequest request) {
        
        logger.info("Creating new task for " + request);
        return new LoanGeneratorTask(request, destination, fileRepository, instructionRepository, loanServiceClient, loanRepository, agentFactory);
    }
}
