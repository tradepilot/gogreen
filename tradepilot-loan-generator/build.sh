#!/bin/sh

#docker stop tp-loan-generator
docker rm tp-loan-generator
mvn clean package -Dmaven.test.skip=true

docker build -t tp-loan-generator --build-arg SPRING_PROFILE=local-swarm -f Dockerfile .

docker tag tp-loan-generator dcdockerregistry.azurecr.io/tp-loan-generator:v1
#docker push dcdockerregistry.azurecr.io/tp-loan-generator:v1

docker run --name tp-loan-generator --hostname tp-loan-generator -p 1230:1230 -p 9230:9230 --network trade-pilot-swarm -e KAFKA_ZOOKEEPER_CONNECT=zookeeper-server-swarm:2181 -e SPRING_PROFILE=local-swarm tp-loan-generator

