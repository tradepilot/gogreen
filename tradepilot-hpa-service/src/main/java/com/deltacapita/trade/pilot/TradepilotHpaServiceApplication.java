package com.deltacapita.trade.pilot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradepilotHpaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradepilotHpaServiceApplication.class, args);
	}
}
