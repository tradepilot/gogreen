package com.deltacapita.trade.pilot.metrics;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;


@Component
public class TestMetricsSource {

    private static final Logger logger = LogManager.getLogger(TestMetricsSource.class);
         
    @Autowired
    private MeterRegistry registry;
    
    private AtomicInteger requestRate;

    private AtomicInteger requestCount;
    
    @PostConstruct
    public void init() {
        
        logger.info("Initialised with registry {}", registry);
        requestCount = registry.gauge("tp_hpa_requests_absolute", new AtomicInteger(0));
        requestRate = registry.gauge("tp_hpa-requests_total", new AtomicInteger(0));
        
        List<Meter> meters = registry.getMeters();
        for (Meter meter : meters) {
            logger.info("Found registered meter {}", meter.getId());            
        }
    }
    
    public String processRequest(String name) {
        
        double beforeRate = requestRate.get();
        double beforeAbsolute = requestCount.get();
        
        requestRate.incrementAndGet();
        requestCount.incrementAndGet();
        
        double afterRate = requestRate.get();       
        double afterAbsolute = requestCount.get();       
     
        logger.info("Incremented rate request count from {} to {}", beforeRate, afterRate);
        logger.info("Incremented absolute request count from {} to {}", beforeAbsolute, afterAbsolute);
        
        return String.format("Gauge %s incremented from %2.0f to %2.0f", name, beforeRate, afterRate);
     }
    
    
    public String reset(String name) {
        
        double beforeRate = requestRate.get();
        double beforeAbsolute = requestCount.get();
        
        requestRate.set(0);
        requestCount.set(0);
        
        logger.info("Reset rateGauge from {} to 0", beforeRate);
        logger.info("Reset absoluteGauge from {} to 0", beforeAbsolute);

        return String.format("Reset gauge %s from %2.0f to 0", name, beforeRate);
    }
    
}
