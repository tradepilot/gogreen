package com.deltacapita.trade.pilot.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController 
public class TestService {
   
    @Autowired
    private TestMetricsSource metricsSource;

    @RequestMapping(value = "/test/{system}", method = RequestMethod.GET, produces = "application/json")
    public String testMethod(@PathVariable("system") String system ) {
        
        String result = metricsSource.processRequest(system);
        return result;
    }

    @RequestMapping(value = "/reset/{system}", method = RequestMethod.GET, produces = "application/json")
    public String resetMethod(@PathVariable("system") String system ) {
        
        String result = metricsSource.reset(system);
        return result;
    }

}
